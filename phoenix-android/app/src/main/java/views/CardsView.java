package views;


import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.PorterDuff;
import android.util.Log;
import com.matkaline.phoenix.App;
import com.matkaline.phoenix.R;

import listener.CardViewListener;
import models.models.cards.Card;
import ulitis.L;

public class CardsView extends AnimationView{

    private float width, height;
    public static float RATE;
    private App app;
    public boolean done = false;
    private boolean doneFlip = false;
    private CardViewListener cardViewListener;

    public CardsView(Context ctx) {
        super(ctx);
        rate(ctx);
        app = App.getInstance();
        app.cardMng.soft(width, height);
        start(96);
    }

    private void rate(Context ctx) {
        Activity act = (Activity) ctx;
        cardViewListener = (CardViewListener) ctx;
        width = act.getWindowManager().getDefaultDisplay().getWidth();
        height = act.getWindowManager().getDefaultDisplay().getHeight();
        float wrate = width / 1242;
        float hrate = height / 2208;
        RATE = wrate < hrate ? wrate : hrate;
    }

    private void drawEach(Card[] cards, Canvas c, int cardNo, boolean isTop) {
        cards[cardNo].x = width / 2 - cards[cardNo].w / 2 + cardNo * cards[cardNo].w;
        cards[cardNo].y = height / 2 + cards[cardNo].w / 2 - cardNo * cards[cardNo].w;
        cards[cardNo].drawBig(c, isTop);
    }
    public void draw(Canvas c) {
        Card[] cards = app.cardMng.cardsResult;
            if (!app.cardMng.reconcile) {
//                for (int i = 1; i >= 00; i--) {
//                        cards[i].x = width / 2 - cards[i].w / 2 + i * cards[i].w;
//                        cards[i].y = height / 2 + cards[i].w / 2 - i * cards[i].w;
//                        cards[i].drawBig(c);

//                }
                drawEach(cards, c, 1, false);

//                if(!doneFlip && done) { // incase the done value change true fast, need doneFlip value to stop it
//                    if (app.cardMng.cardsResult[1].number == 0){
//                        cards[0].drawBigRotate(c);
//                    } else if (app.cardMng.cardsResult[1].number == 13){
//                        if (app.cardMng.cardsResult[0].isBad()){
//                            cards[0].drawBigRotate(c);
//                        }
//                    }else if (app.cardMng.cardsResult[0].color == app.cardMng.cardsResult[1].color){
//                        if ((app.cardMng.cardsResult[0].number + app.cardMng.cardsResult[1].number) % 2 == 0) {//cung chan || cung le
//                            cards[0].drawBigRotate(c);
//                        }
//                    }
//                }
                if (app.cardMng.cardsResult[1].number == 0){
                    drawEach(cards, c, 0, true);
                } else if (app.cardMng.cardsResult[1].number == 13){
                    if (app.cardMng.cardsResult[0].isBad()){
                        drawEach(cards, c, 0, true);
                    } else {
                        drawEach(cards, c, 0, false);
                    }
                } else if (app.cardMng.cardsResult[0].color == app.cardMng.cardsResult[1].color){
                    if ((app.cardMng.cardsResult[0].number + app.cardMng.cardsResult[1].number) % 2 == 0) {//cung chan || cung le
                        drawEach(cards, c, 0, true);
                    } else {
                        drawEach(cards, c, 0, false);
                    }
                } else {
                    drawEach(cards, c, 0, false);
                }
                done = cards[0].a3d <= 10;
            } else {
                    cards[2].x = width / 2;
                    cards[2].y = height / 2;
                    cards[2].drawBig(c, false);
                    done = cards[2].a3d <= 10;
//                if(!doneFlip && done) { // incase the done value change true fast, need doneFlip value to stop it
//                    doneFlip = true;
//                    cardViewListener.onFinishFlip();
//                }
            }
        super.draw(c);

    }

}
