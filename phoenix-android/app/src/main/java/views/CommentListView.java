package views;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.matkaline.phoenix.App;
import com.matkaline.phoenix.R;
import com.matkaline.phoenix.RootActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import adapters.CommentListAdapter;
import apis.API;
import http.HttpManager;
import http.retrofit.RequestParams;
import models.Comment;
import models.User;
import ulitis.DateTimeHelper;
import ulitis.UiHelper;
import ulitis.Utils;

/**
 * Created by nguyen hong phuc on 9/29/2016.
 */
public class CommentListView extends LinearLayout {

    private Animation aniVISIBLE, aniGONE;
    private EditText edtComment;
    private Button btSend;
    private TextView txtCommentCount;
    private CommentListAdapter adpComment;
    private List<Comment> objects = new ArrayList<Comment>();
    private ListView lsvComment;
    private NumberFormat nf = NumberFormat.getInstance(Locale.GERMAN);
    private App app;
    public Context ctx;

    public CommentListView(Context ctx) {
        super(ctx);
        this.ctx = ctx;
        app = App.getInstance();
        inflate(ctx, R.layout.layout_comment_list, this);
        aniVISIBLE = AnimationUtils.loadAnimation(ctx, R.anim.slide_up);
        aniGONE = AnimationUtils.loadAnimation(ctx, R.anim.slide_down);
        setVisibility(View.GONE);

        lsvComment = (ListView) findViewById(R.id.listComment);
        adpComment = new CommentListAdapter(getContext(), R.layout.custom_comment_list, objects);
        lsvComment.setAdapter(adpComment);

        init();
    }

    @Override
    public void setVisibility(int visibility) {
        switch (visibility) {
            case View.GONE:
            case View.INVISIBLE:
                ((RootActivity) getContext()).hidenKeyboard();
                startAnimation(aniGONE);
                break;
            case View.VISIBLE:
                if (objects.size() <= 0) {
                    loadComments();
                }
                edtComment.setText("");
                startAnimation(aniVISIBLE);
                break;
        }
        super.setVisibility(visibility);
    }

    private void loadComments() {
        if (app.user.isLogin() && Utils.isNetworkStatusAvialable(getContext())) {
            RequestParams params = new RequestParams();

            app.httpManager.doGet(API.API_CARD + "/" + app.cardMng.getCardId(app.currentCard), params, new HttpManager.OnResponse() {
                @Override
                public void onResponse(JSONObject json) {
                    try {
                        JSONArray jsaArray = json.getJSONArray("commemts");
                        for (int i = 0; i < jsaArray.length(); i++) {
                            JSONObject js = jsaArray.getJSONObject(i);
                            boolean liked = false;
                            int user_id = js.getInt("user_id");
                            String like_user = js.getString("like_user");
                            if (!like_user.equals("")) {
                                String[] args = like_user.split(",");
                                for (String arg : args) {
                                    int id = Integer.valueOf(arg);
                                    if (id == app.user.user_id) {
                                        liked = true;
                                        break;
                                    }
                                }
                            }
                            int id = js.getInt("id");
                            User user = new User(app, js.getInt("user_id"), js.getString("user_name"), js.getString("avatar"));
                            String content = js.getString("content");
                            int like_count = js.getInt("like_count");
                            Date create_at = DateTimeHelper.GetDateFromString(js.getString("created_at"));
                            objects.add(new Comment(id, user, content, like_count, create_at, liked));
                        }
                        updateUI();
                    } catch (JSONException e) {
                        Utils.Alert(ctx, ctx.getString(R.string.alert), "Error");
                        e.printStackTrace();
                    }
                }
            });

        }
    }

    private void init() {
        txtCommentCount = (TextView) findViewById(R.id.numberComment);
        edtComment = (EditText) findViewById(R.id.editText_write);
        btSend = (Button) findViewById(R.id.button_send);
        btSend.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utils.isNetworkStatusAvialable(getContext())) {
                    btSend.setEnabled(false);
                    if (validate()) {
                        send();
                    } else {
                        btSend.setEnabled(true);
                    }
                } else {
                    Utils.AlertNetwork(getContext());
                }
            }
        });
        loadComments();
        LinearLayout line = (LinearLayout) findViewById(R.id.line_close_comment);
        UiHelper.RESIZE(line, 135, -1);
        ImageView btClose = (ImageView) findViewById(R.id.imageButton);
        UiHelper.RESIZE(btClose, 51, 51);

        UiHelper.RESIZE(btSend, 204, 93);
        UiHelper.TEXTSIZE(btSend, 60);
        UiHelper.MARGIN(btSend, 0, 45, 70, 45);


//        UiHelper.RESIZE(edtComment,-1,93);
        UiHelper.MARGIN(edtComment, 70, 45, 26, 45);

        UiHelper.RESIZE(findViewById(R.id.lyTop), -1, 123);
        UiHelper.RESIZE(findViewById(R.id.lyCommentCount), 70 + 210 + 48, -1);
        UiHelper.PADDING(findViewById(R.id.lyCommentCount), 40, 0, 0, 0);

        UiHelper.TEXTSIZE(txtCommentCount, 60);
        UiHelper.MARGIN(txtCommentCount, 0, 0, 22, 0);

        TextView titleComment = (TextView) findViewById(R.id.titleComment);
        UiHelper.TEXTSIZE(titleComment, 60);

        UiHelper.TEXTSIZE(edtComment, 55);
        UiHelper.PADDING(edtComment, 30, 0, 0, 0);

    }

    private boolean validate() {
        String str = edtComment.getText().toString().trim();
        if (str.equals("")) {
            Utils.Alert(getContext(), getResources().getString(R.string.alert), getResources().getString(R.string.noti_comment));
            return false;
        }
        if (str.length() > 500) {
            Utils.Alert(getContext(), getResources().getString(R.string.alert), getResources().getString(R.string.limit_character));
            return false;
        }
        return true;
    }

    private void send() {
        ((RootActivity) getContext()).hidenKeyboard();

        RequestParams params = new RequestParams();
        params.addParam("content", edtComment.getText().toString().trim());

        app.httpManager.doPost(API.API_CARD + "/" + app.cardMng.getCardId(app.currentCard) + "/" + app.user.user_id, params, new HttpManager.OnResponse() {
            @Override
            public void onResponse(JSONObject json) {
                try {
                    JSONArray jsaArray = json.getJSONArray("commemts");
                    JSONObject js = jsaArray.getJSONObject(jsaArray.length() - 1);
                    int id = js.getInt("id");
//                    L.V("send comment id = "+id);
                    objects.add(new Comment(id, app.user, edtComment.getText().toString(), 0, DateTimeHelper.TODAY(), false));
                    adpComment.notifyDataSetInvalidated();
                    edtComment.setText("");
                    lsvComment.setSelection(objects.size() - 1);
                    txtCommentCount.setText(nf.format(objects.size()));
                    ((TextView) ((Activity) getContext()).findViewById(R.id.textComment)).setText(nf.format(objects.size()));
                } catch (JSONException e) {
//                    Utils.ALERT(ctx,e.getMessage());
                    Utils.Alert(ctx, ctx.getString(R.string.alert), "Error");
                    e.printStackTrace();
                }
                btSend.setEnabled(true);
            }
        });
        
    }

    private void updateUI() {
        ((TextView) ((Activity) getContext()).findViewById(R.id.textComment)).setText(nf.format(objects.size()));
        txtCommentCount.setText(nf.format(objects.size()));
        //adpComment = new CommentListAdapter(getContext(), R.layout.custom_comment_list, objects);
        lsvComment.setAdapter(adpComment);
        lsvComment.setSelection(objects.size() - 1);
    }

    public int getCommentCount() {
        return objects.size();
    }

//    /**
//     * Filter for controlling maximum new lines in EditText.
//     */
//    public class MaxLinesInputFilter implements InputFilter {
//
//        private final int mMax;
//
//        public MaxLinesInputFilter(int max) {
//            mMax = max;
//        }
//
//        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
//            int newLinesToBeAdded = countOccurrences(source.toString(), '\n');
//            int newLinesBefore = countOccurrences(dest.toString(), '\n');
//            if (newLinesBefore >= mMax - 1 && newLinesToBeAdded > 0) {
//                // filter
//                Toast.makeText(ctx,"Không quá 4 dòng",Toast.LENGTH_SHORT).show();
//                return "";
//            }
//
//            // do nothing
//            return null;
//        }
//
//        /**
//         * @return the maximum lines enforced by this input filter
//         */
//        public int getMax() {
//            return mMax;
//        }
//
//        /**
//         * Counts the number occurrences of the given char.
//         *
//         * @param string the string
//         * @param charAppearance the char
//         * @return number of occurrences of the char
//         */
//        public int countOccurrences(String string, char charAppearance) {
//            int count = 0;
//            for (int i = 0; i < string.length(); i++) {
//                if (string.charAt(i) == charAppearance) {
//                    count++;
//                }
//            }
//            return count;
//        }
//    }

}
