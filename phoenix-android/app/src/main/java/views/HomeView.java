package views;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import com.matkaline.phoenix.App;
import com.matkaline.phoenix.R;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import models.models.cards.CardAnimation;
import ulitis.L;

public class HomeView extends AnimationView {

    private float width, height;
    private float w, h;
    private float RATE;
    private float DX;
    private float DY;

    private CardAnimation[] cardAnimations = new CardAnimation[26];

    public HomeView(Context ctx) {
        super(ctx);
        Activity act = (Activity) ctx;

        DisplayMetrics displaymetrics = new DisplayMetrics();
        act.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        height = displaymetrics.heightPixels;
//        float button = height / 11;
//        height -= button;
        width = displaymetrics.widthPixels;

        L.V("SCREEN SIZE "+width+"x"+height);
        float wrate = width / 1242f;
        float hrate = height / 2208f;
        RATE = wrate < hrate ? wrate : hrate;
        w = width / RATE;
        h = height / RATE;
        float w = width / RATE;
        float h = height / RATE;
        DX = (width - 1242f * RATE) / 2f;
        DY = (height - 2208f * RATE) / 2f;
        softForHome();
        start(64);
    }

    private Paint pa = new Paint();{
        pa.setColor(Color.RED);
        pa.setStyle(Paint.Style.STROKE);
    }

    public void draw(Canvas c) {
        c.save();
        c.translate(DX, DY);
        c.scale(RATE, RATE);

        for (int i = 0; i < cardAnimations.length; i++) {
            cardAnimations[i].draw(c);
        }
//        c.drawRect(0,0,(int) (1242f),(int)(2208-256),pa);
        c.restore();
        super.draw(c);
    }

    public void update() {
        super.update();
        for (int i = 0; i < cardAnimations.length; i++) {
            cardAnimations[i].update();
        }
    }

    private void softForHome() {
        try {
            List<Point> points = new ArrayList<Point>();
            Scanner scanner = new Scanner(getContext().getAssets().open("home.txt"));
            String line;
            while (scanner.hasNextLine()) {
                line = scanner.nextLine();
                String[] args = line.split(" ");
                int x = Integer.valueOf(args[0]);
                int y = Integer.valueOf(args[1]);
                points.add(new Point(x, y));
            }
            int j = 0;
            int n = points.size();
            for (int i = n - 1; i >= 0; i--) {
                Point p = points.get(i);
                CardAnimation card = cardAnimations[j] = new CardAnimation(getContext(), p.x, p.y);
                switch (j) {
                    case 15:
                    case 17:
                    case 20:
                        card.x = -w;
                        card.y = card.yTarget;
                        break;
                    case 16:
                    case 18:
                    case 21:
                        card.x = w * 2;
                        card.y = card.yTarget;
                        break;
                    case 24:
                        card.x = card.xTarget;
                        card.y = -h;
                        break;
                    case 25:
                        card.x = card.xTarget;
                        card.y = h * 2;
                        break;

                }
                j++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
