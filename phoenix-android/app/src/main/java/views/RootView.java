package views;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.widget.LinearLayout;

/**
 * Created by nmcuong on 9/7/2016.
 */

public class RootView extends LinearLayout {

    public RootView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    protected void go(Class<?> cls) {
        Intent intent = new Intent(getContext(), cls);
        //intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        getContext().startActivity(intent);
    }

}