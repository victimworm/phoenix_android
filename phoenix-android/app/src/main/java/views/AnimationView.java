package views;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Handler;
import android.view.View;

public class AnimationView extends View{

    private Handler handler = new Handler();
    protected int iloop = 0;

    public AnimationView(Context context) {
        super(context);
    }

    public void update(){
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (iloop>0) {
//                    update();
                    invalidate();
                    iloop--;
                }
            }
        });
    }

    @Override
    public void draw(Canvas canvas) {
        update();
    }

    public void start(int iloop){
        this.iloop = iloop;
        update();
    }

    public void stop(){
        iloop = 0;
    }
}
