package services;

import android.app.Service;
import android.content.Intent;
import android.os.Environment;
import android.os.IBinder;
import androidx.annotation.Nullable;

import java.io.File;
import java.io.IOException;

/**
 * Created by nguyen hong phuc on 10/31/2016.
 */
public class LogcateService extends Service {

    public boolean running = false;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //Log.v("MyDebug0","Service:onStartCommand");
//        running = true;
//        new Thread(new Runnable(){
//            public void run() {
//                while(running)
//                {
//                    try {
//                        Thread.sleep(5000);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                    ////Log.v("MyDebug0","Service:do something");
//                    logcate();
//                }
//            }
//        }).start();

        return super.onStartCommand(intent, flags, startId);

    }

    private void logcate(){
        File filename = new File(Environment.getExternalStorageDirectory()+"/phoenix_logcat.log");
        try {
            if (!filename.exists()) {
                filename.createNewFile();
            }
            String cmd = "logcat -d -v time -f"+filename.getAbsolutePath();
            Runtime.getRuntime().exec(cmd);
        } catch (IOException e) {
            //e.printStackTrace();
        }
    }
}
