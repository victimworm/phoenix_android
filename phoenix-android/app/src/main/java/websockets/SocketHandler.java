package websockets;

import android.os.AsyncTask;
import android.os.Build;
import android.view.View;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.net.URISyntaxException;

import ulitis.L;

/**
 * Created by nguyen hong phuc on 10/3/2016.
 */
public class SocketHandler {



    WebSocketClient mWebSocketClient;

//    private class SocketConnector extends AsyncTask<Void,Void,Void>{
//
//        @Override
//        protected Void doInBackground(Void... voids) {
//            try {
//                URI uri = new URI("wss://192.168.1.2:9443/socket");
//                AppWebSoketClient appWebSoketClient = new AppWebSoketClient(uri, new Draft() {
//                })
//            } catch (URISyntaxException e) {
//                e.printStackTrace();
//            }
//
//
//            return null;
//        }
//    }

    private void connectWebSocket() {
        URI uri;
        try {
            uri = new URI("ws://192.168.1.135:9000/");
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return;
        }

        mWebSocketClient = new WebSocketClient(uri) {
            @Override
            public void onOpen(ServerHandshake serverHandshake) {
                L.V("Websocket", "Opened");
                mWebSocketClient.send("Hello from " + Build.MANUFACTURER + " " + Build.MODEL);
            }

            @Override
            public void onMessage(String s) {
                final String message = s;
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        TextView textView = (TextView)findViewById(R.id.messages);
//                        textView.setText(textView.getText() + "\n" + message);
//                    }
//                });
            }

            @Override
            public void onClose(int i, String s, boolean b) {
                L.V("Websocket", "Closed " + s);
            }

            @Override
            public void onError(Exception e) {
                L.V("Websocket", "Error " + e.getMessage());
            }
        };
        mWebSocketClient.connect();
    }

    public void sendMessage(View view) {
//        EditText editText = (EditText)findViewById(R.id.message);
        mWebSocketClient.send("XIN CHAO");
//        editText.setText("");
    }

}
