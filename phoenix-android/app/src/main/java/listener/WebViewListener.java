package listener;

/**
 * Created by duchanh.luu on 4/15/2017.
 */

public interface WebViewListener {
    public void updateContentHeight(int height);
}
