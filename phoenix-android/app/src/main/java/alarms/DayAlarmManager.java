package alarms;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import java.util.Calendar;

import receivers.DayReceiver;


public class DayAlarmManager {

    public Context context;
    public static final String ACTION_REFRESH_DAY = "com.phoenix.REFRESH_EACH_DAY";
    public static final int DAY_ALARM_REQUEST_CODE = 24;

    public DayAlarmManager(Context context) {
        this.context = context;
    }

    /**
     * thiet lap thoi gian show notification sau 24h
     *
     * @param id
     */
    public void sttTimeNotif(int id) {
        Intent intent = new Intent(context, DayReceiver.class);
        intent.setAction(ACTION_REFRESH_DAY);
        PendingIntent pendingIntent;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            pendingIntent = PendingIntent.getService(
                    context,
                    id,
                    intent,
                    PendingIntent.FLAG_IMMUTABLE | PendingIntent.FLAG_UPDATE_CURRENT
            );
        } else {
            pendingIntent = PendingIntent.getBroadcast(
                    context,
                    id,
                    intent,
                    PendingIntent.FLAG_UPDATE_CURRENT
            );
        }

        AlarmManager alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.add(Calendar.DAY_OF_MONTH, 1); // enable after test
//        calendar.add(Calendar.MINUTE, 1); // test 1 mins only
        alarm.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
    }

}
