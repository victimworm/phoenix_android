package dbs;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

@Table(name = "Explain")
public class Explain extends Model {

    @Column(name = "card_id")
    public int card_id;
    @Column(name = "name")
    public String name;
    @Column(name = "expn1")
    public String expn1 = "";
    @Column(name = "expn2")
    public String expn2 ="";
    @Column(name = "ADJECTIF")
    public String ADJECTIF = "";
    @Column(name = "OMBRE")
    public String OMBRE = "";
    @Column(name = "NOM")
    public String NOM = "";

    public static List<Explain> getAll() {
        return new Select().from(Explain.class).execute();
    }

    public static Explain get(int id) {
        return new Select().from(Explain.class).where("card_id=?",id).executeSingle();
    }

    public static Explain GetByDate(String date) {
        return new Select().from(Explain.class).where("created_at == ?", date).executeSingle();
    }


}
