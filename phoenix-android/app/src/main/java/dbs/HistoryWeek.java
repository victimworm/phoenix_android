package dbs;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

import java.util.List;

import ulitis.DateTimeHelper;

/**
 * Created by Nguyen on 25-May-17.
 */
@Table(name = "HistoryWeek")
public class HistoryWeek extends Model {
    @Column(name = "_id")
    public int _id;
    @Column(name = "user_id")
    public int user_id;
    @Column(name = "date_card",unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    public long date_card;
    @Column(name = "card_1")
    public int card_1;
    @Column(name = "card_2")
    public int card_2;


    public static List<HistoryWeek> getAll() {
        return new Select().from(HistoryWeek.class).execute();
    }

    public HistoryWeek() {
        super();
    }

    public List<HistoryWeek> itemDate(){
        return getMany(HistoryWeek.class,"date_card");
    }

    public List<HistoryWeek> itemCard_1(){
        return getMany(HistoryWeek.class,"card_1");
    }
    public List<HistoryWeek> itemCard_2(){
        return getMany(HistoryWeek.class,"card_2");
    }



    @Override
    public String toString() {
        return user_id + ": "+ card_1+ ": "+ card_2+": "+date_card;
    }

    /**
     * return cards dc boc tu thu 2 - cua tuan hien tai
     */
    public static List<HistoryWeek> GetCards() {
        //remove all cards befor first day of current week
        long t = DateTimeHelper.FirstDayOfCurrentWeek();
        new Delete().from(HistoryWeek.class).where("date_card < ?",t).execute();
        //return all cards order by date_card
        return new Select().from(HistoryWeek.class).orderBy("date_card ASC").execute();
    }
}
