package dbs;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.Date;
import java.util.List;

@Table(name = "History")
public class History extends Model {


    @Column(name = "user_id")
    public int user_id;
    @Column(name = "created_at")
    public String created_at;
    @Column(name = "infor")
    public String infor;

    public static List<History> getAll() {
        return new Select().from(History.class).execute();
    }

    public static History GetByDate(String date) {
        return new Select().from(History.class).where("created_at == ?", date).executeSingle();
    }


    @Override
    public String toString() {
        return user_id + " : " + created_at + " : " + infor;
    }
}
