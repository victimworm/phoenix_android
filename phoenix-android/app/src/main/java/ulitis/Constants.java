package ulitis;

import android.os.Environment;

import java.io.File;
import java.util.Arrays;
import java.util.List;

/**
 * Created by nvdai on 9/20/2016.
 */
public class Constants {
    public static int ACTION_REQUEST_IMAGE = 1000;
    public static final String PICTURE_DIRECTORY = Environment.getExternalStorageDirectory()
            + File.separator + "DCIM" + File.separator + "ProfilePicture" + File.separator;
    public static final int NUM_OF_COLUMNS = 2;
    public static final String MUSIC_STATUS = "music_status";
    // Gridview image padding
    public static final int GRID_PADDING = 16; // in dp

    // SD card image directory
    public static final String PHOTO_ALBUM = "androidhive";

    // supported file formats
    public static final List<String> FILE_EXTN = Arrays.asList("jpg", "jpeg", "png");

    public interface Notification {
        String NOTIFY_24_HOURS = "notify_each_day";
        String NOTIFY_MORNING = "morning";
        String NOTIFY_NOON = "afternoon";
        String NOTIFY_NIGHT = "night";

    }
}
