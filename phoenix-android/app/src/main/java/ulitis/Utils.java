package ulitis;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.MediaStore;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.matkaline.phoenix.AlertActivity;
import com.matkaline.phoenix.R;
import com.matkaline.phoenix.RootActivity;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import models.models.cards.CardItem;

import static android.util.TypedValue.COMPLEX_UNIT_SP;

/**
 * Created by nvdai on 9/14/2016.
 */
public class Utils {
    private Context _context;
    private AssetManager assetManager;
    private InputStream inputStream;
    private File file;
    private MediaPlayer mediaPlayer;
    private SharedPreferences preferences;
    private final String STORAGE = "com.matkaline.phoenix.STORAGE";

    public Utils(Context context) {
        this._context = context;
        assetManager = context.getAssets();

    }

    public String getCardFacebookFlammeWord() {
        preferences = _context.getSharedPreferences(STORAGE, Context.MODE_PRIVATE);
        return preferences.getString("fbWord", null);
    }

    public void storeCardFacebookFlammeWord(String fbWord) {
        preferences = _context.getSharedPreferences(STORAGE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("fbWord", fbWord);
        editor.apply();
    }

    public String getCardFacebookDefiWord() {
        preferences = _context.getSharedPreferences(STORAGE, Context.MODE_PRIVATE);
        return preferences.getString("fbDefiWord", null);
    }

    public void storeCardFacebookDefiWord(String fbWord) {
        preferences = _context.getSharedPreferences(STORAGE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("fbDefiWord", fbWord);
        editor.apply();
    }

    public String getCardFacebookFlammeName() {
        preferences = _context.getSharedPreferences(STORAGE, Context.MODE_PRIVATE);
        return preferences.getString("fbFlammeName", null);
    }

    public void storeCardFacebookFlammeName(String fbTitle) {
        preferences = _context.getSharedPreferences(STORAGE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("fbFlammeName", fbTitle);
        editor.apply();
    }

    public String getCardFacebookDefiName() {
        preferences = _context.getSharedPreferences(STORAGE, Context.MODE_PRIVATE);
        return preferences.getString("fbDefiName", null);
    }

    public void storeCardFacebookDefiName(String fbTitle) {
        preferences = _context.getSharedPreferences(STORAGE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("fbDefiName", fbTitle);
        editor.apply();
    }

    public String getCardFacebookFlammeTeam() {
        preferences = _context.getSharedPreferences(STORAGE, Context.MODE_PRIVATE);
        return preferences.getString("fbFlammeTeam", null);
    }

    public void storeCardFacebookFlammeTeam(String fbTeam) {
        preferences = _context.getSharedPreferences(STORAGE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("fbFlammeTeam", fbTeam);
        editor.apply();
    }

    public String getCardFacebookDefiTeam() {
        preferences = _context.getSharedPreferences(STORAGE, Context.MODE_PRIVATE);
        return preferences.getString("fbDefiTeam", null);
    }

    public void storeCardFacebookDefiTeam(String fbTeam) {
        preferences = _context.getSharedPreferences(STORAGE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("fbDefiTeam", fbTeam);
        editor.apply();
    }

    public void playMusic(int music) {
        mediaPlayer= MediaPlayer.create(_context, music);
        mediaPlayer.start();
    }

    public void loopMusic(int music) {
        mediaPlayer= MediaPlayer.create(_context, music);
        mediaPlayer.setLooping(true);
        mediaPlayer.start();
    }

    public void stopMusic() {
        if(mediaPlayer!=null)
        {
            try {
                if(mediaPlayer.isPlaying()) {
                    mediaPlayer.stop();
                }
                mediaPlayer.reset();//It requires again setDataSource for player object.
                mediaPlayer.release();
            } catch (IllegalStateException e) {
//                mediaPlayer.reset();//It requires again setDataSource for player object.
//                mediaPlayer.release();
            }


        }
    }

    public void muteMusic() {
        if(mediaPlayer!=null) {
            mediaPlayer.setVolume(0,0);
        }

    }

    public void unmuteMusic() {
        if(mediaPlayer!=null) {
            mediaPlayer.setVolume(1,1);
        }
    }

    public static void CopyStream(InputStream is, OutputStream os) {
        final int buffer_size = 1024;
        try {
            byte[] bytes = new byte[buffer_size];
            for (; ; ) {
                int count = is.read(bytes, 0, buffer_size);
                if (count == -1)
                    break;
                os.write(bytes, 0, count);
            }
        } catch (Exception ex) {
        }
    }

    public List<CardItem> getFilePaths() {

        List<CardItem> cardItems = new ArrayList<CardItem>();
        CardItem item = new CardItem(0);
        cardItems.add(item);
        for (int i = 1; i <= 12; i++) {
            item = new CardItem(i);
            cardItems.add(item);
        }
        item = new CardItem(13);
        cardItems.add(item);
        for (int i = 1; i <= 12; i++) {
            item = new CardItem(i);
            item.reverse = true;
            cardItems.add(item);
        }
        for (int i = 14; i <= 25; i++) {
            item = new CardItem(i);
            item.reverse = true;
            cardItems.add(item);
        }
        for (int i = 14; i <= 25; i++) {
            item = new CardItem(i);
            cardItems.add(item);
        }
        //Log.v("MyDebug0","cardItem.size = " + cardItems.size());
        return cardItems;
    }

    public List<CardItem> getPath(){
        List<CardItem> cardItems = new ArrayList<CardItem>();
        for (int i = 0; i < 26; i++){
            CardItem item = new CardItem(i);
            cardItems.add(item);
        }
        return cardItems;
    }

    /**
     * getting screen width
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public int getScreenWidth() {
        int columnWidth;
        WindowManager wm = (WindowManager) _context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();

        final Point point = new Point();
        try {
            display.getSize(point);
        } catch (NoSuchMethodError ignore) { // Older device
            point.x = display.getWidth();
            point.y = display.getHeight();
        }
        columnWidth = point.x;
        return columnWidth;
    }

    /**
     * Thumbnail image
     */
    public static Bitmap getThumbnail(ContentResolver cr, String path) throws Exception {

        Cursor ca = cr.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new String[]{MediaStore.MediaColumns._ID}, MediaStore.MediaColumns.DATA + "=?", new String[]{path}, null);
        if (ca != null && ca.moveToFirst()) {
            int id = ca.getInt(ca.getColumnIndex(MediaStore.MediaColumns._ID));
            ca.close();
            return MediaStore.Images.Thumbnails.getThumbnail(cr, id, MediaStore.Images.Thumbnails.MICRO_KIND, null);
        }

        ca.close();
        return null;

    }

    /**
     * return text from text file on assets
     * @param path
     * @return
     */
    public static String GetText(AssetManager asset ,String path) {
        String str = "";
        InputStream stream;
        try {
            stream = asset.open(path);
            int size = stream.available();
            byte[] buffer = new byte[size];
            stream.read(buffer);
            stream.close();
            str = new String(buffer);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static String[] GetLines(AssetManager asset ,String path) {
        String[] arrLine = null;
        String str = "";
        InputStream stream;
        try {
            stream = asset.open(path);
            Scanner scanner = new Scanner(stream,"UTF-8");
            List<String> lines = new ArrayList<String>();
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                L.V(line);
                lines.add(line);
            }
            arrLine = new String[lines.size()];
            lines.toArray(arrLine);
            scanner.close();
            stream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return arrLine;
    }

    /**
     * Check NetWork
     */
    public static boolean isNetworkStatusAvialable(Context ctx) {
        ConnectivityManager connectivityManager = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            NetworkInfo netInfos = connectivityManager.getActiveNetworkInfo();
            if (netInfos != null)
                if (netInfos.isConnected())
                    return true;
        }
        return false;
    }


    public static void AlertNetwork(Context ctx){
        ((RootActivity) ctx).alert(ctx.getString(R.string.alert), ctx.getString(R.string.alert_offline), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
    }

    public static void Alert(Context ctx,String tile,String msg){
        dialogBuilder(ctx,tile,msg,new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        },null,false);
    }

    public static void ALERT(Context ctx,String msg) {
        Intent intent = new Intent(ctx, AlertActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra("msg",msg);
        ctx.startActivity(intent);
    }

    private static void dialogBuilder(Context ctx,String title, String msg,DialogInterface.OnClickListener onPositiveBtnClick,DialogInterface.OnClickListener onNegativeBtnClick,boolean cancelable) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ctx);
        alertDialogBuilder.setCancelable(cancelable);

        if (onNegativeBtnClick == null && onPositiveBtnClick == null) {
            alertDialogBuilder.setPositiveButton("OK", null);
        } else if (onNegativeBtnClick == null && onPositiveBtnClick != null) {
            alertDialogBuilder.setPositiveButton("OK", onPositiveBtnClick);
        } else {
            alertDialogBuilder.setNegativeButton("Hủy", onNegativeBtnClick);
            alertDialogBuilder.setPositiveButton("Đồng ý", onPositiveBtnClick);
        }

        //Add dialog's content
        View content = LayoutInflater.from(ctx).inflate(R.layout.dialog_content, null);
        alertDialogBuilder.setView(content);

        ((TextView) content.findViewById(R.id.title)).setText(title);
        ((TextView) content.findViewById(R.id.message)).setText(msg);


        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

        //Change button style
        TextView positiveBtn = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
        TextView negativeBtn = alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE);

        positiveBtn.setTextColor(Color.parseColor("#007aff"));
        negativeBtn.setTextColor(Color.parseColor("#007aff"));

        positiveBtn.setTextSize(COMPLEX_UNIT_SP, 17);
        negativeBtn.setTextSize(COMPLEX_UNIT_SP, 17);

        positiveBtn.setTypeface(null, Typeface.BOLD);
    }

    public void audioPlayer(String path, String fileName){
        //set up MediaPlayer
        MediaPlayer mp = new MediaPlayer();

        try {
            mp.setDataSource(path + File.separator + fileName);
            mp.prepare();
            mp.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
