package ulitis;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by nguyen hong phuc on 9/15/2016.
 */
public class DateTimeHelper {

    public static Date GetDateFromString(String strDate) {
        if (strDate == null) return null;
        L.V("GetDateFromString = " + strDate);
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        try {
            return df.parse(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String GetStringFromDate(Date date) {
        //L.V("GetStringFromDate = "+date);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);

        int hour24 = cal.get(Calendar.HOUR_OF_DAY);

        Calendar today = Calendar.getInstance();
        cal.setTimeInMillis(System.currentTimeMillis());
        int yearToday = cal.get(Calendar.YEAR);
        int monthToday = cal.get(Calendar.MONTH);
        int dayToday = cal.get(Calendar.DAY_OF_MONTH);

        DateFormat df;
        if (year == yearToday && month == monthToday && dayToday - day == 1) {
            df = new SimpleDateFormat("'Yesterday at' hh:mm ");
        } else {
            df = new SimpleDateFormat("dd/MM/yyyy 'at' hh:mm ");
        }
        df.setTimeZone(TimeZone.getDefault());
        return df.format(date) + (hour24 >= 12 ? "PM" : "AM");
    }


    public static String STRINGFROMDATE(Date date, Locale locale) {
//        DateFormat df = DateFormat.getDateInstance(DateFormat.LONG, locale);
        SimpleDateFormat spf = new SimpleDateFormat("dd.MM.yyyy", locale);
        return spf.format(date);
    }

    public static String STRINGFROMDATEV2(Date date, Locale locale) {
        SimpleDateFormat spf = new SimpleDateFormat("dd MMMM yyyy", locale);
        return spf.format(date);
    }

    public static String dateLocale(Date date, Locale locale) {
        SimpleDateFormat spf = new SimpleDateFormat("MMMM yyyy", locale);
        return spf.format(date);
    }

    public static String dateLocaleSimple(Date date, Locale locale) {
        SimpleDateFormat spf = new SimpleDateFormat("MMM yyyy", locale);
        return spf.format(date);
    }

    /**
     * display date is full format
     *
     * @param date
     * @param locale
     * @return
     */
    public static String FULLDATE(Date date, Locale locale) {
        DateFormat df = DateFormat.getDateInstance(DateFormat.FULL, locale);
        return df.format(date);
    }

    public static Date TODAY() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getDefault());
        return calendar.getTime();
    }

    public static Date GETDATE(int y, int m, int d) {
        DateFormat df = new SimpleDateFormat("yyyy/MM/dd");
        try {
            return df.parse(y + "/" + m + "/" + d);
        } catch (ParseException e) {
            return null;
        }
    }

    /**
     * return long time of first day of current week
     * SUNDAY = 1, MONDAY = 2, ...
     *
     * @return
     */
    public static long FirstDayOfCurrentWeek() {
        // get today and clear time of day
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0); // ! clear would not reset the hour of day !
        cal.clear(Calendar.MINUTE);
        cal.clear(Calendar.SECOND);
        cal.clear(Calendar.MILLISECOND);
        // get start of this week in milliseconds
        cal.set(Calendar.DAY_OF_WEEK, cal.getFirstDayOfWeek());
        return cal.getTimeInMillis();
    }

    public static long FirstOfCurrentDay() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0); // ! clear would not reset the hour of day !
        cal.clear(Calendar.MINUTE);
        cal.clear(Calendar.SECOND);
        cal.clear(Calendar.MILLISECOND);
        return cal.getTimeInMillis();
    }
}
