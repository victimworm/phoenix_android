package ulitis;

import android.content.Context;

import java.io.File;

/**
 * Created by nvdai on 9/14/2016.
 */

/**
 * Create Folder TTImages_cache.
 */
public class FileCache {
    private File cacheDir;

    public FileCache(Context context) {
        //Find the dir to icon_accept cached images
        if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))
            cacheDir = new File(android.os.Environment.getExternalStorageDirectory(), "TTImages_cache");
        else
            cacheDir = context.getCacheDir();
        if (!cacheDir.exists())
            cacheDir.mkdirs();
    }

    /**
     * khai bao 1 file, file se luu cache
     * @param url
     * @return
     */
    public File getFile(String url) {
        String filename =  "phoeniex"+String.valueOf(url.hashCode())+".jpg";
        File f = new File(cacheDir, filename);
        return f;

    }

    public void clear() {
        File[] files = cacheDir.listFiles();
        if (files == null)
            return;
        for (File f : files)
            f.delete();
    }
}
