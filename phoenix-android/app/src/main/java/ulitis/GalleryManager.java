package ulitis;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class GalleryManager {

    public static Uri mCapturedImageURI = null;

    public static void openGallery(Activity act) {
//        Intent intent = new Intent();
//        intent.setType("image/*");
//        intent.setAction(Intent.ACTION_GET_CONTENT);
////		intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
//        act.startActivityForResult(Intent.createChooser(intent, "Select Picture"), 0);

/** Choose image from Gallery and Camera*/

        File rootDir = new File(Constants.PICTURE_DIRECTORY);
        rootDir.mkdir();
        File file = new File(rootDir, "Phoenix_" + String.valueOf(System.currentTimeMillis()) + ".jpg");

        mCapturedImageURI = Uri.fromFile(file);

        // Initialize a list to hold any camera application intents.
        final List<Intent> cameraIntents = new ArrayList<Intent>();
        // Get the default camera capture intent
        final Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        //Get the package Manager
        final PackageManager packageManager = act.getPackageManager();
        // Ensure the package manager exists.
        if (packageManager != null) {

            // Get all available image capture app activities.
            final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);

            // Create camera intents for all image capture app activities.
            for (ResolveInfo res : listCam) {

                // Ensure the activity info exists.
                if (res.activityInfo != null) {

                    // Get the activity's package name.
                    final String packageName = res.activityInfo.packageName;

                    // Create a new camera intent based on android's default capture intent.
                    final Intent intent = new Intent(captureIntent);

                    // Set the intent data for the current image capture app.
                    intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
                    intent.setPackage(packageName);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI);

                    // Add the intent to available camera intents.
                    cameraIntents.add(intent);
                }
            }
            // Create an intent to get pictures from the filesystem.
            final Intent galleryIntent = new Intent();
            galleryIntent.setType("image/*");
            galleryIntent.setAction(Intent.ACTION_GET_CONTENT);

            // Chooser of filesystem options.
            final Intent chooserIntent = Intent.createChooser(galleryIntent, "Select Source");

            // Add the camera options.
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[cameraIntents.size()]));

            // Start activity to choose or take a picture.
            act.startActivityForResult(chooserIntent, Constants.ACTION_REQUEST_IMAGE);
        }
    }


    /**
     * please, put me in onActivityResult
     */
    public static String getPathImageSelected(Intent data, Activity act) {
        Uri uri = data.getData();
        //Log.v("MyDebug0", "uri = " + uri);
        return GalleryManager.getPathFileFromUri(uri, act);
    }

    /**
     * return path to file from uri
     *
     * @param uri
     * @return
     */
    public static String getPathFileFromUri(Uri uri, Activity act) {
        String[] projection = {MediaStore.Images.Media.DATA};
        //Log.v("MyDebug0", "projection : " + projection);
        for (String columnName : projection) {
            //Log.v("MyDebug0", "columnName : " + columnName);
        }
//		Cursor cursor = act.managedQuery(uri, projection, null, null, null);

        Cursor cursor = act.getContentResolver().query(uri, projection, null, null, null);
        if (cursor!=null) {
            String[] columnNames = cursor.getColumnNames();
            cursor.moveToFirst();
            //Log.v("MyDebug0", "cursor = " + cursor);
            for (String columnName : columnNames) {
                //Log.v("MyDebug0", "columnName : " + columnName);
                int index = -1;
                //Log.v("MyDebug0", "columnIndex : " + (index = cursor.getColumnIndex(columnName)));
                //Log.v("MyDebug0", "columnValue : " + cursor.getString(index));
            }

            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }else{
            String path = uri.getPath();
            File f = new File(path);
            if (f.exists()){
                return path;
            }else{
                return null;
            }


        }
    }

}
