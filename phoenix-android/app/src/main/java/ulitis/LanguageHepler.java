package ulitis;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;


import java.util.Locale;

/**
 * Created by nvdai on 10/28/2016.
 */
public class LanguageHepler{

    public static void changeLocale(Resources resources, String locale){
        Configuration config;
        config = new Configuration(resources.getConfiguration());
        switch(locale){
            case "vi":
                config.locale = new Locale("vi");
                break;
            case "fr":
                config.locale = Locale.FRENCH;
                break;
            default:
                config.locale = Locale.ENGLISH;
                break;
        }
        resources.updateConfiguration(config, resources.getDisplayMetrics());
    }


}
