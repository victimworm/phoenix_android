package com.matkaline.phoenix;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import adapters.GridViewImageAdapter;
import models.ZoomView;
import models.models.cards.CardItem;
import ulitis.Constants;
import ulitis.UiHelper;
import ulitis.Utils;

/**
 * Created by nmcuong on 9/6/2016.
 */
public class ListCardActivity extends RootActivity implements View.OnClickListener {

    public ImageView image;
    private Bitmap bm;
    private TextView txtTitle;
    private ImageView imageBack, iv_music;
    private LinearLayout line1;
    private Utils utils;
    private List<CardItem> listCard = new ArrayList<CardItem>();
    private GridViewImageAdapter adapter;
    private ListView listview;
    private int columnWidth;
//    private LinearLayout container;
    private TextView tv_my_cash;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.custom_activity_listcard);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        //initSlidingMenu();
        resize();

        tv_my_cash = (TextView) findViewById(R.id.tv_my_cash);
        if(app.user.coin > 0) {
            tv_my_cash.setText(String.valueOf(app.user.coin));
        } else {
            tv_my_cash.setText(String.valueOf(0));
        }

        utils = new Utils(this);
        InitilizeGridLayout();
        listCard = utils.getPath();
        adapter = new GridViewImageAdapter(ListCardActivity.this, listCard, columnWidth);
        iv_music.setOnClickListener(this);

//        listview = new ListView(ListCardActivity.this);
//        listview.setLayoutParams(new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));

//        ZoomView zoomView = new ZoomView(ListCardActivity.this);
//        zoomView.setLayoutParams(new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
//        zoomView.addView(listview);
//        container.addView(zoomView);

        listview.setAdapter(adapter);

//        View v = findViewById(R.id.aaaaaa); // get reference to root activity view
//        v.setOnClickListener(new View.OnClickListener() {
//            float zoomFactor = 1.5f;
//            boolean zoomedOut = false;
//
//            @Override
//            public void onClick(View v) {
//                if(zoomedOut) {
//                    // now zoom in
//                    v.setScaleX(1);
//                    v.setScaleY(1);
//                    zoomedOut = false;
//                }
//                else {
//                    v.setScaleX(zoomFactor);
//                    v.setScaleY(zoomFactor);
//                    zoomedOut = true;
//                }
//            }
//        });
    }

    public void recycle() {
        if (bm != null && !bm.isRecycled())
            bm.recycle();
    }

    /**
     * Initilizing Grid View
     */
    private void InitilizeGridLayout() {
        Resources r = getResources();
        float padding = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, Constants.GRID_PADDING, r.getDisplayMetrics());

        columnWidth = (int) ((utils.getScreenWidth() - ((Constants.NUM_OF_COLUMNS + Constants.NUM_OF_COLUMNS) * padding) - padding) / Constants.NUM_OF_COLUMNS);

//        Log.v("MyDebug0","Padding = "+ padding);
//        Log.v("MyDebug0","getScreenWidth = "+ utils.getScreenWidth());
//        Log.v("MyDebug0","columnWidth = "+ columnWidth);

        listview.setDividerHeight((int)padding*2);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        adapter.destroy();
    }

    private void resize(){
//        ImageButton menu = (ImageButton) findViewById(R.id.buttonMenu);
//        UiHelper.RESIZE(menu,106,88);
//        UiHelper.MARGIN(menu,36,50,0,0);
        line1 = (LinearLayout) findViewById(R.id.linear_list_card);
        imageBack = (ImageView) findViewById(R.id.icon_back);
//        container = (LinearLayout) findViewById(R.id.aaaaaa);
        listview = (ListView) findViewById(R.id.grid_view);
        txtTitle = (TextView) findViewById(R.id.title_listCard);
        iv_music = (ImageView) findViewById(R.id.iv_music);

        UiHelper.RESIZE(iv_music, 60, 60);
        UiHelper.RESIZE(line1,165,135);
        UiHelper.RESIZE(imageBack,43,73);
        UiHelper.MARGIN(imageBack,0,35,0,45);
        UiHelper.TEXTSIZE(txtTitle,60);
        UiHelper.MARGIN(txtTitle,0,40,0,120);

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
    }

    @Override
    public void onResume() {
        super.onResume();
        if(!loadMusicStatus()) {
            iv_music.setAlpha(1.0f);
        } else {
            iv_music.setAlpha(0.3f);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_music:
                if(!loadMusicStatus()) {
                    saveMusicStatus(true);
                    iv_music.setAlpha(0.3f);
                    utils.stopMusic();
//                    utils.muteMusic();
                } else {
                    saveMusicStatus(false);
                    iv_music.setAlpha(1.0f);
//                    utils.playMusic(R.raw.card_phoenix);
//                    utils.unmuteMusic();
                }
                break;

        }
    }
}


