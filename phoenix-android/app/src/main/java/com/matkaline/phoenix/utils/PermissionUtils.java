package com.matkaline.phoenix.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.PermissionChecker;
import androidx.fragment.app.Fragment;

/**
 * Check, request permission utils
 */
public class PermissionUtils {
    public static final int TURN_ON_DEVICE_GPS_REQUEST_CODE = 5151;

    public static boolean checkToRequest(Activity activity, String permission, int requestCode) {
        int permissionCheck = ContextCompat.checkSelfPermission(activity, permission);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
                Log.i("PermissionUtils", "Should show request permission rationale");
            } else {
                ActivityCompat.requestPermissions(activity, new String[]{permission}, requestCode);
            }

            return false;
        } else {
            return true;
        }
    }

    public static boolean needRequestPermissions(Context context, Fragment fragment, String[] permissions, int requestCode) {
        if (hasPermissions(context, permissions)) {
            return false;
        }

        fragment.requestPermissions(permissions, requestCode);
        return true;
    }

    public static boolean needRequestPermissions(Activity context, Fragment fragment, String[] permissions, int requestCode) {
        if (hasPermissions(context, permissions)) {
            return false;
        }
        fragment.requestPermissions(permissions, requestCode);
        return true;
    }

    public static boolean hasPermissions(Context context, String[] permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean hasPermissions(Context context, String[] permissions, int requestCode) {
        if (!hasPermissions(context, permissions)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
                ActivityCompat.requestPermissions((Activity) context, permissions, requestCode);
                return false;
            }
        }
        return true;
    }

    public static boolean selfPermissionGranted(String permission, Context context) {
        boolean result = true;
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                result = context.checkSelfPermission(permission)
                        == PackageManager.PERMISSION_GRANTED;
            } else {
                result = PermissionChecker.checkSelfPermission(context, permission)
                        == PermissionChecker.PERMISSION_GRANTED;
                //Utils.info("");
            }
        } catch (Exception e) {
            //Utils.info(e.toString());
        }
        return result;
    }

    public static boolean isPermissionsGranted(int[] grantResults) {
        for (int grant : grantResults) {
            if (grant != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    public static boolean isPermissionsDenied(int[] grantResults) {
        for (int grant : grantResults) {
            if (grant != PackageManager.PERMISSION_DENIED) {
                return false;
            }
        }
        return true;
    }

    public static void showErrorPermissionsAlert(String title, String mes, final Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(mes);
        builder.setPositiveButton("Cài đặt", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", context.getPackageName(), null);
                intent.setData(uri);
                context.startActivity(intent);
            }
        });

        builder.setNegativeButton("Bỏ qua", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

}
