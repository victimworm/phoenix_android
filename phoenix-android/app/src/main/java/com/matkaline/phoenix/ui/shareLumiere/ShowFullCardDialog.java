package com.matkaline.phoenix.ui.shareLumiere;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.view.Window;

import com.matkaline.phoenix.R;

import views.TouchImageView;

public class ShowFullCardDialog {

    private Context mContext;
    private Bitmap bm;

    public void setBitmap(Bitmap bitmap) {
        this.bm = bitmap;
    }

    public ShowFullCardDialog(Context context) {
        this.mContext = context;
    }

    public void show() {
        final Dialog dialog = new Dialog(mContext, android.R.style.Theme_NoTitleBar_Fullscreen);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_show_full_card);

        dialog.setCanceledOnTouchOutside(true);

        TouchImageView imageView = dialog.findViewById(R.id.dialog_show_full_card_image);
        if (bm == null) {
            bm = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.card_back);
        }
        imageView.setImageBitmap(bm);

        dialog.findViewById(R.id.dialog_show_full_card_img_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }
}
