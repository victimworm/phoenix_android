package com.matkaline.phoenix;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import androidx.core.content.ContextCompat;
import android.text.Html;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.matkaline.phoenix.data.local.prefs.AppPreferencesHelper;
import com.matkaline.phoenix.utils.AppConstants;

import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Random;
import java.util.TimeZone;

import dbs.HistoryWeek;
import dialogs.LoginChooseDialog;
import http.HttpManager;
import models.models.cards.Card;
import models.models.cards.CardItem;
import models.models.cards.PlayCardType;
import ulitis.DateTimeHelper;
import ulitis.UiHelper;
import ulitis.Utils;
import views.CommentListView;
import views.TouchImageView;

public class ResultActivity extends RootActivity implements View.OnClickListener {
    CallbackManager callbackManager;
    private TextView txtMsg;
    private TextView txtDate;
    private TextView tv_user_name;
    private TextView txtYourQuestion;
    private WebView webDes;
    private TextView txtTitle;
    private TextView txtNameApp;
    private TextView tv_team_result_card;
    private Button btResult, btMerge, btn_result_sensage, btn_result_normal, btn_result_lumiere;
    private ImageView imgCard, image_trans, iv_music;
    //    private ImageButton menu;
//    private View imageBG;
    private ImageView imageBack;
    private LinearLayout line1;

    private boolean isShareButtonEnable = true;
    private LinearLayout btnShare;

    private LinearLayout ll_six_line_msg;
    private LinearLayout linearTitle;
    private TextView tv_type_result_card;
    private LinearLayout ll_result_sensage;
    private LinearLayout ll_result_lumiere;
    private RelativeLayout layoutCard;
    private TextView tv_my_cash;
    private TextView tv_cash_sensage;
    private TextView tv_cash_lumiere;
    private LinearLayout ll_cash_sensage;
    private LinearLayout ll_cash_lumiere;
    private ImageView iv_cash_sensage;
    private ImageView iv_cash_lumiere;
    private LinearLayout bottom_black_bar;
    public String descCard = "";
    public static final int REQUEST_CODE_MAIL = 1000;
    private final Integer[] TEAM_CREATION = {15, 17, 19, 21, 23, 25};
    private final Integer[] TEAM_ZEN = {14, 16, 18, 20, 22, 24};
    private final Integer[] TEAM_DETRESSE = {2, 4, 6, 8, 10, 12};
    private final Integer[] TEAM_REVOLTE = {1, 3, 5, 7, 9, 11};

    private final Integer[] TEAM_CREATION_REVERSE = {1, 3, 5, 7, 9, 11};
    private final Integer[] TEAM_ZEN_REVERSE = {2, 4, 6, 8, 10};
    private final Integer[] TEAM_DETRESSE_REVERSE = {14, 16, 18, 20, 22, 24};
    private final Integer[] TEAM_REVOLTE_REVERSE = {15, 17, 19, 21, 23, 25};
    private final String CREATION = "Team Création";
    private final String REVOLTE = "Team Révolte";
    private final String SOUCI = "Team Souci";
    private final String ZEN = "Team Zen";
    private final String CHOC_EXT = "Choc Externe";
    private final String CHARM_EXT = "Charme Externe";
    private final String CHOC_INT = "Choc Interne";
    private final String CHARM_INT = "Charme Interne";
    //    private final String FB_
    private File imagePath;
    private CommentListView vCommentList;
    private RelativeLayout lyMain;
    public static boolean isDefi = false;
    public HistoryWeek historyWeek = null;
    private Utils utils;
    private String[] ombre;
    private final String IS_PAID_TODAY = "is_paid_today";
    private final String IS_PAID_TODAY_DEFI = "is_paid_today_defi";
    private final String CURRENT_TODAY = "current_today";
    public static final String SENTENCE_SENSAGE = "sentence_sensage";
    public static final String SENTENCE_SENSAGE_DEFI = "sentence_sensage_defi";
    private Random rd;
    private SharedPreferences prefs;

    //SonLA_thêm key to cache purchase lumiere today - S
    private final String IS_PAID_LUMIERE_TODAY = "is_paid_lumiere_today";
    private final String IS_PAID_LUMIERE_RANDOM = "is_paid_lumiere_random";
    //SonLA_thêm key to cache purchase lumiere today - S

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(0, 0);
        setContentView(R.layout.custom_screen_result);
//        initSlidingMenu();
        init();
        callbackManager = CallbackManager.Factory.create();
        prefs = app.getSharedPreferences(app.getPackageName(), app.MODE_PRIVATE);
        utils = new Utils(this);
        app.currentCard = app.cardMng.getResultCard();
        checkNewDay(); // must put below getResultCard() because it will calculate the reserve result later

        if (app.user.coin > 0) {
            tv_my_cash.setText(String.valueOf(app.user.coin));
        } else {
            tv_my_cash.setText(String.valueOf(0));
        }

        AppPreferencesHelper preferencesHelper = new AppPreferencesHelper(getApplicationContext(), AppConstants.PREF_NAME);
        String yourQuestion = preferencesHelper.getYourQuestion();
        if (yourQuestion == null) {
            txtYourQuestion.setVisibility(View.GONE);
        } else {
            txtYourQuestion.setVisibility(View.VISIBLE);
            txtYourQuestion.setText(yourQuestion);
        }

        if (app.cardMng.datSelectCard != null && !app.cardMng.isButton) {//tương lai
            tv_user_name.setVisibility(View.GONE);
            txtDate.setText(getResources().getString(R.string.title_future) + ": " + DateTimeHelper.STRINGFROMDATE(app.cardMng.datSelectCard, getCurrentLanguage()));
            //nếu tương lai thì bỏ lumiere
            ll_cash_lumiere.setVisibility(View.GONE);
            btn_result_lumiere.setVisibility(View.GONE);
        } else if (app.cardMng.isButton) {//
            txtDate.setText(DateTimeHelper.STRINGFROMDATE(app.cardMng.datSelectCard, getCurrentLanguage()));
            txtTitle.setText(getResources().getString(R.string.title_marcate));
            if (app.user.isLogin()) {
                tv_user_name.setVisibility(View.VISIBLE);
                tv_user_name.setText(app.user.user_name);
            }
        } else {//random
            tv_user_name.setVisibility(View.GONE);
            bottom_black_bar.setVisibility(View.GONE);
            if (app.cardMng.question != null) {
                //SonLA_nếu là lumiere thì lấy question từ preference property - S
                if (yourQuestion != null) {
                } else {
                    app.cardMng.getQuestionFromUs();
                    txtDate.setText(app.cardMng.question);
                }
                //SonLA_nếu là lumiere thì lấy question từ preference property - E
            } else {
                txtDate.setText(getResources().getString(R.string.title_question_of));
            }
        }

        if (!app.cardMng.reconcile) {//chua hoa giai : ghi nhan la dc quan tot hay xau
            app.cardMng.isBad = app.currentCard.isBad();
            if (app.currentCard.isBad() && app.cardMng.datSelectCard != null) {
                txtMsg.setText(getResources().getString(R.string.message_text));
                btMerge.setVisibility(View.VISIBLE);
            } else {
                txtMsg.setVisibility(View.GONE);// GONE -> INVISIBLE
                btMerge.setVisibility(View.INVISIBLE); // GONE -> INVISIBLE
            }
        } else {
            txtMsg.setText(getResources().getString(R.string.after_merging));
            btMerge.setVisibility(View.INVISIBLE); // GONE -> INVISIBLE
        }

        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isShareButtonEnable) {
                    isShareButtonEnable = false;
                    if (!isNetworkStatusAvialable()) {
                        alert(getString(R.string.alert), getString(R.string.alert_offline), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        isShareButtonEnable = true;
                    } else {
                        // not retake work because challenge can override word
                        if (!app.cardMng.reconcile) {
                            getFacebookWords();
                        } else {

                        }

                        Intent intent = new Intent(ResultActivity.this, FacebookShareScreen.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(intent);
                        isShareButtonEnable = true;
                    }
                }
            }
        });

        imgCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFullImage(app.currentCard.number, app.currentCard.isReverse);
            }
        });
        ////Log.v("MyDebug0", "Result cards = " + app.cardMng.cardsResult[0] + " " + app.cardMng.cardsResult[1] + " " + app.cardMng.cardsResult[2]);

        if (app.cardMng.datSelectCard != null) {
//            String strDate = DateTimeHelper.STRINGFROMDATE(app.cardMng.datSelectCard, getLocale());
//            History history = History.GetByDate(strDate);
            /*History history = null;
            if (history == null)
                history = new History();
            history.created_at = strDate;

            Card car1 = app.cardMng.cardsResult[0];
            Card car2 = app.cardMng.cardsResult[1];
            Card car3 = app.cardMng.cardsResult[2];
            if (app.cardMng.isBad) {
                if (app.cardMng.reconcile) {
                    history.infor = app.cardMng.getCardId(car1) + "," + app.cardMng.getCardId(car2) + "," + app.cardMng.getCardId(car3);
                } else {
                    history.infor = app.cardMng.getCardId(car1) + "," + app.cardMng.getCardId(car2) + ", null";
                }
            } else {
                history.infor = app.cardMng.getCardId(car1) + "," + app.cardMng.getCardId(car2) + ",";
            }
            history.user_id = app.user.user_id;*/
        }

//        save();

        addCommentView();
        contentCard();
        String titleTeam = addTeamOfCard(app.currentCard);
        if (!TextUtils.isEmpty(titleTeam) && titleTeam.equals(CREATION)) {
            tv_team_result_card.setText(titleTeam);
            tv_team_result_card.setTextColor(Color.GREEN);
            tv_type_result_card.setText(CHARM_EXT);
        } else if (!TextUtils.isEmpty(titleTeam) && titleTeam.equals(REVOLTE)) {
            tv_team_result_card.setText(titleTeam);
            tv_team_result_card.setTextColor(Color.RED);
            tv_type_result_card.setText(CHOC_EXT);
        } else if (!TextUtils.isEmpty(titleTeam) && titleTeam.equals(SOUCI)) {
            tv_team_result_card.setText(titleTeam);
            tv_team_result_card.setTextColor(Color.BLUE);
            tv_type_result_card.setText(CHOC_INT);
        } else if (!TextUtils.isEmpty(titleTeam) && titleTeam.equals(ZEN)) {
            tv_team_result_card.setText(titleTeam);
            tv_team_result_card.setTextColor(Color.YELLOW);
            tv_type_result_card.setText(CHARM_INT);

        }

        if (app.cardMng.isButton) { // today
            tv_team_result_card.setVisibility(View.VISIBLE);
            tv_type_result_card.setVisibility(View.VISIBLE);
            ll_result_sensage.setVisibility(View.VISIBLE);
        } else { // random
            tv_team_result_card.setVisibility(View.INVISIBLE);
            tv_type_result_card.setVisibility(View.INVISIBLE);
            ll_result_sensage.setVisibility(View.GONE);
        }

        addSixLinesContent(app.currentCard);
        iv_music.setOnClickListener(this);
    }

    private String addTeamOfCard(Card currentCard) {
        if (currentCard.isReverse) { // use abnormal case cause it IS reverse
            if (Arrays.asList(TEAM_CREATION_REVERSE).contains((int) currentCard.number)) {
                return CREATION;
            } else if (Arrays.asList(TEAM_DETRESSE_REVERSE).contains((int) currentCard.number)) {
                return SOUCI;
            } else if (Arrays.asList(TEAM_REVOLTE_REVERSE).contains((int) currentCard.number)) {
                return REVOLTE;
            } else if (Arrays.asList(TEAM_ZEN_REVERSE).contains((int) currentCard.number)) {
                return ZEN;
            } else {
                return null;
            }
        } else { // use normal case
            if (Arrays.asList(TEAM_CREATION).contains((int) currentCard.number)) {
                return CREATION;
            } else if (Arrays.asList(TEAM_DETRESSE).contains((int) currentCard.number)) {
                return SOUCI;
            } else if (Arrays.asList(TEAM_REVOLTE).contains((int) currentCard.number)) {
                return REVOLTE;
            } else if (Arrays.asList(TEAM_ZEN).contains((int) currentCard.number)) {
                return ZEN;
            } else {
                return null;
            }
        }


    }

    private void addSixLinesContent(Card currentCard) {
        String[] arrGroup;
        switch (getCurrentLanguage().getLanguage()) {
            case "vi":
                if (currentCard.isReverse) {
                    arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_vi/group_1/minus_" + currentCard.number);
                } else {
                    arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_vi/group_1/" + currentCard.number);
                }
                break;
            case "en":
                if (currentCard.isReverse) {
                    arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_en/group_1/minus_" + currentCard.number);
                } else {
                    arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_en/group_1/" + currentCard.number);
                }
                break;
            default:
                if (currentCard.isReverse) {
                    arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_1/minus_" + currentCard.number);
                } else {
                    arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_1/" + currentCard.number);
                }
                break;
        }

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
//        Resources r = this.getResources();
//        int px = (int) TypedValue.applyDimension(
//                TypedValue.COMPLEX_UNIT_DIP,
//                10,
//                r.getDisplayMetrics()
//        );
//        params.setMargins(0, px, 0, 0);
        if (arrGroup != null && arrGroup.length > 0) {
            for (int i = 0; i < arrGroup.length; i++) {
                String word = arrGroup[i].substring(0, 1).toUpperCase() + arrGroup[i].substring(1).toLowerCase();
                TextView cardLine = new TextView(this);
                cardLine.setLayoutParams(params);
                cardLine.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
                cardLine.setTextColor(Color.WHITE);
                cardLine.setText(word);
                UiHelper.TEXTSIZE(cardLine, 42);
                UiHelper.MARGIN(cardLine, 60, 50, 0, 0);
                ll_six_line_msg.addView(cardLine);

            }
        }

    }

    private void showFullImage(Byte cardNum, boolean isRotate) {
        final Dialog d = new Dialog(this, R.style.Theme_Dialog);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = d.getWindow();
        window.setFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND, WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        window.setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(this, R.color.black_trans_50)));
        d.setContentView(R.layout.layout_full_image);
        d.setCancelable(true);
        d.setCanceledOnTouchOutside(true);

        TouchImageView iv_full_screen = (TouchImageView) d.findViewById(R.id.iv_full_screen);
        iv_full_screen.setImageBitmap(app.currentCard.bm);
        if (isRotate) {
            iv_full_screen.setScaleX(-1);
            iv_full_screen.setScaleY(-1);
        }
        ImageView iv_close_ic = (ImageView) d.findViewById(R.id.iv_close_ic);
        iv_close_ic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                d.dismiss();
            }
        });
        UiHelper.MARGIN(iv_close_ic, 0, 50, 50, 0);
        UiHelper.RESIZE(iv_close_ic, 100, 100);

        d.show();
    }

    /**
     * save card today into database
     * no future, no random
     */
    private void save() {
        if (historyWeek == null) {
            historyWeek = new HistoryWeek();
        }
        if (app.cardMng.isButton) {//quan bai la boc cho today
            historyWeek.user_id = app.user.user_id;
            historyWeek.date_card = DateTimeHelper.FirstOfCurrentDay();//thoi diem boc - tinh la first of current day
            Card wCard1 = app.cardMng.cardsResult[0];//quan boc
            historyWeek.card_1 = app.cardMng.getCardId(wCard1);//ghi nhan quan boc
            if (app.cardMng.reconcile) {//co hoa giai
                Card wCard2 = app.cardMng.cardsResult[2];//quan hoa giai
                historyWeek.card_2 = app.cardMng.getCardId(wCard2);//ghi nhan quan hoa giai
            } else {
                historyWeek.card_2 = historyWeek.card_1;//khi khong hoa giai - coi quan hoa giai = quan boc
            }
            historyWeek.save();
        }
    }


    private void addCommentView() {
        lyMain = (RelativeLayout) findViewById(R.id.Relative1);
        lyMain.addView(vCommentList = new CommentListView(ResultActivity.this), new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        UiHelper.MARGIN(vCommentList, 0, 292, 0, 0);
        findViewById(R.id.line_bar_comment).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isNetworkStatusAvialable()) {
                    if (app.user.isLogin()) {
                        if (vCommentList.getVisibility() == View.VISIBLE) {
                            vCommentList.setVisibility(View.GONE);
                        } else {
                            vCommentList.setVisibility(View.VISIBLE);
                        }
                    } else {
                        app.dlgLoginChoose.show();
                    }
                } else {
                    alert(getString(R.string.alert), getString(R.string.alert_offline), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                }
            }
        });
    }

    private float exdown, eydown;
    private boolean isShowingDesc = false;
    private long t = System.currentTimeMillis();

    private void contentCard() {
        String strDes = app.cardMng.getExplain();
        descCard = Html.fromHtml(strDes).toString();
        webViewSetData(webDes, strDes, UiHelper.GETSIZE(55));

        webDes.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                String text = url.replace("file:///android_asset/", "");
                int number = Integer.parseInt(text);
                int numberId = Math.abs(number);
                CardItem cardItem = new CardItem(numberId);
                if (number < 0) {
                    cardItem.reverse = true;
                } else {
                    cardItem.reverse = false;
                }
//                app.cardID = numberId;
//                app.gameInforCardItem = cardItem;
                app.cardID = app.cardMng.getCardId(cardItem);
                app.gameInforCardItem = cardItem;

                Intent intent = new Intent(getApplicationContext(), MeanCardActivity.class);
                startActivity(intent);
                finish();
//                go(MeanCardActivity.class);

                return super.shouldOverrideUrlLoading(view, url);
            }
        });
    }

    private void webViewAnimation() {
        if (!isShowingDesc) {
            isShowingDesc = true;
            linearTitle.setVisibility(View.INVISIBLE);
            btMerge.setVisibility(View.INVISIBLE);
//            imageBG.setVisibility(View.GONE);
            txtTitle.setText(getResources().getString(R.string.explain));
            Animation slide = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_up);
//            layoutCard.setBackgroundResource(R.drawable.drawable_rectangle);
            UiHelper.MARGIN(webDes, 0, 0, 0, 0, true);
            webDes.startAnimation(slide);
            webDes.setSelected(false);
        } else {
            isShowingDesc = false;
            Animation slide = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_down);
            webDes.setScrollY(0);
            webDes.startAnimation(slide);
            slide.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    webDes.setScrollY(0);
                    UiHelper.MARGIN(webDes, 0, 1400, 0, 0, true);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            linearTitle.setVisibility(View.VISIBLE);
//            imageBG.setVisibility(View.VISIBLE);
            if (app.cardMng.isBad == false || isResult == true || app.cardMng.reconcile == true || app.cardMng.datSelectCard == null) {
                btMerge.setVisibility(View.INVISIBLE);
            } else {
                btMerge.setVisibility(View.VISIBLE);
            }
            txtTitle.setText(getResources().getString(R.string.title_result));
//            layoutCard.setBackgroundResource(R.color.Transparent);
        }
    }

    private void setImage(Card card) {
        imgCard.setImageBitmap(card.bm);
        if (card.isReverse) {
            imgCard.setScaleX(-1);
            imgCard.setScaleY(-1);
        }
    }

    private boolean isResult = false;

    public void result(View v) {
        isResult = true;
        txtMsg.setVisibility(View.GONE);
        btMerge.setVisibility(View.INVISIBLE);
    }

    public void reconcile(View v) {
        app.cardMng.reconcile = true;
        isDefi = true;
        go(CardsActivity.class);
        finish();
    }

    public void cardResult(View v) {
        String strDes = app.cardMng.getExplain();
        descCard = Html.fromHtml(strDes).toString();
        webViewSetData(webDes, strDes, UiHelper.GETSIZE(55));
        btn_result_normal.setVisibility(View.GONE);
        btn_result_sensage.setVisibility(View.VISIBLE);
    }

    private void cardLumiere() {
        //do something
        if (isNetworkStatusAvialable()) {
            if (app.user.isLogin()) {
                boolean isPaidLumiere = false;
                if (app.cardMng.isButton) {
                    isPaidLumiere = prefs.getBoolean(IS_PAID_LUMIERE_TODAY, false);
                } else {
                    isPaidLumiere = prefs.getBoolean(IS_PAID_LUMIERE_RANDOM, false);
                }
                if (isPaidLumiere) {
                    if (app.cardMng.isButton) {
                        goLumiere(MonthCardActivity.class, PlayCardType.LUMIERE_TODAY);
                    } else {
                        app.cardMng.isResetRandomIndex = false;
                        goLumiere(MonthCardActivity.class, PlayCardType.LUMIERE_RANDOM);
                    }
                } else {
                    if (app.user.coin >= 15) {
                        app.user.submitCoin(-15, new HttpManager.OnResponse() {
                            @Override
                            public void onResponse(JSONObject jso) {
                                ll_cash_lumiere.setVisibility(View.GONE);
                                SharedPreferences.Editor editor = prefs.edit();
                                if (app.cardMng.isButton) {
                                    editor.putBoolean(IS_PAID_LUMIERE_TODAY, true);
                                    editor.commit();
                                    goLumiere(MonthCardActivity.class, PlayCardType.LUMIERE_TODAY);
                                } else {
                                    editor.putBoolean(IS_PAID_LUMIERE_RANDOM,true);
                                    editor.commit();
                                    //thay đổi trọng số để rút ra lá bài giống nhau - S
                                    Random r = new Random();
                                    app.cardMng.deltaNumber = r.nextInt(99999 - 10000) + 10000;
                                    app.cardMng.isResetRandomIndex = true;
                                    //thay đổi trọng số để rút ra lá bài giống nhau - E
                                    goLumiere(MonthCardActivity.class, PlayCardType.LUMIERE_RANDOM);
                                }

                                app.user.updateCoinCount(jso);
                                if (app.user.coin > 0) {
                                    tv_my_cash.setText(String.valueOf(app.user.coin));
                                } else {
                                    tv_my_cash.setText(String.valueOf(0));
                                }
                            }
                        });
                    } else {
                        go(MyCashActivity.class);
                    }
                }
            }
        }

    }

    public void cardSensage(View v) {
        if (isNetworkStatusAvialable()) {
            if (app.user.isLogin()) {
                boolean isPaid = prefs.getBoolean(IS_PAID_TODAY, false);
                boolean isPaidDefi = prefs.getBoolean(IS_PAID_TODAY_DEFI, false);
                if (app.cardMng.reconcile) { // defi screen
                    if (isPaidDefi) {
                        btn_result_normal.setVisibility(View.VISIBLE);
                        btn_result_sensage.setVisibility(View.GONE);
                        webViewSetData(webDes, prefs.getString(ResultActivity.SENTENCE_SENSAGE_DEFI, null), UiHelper.GETSIZE(55));
                    } else {
                        if (app.user.coin >= 9) {
                            app.user.submitCoin(-9, new HttpManager.OnResponse() {
                                @Override
                                public void onResponse(JSONObject jso) {
                                    app.user.updateCoinCount(jso);
                                    SharedPreferences.Editor editor = prefs.edit();
                                    editor.putBoolean(IS_PAID_TODAY_DEFI, true);
                                    editor.commit();
                                    webViewSetData(webDes, prefs.getString(ResultActivity.SENTENCE_SENSAGE_DEFI, null), UiHelper.GETSIZE(55));
                                    btn_result_normal.setVisibility(View.VISIBLE);
                                    ll_cash_sensage.setVisibility(View.INVISIBLE);
                                    btn_result_sensage.setVisibility(View.GONE);
                                }
                            });
                        } else {
                            go(MyCashActivity.class);
                        }
                    }
                } else { // normal screen
                    if (isPaid) {
                        webViewSetData(webDes, prefs.getString(ResultActivity.SENTENCE_SENSAGE, null), UiHelper.GETSIZE(55));
                        btn_result_normal.setVisibility(View.VISIBLE);
                        btn_result_sensage.setVisibility(View.GONE);
                    } else {
                        if (app.user.coin >= 9) {
                            app.user.submitCoin(-9, new HttpManager.OnResponse() {
                                @Override
                                public void onResponse(JSONObject jso) {
                                    app.user.updateCoinCount(jso);
                                    SharedPreferences.Editor editor = prefs.edit();
                                    editor.putBoolean(IS_PAID_TODAY, true);
                                    editor.commit();
                                    webViewSetData(webDes, prefs.getString(ResultActivity.SENTENCE_SENSAGE, null), UiHelper.GETSIZE(55));
                                    btn_result_normal.setVisibility(View.VISIBLE);
                                    ll_cash_sensage.setVisibility(View.INVISIBLE);
                                    btn_result_sensage.setVisibility(View.GONE);
                                }
                            });
                        } else {
                            go(MyCashActivity.class);
                        }
                    }
                }

            } else {
                app.dlgLoginChoose = new LoginChooseDialog(this,this);
                app.dlgLoginChoose.show();
            }
        } else {
            alert(getString(R.string.alert), getString(R.string.alert_offline), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
        }

    }

    private void checkNewDay() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getDefault());
        int dayOfWeek = calendar.get(Calendar.DAY_OF_MONTH);
        int storedToday = prefs.getInt(CURRENT_TODAY, -1);
        SharedPreferences.Editor editor = prefs.edit();
        if (app.cardMng.reconcile) {
            String sentenceDefi = getSensageOmbre();
            editor.putString(SENTENCE_SENSAGE_DEFI, sentenceDefi);
        } else {
            editor.putString(SENTENCE_SENSAGE_DEFI, null);
        }
        if (dayOfWeek != storedToday) { // today is new! reset value! also get the sentence sensage for this day
            String sentence = getSensageOmbre();
            editor.putInt(CURRENT_TODAY, calendar.get(Calendar.DAY_OF_MONTH));
            editor.putBoolean(IS_PAID_TODAY, false);
            editor.putBoolean(IS_PAID_LUMIERE_TODAY, false);
            editor.putBoolean(IS_PAID_TODAY_DEFI, false);
            editor.putString(SENTENCE_SENSAGE, sentence);
        }
        editor.commit();

    }

    private String getSensageOmbre() {
        // reset type card to get exactly correct type.
        // use this in case function isBad() is not working
//        if (!app.cardMng.reconcile) {
//            if (app.cardMng.cardsResult[1].number == 0){
//                for (int i = 1; i <= 12; i++) {
//                    if(app.cardMng.cardsResult[0].number == i) {
//                        app.cardMng.cardsResult[0].type = Card.CHARME;
//                        break;
//                    }
//                }
//                for (int i = 14; i <= 25; i++) {
//                    if(app.cardMng.cardsResult[0].number == i) {
//                        app.cardMng.cardsResult[0].type = Card.CHOC;
//                        break;
//                    }
//                }
//            } else if (app.cardMng.cardsResult[1].number == 13){
//                if (app.cardMng.cardsResult[0].isBad()){
//                    for (int i = 1; i <= 12; i++) {
//                        if(app.cardMng.cardsResult[0].number == i) {
//                            app.cardMng.cardsResult[0].type = Card.CHARME;
//                            break;
//                        }
//                    }
//                    for (int i = 14; i <= 25; i++) {
//                        if(app.cardMng.cardsResult[0].number == i) {
//                            app.cardMng.cardsResult[0].type = Card.CHOC;
//                            break;
//                        }
//                    }
//                }else{
//                }
//            }else if (app.cardMng.cardsResult[0].color == app.cardMng.cardsResult[1].color){
//                if ((app.cardMng.cardsResult[0].number + app.cardMng.cardsResult[1].number) % 2 == 0) {//cung chan || cung le
//                    for (int i = 1; i <= 12; i++) {
//                        if(app.cardMng.cardsResult[0].number == i) {
//                            app.cardMng.cardsResult[0].type = Card.CHARME;
//                            break;
//                        }
//                    }
//                    for (int i = 14; i <= 25; i++) {
//                        if(app.cardMng.cardsResult[0].number == i) {
//                            app.cardMng.cardsResult[0].type = Card.CHOC;
//                            break;
//                        }
//                    }
//                }
//            }
//        }
        String strExplain = null;
        if (app.cardMng.datSelectCard != null) {//chon TODAY | FUTURE
            if (!app.cardMng.reconcile) {//chua hoac ko hoa giai
                if (app.cardMng.cardsResult[0].isReverse) {//quan bai nguoc
//                    Explain explain = Explain.get(-cardsResult[0].number);
//                    strExplain = explain.expn1;
                    switch (getCurrentLanguage().getLanguage()) {
                        case "vi":
                            strExplain = Utils.GetText(this.getAssets(), "sensage_vi/minus_" + app.cardMng.cardsResult[0].number + ".txt");
                            break;
                        case "en":
                            strExplain = Utils.GetText(this.getAssets(), "sensage/minus_" + app.cardMng.cardsResult[0].number);
                            break;
                        case "ru":
                            strExplain = Utils.GetText(this.getAssets(), "sensage_ru/minus_" + app.cardMng.cardsResult[0].number);
                            break;
                        default:
                            strExplain = Utils.GetText(this.getAssets(), "sensage/minus_" + app.cardMng.cardsResult[0].number);
                            break;
                    }

                    String strRandom = null;
                    if (app.cardMng.cardsResult[0].isBad()) {
                        strRandom = app.cardMng.getRandomChoc();
                    } else {
                        strRandom = app.cardMng.getRandomCharm();
                    }
                    String strOmbre = null;
                    switch (getCurrentLanguage().getLanguage()) {
                        case "vi":
                            strOmbre = Utils.GetText(this.getAssets(), "sensage_vi/ombre/" + app.cardMng.cardsResult[1].number + ".txt");
                            break;
                        case "en":
                            strOmbre = Utils.GetText(this.getAssets(), "sensage/ombre/" + app.cardMng.cardsResult[1].number);
                            break;
                        case "ru":
                            strOmbre = Utils.GetText(this.getAssets(), "sensage_ru/ombre/" + app.cardMng.cardsResult[1].number);
                            break;
                        default:
                            strOmbre = Utils.GetText(this.getAssets(), "sensage/ombre/" + app.cardMng.cardsResult[1].number);
                            break;
                    }

                    strRandom = strRandom.replace("[OMBRE]", strOmbre);
                    strExplain = strExplain.replace("[SENSAGE OMBRE]", strRandom);
                } else {//quan bai
//                    Explain explain = Explain.get(cardsResult[0].number);
//                    strExplain = explain.expn1;
                    switch (getCurrentLanguage().getLanguage()) {
                        case "vi":
                            strExplain = Utils.GetText(this.getAssets(), "sensage_vi/" + app.cardMng.cardsResult[0].number + ".txt");
                            break;
                        case "en":
                            strExplain = Utils.GetText(this.getAssets(), "sensage/" + app.cardMng.cardsResult[0].number);
                            break;
                        case "ru":
                            strExplain = Utils.GetText(this.getAssets(), "sensage_ru/" + app.cardMng.cardsResult[0].number);
                            break;
                        default:
                            strExplain = Utils.GetText(this.getAssets(), "sensage/" + app.cardMng.cardsResult[0].number);
                            break;
                    }

                }
            } else {
//                Explain explain = null;
//                if (cardsResult[2].number <=12 && cardsResult[2].number>=1){
//                    explain = Explain.get(-cardsResult[2].number);
//                }else{
//                    explain = Explain.get(cardsResult[2].number);
//                }
//
//                Explain explain1 = Explain.get(cardsResult[0].number);
//
                String strNom = null; // nom all is CHARM
                String strAdj = null; // adj all is CHOC
                String strDefi = null; // defi all is CHARM
                String strName = null; // name all is CHARM
                // thong tin cua la bai defi
                if (app.cardMng.cardsResult[2].isReverse) { // quan bai defi am
                    switch (getCurrentLanguage().getLanguage()) {
                        case "vi":
                            strName = Utils.GetText(this.getAssets(), "sensage_vi/name/minus_" + app.cardMng.cardsResult[2].number + ".txt");
                            strNom = Utils.GetText(this.getAssets(), "sensage_vi/nom/minus_" + app.cardMng.cardsResult[2].number + ".txt");
                            strDefi = Utils.GetText(this.getAssets(), "sensage_vi/defi/minus_" + app.cardMng.cardsResult[2].number + ".txt");
                            break;
                        case "en":
                            strName = Utils.GetText(this.getAssets(), "sensage/name/minus_" + app.cardMng.cardsResult[2].number);
                            strNom = Utils.GetText(this.getAssets(), "sensage/nom/minus_" + app.cardMng.cardsResult[2].number);
                            strDefi = Utils.GetText(this.getAssets(), "sensage/defi/minus_" + app.cardMng.cardsResult[2].number);
                            break;
                        case "ru":
                            strName = Utils.GetText(this.getAssets(), "sensage_ru/name/minus_" + app.cardMng.cardsResult[2].number);
                            strNom = Utils.GetText(this.getAssets(), "sensage_ru/nom/minus_" + app.cardMng.cardsResult[2].number);
                            strDefi = Utils.GetText(this.getAssets(), "sensage_ru/defi/minus_" + app.cardMng.cardsResult[2].number);
                            break;
                        default:
                            strName = Utils.GetText(this.getAssets(), "sensage/name/minus_" + app.cardMng.cardsResult[2].number);
                            strNom = Utils.GetText(this.getAssets(), "sensage/nom/minus_" + app.cardMng.cardsResult[2].number);
                            strDefi = Utils.GetText(this.getAssets(), "sensage/defi/minus_" + app.cardMng.cardsResult[2].number);
                            break;
                    }
                } else { // quan bai defi duong
                    switch (getCurrentLanguage().getLanguage()) {
                        case "vi":
                            strName = Utils.GetText(this.getAssets(), "sensage_vi/name/" + app.cardMng.cardsResult[2].number + ".txt");
                            strNom = Utils.GetText(this.getAssets(), "sensage_vi/nom/" + app.cardMng.cardsResult[2].number + ".txt");
                            strDefi = Utils.GetText(this.getAssets(), "sensage_vi/defi/" + app.cardMng.cardsResult[2].number + ".txt");
                            break;
                        case "en":
                            strName = Utils.GetText(this.getAssets(), "sensage/name/" + app.cardMng.cardsResult[2].number);
                            strNom = Utils.GetText(this.getAssets(), "sensage/nom/" + app.cardMng.cardsResult[2].number);
                            strDefi = Utils.GetText(this.getAssets(), "sensage/defi/" + app.cardMng.cardsResult[2].number);
                            break;
                        case "ru":
                            strName = Utils.GetText(this.getAssets(), "sensage_ru/name/" + app.cardMng.cardsResult[2].number);
                            strNom = Utils.GetText(this.getAssets(), "sensage_ru/nom/" + app.cardMng.cardsResult[2].number);
                            strDefi = Utils.GetText(this.getAssets(), "sensage_ru/defi/" + app.cardMng.cardsResult[2].number);
                            break;
                        default:
                            strName = Utils.GetText(this.getAssets(), "sensage/name/" + app.cardMng.cardsResult[2].number);
                            strNom = Utils.GetText(this.getAssets(), "sensage/nom/" + app.cardMng.cardsResult[2].number);
                            strDefi = Utils.GetText(this.getAssets(), "sensage/defi/" + app.cardMng.cardsResult[2].number);
                            break;
                    }

                }

                // adj lay tu quan bai trong ngay
                if (app.cardMng.cardsResult[0].isReverse) {
                    switch (getCurrentLanguage().getLanguage()) {
                        case "vi":
                            strAdj = Utils.GetText(this.getAssets(), "sensage_vi/adj/minus_" + app.cardMng.cardsResult[0].number + ".txt");
                            break;
                        case "en":
                            strAdj = Utils.GetText(this.getAssets(), "sensage/adj/minus_" + app.cardMng.cardsResult[0].number);
                            break;
                        case "ru":
                            strAdj = Utils.GetText(this.getAssets(), "sensage_ru/adj/minus_" + app.cardMng.cardsResult[0].number);
                            break;
                        default:
                            strAdj = Utils.GetText(this.getAssets(), "sensage/adj/minus_" + app.cardMng.cardsResult[0].number);
                            break;
                    }
                } else {
                    switch (getCurrentLanguage().getLanguage()) {
                        case "vi":
                            strAdj = Utils.GetText(this.getAssets(), "sensage_vi/adj/" + app.cardMng.cardsResult[0].number + ".txt");
                            break;
                        case "en":
                            strAdj = Utils.GetText(this.getAssets(), "sensage/adj/" + app.cardMng.cardsResult[0].number);
                            break;
                        case "ru":
                            strAdj = Utils.GetText(this.getAssets(), "sensage_ru/adj/" + app.cardMng.cardsResult[0].number);
                            break;
                        default:
                            strAdj = Utils.GetText(this.getAssets(), "sensage/adj/" + app.cardMng.cardsResult[0].number);
                            break;
                    }
                }


                String strSegment1 = app.cardMng.getRandomSegment1();
                strSegment1 = strSegment1.replace("[DÉFI]", strName);
                strSegment1 = strSegment1.replace("[ADJECTIF]", strAdj);
                strSegment1 = strSegment1.replace("[NOM]", strNom);
                String strSegment3 = app.cardMng.getRandomSegment3();
//                strExplain = strSegment1 + "<br>" + strSegment2 + "<br>" + strSegment3;
                strExplain = strSegment1 + strDefi + strSegment3;
            }
        }

      /*  String strOmbre = Utils.GetText(this.getAssets(),"sensage/ombre/" + app.cardMng.cardsResult[1].number);
        String sensageOmbre = null;
        if (app.cardMng.cardsResult[0].isBad()) {
            strRandom = getRandomBad();
        } else {
            strRandom = getRandomGood();
        }
        if(app.cardMng.cardsResult[0].type == Card.CHARME) {
            sensageOmbre = app.cardMng.getRandomCharm();
            sensageOmbre = sensageOmbre.replace("[OMBRE]", strOmbre);
        } else if(app.cardMng.cardsResult[0].type == Card.CHOC) {

        }*/
        return strExplain;
    }

    private void webViewSettingOnly(WebView web) {
        web.setBackgroundColor(0x00000000);
        web.setInitialScale(100);
        web.getSettings().setDefaultFontSize(UiHelper.GETSIZE(60));
    }

    private void init() {
        txtTitle = (TextView) findViewById(R.id.txtTitleRst);
        txtNameApp = (TextView) findViewById(R.id.textNameApp);
        txtMsg = (TextView) findViewById(R.id.txtMessage);
        txtDate = (TextView) findViewById(R.id.txtDate);
        tv_user_name = (TextView) findViewById(R.id.tv_user_name);
        txtYourQuestion = findViewById(R.id.custom_screen_result_txt_your_question);
        webDes = (WebView) findViewById(R.id.webDesc);
        ll_six_line_msg = (LinearLayout) findViewById(R.id.ll_six_line_msg);
        tv_team_result_card = (TextView) findViewById(R.id.tv_team_result_card);
        webViewSettingOnly(webDes);
        btn_result_sensage = (Button) findViewById(R.id.btn_result_sensage);
        btn_result_lumiere = findViewById(R.id.btn_result_lumiere);
        btn_result_normal = (Button) findViewById(R.id.btn_result_normal);
        ll_cash_sensage = (LinearLayout) findViewById(R.id.ll_cash_sensage);
        ll_cash_lumiere = findViewById(R.id.ll_cash_lumiere);
        iv_cash_sensage = (ImageView) findViewById(R.id.iv_cash_sensage);
        iv_cash_lumiere = findViewById(R.id.iv_cash_lumiere);
        tv_cash_sensage = (TextView) findViewById(R.id.tv_cash_sensage);
        tv_cash_lumiere = findViewById(R.id.tv_cash_lumiere);
        btResult = (Button) findViewById(R.id.btResult);
        btMerge = (Button) findViewById(R.id.btMerge);
        imgCard = (ImageView) findViewById(R.id.image);
//        image_trans = (ImageView) findViewById(R.id.image_trans);
//        menu = (ImageButton) findViewById(R.id.buttonMenu);
//        imageBG = (View) findViewById(R.id.imageBR);
        btnShare = findViewById(R.id.line_bar_share);
        bottom_black_bar = findViewById(R.id.bottom_bar_result_card);
        linearTitle = (LinearLayout) findViewById(R.id.linear_title_result);
//        linearButton = (LinearLayout) findViewById(R.id.linear_btn_conflict);
        line1 = (LinearLayout) findViewById(R.id.linear_result_card);
        ll_result_sensage = (LinearLayout) findViewById(R.id.ll_result_sensage);
        ll_result_lumiere = findViewById(R.id.ll_result_lumiere);
        imageBack = (ImageView) findViewById(R.id.icon_back);
        tv_my_cash = (TextView) findViewById(R.id.tv_my_cash);
        tv_type_result_card = (TextView) findViewById(R.id.tv_type_result_card);
        ImageView iv_cash = (ImageView) findViewById(R.id.iv_cash);

//        ll_result_sensage.setAlpha(0.5f); // TODO: Remove when implement sensage
        if (app.cardMng.datSelectCard != null) {
            ll_result_sensage.setVisibility(View.VISIBLE);
        } else {
            ll_result_sensage.setVisibility(View.INVISIBLE);
        }
        iv_music = (ImageView) findViewById(R.id.iv_music);

        UiHelper.RESIZE(iv_music, 60, 60);
        UiHelper.RESIZE(line1, 165, 135);
        UiHelper.RESIZE(imageBack, 43, 73);
        UiHelper.MARGIN(imageBack, 0, 35, 0, 45);

//        UiHelper.RESIZE(menu,106,88);
//        UiHelper.MARGIN(menu,36,50,0,0);

        UiHelper.TEXTSIZE(txtNameApp, 60);
        UiHelper.MARGIN(txtNameApp, 0, 55, 0, 0);

//        UiHelper.RESIZE(imageBG,-1,593);

        UiHelper.TEXTSIZE(txtTitle, 60);
        UiHelper.MARGIN(txtTitle, 0, 60, 0, 0);
        UiHelper.TEXTSIZE(txtDate, 45);
        UiHelper.TEXTSIZE(tv_user_name, 45);
        UiHelper.TEXTSIZE(txtMsg, 55);
        UiHelper.TEXTSIZE(txtYourQuestion, 45);
        UiHelper.TEXTSIZE(tv_team_result_card, 60);
        UiHelper.MARGIN(tv_team_result_card, 0, 15, 0, 10);
        UiHelper.TEXTSIZE(tv_type_result_card, 30);
        UiHelper.MARGIN(txtMsg, 96, 0, 96, 0);
        UiHelper.RESIZE(btResult, 260, 115);
        UiHelper.RESIZE(btMerge, 350, 115);
        UiHelper.RESIZE(btn_result_sensage, 350, 125);
        UiHelper.RESIZE(btn_result_lumiere, 350, 125);
        UiHelper.RESIZE(btn_result_normal, 350, 125);
        UiHelper.RESIZE(iv_cash_sensage, 50, 50);
        UiHelper.RESIZE(iv_cash_lumiere, 50, 50);
        UiHelper.TEXTSIZE(tv_cash_sensage, 45);
        UiHelper.MARGIN(tv_cash_sensage, 0, 0, 0, 0);
        UiHelper.TEXTSIZE(tv_cash_lumiere, 45);
        UiHelper.MARGIN(tv_cash_lumiere, 10, 0, 0, 0);
        UiHelper.MARGIN(iv_cash_sensage, 10, 0, 0, 0);
        UiHelper.MARGIN(iv_cash_lumiere, 10, 0, 0, 0);
        UiHelper.MARGIN(iv_cash, 200, 0, 0, 0);
        UiHelper.TEXTSIZE(btResult, 55);
        UiHelper.TEXTSIZE(btMerge, 40);
        UiHelper.TEXTSIZE(btn_result_sensage, 40);
        UiHelper.TEXTSIZE(btn_result_lumiere, 28);
        UiHelper.TEXTSIZE(btn_result_normal, 40);
        UiHelper.MARGIN(btResult, 0, 50, 15, 50);
        UiHelper.MARGIN(btMerge, 10, 200, 0, 0);
        UiHelper.MARGIN(btn_result_sensage, 10, 15, 0, 0);
        UiHelper.MARGIN(btn_result_lumiere, 10, 15, 0, 0);
        UiHelper.MARGIN(btn_result_normal, 10, 15, 0, 0);

        UiHelper.RESIZE(iv_cash, 60, 60);
        UiHelper.TEXTSIZE(tv_my_cash, 45);
        UiHelper.MARGIN(tv_my_cash, 10, 0, 100, 0);

        layoutCard = (RelativeLayout) findViewById(R.id.layout_meanCard);
//        UiHelper.RESIZE(layoutCard,-1,2280);
        UiHelper.MARGIN(layoutCard, 0, 150, 0, 0);
//        setupWebView();
//        webDes.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
//            @Override
//            public boolean onPreDraw() {
//                int height = webDes.getHeight();
//                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(webDes.getLayoutParams());
//
//                if( height != 0 ){
//                    Toast.makeText(getApplication(), "height:"+height,Toast.LENGTH_SHORT).show();
//                    webDes.getViewTreeObserver().removeOnPreDrawListener(this);
//                }
//                return false;
//            }
//        });

        LinearLayout lyBottomBar = (LinearLayout) findViewById(R.id.linearLayout_bottom_bar);
        UiHelper.RESIZE(lyBottomBar, -1, 192);

        ImageView imgComment = (ImageView) findViewById(R.id.imgComment);
        UiHelper.RESIZE(imgComment, 75, 75);
        UiHelper.MARGIN(imgComment, 96, 0, 0, 0);

        TextView txtComment = (TextView) findViewById(R.id.textComment);
        UiHelper.TEXTSIZE(txtComment, 55);
        UiHelper.MARGIN(txtComment, 36, 0, 0, 0);


        TextView txtShare = (TextView) findViewById(R.id.textShare);
        UiHelper.TEXTSIZE(txtShare, 55);
        UiHelper.MARGIN(txtShare, 0, 0, 96, 0);

        ImageView imgShare = (ImageView) findViewById(R.id.imgShare);
        UiHelper.RESIZE(imgShare, 83, 88);
        UiHelper.MARGIN(imgShare, 0, 0, 36, 0);


        UiHelper.RESIZE(imgCard, 740, 1380);
        UiHelper.MARGIN(imgCard, 25, 15, 0, 0);

        UiHelper.PADDING(webDes, 96, 0, 96, 48);
        rd = new Random();

        btn_result_lumiere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cardLumiere();
            }
        });
    }

    private void newShare(Uri uri) {
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
        shareIntent.setType("image/jpeg");
        startActivity(Intent.createChooser(shareIntent, getResources().getString(R.string.title_share)));
    }

    //Previous function use
    private void share(Uri uri1) {
        List<Intent> targetedShareIntents = new ArrayList<Intent>();

        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject test");
        shareIntent.putExtra(Intent.EXTRA_CC, "Extra Email");
        shareIntent.setType("image/*");
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//        Uri uri1 = Uri.fromFile(imagePath);
//        Uri uri2 = getLocalBitmapUri(imgCard);

        PackageManager pm = getPackageManager();
        List<ResolveInfo> activityList = pm.queryIntentActivities(shareIntent, 0);
        for (ResolveInfo app : activityList) {
            String packageName = app.activityInfo.packageName;
            Intent targetedShareIntent = new Intent(Intent.ACTION_SEND);
            targetedShareIntent.setType("image/*");
//            targetedShareIntent.setType("text/plain");
//            targetedShareIntent.putExtra(Intent.EXTRA_TEXT, "Meaning Card\n" + descCard);

//            targetedShareIntent.setType("*/*");
//            String[] mimetypes = {"image/*", "text/plain"};
//            targetedShareIntent.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes);
            if (TextUtils.equals(packageName, "com.facebook.katana")) {
                targetedShareIntent.putExtra(Intent.EXTRA_STREAM, uri1);
//                targetedShareIntent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=com.matkaline.phoenix");
            } else {
//                targetedShareIntent.putExtra(Intent.EXTRA_STREAM, uri2);
            }
            targetedShareIntent.setPackage(packageName);
            targetedShareIntents.add(targetedShareIntent);
        }

        try {
            Intent chooserIntent = Intent.createChooser(targetedShareIntents.remove(0), getResources().getString(R.string.title_share));
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toArray(new Parcelable[]{}));
            startActivityForResult(chooserIntent, REQUEST_CODE_MAIL);
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(ResultActivity.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
            ex.printStackTrace();
        }

    }

    private void share() {
        List<Intent> targetedShareIntents = new ArrayList<Intent>();

        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject test");
        shareIntent.putExtra(Intent.EXTRA_CC, "Extra Email");
        shareIntent.setType("image/*");
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        Uri uri1 = Uri.fromFile(imagePath);
        Uri uri2 = getLocalBitmapUri(imgCard);

        PackageManager pm = getPackageManager();
        List<ResolveInfo> activityList = pm.queryIntentActivities(shareIntent, 0);
        for (ResolveInfo app : activityList) {
            String packageName = app.activityInfo.packageName;
            Intent targetedShareIntent = new Intent(Intent.ACTION_SEND);
            targetedShareIntent.setType("image/*");
            targetedShareIntent.putExtra(Intent.EXTRA_TEXT, "Meaning Card\n" + descCard);
            if (TextUtils.equals(packageName, "com.facebook.katana")) {
                targetedShareIntent.putExtra(Intent.EXTRA_STREAM, uri1);
            } else {
                targetedShareIntent.putExtra(Intent.EXTRA_STREAM, uri2);
            }
            targetedShareIntent.setPackage(packageName);
            targetedShareIntents.add(targetedShareIntent);
        }

        try {
            Intent chooserIntent = Intent.createChooser(targetedShareIntents.remove(0), getResources().getString(R.string.title_share));
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toArray(new Parcelable[]{}));
            startActivityForResult(chooserIntent, REQUEST_CODE_MAIL);
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(ResultActivity.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
            ex.printStackTrace();
        }

    }

    public Uri getLocalBitmapUri(ImageView imageView) {
        Drawable drawable = imageView.getDrawable();
        Bitmap bmp = null;
        if (drawable instanceof BitmapDrawable) {
            bmp = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        } else {
            return null;
        }
        Uri bmpUri = null;
        try {
            File file = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "share_image.jpg");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_MAIL) {
            if (resultCode == RESULT_OK) {
                Toast.makeText(ResultActivity.this, "Sharing the success.", Toast.LENGTH_SHORT).show();
                isShareButtonEnable = true;
            } else if (resultCode == RESULT_CANCELED) {
                isShareButtonEnable = true;
            }
        }
    }

    private void share2(Uri uri) {
//        ShareLinkContent shareLinkContent = new ShareLinkContent.Builder()
//                .setContentTitle("Your Title")
//                .setContentDescription("Your Description")
//                .setContentUrl(Uri.parse("https://developers.facebook.com"))
//                .setImageUrl(uri)
//                .build();
//        ShareDialog.show(ResultActivity.this,shareLinkContent);

        ShareDialog shareDialog;
        FacebookSdk.sdkInitialize(getApplicationContext());
        shareDialog = new ShareDialog(this);
        if (ShareDialog.canShow(ShareLinkContent.class)) {
            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                    .setContentUrl(Uri.parse("https://developers.facebook.com"))
//                    .setImageUrl(Uri.parse("https://scontent.fhan2-3.fna.fbcdn.net/v/t1.15752-9/34119482_10156305144034360_6632033824102416384_n.jpg?_nc_cat=0&oh=318fb3e07769de4d89aceccba6f73ab7&oe=5BB54326"))
                    .build();

            shareDialog.show(linkContent);
        }

    }

    public void commentListHiden(View v) {
        vCommentList.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        app.currentCard.showBack();
        if (vCommentList != null && vCommentList.getVisibility() == View.VISIBLE) {
            vCommentList.setVisibility(View.GONE);
        }
//        else if (isShowingDesc){
//            webViewAnimation();
//        }
        else {
            super.onBackPressed();
            finish();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        setImage(app.currentCard);
    }

    @Override
    protected void onPause() {
        super.onPause();
//        Log.v("MyDebug","Result pause");
    }

    @Override
    public void onResume() {
        super.onResume();
//        Log.v("MyDebug","Result resume");
        if (!loadMusicStatus()) {
            iv_music.setAlpha(1.0f);
        } else {
            iv_music.setAlpha(0.3f);
        }
        boolean isPaid = prefs.getBoolean(IS_PAID_TODAY, false);
        boolean isPaidDefi = prefs.getBoolean(IS_PAID_TODAY_DEFI, false);
        boolean isPaidLumiereToday = prefs.getBoolean(IS_PAID_LUMIERE_TODAY, false);
        boolean isPaidLumiereRandom = prefs.getBoolean(IS_PAID_LUMIERE_RANDOM, false);
        if (app.cardMng.reconcile) { // defi screen
            if (isPaidDefi) {
                ll_cash_sensage.setVisibility(View.INVISIBLE);
            } else {
                ll_cash_sensage.setVisibility(View.VISIBLE);
            }
        } else { // normal screen
            if (isPaid) {
                ll_cash_sensage.setVisibility(View.INVISIBLE);
            } else {
                ll_cash_sensage.setVisibility(View.VISIBLE);
            }
        }

        if (app.cardMng.isButton) {//rút today
            if (isPaidLumiereToday) {
                ll_cash_lumiere.setVisibility(View.GONE);
            }
        } else {//rút random
            if (isPaidLumiereRandom) {
                ll_cash_lumiere.setVisibility(View.GONE);
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //bỏ paid lumiere random nếu thoát khỏi màn random card
        if (!app.cardMng.isButton) {
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean(IS_PAID_LUMIERE_RANDOM, false);
            editor.apply();
        }
    }

    public Bitmap takeScreenshot() {
        View rootView = findViewById(android.R.id.content).getRootView();
        rootView.setDrawingCacheEnabled(true);
        return rootView.getDrawingCache();
    }

    public void saveBitmap(Bitmap bitmap) {
        imagePath = new File(Environment.getExternalStorageDirectory() + "/screenshot.png");
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(imagePath);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
//            Log.e("MyDebug", e.getMessage(), e);
        } catch (IOException e) {
//            Log.e("MyDebug", e.getMessage(), e);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_music:
                if (!loadMusicStatus()) {
                    saveMusicStatus(true);
                    iv_music.setAlpha(0.3f);
                    utils.stopMusic();
//                    utils.muteMusic();
                } else {
                    saveMusicStatus(false);
                    iv_music.setAlpha(1.0f);
//                    utils.playMusic(R.raw.card_phoenix);
//                    utils.unmuteMusic();
                }
                break;

        }
    }

    private void getFacebookWords() {
        String wordFlamma = null;
        String wordDefi = null;
        String cardFlammeName = null;
        String cardFlammeTeam = null;
        String cardDefiTitle = null;
        String cardDefiName = null;
        String[] cardFlammeInfo = null;
        String[] cardDefiInfo = null;
        String[] arrFirstLines = null;
        String[] arrSecondLines = null;
        String[] defiWords = null;
        if (!app.cardMng.cardsResult[0].isReverse) {//quan bai nguoc
            switch (getCurrentLanguage().getLanguage()) {
                case "vi": {
                    cardFlammeInfo = Utils.GetLines(this.getAssets(), "facebook_vi/france/" + app.cardMng.cardsResult[0].number);
                    cardDefiInfo = Utils.GetLines(this.getAssets(), "facebook_vi/france/" + app.cardMng.cardsResult[2].number);
                    arrFirstLines = Utils.GetLines(this.getAssets(), "facebook_vi/flamma/" + app.cardMng.cardsResult[0].number);
                    arrSecondLines = Utils.GetLines(this.getAssets(), "sensage_vi/" + app.cardMng.cardsResult[0].number + ".txt");
                    break;
                }
                default: {
                    cardFlammeInfo = Utils.GetLines(this.getAssets(), "facebook/france/" + app.cardMng.cardsResult[0].number);
                    cardDefiInfo = Utils.GetLines(this.getAssets(), "facebook/france/" + app.cardMng.cardsResult[2].number);
                    arrFirstLines = Utils.GetLines(this.getAssets(), "facebook/flamma/" + app.cardMng.cardsResult[0].number);
                    arrSecondLines = Utils.GetLines(this.getAssets(), "sensage/" + app.cardMng.cardsResult[0].number);
                    break;
                }
            }
        } else {
            switch (getCurrentLanguage().getLanguage()) {
                case "vi": {
                    cardFlammeInfo = Utils.GetLines(this.getAssets(), "facebook_vi/france/minus_" + app.cardMng.cardsResult[0].number);
                    cardDefiInfo = Utils.GetLines(this.getAssets(), "facebook_vi/france/minus_" + app.cardMng.cardsResult[2].number);
                    arrFirstLines = Utils.GetLines(this.getAssets(), "facebook_vi/flamma/minus_" + app.cardMng.cardsResult[0].number);
                    arrSecondLines = Utils.GetLines(this.getAssets(), "sensage_vi/minus_" + app.cardMng.cardsResult[0].number + ".txt");
                    break;
                }
                default: {
                    cardFlammeInfo = Utils.GetLines(this.getAssets(), "facebook/france/minus_" + app.cardMng.cardsResult[0].number);
                    cardDefiInfo = Utils.GetLines(this.getAssets(), "facebook/france/minus_" + app.cardMng.cardsResult[2].number);
                    arrFirstLines = Utils.GetLines(this.getAssets(), "facebook/flamma/minus_" + app.cardMng.cardsResult[0].number);
                    arrSecondLines = Utils.GetLines(this.getAssets(), "sensage/minus_" + app.cardMng.cardsResult[0].number);
                    break;
                }
            }
        }

        if (!app.cardMng.cardsResult[2].isReverse) {
            switch (getCurrentLanguage().getLanguage()) {
                case "vi": {
                    defiWords = Utils.GetLines(this.getAssets(), "facebook_vi/defi/" + app.cardMng.cardsResult[2].number);
                    break;
                }
                default: {
                    defiWords = Utils.GetLines(this.getAssets(), "facebook/defi/" + app.cardMng.cardsResult[2].number);
                    break;
                }
            }
        } else {
            switch (getCurrentLanguage().getLanguage()) {
                case "vi": {
                    defiWords = Utils.GetLines(this.getAssets(), "facebook_vi/defi/minus_" + app.cardMng.cardsResult[2].number);
                    break;
                }
                default: {
                    defiWords = Utils.GetLines(this.getAssets(), "facebook/defi/minus_" + app.cardMng.cardsResult[2].number);
                    break;
                }
            }
        }


        if (cardFlammeInfo != null && cardFlammeInfo.length > 0) {
            cardFlammeName = cardFlammeInfo[0];
            cardFlammeTeam = cardFlammeInfo[1];
        }
        if (cardDefiInfo != null && cardDefiInfo.length > 0) {
            cardDefiTitle = cardDefiInfo[0];
            cardDefiName = cardDefiInfo[1];
        }
        if (defiWords != null && defiWords.length > 0) {
            wordDefi = getResources().getString(R.string.fb_defi_start) + " " + defiWords[rd.nextInt(defiWords.length)].trim() + "<br>";
        }


        String firstLine = getResources().getString(R.string.fb_flamma_start) + " " + arrFirstLines[rd.nextInt(arrFirstLines.length)].trim() + "<br>";
        String secondLine = arrSecondLines[1].substring(arrSecondLines[1].lastIndexOf("<p>") + 3, arrSecondLines[1].indexOf("."));
        wordFlamma = firstLine + secondLine + ".";

        utils.storeCardFacebookFlammeName(cardFlammeName);
        utils.storeCardFacebookFlammeTeam(cardFlammeTeam);
        utils.storeCardFacebookFlammeWord(wordFlamma);
        if (app.currentCard.isBad() && app.cardMng.datSelectCard != null) { // has defi, show defi view
            utils.storeCardFacebookDefiName(cardDefiTitle);
            utils.storeCardFacebookDefiWord(wordDefi);

            // force reconcile for init bitmap of defiCard
            app.cardMng.reconcile = true;
            try {
                app.cardMng.cardsResult[2].bm = BitmapFactory.decodeStream(this.getAssets().open(app.cardMng.cardsResult[2].number + ".jpg"));
            } catch (IOException e) {
                e.printStackTrace();
            }


        } else { // not has defi, disable defi view
            utils.storeCardFacebookDefiName(null);
            utils.storeCardFacebookDefiWord(null);
        }
    }
}