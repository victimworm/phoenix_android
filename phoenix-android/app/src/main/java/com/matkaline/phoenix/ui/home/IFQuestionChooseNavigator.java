package com.matkaline.phoenix.ui.home;

public interface IFQuestionChooseNavigator {

    void showQuestionPopup();

    void getQuestionFromUs();
}
