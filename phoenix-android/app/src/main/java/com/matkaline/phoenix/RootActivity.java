package com.matkaline.phoenix;

import static android.util.TypedValue.COMPLEX_UNIT_SP;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.akexorcist.localizationactivity.ui.LocalizationActivity;
import com.google.android.material.navigation.NavigationView;
import com.matkaline.phoenix.utils.PermissionUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

import dialogs.LoginChooseDialog;
import dialogs.MultiLanguageDialog;
import dialogs.QuestionChooseDialog;
import dialogs.QuestionContentDialog;
import models.models.cards.PlayCardType;
import ulitis.Constants;
import ulitis.ImageLoader;
import ulitis.L;
import ulitis.UiHelper;

public class RootActivity extends LocalizationActivity {

    public static final long START = 9 * 60 * 60000; //9H AM
    private NotificationManager mManager;
    public App app;
    protected QuestionChooseDialog qstChooseDialog;
    protected QuestionContentDialog qstContentDialog;
    protected MultiLanguageDialog multiLanguageDialog;
    private SharedPreferences prefs = null;
    private View.OnClickListener oclMenu = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int id = view.getId();
            switch (id) {
                case R.id.lyHome: {
                    home();
                    break;
                }
                case R.id.lyAccount: {
                    account();
                    break;
                }
                case R.id.lyPurchaseCoin: {
                    myCash();
                    break;
                }
                case R.id.lyLang: {
                    multiLanguage();
                    break;
                }
                case R.id.lyHowToPlay: {
                    go(HowToPlayActivity.class);
                    break;
                }
                case R.id.lyYourWeek: {
                    go(YourWeekActivity.class);
                    break;
                }
                case R.id.lyGinfo: {
                    gameInfo();
                    break;
                }
                case R.id.lyPolicy: {
                    policy();
                    break;
                }
                case R.id.lyContactUs: {
                    contactUs();
                    break;
                }
                case R.id.lyLogout: {
                    logout();
                    break;
                }
                default:
                    break;
            }
            setClose(null);
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UiHelper.INIT(this);
        app = App.getInstance();
        app.cardMng.setRootActivity(this);
        if (app.user == null) {
        } else {
            if (app.user.coin < 0) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(this);
                dialog.setTitle("LOGGING");
                dialog.setCancelable(false);
                dialog.setMessage("User\nid = " + app.user.user_id + "\ncoin = " + app.user.coin + " ");
                dialog.create().show();
            }
        }
        app.dlgLoginChoose = new LoginChooseDialog(this,this);
        qstChooseDialog = new QuestionChooseDialog(this);
        qstContentDialog = new QuestionContentDialog(this);
        multiLanguageDialog = new MultiLanguageDialog(this);
        prefs = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        app.user.ggHelper.init(this);
    }

/*    private void logcate() {
        File filename = new File(Environment.getExternalStorageDirectory() + "/phoenix_logcat.log");
        try {
            if (!filename.exists()) {
                filename.createNewFile();
            }
            String cmd = "logcat -d -f" + filename.getAbsolutePath();
            Runtime.getRuntime().exec(cmd);
        } catch (IOException e) {
            e.printStackTrace();
            finish();
        }
    }*/

    @Override
    protected void onStart() {
        super.onStart();
        fixUiTitle();
        app.httpManager.setContext(this);
        app.user.setContext(this);
    }

    private void fixUiTitle() {
        TextView txtTitle = (TextView) findViewById(R.id.txtTitle);
        ImageView ibtBack = (ImageView) findViewById(R.id.btnBack);
        ImageView ibtedit = (ImageView) findViewById(R.id.icon_edit);
        ImageView ibtsave = (ImageView) findViewById(R.id.icon_save);
        RelativeLayout lyTitle = (RelativeLayout) findViewById(R.id.lyTitle);
        LinearLayout lyBack = (LinearLayout) findViewById(R.id.inBackButton);
        LinearLayout lyEdit = (LinearLayout) findViewById(R.id.line_edit);
        if (txtTitle != null) {
            if (lyTitle != null)
                UiHelper.RESIZE(lyTitle, -1, 135);
            UiHelper.TEXTSIZE(txtTitle, 60);
            UiHelper.RESIZE(ibtBack, 43, 73);
            UiHelper.RESIZE(ibtedit, 73, 73);
            UiHelper.RESIZE(ibtsave, 73, 73);
            UiHelper.RESIZE(lyBack, 165, 135);
            UiHelper.RESIZE(lyEdit, 165, 135);
        }
    }

    private void slidingUpdateLanguage() {
        TextView txtHome = (TextView) layout_hear1.findViewById(R.id.txtHome);
        //txtHome.setText(R.string.home);

    }

    private void fixUiSliding() {
        UiHelper.RESIZE(app.navigationView, 1037, 2208);
        UiHelper.RESIZE(layout_hear, 1037, 798);
        UiHelper.RESIZE(close, 90, 90);
        UiHelper.MARGIN(close, 35, 40, 0, 0);
        UiHelper.RESIZE(avatarUser, 329, 329);
        UiHelper.TEXTSIZE(nameUser, 75);
        UiHelper.TEXTSIZE(coinUser, 60);

        LinearLayout lyHome = (LinearLayout) layout_hear1.findViewById(R.id.lyHome);
        ImageView imgHome = (ImageView) layout_hear1.findViewById(R.id.imgHome);
        TextView txtHome = (TextView) layout_hear1.findViewById(R.id.txtHome);
        lyHome.setOnClickListener(oclMenu);
        UiHelper.RESIZE(lyHome, -1, 140);
        UiHelper.PADDING(lyHome, 80, 0, 0, 0);
        UiHelper.RESIZE(imgHome, 75, 75);
        UiHelper.TEXTSIZE(txtHome, 60);
        UiHelper.MARGIN(txtHome, 75, 0, 0, 0);

        lyHome = (LinearLayout) layout_hear1.findViewById(R.id.lyAccount);
        imgHome = (ImageView) layout_hear1.findViewById(R.id.imgAccount);
        txtHome = (TextView) layout_hear1.findViewById(R.id.txtAccount);
        lyHome.setOnClickListener(oclMenu);
        UiHelper.RESIZE(lyHome, -1, 140);
        UiHelper.PADDING(lyHome, 80, 0, 0, 0);
        UiHelper.RESIZE(imgHome, 75, 75);
        UiHelper.TEXTSIZE(txtHome, 60);
        UiHelper.MARGIN(txtHome, 75, 0, 0, 0);

        lyHome = (LinearLayout) layout_hear1.findViewById(R.id.lyPurchaseCoin);
        imgHome = (ImageView) layout_hear1.findViewById(R.id.imgPurchaseCoin);
        txtHome = (TextView) layout_hear1.findViewById(R.id.txtPurchaseCoin);
        lyHome.setOnClickListener(oclMenu);
        UiHelper.RESIZE(lyHome, -1, 140);
        UiHelper.PADDING(lyHome, 80, 0, 0, 0);
        UiHelper.RESIZE(imgHome, 75, 75);
        UiHelper.TEXTSIZE(txtHome, 60);
        UiHelper.MARGIN(txtHome, 75, 0, 0, 0);

        lyHome = (LinearLayout) layout_hear1.findViewById(R.id.lyLang);
        imgHome = (ImageView) layout_hear1.findViewById(R.id.imgLang);
        txtHome = (TextView) layout_hear1.findViewById(R.id.txtLang);
        lyHome.setOnClickListener(oclMenu);
        UiHelper.RESIZE(lyHome, -1, 140);
        UiHelper.PADDING(lyHome, 80, 0, 0, 0);
        UiHelper.RESIZE(imgHome, 75, 75);
        UiHelper.TEXTSIZE(txtHome, 60);
        UiHelper.MARGIN(txtHome, 75, 0, 0, 0);

        lyHome = (LinearLayout) layout_hear1.findViewById(R.id.lyHowToPlay);
        imgHome = (ImageView) layout_hear1.findViewById(R.id.imgHowToPlay);
        txtHome = (TextView) layout_hear1.findViewById(R.id.txtHowToPlay);
        lyHome.setOnClickListener(oclMenu);
        UiHelper.RESIZE(lyHome, -1, 140);
        UiHelper.PADDING(lyHome, 80, 0, 0, 0);
        UiHelper.RESIZE(imgHome, 75, 75);
        UiHelper.TEXTSIZE(txtHome, 60);
        UiHelper.MARGIN(txtHome, 75, 0, 0, 0);

        lyHome = (LinearLayout) layout_hear1.findViewById(R.id.lyYourWeek);
        imgHome = (ImageView) layout_hear1.findViewById(R.id.imgYourWeek);
        txtHome = (TextView) layout_hear1.findViewById(R.id.txtYourWeek);
        lyHome.setOnClickListener(oclMenu);
        UiHelper.RESIZE(lyHome, -1, 140);
        UiHelper.PADDING(lyHome, 80, 0, 0, 0);
        UiHelper.RESIZE(imgHome, 75, 75);
        UiHelper.TEXTSIZE(txtHome, 60);
        UiHelper.MARGIN(txtHome, 75, 0, 0, 0);
        lyHome.setVisibility(View.GONE);

        lyHome = (LinearLayout) layout_hear1.findViewById(R.id.lyGinfo);
        imgHome = (ImageView) layout_hear1.findViewById(R.id.imgGinfo);
        txtHome = (TextView) layout_hear1.findViewById(R.id.txtGinfo);
        lyHome.setOnClickListener(oclMenu);
        UiHelper.RESIZE(lyHome, -1, 140);
        UiHelper.PADDING(lyHome, 80, 0, 0, 0);
        UiHelper.RESIZE(imgHome, 75, 75);
        UiHelper.TEXTSIZE(txtHome, 60);
        UiHelper.MARGIN(txtHome, 75, 0, 0, 0);

        lyHome = (LinearLayout) layout_hear1.findViewById(R.id.lyPolicy);
        imgHome = (ImageView) layout_hear1.findViewById(R.id.imgPolicy);
        txtHome = (TextView) layout_hear1.findViewById(R.id.txtPolicy);
        lyHome.setOnClickListener(oclMenu);
        UiHelper.RESIZE(lyHome, -1, 140);
        UiHelper.PADDING(lyHome, 80, 0, 0, 0);
        UiHelper.RESIZE(imgHome, 75, 75);
        UiHelper.TEXTSIZE(txtHome, 60);
        UiHelper.MARGIN(txtHome, 75, 0, 0, 0);

        lyHome = layout_hear1.findViewById(R.id.lyContactUs);
        imgHome = layout_hear1.findViewById(R.id.imgContactUs);
        txtHome = layout_hear1.findViewById(R.id.txtContactUs);
        lyHome.setOnClickListener(oclMenu);
        UiHelper.RESIZE(lyHome, -1, 140);
        UiHelper.PADDING(lyHome, 80, 0, 0, 0);
        UiHelper.RESIZE(imgHome, 75, 75);
        UiHelper.TEXTSIZE(txtHome, 60);
        UiHelper.MARGIN(txtHome, 75, 0, 0, 0);

        lyHome = (LinearLayout) layout_hear1.findViewById(R.id.lyLogout);
        imgHome = (ImageView) layout_hear1.findViewById(R.id.imgLogout);
        txtHome = (TextView) layout_hear1.findViewById(R.id.txtLogout);
        lyHome.setOnClickListener(oclMenu);
        UiHelper.RESIZE(lyHome, -1, 140);
        UiHelper.PADDING(lyHome, 80, 0, 0, 0);
        UiHelper.RESIZE(imgHome, 75, 75);
        UiHelper.TEXTSIZE(txtHome, 60);
        UiHelper.MARGIN(txtHome, 75, 0, 0, 0);

    }

    public void goLumiere(Class<?> cls, PlayCardType type) {
        Intent intent = new Intent(this, cls);
        intent.putExtra("SelectPlayCardType", type);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }

    public void go(Class<?> cls) {
        Intent intent = new Intent(this, cls);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }

    public void left(View v) {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    public void alert(String title, String msg) {
        dialogBuilder(title, msg, null, null);
    }

    public void alert(String title, String msg, DialogInterface.OnClickListener onPositiveBtnClick) {
        dialogBuilder(title, msg, onPositiveBtnClick, null);
    }

    protected void dialogBuilder(String title, String msg,
                                 DialogInterface.OnClickListener onPositiveBtnClick,
                                 DialogInterface.OnClickListener onNegativeBtnClick) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setCancelable(true);

        if (onNegativeBtnClick == null && onPositiveBtnClick == null) {
            alertDialogBuilder.setPositiveButton("OK", null);
        } else if (onNegativeBtnClick == null && onPositiveBtnClick != null) {
            alertDialogBuilder.setPositiveButton("OK", onPositiveBtnClick);
        } else {
            alertDialogBuilder.setNegativeButton(getResources().getString(R.string.negative), onNegativeBtnClick);
            alertDialogBuilder.setPositiveButton(getResources().getString(R.string.positive), onPositiveBtnClick);
        }

        //Add dialog's content
        View content = LayoutInflater.from(this).inflate(R.layout.dialog_content, null);
        alertDialogBuilder.setView(content);

        ((TextView) content.findViewById(R.id.title)).setText(title);
        ((TextView) content.findViewById(R.id.message)).setText(msg);


        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

        //Change button style
        TextView positiveBtn = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
        TextView negativeBtn = alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE);

        positiveBtn.setTextColor(Color.parseColor("#007aff"));
        negativeBtn.setTextColor(Color.parseColor("#007aff"));

        positiveBtn.setTextSize(COMPLEX_UNIT_SP, 17);
        negativeBtn.setTextSize(COMPLEX_UNIT_SP, 17);

        positiveBtn.setTypeface(null, Typeface.BOLD);
    }

    /**
     * Init Sliding Menu
     */
    public void setClose(View v) {
        ((DrawerLayout) findViewById(R.id.drawer_Layout)).closeDrawer(GravityCompat.START);
    }

    protected void setTitleScreen(String title) {
        TextView txtTitle = (TextView) findViewById(R.id.txtTitle);
        txtTitle.setText(title);
    }

    /**
     * goi khi nhan nut menu phia tren, ben trai moi man hinh
     *
     * @param v
     */
    public void showSlidingMenu(View v) {
        DrawerLayout lyDrawer = ((DrawerLayout) findViewById(R.id.drawer_Layout));
        lyDrawer.openDrawer(GravityCompat.START);
    }

    private ImageView avatarUser;
    private ImageView close;
    private TextView nameUser;
    private TextView coinUser;
    private LinearLayout line;
    private LinearLayout layout_hear;
    private LinearLayout layout_hear1;
    private TextView txtLogout;


    void initSlidingMenu() {
        if (app.imageLoader == null)
            app.imageLoader = new ImageLoader(this);
        app.navigationView = (NavigationView) findViewById(R.id.design_navigation_view_home);
        if (app.navigationView == null) {
            app.navigationView = (NavigationView) findViewById(R.id.design_navigation_view_cards);
        }
        if (app.navigationView == null) {
            app.navigationView = (NavigationView) findViewById(R.id.design_navigation_view_result);
        }
        if (app.navigationView == null) {
            app.navigationView = (NavigationView) findViewById(R.id.design_navigation_view_ginf);
        }

        if (app.navigationView != null) {
//            app.navigationView.setNavigationItemSelectedListener(this);
            View header = app.navigationView.getHeaderView(0);
            close = (ImageView) header.findViewById(R.id.buttonClose);
            avatarUser = (ImageView) header.findViewById(R.id.imageUser);
            nameUser = (TextView) header.findViewById(R.id.textViewName);
            coinUser = (TextView) header.findViewById(R.id.textViewCoin);
            line = (LinearLayout) header.findViewById(R.id.line_bar_like);
            layout_hear = (LinearLayout) header.findViewById(R.id.layout_hear);
            layout_hear1 = (LinearLayout) header.findViewById(R.id.layout_hear1);
            txtLogout = (TextView) layout_hear1.findViewById(R.id.txtLogout);
            fixUiSliding();

            updateUISlidingMenu();
        }
        handleMenuEvent();
    }

    public void updateUISlidingMenu() {
        if (app.user.isLogin()) {
            app.user.insertAvatar(avatarUser);
            nameUser.setText(app.user.user_name);
            coinUser.setText(app.user.coin + " " + getResources().getString(R.string.unit_coins));
            line.setVisibility(View.VISIBLE);
            txtLogout.setText(getResources().getString(R.string.logout));
        } else {
            app.imageLoader.DisplayImage(null, avatarUser);
            nameUser.setText(null);
            line.setVisibility(View.INVISIBLE);
            txtLogout.setText(getResources().getString(R.string.login));
        }
        slidingUpdateLanguage();
    }


    @Override
    public boolean onNavigateUp() {
        return super.onNavigateUp();
    }


    private boolean updated = false;
    private float slideOffsetPrev;

    /**
     * handle open|close event of sliding menu
     */
    private void handleMenuEvent() {
        final DrawerLayout drlMenu = ((DrawerLayout) findViewById(R.id.drawer_Layout));
        drlMenu.closeDrawer(GravityCompat.START);
        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this, drlMenu, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                if (slideOffsetPrev != slideOffset) {
                    if (!updated) {
                        updated = true;
                        updateUISlidingMenu();
                    }
                    if (slideOffset < slideOffsetPrev && slideOffset == 0) {
                        updated = false;
                    }
                    slideOffsetPrev = slideOffset;
                }

            }
        };
        drlMenu.setDrawerListener(mDrawerToggle);
    }

    /**
     * Check NetWork
     */
    public boolean isNetworkStatusAvialable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            NetworkInfo netInfos = connectivityManager.getActiveNetworkInfo();
            if (netInfos != null)
                if (netInfos.isConnectedOrConnecting())
                    return true;
        }
        return false;
    }

    private void home() {
        go(HomeActivity.class);
    }

    private void account() {
        if (app.user.isLogin()) {
            go(AccountActivity.class);
        } else if (!isNetworkStatusAvialable()) {
            alert(getString(R.string.alert), getString(R.string.alert_offline), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
        } else {
            app.dlgLoginChoose = new LoginChooseDialog(this,this);
            app.dlgLoginChoose.txtTitle.setText(getResources().getString(R.string.title_login_btn));
            app.dlgLoginChoose.show();
        }

    }

    private void myCash() {
        if (isNetworkStatusAvialable()) {
            if (app.user.isLogin()) {
                go(MyCashActivity.class);
            } else {
                app.dlgLoginChoose = new LoginChooseDialog(this,this);
                app.dlgLoginChoose.txtTitle.setText(getResources().getString(R.string.title_login_btn));
                app.dlgLoginChoose.show();
            }
        } else {
            alert(getString(R.string.alert), getString(R.string.alert_offline), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
        }
    }

    /**
     * Laguage
     */
    private void multiLanguage() {
        multiLanguageDialog.show();
    }

    private void gameInfo() {
        go(ListCardActivity.class);
    }

    /**
     * Policy
     */
    private void policy() {
        go(PolicyActivity.class);
    }

    /*Contact Us */
    private void contactUs() {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL, new String[]{"giangluongha@gmail.com"});
        i.putExtra(Intent.EXTRA_SUBJECT, "Feedback Jeu du Phenix");
//        i.putExtra(Intent.EXTRA_TEXT, "body of email");
        try {
            startActivity(Intent.createChooser(i, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            String error = getResources().getString(R.string.manu_error_email_connect);
            Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Logout
     */
    private void logout() {
        if (isNetworkStatusAvialable()) {
            if (app.user.isLogin()) {
                dialogBuilder(getResources().getString(R.string.alert), getResources().getString(R.string.title_logout),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                setResult(Activity.RESULT_CANCELED);
                                app.user.logout();
                            }
                        }
                        , new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
            } else {
                app.dlgLoginChoose = new LoginChooseDialog(this,this);
                app.dlgLoginChoose.show();
            }
        } else {
            alert(getString(R.string.alert), getString(R.string.alert_offline), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        app.user.callbackManager.onActivityResult(requestCode, resultCode, data);
        app.user.loginGgResult(requestCode, resultCode, data);
        if (app.user.isLogin()) {
            app.dlgLoginChoose.dismiss();
        }
    }

    /**
     * load text mean of card from assets
     *
     * @param inFile
     * @return
     */
    public String LoadData(String inFile) {
        String tContents = "";
        InputStream stream;
        AssetManager assetManager = getAssets();
        try {
            switch (getCurrentLanguage().getLanguage()) {
                case "vi":
                    stream = assetManager.open("mean_card_vn/" + inFile);
                    break;
                case "en":
                    stream = assetManager.open("mean_card_en/" + inFile);
                    break;
                default:
                    stream = assetManager.open("mean_card/" + inFile);
                    break;
            }
            int size = stream.available();
            byte[] buffer = new byte[size];
            stream.read(buffer);
            stream.close();
            tContents = new String(buffer);
        } catch (IOException e) {
            // Handle exceptions here
            e.printStackTrace();
        }
        return tContents;
    }


    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;

    /**
     * Checks if the app has permission to write to device storage
     * If the app does not has permission then the user will be prompted to grant permissions
     */
    public static void verifyStoragePermissions(Activity activity) {
        String[] permissions;
        if (android.os.Build.VERSION.SDK_INT >= 29) {
            permissions = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE};
        } else {
            permissions = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
        }

        if (!PermissionUtils.hasPermissions(activity, permissions)) {
            PermissionUtils.hasPermissions(activity, permissions, REQUEST_EXTERNAL_STORAGE);
        }
    }

    /**
     * Resove Card
     *
     * @param angle
     * @param bitmap
     * @return
     */
    public Bitmap rotated(int angle, Bitmap bitmap) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        Bitmap bmRorated = null;
        bmRorated = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        return bmRorated;
    }

    /**
     * hiden keyboard
     */
    public void hidenKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            if (view instanceof EditText) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void saveMusicStatus(boolean musicStatus) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(Constants.MUSIC_STATUS, musicStatus);
        editor.commit();
    }

    public boolean loadMusicStatus() {
        return prefs.getBoolean(Constants.MUSIC_STATUS, false);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    protected void webViewSetting(WebView web) {
        web.setBackgroundColor(0x00000000);
        if (Build.VERSION.SDK_INT >= 11) web.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
        web.setInitialScale(100);
        web.getSettings().setDefaultFontSize(UiHelper.GETSIZE(60));
    }

    protected void webViewSetData(WebView web, String myData, int padding) {
        String pish = "<html><head>" +
                "<meta name=\"viewport\" content=\"target-densitydpi=device-dpi, width=device-width, user-scalable=no\"/>" +
                "<style type=\"text/css\">body {text-align: left; color: #FFFFFF; padding-top:+" + (10 * padding) + "px+;padding-left: " + padding + "px;padding-right:" + padding + "px}</style></head><body>";
        String pas = "</body></html>";

        String myHtmlString = pish + myData + pas;
        web.loadDataWithBaseURL("file:///android_asset/", myHtmlString, "text/html", "UTF-8", null);
    }

    protected void webViewSetData(WebView web, String img, String myData, int padding, boolean flip) {
        String pish = "<html><head>" +
                "<meta name=\"viewport\" content=\"target-densitydpi=device-dpi, width=device-width, user-scalable=no\"/>";
        pish += "<link rel=\"stylesheet\" type=\"text/css\" href=\"css/all.css\">";
        if (flip) {
            pish += "<link rel=\"stylesheet\" type=\"text/css\" href=\"css/flip.css\">";
        }
        pish += "<style type=\"text/css\">body {text-align: left;padding-left: " + padding + "px;padding-right:" + padding + "px}</style></head><body>";
        String pas = "</body></html>";
        String myHtmlString = pish + img + myData + pas;
        web.loadDataWithBaseURL("file:///android_asset/", myHtmlString, "text/html", "UTF-8", null);
    }


    protected void showProgress() {
        findViewById(R.id.lyProgress).setVisibility(View.VISIBLE);
    }

    protected void hideProgress() {
        findViewById(R.id.lyProgress).setVisibility(View.GONE);
    }

    public void progress(View v) {
        L.V("progressing ...");
    }


    String appPackageName = "";

    public void checkApp() {
        ActivityManager am = (ActivityManager) this.getSystemService(ACTIVITY_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            final List<ActivityManager.RunningAppProcessInfo> processInfos = am.getRunningAppProcesses();
            ActivityManager.RunningAppProcessInfo processInfo = processInfos.get(0);
            if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                appPackageName = (Arrays.asList(processInfo.pkgList).get(0));
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            appPackageName = componentInfo.getPackageName();
        }
    }

    public boolean isCheckApp() {
        if (appPackageName.equalsIgnoreCase("com.matkaline.phoenix")) {
            return true;
        } else {
            return false;
        }
    }
}

