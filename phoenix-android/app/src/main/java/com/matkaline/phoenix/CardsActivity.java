package com.matkaline.phoenix;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.Random;
import java.util.TimeZone;

import http.HttpManager;
import listener.CardViewListener;
import models.models.cards.Card;
import ulitis.UiHelper;
import ulitis.Utils;
import views.CardsView;

public class CardsActivity extends RootActivity implements CardViewListener, View.OnClickListener {

    private CardsView vCards;
    private ImageView imageBack, iv_music;
    private LinearLayout lyCards;
    private LinearLayout line1;
    private TextView tv_my_cash;
    private Utils utils;
    private SharedPreferences prefs = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(0, 0);
        setContentView(R.layout.custom_activity_card);
//        initSlidingMenu();
        resize();

        ImageView iv_cash = (ImageView) findViewById(R.id.iv_cash);

        tv_my_cash = (TextView) findViewById(R.id.tv_my_cash);
        if (app.user.coin > 0) {
            tv_my_cash.setText(String.valueOf(app.user.coin));
        } else {
            tv_my_cash.setText(String.valueOf(0));
        }
        UiHelper.RESIZE(iv_cash, 60, 60);
        UiHelper.TEXTSIZE(tv_my_cash, 45);
        UiHelper.MARGIN(tv_my_cash, 10, 0, 100, 0);

        prefs = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        utils = new Utils(this);
        iv_music.setOnClickListener(this);
        if (!app.cardMng.reconcile) {
            if (app.cardMng.cardsResult[1].number == 0) {
//                cardsResult[0].isReverse = true;
                for (int i = 1; i <= 12; i++) {
                    if (app.cardMng.cardsResult[0].number == i) {
                        app.cardMng.cardsResult[0].type = Card.CHARME;
                        break;
                    }
                }
                for (int i = 14; i <= 25; i++) {
                    if (app.cardMng.cardsResult[0].number == i) {
                        app.cardMng.cardsResult[0].type = Card.CHOC;
                        break;
                    }
                }
            } else if (app.cardMng.cardsResult[1].number == 13) {
                if (app.cardMng.cardsResult[0].isBad()) {
//                    cardsResult[0].isReverse = true;
                    for (int i = 1; i <= 12; i++) {
                        if (app.cardMng.cardsResult[0].number == i) {
                            app.cardMng.cardsResult[0].type = Card.CHARME;
                            break;
                        }
                    }
                    for (int i = 14; i <= 25; i++) {
                        if (app.cardMng.cardsResult[0].number == i) {
                            app.cardMng.cardsResult[0].type = Card.CHOC;
                            break;
                        }
                    }
                } else {
//                    return cardsResult[0];
                }
            } else if (app.cardMng.cardsResult[0].color == app.cardMng.cardsResult[1].color) {
                if ((app.cardMng.cardsResult[0].number + app.cardMng.cardsResult[1].number) % 2 == 0) {//cung chan || cung le
//                    cardsResult[0].isReverse = true;
                    for (int i = 1; i <= 12; i++) {
                        if (app.cardMng.cardsResult[0].number == i) {
                            app.cardMng.cardsResult[0].type = Card.CHARME;
                            break;
                        }
                    }
                    for (int i = 14; i <= 25; i++) {
                        if (app.cardMng.cardsResult[0].number == i) {
                            app.cardMng.cardsResult[0].type = Card.CHOC;
                            break;
                        }
                    }
                }
            }
        } else {
//            return cardsResult[2];
        }

        if (ResultActivity.isDefi) {
            app.cardMng.cardsResult[0].type = Card.CHARME;
            ResultActivity.isDefi = false;
        }

        lyCards = findViewById(R.id.lyCards);

        if (!loadMusicStatus()) {
            Card[] cards = app.cardMng.cardsResult;
            if (cards[0].type == Card.CHOC) {
                utils.playMusic(R.raw.card_choc);
            } else if (cards[0].type == Card.CHARME) {
                utils.playMusic(R.raw.card_charm);
            } else if (cards[0].type == Card.MATKA) {
                utils.playMusic(R.raw.card_matka);
            } else if (cards[0].type == Card.CHOCCHARME) {
                utils.playMusic(R.raw.card_phoenix);
            }
        }
        findViewById(R.id.btHider).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (vCards.done) {
                    vCards.setVisibility(View.INVISIBLE);
                    vCards = null;
                    lyCards.removeAllViews();
                    go(ResultActivity.class);
                    finish();
                }
            }
        });

        getCharmeBonusCoin();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    protected void onStop() {
        super.onStop();
//        utils.stopMusic();
    }

    //SonLA_thêm bonus coin nếu bốc được lá bài charme - S
    private void getCharmeBonusCoin() {
        //check nếu lá today rút được charme thì bonus coin random 4=>10
        //13 cho Phénix, 26 cho matka
        if (!app.user.isLogin() || !app.cardMng.isButton) {
            return;
        }

        if (isNewDay()) {
            if (app.cardMng.cardsResult[0].type == Card.CHARME || app.cardMng.cardsResult[0].type == Card.CHOCCHARME || app.cardMng.cardsResult[0].type == Card.MATKA) {
                Random r = new Random();
                int bonusCoin = r.nextInt(11) + 4;
                if (app.cardMng.cardsResult[0].number == 0) {
                    bonusCoin = 26;
                } else if (app.cardMng.cardsResult[0].number == 13) {
                    bonusCoin = 13;
                }
                final int resultCoint = bonusCoin;

                app.user.submitCoin(bonusCoin, new HttpManager.OnResponse() {
                    @Override
                    public void onResponse(JSONObject jso) {
                        app.user.updateCoinCount(jso);
                        showAlertBonusCoin(resultCoint);
                    }
                });
            }
        }
    }

    private void showAlertBonusCoin(int coin) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        String mess = getResources().getString(R.string.bonus_coin_text).replace("{bonus_coin}", Integer.toString(coin));

        builder.setTitle(R.string.bonus_popup_title);
        builder.setMessage(mess);
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.bonus_popup_close, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private boolean isNewDay() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getDefault());
        int dayOfWeek = calendar.get(Calendar.DAY_OF_MONTH);
        int storedToday = prefs.getInt("current_today_get_bonus", -1);
        if (dayOfWeek != storedToday) {// today is new! reset value!
            SharedPreferences.Editor editor = prefs.edit();
            editor.putInt("current_today_get_bonus", calendar.get(Calendar.DAY_OF_MONTH));
            editor.apply();
            return  true;
        } else {
            return false;
        }
    }
    //SonLA_thêm bonus coin nếu bốc được lá bài charme - E

    private void resize() {
        line1 = (LinearLayout) findViewById(R.id.linear_mean);
        imageBack = (ImageView) findViewById(R.id.icon_back);
        iv_music = (ImageView) findViewById(R.id.iv_music);

        UiHelper.RESIZE(iv_music, 60, 60);
        UiHelper.RESIZE(line1, 165, 135);
        UiHelper.RESIZE(imageBack, 43, 73);
        UiHelper.MARGIN(imageBack, 0, 35, 0, 45);
//        ImageButton menu = (ImageButton) findViewById(R.id.buttonMenu);
//        UiHelper.RESIZE(menu,106,88);
//        UiHelper.MARGIN(menu,36,50,0,0);
    }

    @Override
    protected void onStart() {
        super.onStart();
        lyCards.addView(vCards = new CardsView(this));
        vCards.setVisibility(View.VISIBLE);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!loadMusicStatus()) {
            iv_music.setAlpha(1.0f);
        } else {
            iv_music.setAlpha(0.3f);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    public void onFinishFlip() {
        if (!app.cardMng.reconcile) {
            if (app.cardMng.cardsResult[1].number == 0) {
                vCards.animate().rotation(180).setDuration(1500).start();
            } else if (app.cardMng.cardsResult[1].number == 13) {
                if (app.cardMng.cardsResult[0].isBad()) {
                    vCards.animate().rotation(180).setDuration(1500).start();
                }
            } else if (app.cardMng.cardsResult[0].color == app.cardMng.cardsResult[1].color) {
                if ((app.cardMng.cardsResult[0].number + app.cardMng.cardsResult[1].number) % 2 == 0) {//cung chan || cung le
                    vCards.animate().rotation(180).setDuration(1500).start();
                }
            }
        }
//        Card currentCard = app.cardMng.getResultCard();
//        if(currentCard.isReverse) {
//            vCards.animate().rotation(180).setDuration(1500).start();
//        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_music:
                if (!loadMusicStatus()) {
                    saveMusicStatus(true);
                    iv_music.setAlpha(0.3f);
                    utils.stopMusic();
//                    utils.muteMusic();
                } else {
                    saveMusicStatus(false);
                    iv_music.setAlpha(1.0f);
//                    utils.playMusic(R.raw.card_phoenix);
//                    utils.unmuteMusic();
                }
                break;

        }
    }
}
