package com.matkaline.phoenix.ui.base;

import android.app.Dialog;
import android.content.Context;
import androidx.annotation.LayoutRes;
import android.util.DisplayMetrics;
import android.view.Window;
import android.view.WindowManager;

public abstract class BaseDialog {

    public abstract Context getContext();

    public abstract WindowManager getWindowManager();

    public abstract @LayoutRes
    int getLayout();

    private float widthPercent = 0.7f;

    public void setWidthPercent(float percent) {
        this.widthPercent = percent;
    }


    public abstract void setUpView(Dialog dialog);

    private Dialog dialog;

    public void show() {
        Context context = getContext();
        if (context == null) {
            return;
        }

        dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(getLayout());

        setUpView(dialog);

        dialog.show();
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(dialog.getWindow().getAttributes());

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int deviceWidth = displayMetrics.widthPixels;
        int width = (int) (deviceWidth * widthPercent);
        layoutParams.width = width;
        dialog.getWindow().setAttributes(layoutParams);
    }
}
