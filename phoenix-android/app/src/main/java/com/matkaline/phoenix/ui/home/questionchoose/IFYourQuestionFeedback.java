package com.matkaline.phoenix.ui.home.questionchoose;

public interface IFYourQuestionFeedback {

    void showResult(String question);
}
