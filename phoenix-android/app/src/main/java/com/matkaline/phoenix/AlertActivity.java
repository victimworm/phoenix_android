package com.matkaline.phoenix;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

public class AlertActivity extends AppCompatActivity {

    private EditText txtMsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alert);
        txtMsg = (EditText) findViewById(R.id.txtMsg);
        String msg = getIntent().getExtras().getString("msg");
        txtMsg.setText("ERROR = "+msg);
    }
}
