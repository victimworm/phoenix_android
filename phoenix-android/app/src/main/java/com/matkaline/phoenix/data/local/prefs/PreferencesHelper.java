package com.matkaline.phoenix.data.local.prefs;

public interface PreferencesHelper {

    void setYourQuestion(String question);

    String getYourQuestion();

    void removeYourQuestion();

    void removeTodayStored();

    //Xóa id câu hỏi
    void removeNotificationQuestionData();

    //Lưu type câu hỏi khi push notification
    void saveTypeCardOnNotification(int type);//1 là charm, 0 là choc

    //Lấy type câu hỏi notification
    int getCardTypeOnNotification();
}
