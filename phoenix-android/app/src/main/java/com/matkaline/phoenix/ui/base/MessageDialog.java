package com.matkaline.phoenix.ui.base;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.matkaline.phoenix.R;

public class MessageDialog {

    private String msg = "";

    public MessageDialog setMessage(String message) {
        this.msg = message;
        return this;
    }

    private int deviceWidth = 0;
    private float widthPerDevice = 0.8f;
    private Context context;
    private int width = 0;

    public static MessageDialog doCreate(Context mContext, WindowManager manager) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        manager.getDefaultDisplay().getMetrics(displayMetrics);
        MessageDialog dialog = new MessageDialog();

        dialog.deviceWidth = displayMetrics.widthPixels;
        dialog.context = mContext;

        return dialog;
    }

    public MessageDialog setWidthPercent(float percent) {
        this.widthPerDevice = percent;
        return this;
    }

    public void show() {
        if (this.context == null) {
            return;
        }
        final Dialog dialogMessage = new Dialog(context);
        dialogMessage.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialogMessage.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialogMessage.setContentView(R.layout.layout_message_dialog);
        if (!msg.equals("")) {
//            TextView txtMessageContent = dialogMessage.findViewById(R.id.layout_dialog_message_txt_content);
//            txtMessageContent.setText(msg);
//
//            GhtkTextView btnClose = dialogMessage.findViewById(R.id.layout_dialog_message_btn_close);
//            btnClose.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    dialogMessage.dismiss();
//                }
//            });
//
//            dialogMessage.setOnDismissListener(new DialogInterface.OnDismissListener() {
//                @Override
//                public void onDismiss(DialogInterface dialogInterface) {
//                    if (dialogCallback != null) {
//                        dialogCallback.onDataResult("");
//                    }
//                }
//            });

            if (dialogMessage != null) {
                dialogMessage.show();
                WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
                layoutParams.copyFrom(dialogMessage.getWindow().getAttributes());
                width = (int) (deviceWidth * widthPerDevice);
                layoutParams.width = width;
                dialogMessage.getWindow().setAttributes(layoutParams);
            }
        }
    }
}
