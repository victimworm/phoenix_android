package com.matkaline.phoenix.utils;

import android.content.res.Resources;

import com.akexorcist.localizationactivity.core.LanguageSetting;
import com.matkaline.phoenix.App;

import java.net.URL;

public final class DeviceUtils {

    private DeviceUtils() {

    }

    public static String getSystemLanguage() {
        String language = Resources.getSystem().getConfiguration().locale.getLanguage();
        return language;
    }

    public static String getAppLanguage() {
        //SonLA_Sửa lỗi lấy ngôn ngữ khi set trong app, không phải lấy ngôn ngữ set ở máy - S
        return LanguageSetting.getLanguage(App.getInstance()).getLanguage();
//        String language = Locale.getDefault().getLanguage();
//        return language;
        //SonLA_Sửa lỗi lấy ngôn ngữ khi set trong app, không phải lấy ngôn ngữ set ở máy -
    }

    public static boolean isValidUrl(String url)
    {
        /* Try creating a valid URL */
        try {
            new URL(url).toURI();
            return true;
        }

        // If there was an Exception
        // while creating URL object
        catch (Exception e) {
            return false;
        }
    }
}
