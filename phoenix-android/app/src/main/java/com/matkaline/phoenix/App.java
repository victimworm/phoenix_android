package com.matkaline.phoenix;

import android.app.Dialog;
import android.content.SharedPreferences;
import android.util.Log;

import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

import com.activeandroid.ActiveAndroid;
import com.google.android.material.navigation.NavigationView;

import java.lang.ref.WeakReference;
import java.util.concurrent.atomic.AtomicInteger;

import dialogs.LoginChooseDialog;
import http.HttpManager;
import models.User;
import models.models.cards.Card;
import models.models.cards.CardItem;
import models.models.cards.CardsManager;
import ulitis.ImageLoader;

public class App extends MultiDexApplication {

    public User user;
    private SharedPreferences prefs = null;
    public CardsManager cardMng;
    public HttpManager httpManager;
    public int cardID;
    public Card currentCard;
    public CardItem gameInforCardItem;

    public LoginChooseDialog dlgLoginChoose;
    public NavigationView navigationView;

    public ImageLoader imageLoader;
    public AtomicInteger atomicInteger = new AtomicInteger(0);
//    public ProgressDialog dlgProgress;

    public Dialog mDialog;

    private static WeakReference<App> instance = null;

    public static App getInstance() {
        return instance.get();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);
        instance = new WeakReference<>(this);

        Log.v("MyDebug", "app create");
        httpManager = new HttpManager(this);
        ActiveAndroid.initialize(this);
        user = new User(this);
        cardMng = new CardsManager(this);

//        dlgProgress = new ProgressDialog(this,R.style.MyTheme);
//        dlgProgress.setCancelable(true);
//        dlgProgress.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
//        dlgProgress.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
    }

}
