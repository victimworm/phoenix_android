package com.matkaline.phoenix.ui.home.questionchoose;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.matkaline.phoenix.R;
import com.matkaline.phoenix.ui.base.BaseDialog;

import ulitis.UiHelper;

public class ConfirmSkipQuestionDialog extends BaseDialog {

    private IFConfirmSkipQuestionFeedback feedback;

    public void setFeedback(IFConfirmSkipQuestionFeedback listener) {
        this.feedback = listener;
    }

    private Context mContext;
    private WindowManager manager;

    private ConfirmSkipQuestionDialog() {

    }

    public ConfirmSkipQuestionDialog(Context context, WindowManager manager) {
        this.mContext = context;
        this.manager = manager;
    }

    @Override
    public Context getContext() {
        return this.mContext;
    }

    @Override
    public WindowManager getWindowManager() {
        return this.manager;
    }

    @Override
    public int getLayout() {
        return R.layout.dialog_confirm_skip_question;
    }

    @Override
    public void setUpView(final Dialog dialog) {
        TextView txtTitle = dialog.findViewById(R.id.dialog_confirm_skip_question_txt_title);
        TextView txtContent = dialog.findViewById(R.id.dialog_confirm_skip_question_txt_message);
        View verticalLine = dialog.findViewById(R.id.dialog_confirm_skip_question_verticle_line);
        TextView txtOk = dialog.findViewById(R.id.dialog_confirm_skip_question_txt_ok);
        View horizontalLine = dialog.findViewById(R.id.dialog_confirm_skip_question_horizontal_line);
        TextView txtCancel = dialog.findViewById(R.id.dialog_confirm_skip_question_txt_cancel);

        UiHelper.TEXTSIZE(txtTitle, 60);
        UiHelper.PADDING(txtTitle, 0, 30, 0, 30);
        UiHelper.TEXTSIZE(txtContent, 50);
        UiHelper.PADDING(txtContent, 30, 0, 30, 40);
        UiHelper.TEXTSIZE(txtOk, 50);
        UiHelper.PADDING(txtOk, 0, 35, 0, 35);
        UiHelper.TEXTSIZE(txtCancel, 50);
        UiHelper.PADDING(txtCancel, 0, 35, 0, 35);

        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        txtOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (feedback != null) {
                    feedback.goGetCardWithoutQuestion();
                }
                dialog.dismiss();
            }
        });
    }
}
