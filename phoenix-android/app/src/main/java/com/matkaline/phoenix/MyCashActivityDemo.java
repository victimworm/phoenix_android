package com.matkaline.phoenix;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.inappbilling.inappbilling.util.IabHelper;
import com.example.inappbilling.inappbilling.util.IabResult;
import com.example.inappbilling.inappbilling.util.Inventory;
import com.example.inappbilling.inappbilling.util.Purchase;

public class MyCashActivityDemo extends AppCompatActivity {

    private Button clickButton;
    private Button buyButton;

    IabHelper mHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_cash_demo);

        buyButton = (Button) findViewById(R.id.buyButton);
        clickButton = (Button) findViewById(R.id.clickButton);
        clickButton.setEnabled(false);

        String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAgpKtQlqPMtjXOBW359qvwdh + vYx + fT4 + fcpEHp6eVwUydOiNWGa8BldJb8cXs0xuYGSmTVAqZkT97BWBogvyBueyBNGkB33GwqPtl7XlJJwPiwxxeu5naDA / r8vKPm + p / JqtgQB3h10moM7BxNwjeNEJgbBwFg8lTcwGP64LhClyFRR2f9lSwe0JYYHgKGHAPjFHM7iWk / EC2BYOHOYgtrsz / uipfxjFEQw / 0BOs0qT5x7Thssy38QrXBD3YTpfMxMRwY21BnAY + pJu0xuzRuU7qdezn2VI2px4OWP3HrqYTdcajLQeDlsN54XCVo8KGRDosi3 + 21O0dekbpOrDKaQIDAQAB";
        mHelper = new IabHelper(this, base64EncodedPublicKey);
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    Log.d("MyDebug", "In-app Billing setup failed: " + result);
                } else {
                    Log.d("MyDebug", "In-app Billing is set up OK");
                }
            }
        });

    }

    public void consume(View v) {
        //mHelper.consumeAsync();
        //consumeItem();
        //int response = get .consumePurchase(3, ITEM_SKU,"mypurchasetoken");
//        IInAppBillingService service = null;
//        service.consumePurchase(3,ITEM_SKU,"mypurchasetoken");

//        mHelper.

        consumeItem();
    }

    public void buttonClicked(View view) {
        clickButton.setEnabled(false);
        buyButton.setEnabled(true);
    }

    static final String ITEM_SKU = "android.test.purchased";

    public void buyClick(View view) {
        mHelper.launchPurchaseFlow(this, ITEM_SKU, 10000, mPurchaseFinishedListener, "mypurchasetokenPhuc4");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            if (result.isFailure()) {
                // Handle error
                ////Log.v("MyDebug0", "mPurchaseFinishedListener : " + result.getMessage() + " " + purchase);
                return;
            } else if (purchase.getSku().equals(ITEM_SKU)) {
                ////Log.v("MyDebug0", "purchase : " + purchase);
                consumeItem();
                buyButton.setEnabled(false);
            }

        }
    };

    public void consumeItem() {
        mHelper.queryInventoryAsync(mReceivedInventoryListener);
    }

    IabHelper.QueryInventoryFinishedListener mReceivedInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
//            if (inventory!=null) {
//                Purchase purchase = inventory.getPurchase(ITEM_SKU);
//                if (purchase != null) {
//                    mHelper.consumeAsync(purchase, new IabHelper.OnConsumeFinishedListener() {
//                        @Override
//                        public void onConsumeFinished(Purchase purchase, IabResult result) {
//                            ////Log.v("MyDebug0", "onConsumeFinished : " + result.getMessage());
//                        }
//                    });
//                }
//            }
            if (result.isFailure()) {
                ////Log.v("MyDebug0", "ReceivedInventoryListener : " + result.getMessage());
                // Handle failure
            } else {
                mHelper.consumeAsync(inventory.getPurchase(ITEM_SKU), mConsumeFinishedListener);
            }
        }
    };

    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
        public void onConsumeFinished(Purchase purchase, IabResult result) {
            if (result.isSuccess()) {
                clickButton.setEnabled(true);
            } else {
                // handle error
                ////Log.v("MyDebug0", "mConsumeFinishedListener : " + result.getMessage());
            }
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mHelper != null) mHelper.dispose();
        mHelper = null;
    }
}
