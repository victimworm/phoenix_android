package com.matkaline.phoenix;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.matkaline.phoenix.data.local.prefs.AppPreferencesHelper;
import com.matkaline.phoenix.ui.home.IFQuestionChooseNavigator;
import com.matkaline.phoenix.ui.home.IFQuestionFromUsNavigator;
import com.matkaline.phoenix.ui.home.lumierequestion.IFLumiereQuestionListener;
import com.matkaline.phoenix.ui.home.lumierequestion.LumiereQuestionDialog;
import com.matkaline.phoenix.ui.home.questionchoose.IFYourQuestionFeedback;
import com.matkaline.phoenix.ui.home.questionchoose.YourQuestionDialog;
import com.matkaline.phoenix.ui.splash.SplashActivity;
import com.matkaline.phoenix.utils.AppConstants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import apis.API;
import dbs.Explain;
import dialogs.LoginChooseDialog;
import dialogs.QuestionChooseDialog;
import dialogs.QuestionContentDialog;
import http.HttpManager;
import models.models.cards.Card;
import models.models.cards.PlayCardType;
import ulitis.Constants;
import ulitis.DateTimeHelper;
import ulitis.L;
import ulitis.UiHelper;
import ulitis.Utils;
import views.HomeView;

public class HomeActivity extends RootActivity implements View.OnClickListener {

    private HomeView vHome = null;

    Button btnDialog;
    Button btnLayout;
    Context context = this;
    private TextView tv_cash_sensage, tv_cash_lumiere, tv_profile;
    private LinearLayout ll_topbtn, ll_midbtn;
    private ImageView iv_cash_sensage, iv_music, iv_cash_lumiere, iv_profile;
    private Button btRandom, btToday, btFuture, btLumiere, btnProfile;
    private View viewSpace;
    private LinearLayout llProfile, llLumiere, llFuture;
    private ImageButton menu;
    private TextView tv_my_cash;
    private QuestionChooseDialog dlgQuestionChoose;
    private QuestionContentDialog dlgQuestionContent;
    private Button iv_test_month_card, btn_last_month_card, btn_next_month_card;
    private HomeView vCards;
    private SharedPreferences prefs = null;
    private Utils utils;
    private HomeActivity currentActivity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        currentActivity = this;
        overridePendingTransition(0, 0);
        setContentView(R.layout.activity_home);
        initSlidingMenu();
        checkVersionApp();


        iv_test_month_card = (Button) findViewById(R.id.iv_test_month_card);
        btn_last_month_card = (Button) findViewById(R.id.btn_last_month_card);
        btn_next_month_card = (Button) findViewById(R.id.btn_next_month_card);
        ImageView iv_cash = (ImageView) findViewById(R.id.iv_cash);
        tv_my_cash = (TextView) findViewById(R.id.tv_my_cash);
        if (app.user.coin > 0) {
            tv_my_cash.setText(String.valueOf(app.user.coin));
        } else {
            tv_my_cash.setText(String.valueOf(0));
        }
        prefs = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        utils = new Utils(this);

        menu = (ImageButton) findViewById(R.id.buttonMenu);
        btRandom = (Button) findViewById(R.id.btRandom);
        btToday = (Button) findViewById(R.id.btToday);
        btFuture = (Button) findViewById(R.id.btFuture);
        btLumiere = findViewById(R.id.btnLumiere);
        ll_topbtn = findViewById(R.id.customer_activity_home_ll_top_btn);
        ll_midbtn = findViewById(R.id.customer_activity_home_ll_mid_btn);
        btnProfile = findViewById(R.id.btnProfile);
        llLumiere = findViewById(R.id.customer_activity_home_ll_lumiere);
        llFuture = findViewById(R.id.customer_activity_home_ll_future);
        llProfile = findViewById(R.id.customer_activity_home_ll_profile);
        viewSpace = findViewById(R.id.customer_activity_home_space);
        iv_cash_sensage = (ImageView) findViewById(R.id.iv_cash_sensage);
        iv_cash_lumiere = findViewById(R.id.iv_cash_lumiere);
        iv_profile = findViewById(R.id.iv_cash_profile);
        tv_cash_sensage = (TextView) findViewById(R.id.tv_cash_sensage);
        tv_cash_lumiere = findViewById(R.id.tv_my_cash_lumiere);
        tv_profile = findViewById(R.id.tv_my_cash_profile);
        iv_music = (ImageView) findViewById(R.id.iv_music);

        Calendar calendarNextMonth = Calendar.getInstance();
        Calendar calendarLastMonth = Calendar.getInstance();
        Calendar calendarThisMonth = Calendar.getInstance();
        calendarNextMonth.setTimeZone(TimeZone.getDefault());
        calendarLastMonth.setTimeZone(TimeZone.getDefault());
        calendarThisMonth.setTimeZone(TimeZone.getDefault());
        calendarNextMonth.set(Calendar.MONTH, calendarNextMonth.get(Calendar.MONTH) + 1); // plus for next month
        calendarLastMonth.set(Calendar.MONTH, calendarLastMonth.get(Calendar.MONTH) - 1); // minus for last month
        String dateNextMonth = null;
        String dateLastMonth = null;
        String dateThisMonth = null;
        switch (getCurrentLanguage().getLanguage()) {
            case "vi":
                dateNextMonth = DateTimeHelper.dateLocaleSimple(calendarNextMonth.getTime(), new Locale("vi"));
                dateLastMonth = DateTimeHelper.dateLocaleSimple(calendarLastMonth.getTime(), new Locale("vi"));
                dateThisMonth = DateTimeHelper.dateLocaleSimple(calendarThisMonth.getTime(), new Locale("vi"));
                break;
            case "en":
                dateNextMonth = DateTimeHelper.dateLocaleSimple(calendarNextMonth.getTime(), Locale.US);
                dateLastMonth = DateTimeHelper.dateLocaleSimple(calendarLastMonth.getTime(), Locale.US);
                dateThisMonth = DateTimeHelper.dateLocaleSimple(calendarThisMonth.getTime(), Locale.US);
                break;
            default:
                dateNextMonth = DateTimeHelper.dateLocaleSimple(calendarNextMonth.getTime(), Locale.FRANCE);
                dateLastMonth = DateTimeHelper.dateLocaleSimple(calendarLastMonth.getTime(), Locale.FRANCE);
                dateThisMonth = DateTimeHelper.dateLocaleSimple(calendarThisMonth.getTime(), Locale.FRANCE);
                break;
        }
        btn_next_month_card.setText(dateNextMonth);
        btn_last_month_card.setText(dateLastMonth);
        iv_test_month_card.setText(dateThisMonth);

        UiHelper.RESIZE(menu, 106, 88);
        UiHelper.MARGIN(menu, 36, 50, 0, 0);

        UiHelper.RESIZE(iv_music, 60, 60);
        UiHelper.MARGIN(iv_cash, 200, 0, 0, 0);
        UiHelper.RESIZE(iv_cash, 60, 60);
        UiHelper.TEXTSIZE(tv_my_cash, 45);
        UiHelper.MARGIN(tv_my_cash, 10, 0, 100, 0);

        UiHelper.RESIZE(iv_test_month_card, 320, 125);
        UiHelper.TEXTSIZE(iv_test_month_card, 32);
        UiHelper.MARGIN(iv_test_month_card, 0, 0, 0, 30);

        UiHelper.RESIZE(btn_last_month_card, 320, 125);
        UiHelper.TEXTSIZE(btn_last_month_card, 32);
        UiHelper.MARGIN(btn_last_month_card, 0, 0, 80, 30);

        UiHelper.RESIZE(btn_next_month_card, 320, 125);
        UiHelper.TEXTSIZE(btn_next_month_card, 32);
        UiHelper.MARGIN(btn_next_month_card, 80, 0, 0, 30);

        UiHelper.RESIZE(btRandom, 320, 125);
        UiHelper.TEXTSIZE(btRandom, 32);
        UiHelper.MARGIN(btRandom, 0, 0, 80, 0);

        UiHelper.RESIZE(btToday, 320, 125);
        UiHelper.RESIZE(btLumiere, 320, 125);
        UiHelper.RESIZE(btnProfile, 320, 125);
        UiHelper.TEXTSIZE(btToday, 32);
        UiHelper.TEXTSIZE(btLumiere, 32);
        UiHelper.TEXTSIZE(btnProfile, 32);

        UiHelper.MARGIN(llProfile, 0, 0, 80, 0);
        UiHelper.RESIZE(viewSpace, 320, 125);
        UiHelper.MARGIN(llLumiere, 80, 0, 0, 0);

        UiHelper.MARGIN(ll_midbtn, 0, 0, 0, 30);

        UiHelper.RESIZE(btFuture, 320, 125);
        UiHelper.TEXTSIZE(btFuture, 32);

        UiHelper.MARGIN(llFuture, 80, 0, 0, 0);

        UiHelper.RESIZE(iv_cash_sensage, 50, 50);
        UiHelper.RESIZE(iv_cash_lumiere, 50, 50);
        UiHelper.RESIZE(iv_profile, 50, 50);
        UiHelper.TEXTSIZE(tv_cash_sensage, 45);
//        UiHelper.MARGIN(tv_cash_sensage, 70, 0, 0, 0);
        UiHelper.TEXTSIZE(tv_cash_lumiere, 45);
        UiHelper.TEXTSIZE(tv_profile, 45);
        UiHelper.MARGIN(iv_cash_sensage, 10, 10, 0, 0);
        UiHelper.MARGIN(iv_cash_lumiere, 10, 0, 0, 0);
        UiHelper.MARGIN(iv_profile, 10, 0, 0, 0);

        dlgQuestionContent = new QuestionContentDialog(this);
        dlgQuestionChoose = new QuestionChooseDialog(this);
        dlgQuestionChoose.setNavigator(new IFQuestionChooseNavigator() {
            @Override
            public void showQuestionPopup() {
                final YourQuestionDialog dialog = YourQuestionDialog.doCreate(currentActivity, getWindowManager());
                dialog.setFeedbackDialogListener(new IFYourQuestionFeedback() {
                    @Override
                    public void showResult(String question) {
                        if (question != null) {
                            if (question.equals("") == false) {
                                AppPreferencesHelper preferencesHelper = new AppPreferencesHelper(getApplicationContext(), AppConstants.PREF_NAME);
                                preferencesHelper.setYourQuestion(question);
                            }
                        }
                        go(CardsActivity.class);

                    }
                });
                dialog.show();
            }

            @Override
            public void getQuestionFromUs() {
                validateTypeCard();
                app.cardMng.getId();
                app.cardMng.getQuestionFromUs();
                dlgQuestionContent.txtQues.setText(app.cardMng.question);
                dlgQuestionContent.show();
            }
        });

        dlgQuestionContent.setNavigator(new IFQuestionFromUsNavigator() {
            @Override
            public void showResult() {
                go(CardsActivity.class);
                AppPreferencesHelper preferencesHelper = new AppPreferencesHelper(getApplicationContext(), AppConstants.PREF_NAME);
                preferencesHelper.setYourQuestion(app.cardMng.question);
            }
        });

        iv_test_month_card.setOnClickListener(this);
        btn_last_month_card.setOnClickListener(this);
        btn_next_month_card.setOnClickListener(this);
        iv_music.setOnClickListener(this);

        LinearLayout lyCards = (LinearLayout) findViewById(R.id.lyCards);
        lyCards.addView(vCards = new HomeView(this));
        verifyStoragePermissions(this);

        startService(new Intent(HomeActivity.this, services.LogcateService.class));

    }

    private void validateTypeCard() {
        if (!app.cardMng.reconcile) {
            if (app.cardMng.cardsResult[1].number == 0) {
                for (int i = 1; i <= 12; i++) {
                    if (app.cardMng.cardsResult[0].number == i) {
                        app.cardMng.cardsResult[0].type = Card.CHARME;
                        break;
                    }
                }
                for (int i = 14; i <= 25; i++) {
                    if (app.cardMng.cardsResult[0].number == i) {
                        app.cardMng.cardsResult[0].type = Card.CHOC;
                        break;
                    }
                }
            } else if (app.cardMng.cardsResult[1].number == 13) {
                if (app.cardMng.cardsResult[0].isBad()) {
                    for (int i = 1; i <= 12; i++) {
                        if (app.cardMng.cardsResult[0].number == i) {
                            app.cardMng.cardsResult[0].type = Card.CHARME;
                            break;
                        }
                    }
                    for (int i = 14; i <= 25; i++) {
                        if (app.cardMng.cardsResult[0].number == i) {
                            app.cardMng.cardsResult[0].type = Card.CHOC;
                            break;
                        }
                    }
                } else {
                }
            } else if (app.cardMng.cardsResult[0].color == app.cardMng.cardsResult[1].color) {
                if ((app.cardMng.cardsResult[0].number + app.cardMng.cardsResult[1].number) % 2 == 0) {//cung chan || cung le
                    for (int i = 1; i <= 12; i++) {
                        if (app.cardMng.cardsResult[0].number == i) {
                            app.cardMng.cardsResult[0].type = Card.CHARME;
                            break;
                        }
                    }
                    for (int i = 14; i <= 25; i++) {
                        if (app.cardMng.cardsResult[0].number == i) {
                            app.cardMng.cardsResult[0].type = Card.CHOC;
                            break;
                        }
                    }
                }
            }
        }
    }

    public void random(View v) {
        //SonLA _ remove current question - S
        AppPreferencesHelper preferencesHelper = new AppPreferencesHelper(getApplicationContext(), AppConstants.PREF_NAME);
        preferencesHelper.removeYourQuestion();
        //SonLA _ remove current question - E

        app.cardMng.question = null;
        app.cardMng.isButton = false;
//        app.cardMng.datSelectCard = DateTimeHelper.TODAY();
        app.cardMng.datSelectCard = null;
        app.cardMng.genCards();
        dlgQuestionChoose.show();
    }

    /**
     * when user select today button
     *
     * @param v
     */
    public void today(View v) {
        //SonLA _ remove current question - S
        AppPreferencesHelper preferencesHelper = new AppPreferencesHelper(getApplicationContext(), AppConstants.PREF_NAME);
        preferencesHelper.removeYourQuestion();
        //SonLA _ remove current question - E

        if (isNetworkStatusAvialable()) {
            if (app.user.isLogin()) {
                app.cardMng.datSelectCard = DateTimeHelper.TODAY();
                app.cardMng.isButton = true;//isButton = true là rút lá bài today
                app.cardMng.genCards();
                go(CardsActivity.class);
            } else {
                app.dlgLoginChoose = new LoginChooseDialog(this, this);
                app.dlgLoginChoose.show();
            }
        } else {
            alert(getString(R.string.alert), getString(R.string.alert_offline), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
        }
    }

    public void profile(View view) {
        if (isNetworkStatusAvialable()) {
            if (app.user.isLogin()) {
                if (app.user.coin >= 15) {
                    LumiereQuestionDialog lumiereQuestionDialog = LumiereQuestionDialog.doCreate(this, getWindowManager());
                    lumiereQuestionDialog.setFeedbackDialogListener(new IFLumiereQuestionListener() {
                        @Override
                        public void doListen(String question) {
                            app.user.submitCoin(-26, new HttpManager.OnResponse() {
                                @Override
                                public void onResponse(JSONObject jso) {
                                    app.user.updateCoinCount(jso);
                                    if (app.user.coin > 0) {
                                        tv_my_cash.setText(String.valueOf(app.user.coin));
                                    } else {
                                        tv_my_cash.setText(String.valueOf(0));
                                    }
                                    goLumiere(MonthCardActivity.class, PlayCardType.PROFILE);
                                }
                            });

                            if (question != null) {
                                if (question.equals("") == false) {
                                    AppPreferencesHelper preferencesHelper = new AppPreferencesHelper(getApplicationContext(), AppConstants.PREF_NAME);
                                    preferencesHelper.setYourQuestion(question);
                                }
                            }

                        }
                    });
                    lumiereQuestionDialog.show();
                } else {
                    go(MyCashActivity.class);
                }
            } else {
                app.dlgLoginChoose = new LoginChooseDialog(this, this);
                app.dlgLoginChoose.show();
            }
        } else {
            alert(getString(R.string.alert), getString(R.string.alert_offline), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
        }

        //SonLA _ remove current question - S
        AppPreferencesHelper preferencesHelper = new AppPreferencesHelper(getApplicationContext(), AppConstants.PREF_NAME);
        preferencesHelper.removeYourQuestion();
        //SonLA _ remove current question - E
    }

    public void lumiere(View view) {
        if (isNetworkStatusAvialable()) {
            if (app.user.isLogin()) {
                if (app.user.coin >= 15) {
                    LumiereQuestionDialog lumiereQuestionDialog = LumiereQuestionDialog.doCreate(this, getWindowManager());
                    lumiereQuestionDialog.setFeedbackDialogListener(new IFLumiereQuestionListener() {
                        @Override
                        public void doListen(String question) {
                            app.user.submitCoin(-15, new HttpManager.OnResponse() {
                                @Override
                                public void onResponse(JSONObject jso) {
                                    app.user.updateCoinCount(jso);
                                    if (app.user.coin > 0) {
                                        tv_my_cash.setText(String.valueOf(app.user.coin));
                                    } else {
                                        tv_my_cash.setText(String.valueOf(0));
                                    }
                                    goLumiere(MonthCardActivity.class, PlayCardType.LUMIERE_RANDOM_ALL);
                                }
                            });

                            if (question != null) {
                                if (question.equals("") == false) {
                                    AppPreferencesHelper preferencesHelper = new AppPreferencesHelper(getApplicationContext(), AppConstants.PREF_NAME);
                                    preferencesHelper.setYourQuestion(question);
                                }
                            }

                        }
                    });
                    lumiereQuestionDialog.show();
                } else {
                    go(MyCashActivity.class);
                }
            } else {
                app.dlgLoginChoose = new LoginChooseDialog(this, this);
                app.dlgLoginChoose.show();
            }
        } else {
            alert(getString(R.string.alert), getString(R.string.alert_offline), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
        }

        //SonLA _ remove current question - S
        AppPreferencesHelper preferencesHelper = new AppPreferencesHelper(getApplicationContext(), AppConstants.PREF_NAME);
        preferencesHelper.removeYourQuestion();
        //SonLA _ remove current question - E
    }

    public void future(View v) {
        if (isNetworkStatusAvialable()) {
            if (app.user.isLogin()) {
                if (app.user.coin >= 13) {
                    showDateDialog();
                } else {
                    go(MyCashActivity.class);
                }
            } else {
                app.dlgLoginChoose = new LoginChooseDialog(this, this);
                app.dlgLoginChoose.show();
            }
        } else {
            alert(getString(R.string.alert), getString(R.string.alert_offline), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
        }

    }

    DatePickerDialog dlgDatePicker;

    private void showDateDialog() {
        //SonLA _ remove current question - S
        AppPreferencesHelper preferencesHelper = new AppPreferencesHelper(getApplicationContext(), AppConstants.PREF_NAME);
        preferencesHelper.removeYourQuestion();
        //SonLA _ remove current question - E

        if (dlgDatePicker == null) {
            Calendar calendar = Calendar.getInstance();
            int day = calendar.get(Calendar.DAY_OF_MONTH);
            int month = calendar.get(Calendar.MONTH);
            int year = calendar.get(Calendar.YEAR);
            dlgDatePicker = new DatePickerDialog(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker datePicker, int y, int m, int d) {

                }
            }, year, month, day);
            dlgDatePicker.getDatePicker().setMinDate(System.currentTimeMillis() + DateUtils.DAY_IN_MILLIS);
            dlgDatePicker.setCancelable(true);
            dlgDatePicker.setButton(DatePickerDialog.BUTTON_NEGATIVE, getString(R.string.cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    if (which == DialogInterface.BUTTON_NEGATIVE) {

                    }
                }
            });
            dlgDatePicker.setButton(DatePickerDialog.BUTTON_POSITIVE, getString(R.string.ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int which) {
                    if (which == DialogInterface.BUTTON_POSITIVE) {
                        int y = dlgDatePicker.getDatePicker().getYear();
                        int m = dlgDatePicker.getDatePicker().getMonth();
                        int d = dlgDatePicker.getDatePicker().getDayOfMonth();
                        app.cardMng.datSelectCard = DateTimeHelper.GETDATE(y, m + 1, d);
                        app.cardMng.isButton = false;
                        app.cardMng.genCards();
                        app.user.submitCoin(-13, new HttpManager.OnResponse() {
                            @Override
                            public void onResponse(JSONObject jso) {
                                app.user.updateCoinCount(jso);
                                go(CardsActivity.class);
                            }
                        });
                    }
                }
            });
            dlgDatePicker.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {

                }
            });

        }
        if (!dlgDatePicker.isShowing()) {
            dlgDatePicker.show();
        }
    }

    private void initData() {
        for (int i = 1; i <= 26; i++) {
            Explain explain = new Explain();
            String[] arr = Utils.GetLines(getAssets(), "desc/desc_" + i + ".html");
            if (arr != null) {
                explain = Explain.get(i);
                if (explain == null) {
                    explain = new Explain();
                    explain.card_id = i;
                }

                int j = 0;
                for (j = 0; j < arr.length; j++) {
                    String line = arr[j];
                    if (j == 0) {
                        line = line.replace("</br>", "").replace("<b>", "").replace("</b>", "");
                        int i2 = line.indexOf(">");
                        if (i2 > -1) {
                            explain.name = line.substring(0, i2);
                        } else {
                            explain.name = line;
                        }
                    }
                    if (line.indexOf("ADJECTIF") > -1) {
                        explain.ADJECTIF = line.replace("ADJECTIF", "").replace(":", "").trim();
                    } else if (line.indexOf("OMBRE ") > -1) {
                        explain.OMBRE = line.replace("OMBRE", "").replace(":", "").trim();
                    } else {
                        explain.expn1 += arr[j];
                    }
                }
                L.V("########" + i + "###########");
                L.V(explain.expn1);
                L.V("ADJECTIF : ", explain.ADJECTIF);
                L.V("OMBRE : ", explain.OMBRE);
                explain.save();
            }
        }

        for (int i = -1; i >= -26; i--) {
            Explain explain = new Explain();
            String[] arr = Utils.GetLines(getAssets(), "desc/desc_" + i + ".html");
            if (arr != null) {
                explain = Explain.get(i);
                if (explain == null) {
                    explain = new Explain();
                    explain.card_id = i;
                }

                int j = 0;
                for (j = 0; j < arr.length; j++) {
                    String line = arr[j];
                    if (j == 0) {
                        line = line.replace("</br>", "").replace("<b>", "").replace("</b>", "");
                        int i2 = line.indexOf(">");
                        if (i2 > -1) {
                            explain.name = line.substring(0, i2);
                        } else {
                            explain.name = line;
                        }
                    }
                    if (line.indexOf("ADJECTIF") > -1) {
                        explain.ADJECTIF = line.replace("ADJECTIF", "").replace(":", "").trim();
                    } else if (line.indexOf("OMBRE ") > -1) {
                        explain.OMBRE = line.replace("OMBRE", "").replace(":", "").trim();
                    } else {
                        explain.expn1 += arr[j];
                    }
                }
                L.V("########" + i + "###########");
                L.V(explain.expn1);
                L.V("ADJECTIF : ", explain.ADJECTIF);
                L.V("OMBRE : ", explain.OMBRE);
                explain.save();
            }
        }

        for (int i = 1; i <= 26; i++) {
            Explain explain = new Explain();
            String[] arr = Utils.GetLines(getAssets(), "desc/descHoaGiai_" + i + ".html");
            if (arr != null) {
                explain = Explain.get(i);
                if (explain == null) {
                    explain = new Explain();
                    explain.card_id = i;
                }

                int j = 0;
                for (j = 0; j < arr.length; j++) {
                    String line = arr[j];
                    if (j == 0) {
                        line = line.replace("</br>", "").replace("<b>", "").replace("</b>", "");
                        int i2 = line.indexOf(">");
                        if (i2 > -1) {
                            explain.name = line.substring(0, i2);
                        } else {
                            explain.name = line;
                        }
                    }
                    if (line.indexOf("NOM") > -1) {
                        explain.NOM = line.replace("NOM", "").replace(":", "").trim();
                    } else {
                        explain.expn2 += arr[j];
                    }
                }
                L.V("########" + i + "###########");
                L.V(explain.expn2);
                L.V("NOM : ", explain.NOM);
                explain.save();
            }
        }

        for (int i = -1; i >= -26; i--) {
            Explain explain = new Explain();
            String[] arr = Utils.GetLines(getAssets(), "desc/descHoaGiai_" + i + ".html");
            if (arr != null) {
                explain = Explain.get(i);
                if (explain == null) {
                    explain = new Explain();
                    explain.card_id = i;
                }

                int j = 0;
                for (j = 0; j < arr.length; j++) {
                    String line = arr[j];
                    if (j == 0) {
                        line = line.replace("</br>", "").replace("<b>", "").replace("</b>", "");
                        int i2 = line.indexOf(">");
                        if (i2 > -1) {
                            explain.name = line.substring(0, i2);
                        } else {
                            explain.name = line;
                        }
                    }
                    if (line.indexOf("NOM") > -1) {
                        explain.NOM = line.replace("NOM", "").replace(":", "").trim();
                    } else {
                        explain.expn2 += arr[j];
                    }
                }
                L.V("########" + i + "###########");
                L.V(explain.expn2);
                L.V("NOM : ", explain.NOM);
                explain.save();
            }
        }


    }

    private void listData() {
        List<Explain> expls = Explain.getAll();
        for (int i = 0; i < expls.size(); i++) {
            Explain expl = expls.get(i);
            L.V("------------------" + i + "------------------" + expl.name);
            L.V(expl.expn1);
            L.V(expl.expn2);
            L.V("ADJECTIF", expl.ADJECTIF);
            L.V("OMBRE", expl.OMBRE);
            L.V("NOM", expl.NOM);
        }
    }

/*    private void backupDatabase() throws IOException {
        //Open your local db as the input stream
        String inFileName = "/data/data/com.matkaline.phoenix/databases/phoenix.sqlite";
        File dbFile = new File(inFileName);
        FileInputStream fis = new FileInputStream(dbFile);

        String outFileName = Environment.getExternalStorageDirectory() + "/phoenix.sqlite";
        //Open the empty db as the output stream
        OutputStream output = new FileOutputStream(outFileName);
        //transfer bytes from the inputfile to the outputfile
        byte[] buffer = new byte[1024];
        int length;
        while ((length = fis.read(buffer)) > 0) {
            output.write(buffer, 0, length);
        }
        //Close the streams
        output.flush();
        output.close();
        fis.close();
    }*/


    @Override
    protected void onStart() {
        btFuture.setSelected(false);
        btRandom.setSelected(false);
        btToday.setSelected(false);
        btLumiere.setSelected(false);
        btnProfile.setSelected(false);
        super.onStart();
        notif();
    }

    /**
     * kiem tra app dc goi de tu notification hay open app binh thuong
     */
    private void notif() {
        if (SplashActivity.NOTIF > 0) {
            /* SonLA - dont know what is this
            switch (SplashActivity.NOTIF) {
                case CardsManager.NOTIF_LIST:
                case CardsManager.NOTIF_DEFAULT:
                    app.cardMng.getQuestionFromUs();
                    app.cardMng.getCardByRandom();
                    go(CardsActivity.class);
                    break;
                case CardsManager.NOTIF_TODAY:
                    app.cardMng.isButton = true;
                    today(null);
                    break;
            }
            SplashActivity.NOTIF = 0;
            */
        } else {
            app.cardMng.question = null;
            app.cardMng.isButton = false;
            app.cardMng.datSelectCard = null;

            String timeFrame = SplashActivity.timeFrame;
            if (!TextUtils.isEmpty(timeFrame) && timeFrame.equals(Constants.Notification.NOTIFY_MORNING)) {
                AppPreferencesHelper appPreferen = new AppPreferencesHelper(getApplicationContext(), AppConstants.PREF_NAME);
                //Lấy câu hỏigoLumiere
                String notificationQuestion = appPreferen.getYourQuestion();
                //Lấy Type
                int cardType = appPreferen.getCardTypeOnNotification();
                app.cardMng.getQuestionFromNotify(cardType, notificationQuestion);

                appPreferen.removeNotificationQuestionData();
                go(CardsActivity.class);
            } else if (!TextUtils.isEmpty(timeFrame) && timeFrame.equals(Constants.Notification.NOTIFY_NOON)) {
                app.cardMng.isButton = true;
                today(null);
            } else if (!TextUtils.isEmpty(timeFrame) && timeFrame.equals(Constants.Notification.NOTIFY_NIGHT)) {
                app.cardMng.question = context.getString(R.string.title_dream);
                app.cardMng.getCardByRandom();
                go(CardsActivity.class);
            }
            SplashActivity.timeFrame = null;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!loadMusicStatus()) {
            iv_music.setAlpha(1.0f);
        } else {
            iv_music.setAlpha(0.3f);
        }
        if (app.user.coin > 0) {
            tv_my_cash.setText(String.valueOf(app.user.coin));
        } else {
            tv_my_cash.setText(String.valueOf(0));
        }
    }


    private void googlePlay() {
        final String appPackageName = this.getPackageName(); // getPackageName() from Context or Activity object
//        Log.v("MyDebug","appPackageName:"+appPackageName);
        try {
            this.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException ex) {
            this.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    private float getVersionCode() {
        PackageManager manager = this.getPackageManager();
        PackageInfo info = null;
        try {
            info = manager.getPackageInfo(this.getPackageName(), 0);
            float versionName = Float.parseFloat(info.versionName);
            int versionCode = info.versionCode;
            return versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return 0;
    }

    private void checkVersionApp() {
        if (isNetworkStatusAvialable()) {
            app.httpManager.doGet(API.ANDROID_VERSION, null, new HttpManager.OnResponse() {
                @Override
                public void onResponse(JSONObject jso) {
                    if (jso != null) {
                        try {
                            float version_code = Float.parseFloat(jso.getString("version").replace(" ", ""));
                            float versionCode = getVersionCode();
                            if (version_code > versionCode) {
                                dialogBuilder(getString(R.string.alert), getString(R.string.checkversion), new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                googlePlay();
                                                finish();
                                            }
                                        }
                                        , new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                            }
                                        });
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_test_month_card:
                if (isNetworkStatusAvialable()) {
                    if (app.user.isLogin()) {
                        go(MonthCardActivity.class);
                    } else {
                        app.dlgLoginChoose = new LoginChooseDialog(this, this);
                        app.dlgLoginChoose.show();
                    }
                } else {
                    alert(getString(R.string.alert), getString(R.string.alert_offline), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                }
                break;
            case R.id.btn_last_month_card:
                if (isNetworkStatusAvialable()) {
                    if (app.user.isLogin()) {
                        go(LastMonthActivity.class);
                    } else {
                        app.dlgLoginChoose = new LoginChooseDialog(this, this);
                        app.dlgLoginChoose.show();
                    }
                } else {
                    alert(getString(R.string.alert), getString(R.string.alert_offline), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                }
                break;
            case R.id.btn_next_month_card:
                if (isNetworkStatusAvialable()) {
                    if (app.user.isLogin()) {
                        go(NextMonthActivity.class);
                    } else {
                        app.dlgLoginChoose = new LoginChooseDialog(this, this);
                        app.dlgLoginChoose.show();
                    }
                } else {
                    alert(getString(R.string.alert), getString(R.string.alert_offline), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                }
                break;
            case R.id.iv_music:
                if (!loadMusicStatus()) {
                    saveMusicStatus(true);
                    iv_music.setAlpha(0.3f);
                    utils.stopMusic();
//                    utils.muteMusic();
                } else {
                    saveMusicStatus(false);
                    iv_music.setAlpha(1.0f);
                    utils.playMusic(R.raw.card_phoenix);
//                    utils.unmuteMusic();
                }
                break;

        }

    }
}
