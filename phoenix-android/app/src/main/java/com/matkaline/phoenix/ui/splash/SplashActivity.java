package com.matkaline.phoenix.ui.splash;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.matkaline.phoenix.HomeActivity;
import com.matkaline.phoenix.R;
import com.matkaline.phoenix.RootActivity;

import java.util.Locale;

import alarms.AppAlarmManager;
import alarms.DayAlarmManager;
import models.models.cards.CardsManager;
import receivers.DayReceiver;
import ulitis.Constants;
import ulitis.UiHelper;
import ulitis.Utils;

public class SplashActivity extends RootActivity {

    private AppAlarmManager appAlarmManager;
    //    private DayAlarmManager dayAlarmManager;
    private SharedPreferences prefs = null;
    public static int NOTIF = 0;
    public static String timeFrame;
    private Utils utils;

    private void createAlarmNextDay() {
        AlarmManager alarmManager = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this, DayReceiver.class);
        intent.setAction(DayAlarmManager.ACTION_REFRESH_DAY);

        PendingIntent pendingIntent;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            pendingIntent = PendingIntent.getService(
                    this,
                    DayAlarmManager.DAY_ALARM_REQUEST_CODE,
                    intent,
                    PendingIntent.FLAG_IMMUTABLE | PendingIntent.FLAG_UPDATE_CURRENT
            );
        } else {
            pendingIntent = PendingIntent.getBroadcast(
                    this,
                    DayAlarmManager.DAY_ALARM_REQUEST_CODE,
                    intent,
                    PendingIntent.FLAG_UPDATE_CURRENT
            );
        }

        // Cancel alarms
        try {
            alarmManager.cancel(pendingIntent);
        } catch (Exception e) {
            Log.e("ALARM", "AlarmManager update was not canceled. " + e.toString());
        }

        DayAlarmManager dayAlarmManager = new DayAlarmManager(this);
        dayAlarmManager.sttTimeNotif(DayAlarmManager.DAY_ALARM_REQUEST_CODE);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        if (Locale.getDefault().getLanguage().equals("fr")) {
            setLanguageWithoutNotification(Locale.FRENCH.toString());
        } else if (Locale.getDefault().getLanguage().equals("vi")) {
            setLanguageWithoutNotification("vi");
        } else {
            setLanguageWithoutNotification(Locale.ENGLISH.toString());
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_activity);
        utils = new Utils(this);

        ImageView imgLogo = (ImageView) findViewById(R.id.icon_image1);
        UiHelper.INIT(this);
        UiHelper.RESIZE(imgLogo, 550, 411);
        RelativeLayout lyBorder = (RelativeLayout) findViewById(R.id.lyBorder);
        GradientDrawable shap = (GradientDrawable) lyBorder.getBackground();
        shap.setStroke(UiHelper.GETSIZE(24), Color.WHITE);
        UiHelper.MARGIN(lyBorder, 92, 134, 92, 134);
        imgLogo.setBackgroundColor(Color.parseColor("#00000000"));
        app.cardMng = new CardsManager(this);

        notif();

        //Create alarm push notification on next day
        createAlarmNextDay();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                go(HomeActivity.class);
                finish();
            }
        }, 400);


        init();
        saveMusicStatus(true);
        if (!loadMusicStatus()) {
            utils.playMusic(R.raw.card_phoenix);
        }
    }

//    private boolean loadMusicStatus() {
//        return prefs.getBoolean(Constants.MUSIC_STATUS, false);
//    }

    /**
     * show notification
     */
    private void init() {
        appAlarmManager = new AppAlarmManager(this);
        prefs = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        boolean a = prefs.getBoolean("first", true);
//        dayAlarmManager = new DayAlarmManager(this);
//        dayAlarmManager.sttTimeNotif(24);
        if (a) {
            appAlarmManager.sttTimeNotif(11);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean("first", false);
            editor.commit();
        }
    }

    /**
     *
     */
    private void notif() {
        if (getIntent().getExtras() != null) {
            NOTIF = getIntent().getExtras().getInt("icase");
            timeFrame = getIntent().getStringExtra(Constants.Notification.NOTIFY_24_HOURS);
        }
    }

}
