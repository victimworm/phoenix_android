package com.matkaline.phoenix;

import android.graphics.Canvas;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import views.AnimationView;
import views.HomeView;

/**
 * Created by nguyen hong phuc on 9/26/2016.
 */
public class AnimationThread extends Thread {
    static final long FPS = 60;

    private AnimationView homeView;

    private int t = 0;

    public AnimationThread(AnimationView homeView) {
        this.homeView = homeView;

    }

    public void setRunning(int t) {
        this.t = t;
    }

    @Override
    public void run() {

        while (t>0) {
            t--;


            homeView.update();
            homeView.invalidate();

        }
    }

}
