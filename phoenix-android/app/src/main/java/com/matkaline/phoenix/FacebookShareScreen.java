package com.matkaline.phoenix;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.matkaline.phoenix.di.module.GlideApp;
import com.matkaline.phoenix.utils.worker.OnCompleted;
import com.matkaline.phoenix.utils.worker.WorkerRunnable;
import com.matkaline.phoenix.utils.worker.WorkerThread;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.Hashtable;

import ulitis.DateTimeHelper;
import ulitis.UiHelper;
import ulitis.Utils;

public class FacebookShareScreen extends RootActivity {

    private LinearLayout llImageShareLayout;
    private LinearLayout layoutRoot;
    private boolean isSharing = false;

    private TextView tv_fb_screen_title, tv_user_name, txtDate, tv_fb_card_flame_name, tv_fb_card_flamme_team, txtTeamType,
            tv_fb_card_flamme_word, tv_fb_defi_title, tv_fb_defi_name, tv_fb_defi_word, tv_telecharge;
    private ImageView iv_fb_flamme, iv_fb_shadow, iv_fb_defi, iv_icon_launcher, logo_google_play, logo_app_store;
    private LinearLayout ll_fb_defi, ll_fb_flamme_content, llBottomContent;
    private RelativeLayout ll_fb_flamma;
    private Utils utils;
    private File imagePath;
    private boolean runOnce = false;
    private static Hashtable<String, Typeface> fontCache = new Hashtable<String, Typeface>();

    private void isFbDefiShowing(boolean isShow) {
        if (isShow) {
            ll_fb_defi.setVisibility(View.VISIBLE);

            LinearLayout.LayoutParams paramFlameContent = new LinearLayout.LayoutParams(
                    0,
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    0.6f
            );
            ll_fb_flamme_content.setLayoutParams(paramFlameContent);

            LinearLayout.LayoutParams paramDefi = new LinearLayout.LayoutParams(
                    0,
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    0.4f
            );
            ll_fb_defi.setLayoutParams(paramDefi);
        } else {
            ll_fb_defi.setVisibility(View.GONE);

            LinearLayout.LayoutParams paramFlameContent = new LinearLayout.LayoutParams(
                    0,
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    1.0f
            );

            ll_fb_flamme_content.setLayoutParams(paramFlameContent);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_facebook_share_screen);

        llImageShareLayout = findViewById(R.id.activity_facebook_share_screen_ll_root);
        layoutRoot = findViewById(R.id.activity_facebook_share_screen_root);
        tv_fb_screen_title = (TextView) findViewById(R.id.tv_fb_screen_title);
        tv_user_name = (TextView) findViewById(R.id.tv_user_name);
        txtDate = (TextView) findViewById(R.id.txtDate);
        tv_fb_card_flame_name = (TextView) findViewById(R.id.tv_fb_card_flame_name);
        tv_fb_card_flamme_team = (TextView) findViewById(R.id.tv_fb_card_flamme_team);
        txtTeamType = findViewById(R.id.activity_facebook_share_screen_txt_team_type);
        tv_fb_card_flamme_word = (TextView) findViewById(R.id.tv_fb_card_flamme_word);
        tv_fb_defi_title = (TextView) findViewById(R.id.tv_fb_defi_title);
        tv_fb_defi_name = (TextView) findViewById(R.id.tv_fb_defi_name);
        tv_fb_defi_word = (TextView) findViewById(R.id.tv_fb_defi_word);
        tv_telecharge = (TextView) findViewById(R.id.tv_telecharge);
        iv_fb_flamme = (ImageView) findViewById(R.id.iv_fb_flamme);
        iv_fb_shadow = (ImageView) findViewById(R.id.iv_fb_shadow);
        iv_fb_defi = (ImageView) findViewById(R.id.iv_fb_defi);

        llBottomContent = findViewById(R.id.activity_facebook_share_screen_ll_bottom_content);
        UiHelper.PADDING(llBottomContent, 250, 0, 0, 30);

        iv_icon_launcher = (ImageView) findViewById(R.id.iv_icon_launcher);
        logo_app_store = findViewById(R.id.activity_facebook_share_screen_img_apple_store_logo);
        logo_google_play = findViewById(R.id.activity_facebook_share_screen_img_google_play_logo);
        ll_fb_flamme_content = (LinearLayout) findViewById(R.id.ll_fb_flamme_content);
        ll_fb_defi = (LinearLayout) findViewById(R.id.ll_fb_defi);
        UiHelper.PADDING(ll_fb_defi, 0, 0, 5, 0);

        ll_fb_flamma = (RelativeLayout) findViewById(R.id.ll_fb_flamma);

        utils = new Utils(this);
        if (app.user.isLogin()) {
            tv_user_name.setVisibility(View.VISIBLE);
            tv_user_name.setText(app.user.user_name);
        }

        Date timeGetCard = app.cardMng.datSelectCard;
        if (null == timeGetCard) {
            timeGetCard = DateTimeHelper.TODAY();
        }
        txtDate.setText(DateTimeHelper.STRINGFROMDATE(timeGetCard, getCurrentLanguage()));

        tv_fb_card_flame_name.setTypeface(get("fonts/showcard.TTF", this));
        tv_fb_card_flame_name.setText(utils.getCardFacebookFlammeName());
        tv_fb_card_flamme_team.setTypeface(get("fonts/showcard.TTF", this));
        txtTeamType.setTypeface(get("fonts/optima.ttf", this));

        String flammeTeamFull = utils.getCardFacebookFlammeTeam();
        String[] arrayFlammeTeam = flammeTeamFull.split(" . ");
        if (arrayFlammeTeam.length > 0) {
            tv_fb_card_flamme_team.setText(Html.fromHtml(arrayFlammeTeam[0]));
            if (arrayFlammeTeam.length > 1) {
                txtTeamType.setText(Html.fromHtml((arrayFlammeTeam[1]).toUpperCase()));
            }
        } else {
            tv_fb_card_flamme_team.setText(Html.fromHtml(flammeTeamFull));
        }

        String contentFrammeWord = utils.getCardFacebookFlammeWord();
        tv_fb_card_flamme_word.setText(Html.fromHtml(contentFrammeWord));

        tv_fb_screen_title.setText(getResources().getString(R.string.title_marcate));

//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                final ByteArrayOutputStream stream = new ByteArrayOutputStream();
//                app.currentCard.bm.compress(Bitmap.CompressFormat.PNG, 100, stream);
//
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        GlideApp.with(getApplicationContext())
//                                .load(stream.toByteArray())
////                .placeholder(R.drawable.card_back)
////                .error(R.drawable.card_back) // glideUrl != null, but download fail
//                                .into(iv_fb_flamme);
//                    }
//                });
//            }
//        }).start();

        WorkerThread.doOnWorkerThread(new WorkerRunnable() {
            @Override
            public ByteArrayOutputStream run() {
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                app.currentCard.bm.compress(android.graphics.Bitmap.CompressFormat.PNG, 100, stream);
                return stream;
            }
        }, new OnCompleted<ByteArrayOutputStream>() {
            @Override
            public void onFinish(ByteArrayOutputStream stream) {
                GlideApp.with(getApplicationContext())
                        .load(stream.toByteArray())
//                .placeholder(R.drawable.card_back)
//                .error(R.drawable.card_back) // glideUrl != null, but download fail
                        .into(iv_fb_flamme);

                iv_fb_flamme.setRotation(7);
                UiHelper.PADDING(iv_fb_flamme, 105, 0, 105, 0);
            }

            @Override
            public void onError(String error) {

            }
        });
//        iv_fb_flamme.setImageBitmap(app.currentCard.bm);
//        iv_fb_shadow.setImageBitmap(app.cardMng.cardsResult[1].bm);

        WorkerThread.doOnWorkerThread(new WorkerRunnable() {
            @Override
            public ByteArrayOutputStream run() {
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                app.cardMng.cardsResult[1].bm.compress(Bitmap.CompressFormat.PNG, 100, stream);
                return stream;
            }
        }, new OnCompleted<ByteArrayOutputStream>() {
            @Override
            public void onFinish(ByteArrayOutputStream stream) {
                GlideApp.with(getApplicationContext())
                        .load(stream.toByteArray())
                        .fitCenter()
//                .placeholder(R.drawable.card_back)
//                .error(R.drawable.card_back) // glideUrl != null, but download fail
                        .into(iv_fb_shadow);

                iv_fb_shadow.setRotation(-10);
                UiHelper.PADDING(iv_fb_shadow, 105, 0, 105, 0);
            }

            @Override
            public void onError(String error) {

            }
        });

//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                final ByteArrayOutputStream stream = new ByteArrayOutputStream();
//                app.cardMng.cardsResult[1].bm.compress(Bitmap.CompressFormat.PNG, 100, stream);
//
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        GlideApp.with(getApplicationContext())
//                                .load(stream.toByteArray())
//                                .fitCenter()
////                .placeholder(R.drawable.card_back)
////                .error(R.drawable.card_back) // glideUrl != null, but download fail
//                                .into(iv_fb_shadow);
//                    }
//                });
//            }
//        }).start();

        // tele charger
        UiHelper.TEXTSIZE(tv_telecharge, 65);
        UiHelper.RESIZE(iv_icon_launcher, 128, 128);
        UiHelper.RESIZE(iv_icon_launcher, 128, 128);
        UiHelper.RESIZE(logo_app_store, 400, 120);
        UiHelper.RESIZE(logo_google_play, 400, 120);

        if (app.currentCard.isBad() && app.cardMng.datSelectCard != null) { // has defi, show defi view
//            ll_fb_defi.setVisibility(View.VISIBLE);
            isFbDefiShowing(true);

            tv_fb_defi_name.setText(utils.getCardFacebookDefiName());
            tv_fb_defi_word.setText(Html.fromHtml(utils.getCardFacebookDefiWord()));
//            stream = new ByteArrayOutputStream();
//            app.cardMng.cardsResult[2].bm.compress(Bitmap.CompressFormat.PNG, 100, stream);
//            Glide.with(this)
//                    .load(stream.toByteArray())
//                    .placeholder(R.drawable.ic_launcher)
//                    .error(R.drawable.ic_launcher) // glideUrl != null, but download fail
//                    .into(iv_fb_defi);
            iv_fb_defi.setImageBitmap(app.cardMng.cardsResult[2].bm);
            iv_fb_defi.setRotation(8);

//            UiHelper.RESIZE(ll_fb_flamme_content, 700, 1000);
            UiHelper.PADDING(ll_fb_flamme_content, 0, 0, 10, 0);

            // screen title
            UiHelper.TEXTSIZE(tv_fb_screen_title, 50);
            UiHelper.MARGIN(tv_fb_screen_title, 0, 40, 0, 0);

            // user name
            UiHelper.TEXTSIZE(tv_user_name, 41);
            UiHelper.MARGIN(tv_user_name, 0, 0, 30, 0);

            // date time
            UiHelper.TEXTSIZE(txtDate, 41);
            UiHelper.MARGIN(txtDate, 0, 0, 0, 0);

            // flamme name
            UiHelper.TEXTSIZE(tv_fb_card_flame_name, 60);
            UiHelper.PADDING(tv_fb_card_flame_name, 100, 0, 0, 0);
            UiHelper.MARGIN(tv_fb_card_flame_name, 0, 20, 0, 0);

            // flamme team
            UiHelper.TEXTSIZE(tv_fb_card_flamme_team, 49);
            UiHelper.MARGIN(tv_fb_card_flamme_team, 0, 20, 0, 0);

            UiHelper.TEXTSIZE(txtTeamType, 45);
            UiHelper.MARGIN(txtTeamType, 0, 20, 0, 0);
//            UiHelper.PADDING(txtTeamType, 20, 0, 0, 0);

            // flamme content
            UiHelper.TEXTSIZE(tv_fb_card_flamme_word, 48);
            UiHelper.MARGIN(tv_fb_card_flamme_word, 0, 20, 0, 0);

            // layout card view
            UiHelper.MARGIN(ll_fb_flamma, 0, 0, 30, 0);

            // card view shadow
            UiHelper.MARGIN(iv_fb_shadow, 0, 60, 0, 180);

            // card view flamme
            UiHelper.MARGIN(iv_fb_flamme, 0, 180, 0, 60);

            /*
             *** Layout Defi
             */
//            UiHelper.RESIZE(ll_fb_defi, 700, 1000);
            // defi title
            UiHelper.TEXTSIZE(tv_fb_defi_title, 55);
            UiHelper.MARGIN(tv_fb_defi_title, 0, 15, 0, 0);

            // defi name
            UiHelper.TEXTSIZE(tv_fb_defi_name, 53);
//            UiHelper.MARGIN(tv_fb_defi_name, 0, 5, 0, 0);

            // defi word
            UiHelper.TEXTSIZE(tv_fb_defi_word, 48);
            UiHelper.MARGIN(tv_fb_defi_word, 0, 10, 0, 0);

            // card view defi
            UiHelper.MARGIN(iv_fb_defi, 0, 60, 0, 60);
            UiHelper.RESIZE(iv_fb_defi, 352, 616);

        } else { // show full flamme info
//            ll_fb_defi.setVisibility(View.GONE);
            isFbDefiShowing(false);

            // screen title
            UiHelper.TEXTSIZE(tv_fb_screen_title, 60);
            UiHelper.MARGIN(tv_fb_screen_title, 0, 60, 0, 0);

            // user name
            UiHelper.TEXTSIZE(tv_user_name, 52);
            UiHelper.MARGIN(tv_user_name, 0, 0, 60, 0);

            // date time
            UiHelper.TEXTSIZE(txtDate, 52);
            UiHelper.MARGIN(txtDate, 0, 0, 0, 0);

            // flamme name
            UiHelper.TEXTSIZE(tv_fb_card_flame_name, 120);
            UiHelper.PADDING(tv_fb_card_flame_name, 80, 0, 0, 0);
            UiHelper.MARGIN(tv_fb_card_flame_name, 0, 0, 0, 0);

            // flamme team
            UiHelper.TEXTSIZE(tv_fb_card_flamme_team, 77);
            UiHelper.MARGIN(tv_fb_card_flamme_team, 0, 0, 0, 0);

            UiHelper.TEXTSIZE(txtTeamType, 69);
            UiHelper.MARGIN(txtTeamType, 0, 0, 0, 0);
//            UiHelper.PADDING(txtTeamType, 20, 0, 0, 0);

            // flamme content
            UiHelper.TEXTSIZE(tv_fb_card_flamme_word, 54);
            UiHelper.MARGIN(tv_fb_card_flamme_word, 0, 0, 0, 0);

            // layout card view
            UiHelper.MARGIN(ll_fb_flamma, 40, 0, 60, 0);

            // card view shadow
            UiHelper.MARGIN(iv_fb_shadow, 0, 60, 0, 180);

            // card view flamme
            UiHelper.MARGIN(iv_fb_flamme, 0, 180, 0, 60);
        }

        layoutRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isSharing == false) {
                    isSharing = true;
                    Bitmap bitmap = takeScreenshot();

                    String title = getResources().getString(R.string.title_share);
                    String bitmapPath = MediaStore.Images.Media.insertImage(getContentResolver(), bitmap, title, title);
                    Uri bitmapUri = Uri.parse(bitmapPath);

                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setType("image/png");
                    intent.putExtra(Intent.EXTRA_STREAM, bitmapUri);
                    startActivity(Intent.createChooser(intent, title));

                    finish();
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        isSharing = false;
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
//        if (!runOnce) {
//            Handler handler = new Handler();
//            handler.postDelayed(new Runnable() {
//                public void run() {
//                    // Actions to do after 10 seconds
//                    runOnce = true;
//
////                    share();
//
//                }
////            }, 1000);
//            }, 5000);
//        }
    }

    public Bitmap takeScreenshot() {
//        View rootView = findViewById(android.R.id.content).getRootView();
//        rootView.setDrawingCacheEnabled(true);
//        return rootView.getDrawingCache();

        //New capture screen 17/07/2019
        llImageShareLayout.setDrawingCacheEnabled(true);
        llImageShareLayout.buildDrawingCache(true);
        Bitmap cs = Bitmap.createBitmap(llImageShareLayout.getDrawingCache());
        llImageShareLayout.setDrawingCacheEnabled(false);

        return cs;
    }

    public void saveBitmap(Bitmap bitmap) {
        imagePath = new File(Environment.getExternalStorageDirectory() + "/screenshot.png");
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(imagePath);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
//            Log.e("MyDebug", e.getMessage(), e);
            System.out.println(e.getMessage());
        } catch (IOException e) {
//            Log.e("MyDebug", e.getMessage(), e);
            System.out.println(e.getMessage());
        }
    }

    public static Typeface get(String name, Context context) {
        Typeface tf = fontCache.get(name);
        if (tf == null) {
            try {
                tf = Typeface.createFromAsset(context.getAssets(), name);
            } catch (Exception e) {
                return null;
            }
            fontCache.put(name, tf);
        }
        return tf;
    }
}
