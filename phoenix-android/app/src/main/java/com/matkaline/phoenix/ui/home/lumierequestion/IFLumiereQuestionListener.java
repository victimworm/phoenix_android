package com.matkaline.phoenix.ui.home.lumierequestion;

public interface IFLumiereQuestionListener {

    void doListen(String question);
}
