package com.matkaline.phoenix;

import android.content.res.AssetManager;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;

import ulitis.UiHelper;


public class PolicyActivity extends RootActivity {

    private WebView webPolicy;
    private TextView tv_my_cash;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_policy);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        init();
        webViewSetData(webPolicy,loadData("policy.txt"), UiHelper.GETSIZE(55));
    }

    private String loadData(String inFile) {
        String tContents = "";
        InputStream stream;
        AssetManager assetManager = getAssets();
        //Log.v("MyDug", "Content" + tContents);
        try {
            stream = assetManager.open(inFile);
            int size = stream.available();
            byte[] buffer = new byte[size];
            stream.read(buffer);
            stream.close();
            tContents = new String(buffer, "UTF-8");
            //Log.v("MyDug", "Content = " + tContents);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return tContents;
    }

    private void init() {
        setTitleScreen(getResources().getString(R.string.myPolicy_title));
        webPolicy = (WebView) findViewById(R.id.webPolicy);
        webViewSetting(webPolicy);

        View lyTitle = findViewById(R.id.inTitleBack);
        lyTitle.findViewById(R.id.inBackButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                left(v);
            }
        });

        tv_my_cash = (TextView) findViewById(R.id.tv_my_cash);
        if(app.user.coin > 0) {
            tv_my_cash.setText(String.valueOf(app.user.coin));
        } else {
            tv_my_cash.setText(String.valueOf(0));
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
    }
}
