package com.matkaline.phoenix;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import androidx.core.content.ContextCompat;

import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TableRow;
import android.widget.TextView;

import com.matkaline.phoenix.data.local.prefs.AppPreferencesHelper;
import com.matkaline.phoenix.ui.shareLumiere.ShareLumiereActivity;
import com.matkaline.phoenix.utils.AppConstants;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;

import dialogs.LoginChooseDialog;
import http.HttpManager;
import models.models.cards.Card;
import models.models.cards.PlayCardType;
import models.models.randoms.MyRandomSeed;
import models.models.row.LumiereDescription;
import models.models.row.RowIdentity;
import models.models.row.RowsIdentity;
import ulitis.DateTimeHelper;
import ulitis.UiHelper;
import ulitis.Utils;
import views.TouchImageView;

public class MonthCardActivity extends RootActivity implements View.OnClickListener {

    //SonLA_Thêm logic cho lumiere mode - S
    public PlayCardType gameType = PlayCardType.MONTH;//default is Month
    private ArrayList<ArrayList<Integer>> totalLumiereIndexs;
    private ArrayList<Integer> selectedRandomIndexs;
    private ArrayList<Card> cardsLumiere;
    private RowsIdentity rowsIdentity;

    private final String KEY_GET_ALL_ROWS_DEFI = "getLumiereAllRowsDefi";
    private final String KEY_GET_PH_INDEXS = "getPHLumierePathIndexs";
    private final String KEY_GET_CARD_DEFI_INDEXS = "getLumiereCardDefiIndexs";
    private final String KEY_GET_ROW_DEFI_INDEXS = "getLumiereRowDefiIndexs";
    private final String KEY_CURRENT_DAY_LUMIERE_TODAY = "currentDayLumiereTodayMode";
    private final String KEY_LUMIERE_RANDOM_INDEX = "currentDayLumiereRandomIndex";
    private final String KEY_LUMIERE_TODAY_INDEX = "currentDayLumiereTodayIndex";
    public int energieIndex = 0;
    //SonLA_Thêm logic cho lumiere mode - E

    private ScrollView sv_container;
    private TextView tv_cash_sensage;
    private LinearLayout ll_cash_sensage, ll_month_sensage;
    private ImageView iv_cash_sensage;
    private ImageView iv_harmonie, iv_aventure, iv_fruit, iv_complicate, iv_energy, iv_guerre,
            iv_desire, iv_intution, iv_besoin,
            iv_harmonie_trans, iv_aventure_trans, iv_fruit_trans, iv_complicate_trans,
            iv_energy_trans, iv_guerre_trans, iv_desire_trans, iv_intution_trans, iv_besoin_trans;
    private TextView tv_my_cash;
    private TextView tv_lumiere_user, tv_lumiere_question, tv_lumiere_description;
    private TextView tv_lumiere_share;
    private ImageView img_lumiere_share;
    private LinearLayout ll_lumiere_share;
    private TextView tv_h, tv_armonie, tv_a, tv_venture, tv_f, tv_ruit, tv_c, tv_omplicate, tv_e,
            tv_nergy, tv_g, tv_uerre, tv_d, tv_esir, tv_i, tv_ntution, tv_b, tv_esoin;
    private Button btn_month_card_sensage;
    private ImageView imageBack, iv_music;
    private LinearLayout line1;
    public Bitmap bm;
    private int width, height;
    private SharedPreferences prefs;
    private final String CURRENT_MONTH = "current_month";
    private final String VALUE_HARMONIE = "harmonie";
    private final String VALUE_AVENTURE = "aventure";
    private final String VALUE_FRUIT = "fruit";
    private final String VALUE_COMPLICATE = "complicate";
    private final String VALUE_ENERGY = "energy";
    private final String VALUE_GUERRE = "guerre";
    private final String VALUE_DESIRE = "desire";
    private final String VALUE_INTUTION = "intution";
    private final String VALUE_BESOIN = "besoin";
    private final String IS_REVERSE_HARMONIE = "isReverse_harmonie";
    private final String IS_REVERSE_AVENTURE = "isReverse_aventure";
    private final String IS_REVERSE_FRUIT = "isReverse_fruit";
    private final String IS_REVERSE_COMPLICATE = "isReverse_complicate";
    private final String IS_REVERSE_ENERGY = "isReverse_energy";
    private final String IS_REVERSE_GUERRE = "isReverse_guerre";
    private final String IS_REVERSE_DESIRE = "isReverse_desire";
    private final String IS_REVERSE_INTUTION = "isReverse_intution";
    private final String IS_REVERSE_BESOIN = "isReverse_besoin";
    private final String MUSIC_STATUS = "music_status";
    private final String IS_PAID_MONTH = "is_paid_month";
    public static final String SENTENCES = "sentences";
    public static final String TITLE = "title";
    public static final String TITLE_TEAM = "title_team";
    public static final String SENTENCES_EN = "sentences_en";
    public static final String TITLE_EN = "title_en";
    public static final String TITLE_TEAM_EN = "title_team_en";
    public static final String SENTENCES_VI = "sentences_vi";
    public static final String TITLE_VI = "title_vi";
    public static final String TITLE_TEAM_VI = "title_team_vi";
    public static final String CREATION = "Team Création";
    public static final String REVOLTE = "Team Révolte";
    public static final String SOUCI = "Team Souci";
    public static final String ZEN = "Team Zen";
    private Byte harmonie, aventure, fruit, complicate, energy, guerre, desire, intution, besoin;
    private boolean isReverseHarmonie, isReverseAventure, isReverseFruit, isReverseComplicate,
            isReverseEnergy, isReverseGuerre, isReverseDesire, isReverseIntution, isReverseBesoin;
    private Random rd;
    private final Integer[] TEAM_CREATION = {15, 17, 19, 21, 23, 25};
    private final Integer[] TEAM_ZEN = {14, 16, 18, 20, 22, 24};
    private final Integer[] TEAM_DETRESSE = {2, 4, 6, 8, 10, 12};
    private final Integer[] TEAM_REVOLTE = {1, 3, 5, 7, 9, 11};
    List<String> arrTeamSentences = new ArrayList<String>();
    List<String> arrRemainSentences = new ArrayList<String>();
    private int resultTeamCreation = 0;
    private int resultTeamZen = 0;
    private int resultTeamDestresse = 0;
    private int resultTeamRevolte = 0;
    private int currentMonth;

    private boolean isHarmonieFlip, isAventureFlip, isFruitFlip, isComplicateFlip, isEnergyFlip,
            isGuerreFlip, isDesireFlip, isIntutionFlip, isBesoinFlip = false;
    private Utils utils;
    private List<Byte> listEightRemainCard;
    private TableRow tblr_third;

    private String[] imageLumiereCardShow = {"1", "1", "1", "1", "1", "1", "1", "1", "1"};
    private String strDescriptionContent4Share = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_month_card);

        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        sv_container = findViewById(R.id.sv_container);
        line1 = (LinearLayout) findViewById(R.id.linear_result_card);
        imageBack = (ImageView) findViewById(R.id.icon_back);
        iv_music = (ImageView) findViewById(R.id.iv_music);
        tv_my_cash = (TextView) findViewById(R.id.tv_my_cash);
        tv_lumiere_user = findViewById(R.id.activity_month_card_txt_lumiere_user_info);
        tv_lumiere_question = findViewById(R.id.activity_month_card_txt_lumiere_question);
        tv_lumiere_description = findViewById(R.id.activity_month_card_txt_lumiere_description);
        ll_lumiere_share = findViewById(R.id.activity_month_card_ll_share);
        tv_lumiere_share = findViewById(R.id.activity_month_card_txt_share);
        img_lumiere_share = findViewById(R.id.activity_month_card_img_share);

        if (app.user.coin > 0) {
            tv_my_cash.setText(String.valueOf(app.user.coin));
        } else {
            tv_my_cash.setText(String.valueOf(0));
        }
        ImageView iv_cash = (ImageView) findViewById(R.id.iv_cash);

        ll_month_sensage = (LinearLayout) findViewById(R.id.ll_month_sensage);
        ll_cash_sensage = (LinearLayout) findViewById(R.id.ll_cash_sensage);
        iv_cash_sensage = (ImageView) findViewById(R.id.iv_cash_sensage);
        tv_cash_sensage = (TextView) findViewById(R.id.tv_cash_sensage);
        tblr_third = (TableRow) findViewById(R.id.tblr_third);
        iv_harmonie = (ImageView) findViewById(R.id.iv_harmonie);
        iv_aventure = (ImageView) findViewById(R.id.iv_aventure);
        iv_fruit = (ImageView) findViewById(R.id.iv_fruit);
        iv_complicate = (ImageView) findViewById(R.id.iv_complicate);
        iv_energy = (ImageView) findViewById(R.id.iv_energy);
        iv_guerre = (ImageView) findViewById(R.id.iv_guerre);
        iv_desire = (ImageView) findViewById(R.id.iv_desire);
        iv_intution = (ImageView) findViewById(R.id.iv_intution);
        iv_besoin = (ImageView) findViewById(R.id.iv_besoin);
        iv_harmonie_trans = (ImageView) findViewById(R.id.iv_harmonie_trans);
        iv_aventure_trans = (ImageView) findViewById(R.id.iv_aventure_trans);
        iv_fruit_trans = (ImageView) findViewById(R.id.iv_fruit_trans);
        iv_complicate_trans = (ImageView) findViewById(R.id.iv_complicate_trans);
        iv_energy_trans = (ImageView) findViewById(R.id.iv_energy_trans);
        iv_guerre_trans = (ImageView) findViewById(R.id.iv_guerre_trans);
        iv_desire_trans = (ImageView) findViewById(R.id.iv_desire_trans);
        iv_intution_trans = (ImageView) findViewById(R.id.iv_intution_trans);
        iv_besoin_trans = (ImageView) findViewById(R.id.iv_besoin_trans);
        btn_month_card_sensage = (Button) findViewById(R.id.btn_month_card_sensage);
        RelativeLayout rl_harmonie = (RelativeLayout) findViewById(R.id.rl_harmonie);
        RelativeLayout rl_aventure = (RelativeLayout) findViewById(R.id.rl_aventure);
        RelativeLayout rl_fruit = (RelativeLayout) findViewById(R.id.rl_fruit);
        RelativeLayout rl_complicate = (RelativeLayout) findViewById(R.id.rl_complicate);
        RelativeLayout rl_energy = (RelativeLayout) findViewById(R.id.rl_energy);
        RelativeLayout rl_guerre = (RelativeLayout) findViewById(R.id.rl_guerre);
        RelativeLayout rl_desire = (RelativeLayout) findViewById(R.id.rl_desire);
        RelativeLayout rl_intution = (RelativeLayout) findViewById(R.id.rl_intution);
        RelativeLayout rl_besoin = (RelativeLayout) findViewById(R.id.rl_besoin);

        tv_h = (TextView) findViewById(R.id.tv_h);
        tv_armonie = (TextView) findViewById(R.id.tv_armonie);
        tv_a = (TextView) findViewById(R.id.tv_a);
        tv_venture = (TextView) findViewById(R.id.tv_venture);
        tv_f = (TextView) findViewById(R.id.tv_f);
        tv_ruit = (TextView) findViewById(R.id.tv_ruit);
        tv_c = (TextView) findViewById(R.id.tv_c);
        tv_omplicate = (TextView) findViewById(R.id.tv_omplicate);
        tv_e = (TextView) findViewById(R.id.tv_e);
        tv_nergy = (TextView) findViewById(R.id.tv_nergy);
        tv_g = (TextView) findViewById(R.id.tv_g);
        tv_uerre = (TextView) findViewById(R.id.tv_uerre);
        tv_d = (TextView) findViewById(R.id.tv_d);
        tv_esir = (TextView) findViewById(R.id.tv_esir);
        tv_i = (TextView) findViewById(R.id.tv_i);
        tv_ntution = (TextView) findViewById(R.id.tv_ntution);
        tv_b = (TextView) findViewById(R.id.tv_b);
        tv_esoin = (TextView) findViewById(R.id.tv_esoin);

        listEightRemainCard = new ArrayList<>();

        UiHelper.RESIZE(iv_cash_sensage, 45, 45);
        UiHelper.TEXTSIZE(tv_cash_sensage, 40);
        UiHelper.MARGIN(tv_cash_sensage, 70, 0, 0, 0);
        UiHelper.MARGIN(iv_cash_sensage, 10, 10, 0, 0);
        UiHelper.MARGIN(ll_cash_sensage, 60, 10, 0, 0);

        UiHelper.RESIZE(line1, 165, 135);
        UiHelper.RESIZE(imageBack, 43, 73);
        UiHelper.MARGIN(imageBack, 0, 35, 0, 45);
        UiHelper.RESIZE(iv_music, 60, 60);
        UiHelper.RESIZE(iv_cash, 60, 60);
        UiHelper.MARGIN(iv_cash, 200, 0, 0, 0);
        UiHelper.TEXTSIZE(tv_lumiere_question, 50);
        UiHelper.PADDING(tv_lumiere_question, 0, 10, 0, 30);
        UiHelper.TEXTSIZE(tv_lumiere_description, 50);
        UiHelper.PADDING(tv_lumiere_description, 30, 0, 30, 70);
        UiHelper.TEXTSIZE(tv_lumiere_share, 60);
        UiHelper.PADDING(tv_lumiere_share, 25, 35, 0, 25);
        UiHelper.RESIZE(img_lumiere_share, 100, 100);

        UiHelper.TEXTSIZE(tv_my_cash, 45);
        UiHelper.MARGIN(tv_my_cash, 10, 0, 100, 0);


        //Bottom 100 với lumiere và 200 với month
        UiHelper.MARGIN(tblr_third, 0, 0, 0, 50);

        UiHelper.TEXTSIZE(tv_h, 52);
        UiHelper.TEXTSIZE(tv_a, 52);
        UiHelper.TEXTSIZE(tv_f, 52);
        UiHelper.TEXTSIZE(tv_c, 52);
        UiHelper.TEXTSIZE(tv_e, 52);
        UiHelper.TEXTSIZE(tv_g, 52);
        UiHelper.TEXTSIZE(tv_d, 52);
        UiHelper.TEXTSIZE(tv_i, 52);
        UiHelper.TEXTSIZE(tv_b, 52);
        UiHelper.TEXTSIZE(tv_armonie, 35);
        UiHelper.TEXTSIZE(tv_venture, 35);
        UiHelper.TEXTSIZE(tv_ruit, 35);
        UiHelper.TEXTSIZE(tv_omplicate, 35);
        UiHelper.TEXTSIZE(tv_nergy, 35);
        UiHelper.TEXTSIZE(tv_uerre, 35);
        UiHelper.TEXTSIZE(tv_esir, 35);
        UiHelper.TEXTSIZE(tv_ntution, 35);
        UiHelper.TEXTSIZE(tv_esoin, 35);

        UiHelper.MARGIN(rl_harmonie, 100, 0, 0, 0);
        UiHelper.MARGIN(rl_aventure, 100, 0, 0, 0);
        UiHelper.MARGIN(rl_fruit, 100, 0, 100, 0);
        UiHelper.MARGIN(rl_complicate, 100, 0, 0, 0);
        UiHelper.MARGIN(rl_energy, 100, 0, 0, 0);
        UiHelper.MARGIN(rl_guerre, 100, 0, 100, 0);
        UiHelper.MARGIN(rl_desire, 100, 0, 0, 0);
        UiHelper.MARGIN(rl_intution, 100, 0, 0, 0);
        UiHelper.MARGIN(rl_besoin, 100, 0, 100, 0);

        UiHelper.RESIZE(iv_harmonie, 269, 500);
        UiHelper.RESIZE(iv_aventure, 269, 500);
        UiHelper.RESIZE(iv_fruit, 269, 500);
        UiHelper.RESIZE(iv_complicate, 269, 500);
        UiHelper.RESIZE(iv_energy, 269, 500);
        UiHelper.RESIZE(iv_guerre, 269, 500);
        UiHelper.RESIZE(iv_desire, 269, 500);
        UiHelper.RESIZE(iv_intution, 269, 500);
        UiHelper.RESIZE(iv_besoin, 269, 500);

        UiHelper.RESIZE(btn_month_card_sensage, 350, 150);
        UiHelper.TEXTSIZE(btn_month_card_sensage, 45);

        UiHelper.RESIZE(iv_harmonie_trans, 269, 500);
        UiHelper.RESIZE(iv_aventure_trans, 269, 500);
        UiHelper.RESIZE(iv_fruit_trans, 269, 500);
        UiHelper.RESIZE(iv_complicate_trans, 269, 500);
        UiHelper.RESIZE(iv_energy_trans, 269, 500);
        UiHelper.RESIZE(iv_guerre_trans, 269, 500);
        UiHelper.RESIZE(iv_desire_trans, 269, 500);
        UiHelper.RESIZE(iv_intution_trans, 269, 500);
        UiHelper.RESIZE(iv_besoin_trans, 269, 500);
        iv_harmonie_trans.setOnClickListener(this);
        iv_aventure_trans.setOnClickListener(this);
        iv_fruit_trans.setOnClickListener(this);
        iv_complicate_trans.setOnClickListener(this);
        iv_guerre_trans.setOnClickListener(this);
        iv_desire_trans.setOnClickListener(this);
        iv_intution_trans.setOnClickListener(this);
        iv_besoin_trans.setOnClickListener(this);
        iv_energy_trans.setOnClickListener(this);


        iv_harmonie.setOnClickListener(this);
        iv_aventure.setOnClickListener(this);
        iv_fruit.setOnClickListener(this);
        iv_complicate.setOnClickListener(this);
        iv_guerre.setOnClickListener(this);
        iv_desire.setOnClickListener(this);
        iv_intution.setOnClickListener(this);
        iv_besoin.setOnClickListener(this);
        iv_energy.setOnClickListener(this);
        iv_music.setOnClickListener(this);
        btn_month_card_sensage.setOnClickListener(this);

        //SonLA_get intent để phân biệt type of lumiere - S
        Intent intent = getIntent();
        PlayCardType type = (PlayCardType) intent.getSerializableExtra("SelectPlayCardType");
        if (type != null) {
            gameType = type;
        }
        //SonLA_get intent để phân biệt type of lumiere - E

        iv_energy.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                iv_energy.getViewTreeObserver().removeOnPreDrawListener(this);

                height = iv_energy.getHeight();
                width = iv_energy.getWidth();
                return false;
            }
        });
        rd = new Random();
        prefs = app.getSharedPreferences(app.getPackageName(), app.MODE_PRIVATE);
        utils = new Utils(this);
//        utils.playMusic(R.raw.card_phoenix);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getDefault());
        int dayOfWeek = calendar.get(Calendar.DAY_OF_MONTH);
        if (dayOfWeek < 26) { // get last 30 days from day 26 of last month
            // do nothing
//            calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - 1);
        } else { // get next 30 days from day 26 of this month

            calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) + 1);
        }
        currentMonth = calendar.get(Calendar.MONTH);
        if (!loadMusicStatus()) {
            iv_music.setAlpha(1.0f);
            if ((currentMonth % 2) == 0) { // tháng lẻ
                utils.playMusic(R.raw.card_phuong_dong_dao);
            } else { // tháng chẵn
                utils.playMusic(R.raw.card_phuong_tay_dao);
            }
        } else {
            iv_music.setAlpha(0.3f);
        }

        String titleTeam = null;
        switch (getCurrentLanguage().getLanguage()) {
            case "vi":
                titleTeam = prefs.getString(TITLE_VI, null);
                break;
            case "en":
                titleTeam = prefs.getString(TITLE_EN, null);
                break;
            default:
                titleTeam = prefs.getString(TITLE, null);
                break;
        }
        if (TextUtils.isEmpty(titleTeam)) {
            boolean isPaid = prefs.getBoolean(IS_PAID_MONTH, false); // check before reset data
            saveCurrentMonthAndCard();
            if (isPaid) { // re-set true data
                SharedPreferences.Editor editor = prefs.edit();
                editor.putBoolean(IS_PAID_MONTH, true);
                editor.apply();
            }

        }

        int storedMonth = prefs.getInt(CURRENT_MONTH, -1);
        if (storedMonth == currentMonth) {
            loadCurrentMonthAndCard();
        } else {
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString(TITLE, null);
            editor.putString(SENTENCES, null);
            editor.putString(TITLE_VI, null);
            editor.putString(SENTENCES_VI, null);
            editor.putString(TITLE_EN, null);
            editor.putString(SENTENCES_EN, null);
            editor.putString(TITLE_TEAM, null);
            editor.putString(TITLE_TEAM_VI, null);
            editor.putString(TITLE_TEAM_EN, null);
            editor.apply();
            saveCurrentMonthAndCard();
        }

        //SonLA_Thêm logic cho lumiere mode - S
        if (gameType != PlayCardType.MONTH) {
            tv_lumiere_description.setAlpha(0.0f);
            String date = DateTimeHelper.STRINGFROMDATE(DateTimeHelper.TODAY(), getCurrentLanguage());
            tv_lumiere_user.setText(app.user.user_name + "     " + date);

            AppPreferencesHelper preferencesHelper = new AppPreferencesHelper(getApplicationContext(), AppConstants.PREF_NAME);
            String yourQuestion = preferencesHelper.getYourQuestion();
            if (yourQuestion == null) {
                tv_lumiere_question.setVisibility(View.GONE);
            } else {
                tv_lumiere_question.setText(yourQuestion);
            }

            if (gameType == PlayCardType.LUMIERE_TODAY || gameType == PlayCardType.LUMIERE_RANDOM) {
                long sToday = getTodayTimeMili();
                SharedPreferences.Editor editor = prefs.edit();
                editor.putLong(KEY_CURRENT_DAY_LUMIERE_TODAY, sToday);
                editor.apply();
            }

            initLumiereIndexArray();
            ll_lumiere_share.setVisibility(View.VISIBLE);
        } else {
            tv_lumiere_description.setVisibility(View.GONE);
            tv_lumiere_question.setVisibility(View.GONE);
            ll_lumiere_share.setVisibility(View.GONE);
        }
        //SonLA_Thêm logic cho lumiere mode - E
    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        utils.stopMusic();
    }

    //SonLA_Thêm logic cho lumiere mode - S
    private void showLumiereCards() {
        //0-H, 1-A, 2-F, 3-C, 4-E, 5-G, 6-D, 7-I, 8-B
        if (!selectedRandomIndexs.contains(0)) {
            iv_harmonie.setVisibility(View.INVISIBLE);
            iv_harmonie_trans.setVisibility(View.INVISIBLE);
            tv_h.setVisibility(View.INVISIBLE);
            tv_armonie.setVisibility(View.INVISIBLE);

            imageLumiereCardShow[0] = "-1";//SonLA_default value to -1 để avoid case bốc đc lá Matka = 0
            isHarmonieFlip = true;
        }
        if (!selectedRandomIndexs.contains(1)) {
            iv_aventure.setVisibility(View.INVISIBLE);
            iv_aventure_trans.setVisibility(View.INVISIBLE);
            tv_a.setVisibility(View.INVISIBLE);
            tv_venture.setVisibility(View.INVISIBLE);

            imageLumiereCardShow[1] = "-1";
            isAventureFlip = true;
        }
        if (!selectedRandomIndexs.contains(2)) {
            iv_fruit.setVisibility(View.INVISIBLE);
            iv_fruit_trans.setVisibility(View.INVISIBLE);
            tv_f.setVisibility(View.INVISIBLE);
            tv_ruit.setVisibility(View.INVISIBLE);

            imageLumiereCardShow[2] = "-1";
            isFruitFlip = true;
        }
        if (!selectedRandomIndexs.contains(3)) {
            iv_complicate.setVisibility(View.INVISIBLE);
            iv_complicate_trans.setVisibility(View.INVISIBLE);
            tv_c.setVisibility(View.INVISIBLE);
            tv_omplicate.setVisibility(View.INVISIBLE);

            imageLumiereCardShow[3] = "-1";
            isComplicateFlip = true;
        }
        if (!selectedRandomIndexs.contains(4)) {
            iv_energy.setVisibility(View.INVISIBLE);
            iv_energy_trans.setVisibility(View.INVISIBLE);
            tv_e.setVisibility(View.INVISIBLE);
            tv_nergy.setVisibility(View.INVISIBLE);

            imageLumiereCardShow[4] = "-1";
            isEnergyFlip = true;
        }
        if (!selectedRandomIndexs.contains(5)) {
            iv_guerre.setVisibility(View.INVISIBLE);
            iv_guerre_trans.setVisibility(View.INVISIBLE);
            tv_g.setVisibility(View.INVISIBLE);
            tv_uerre.setVisibility(View.INVISIBLE);

            imageLumiereCardShow[5] = "-1";
            isGuerreFlip = true;
        }
        if (!selectedRandomIndexs.contains(6)) {
            iv_desire.setVisibility(View.INVISIBLE);
            iv_desire_trans.setVisibility(View.INVISIBLE);
            tv_d.setVisibility(View.INVISIBLE);
            tv_esir.setVisibility(View.INVISIBLE);

            imageLumiereCardShow[6] = "-1";
            isDesireFlip = true;
        }
        if (!selectedRandomIndexs.contains(7)) {
            iv_intution.setVisibility(View.INVISIBLE);
            iv_intution_trans.setVisibility(View.INVISIBLE);
            tv_i.setVisibility(View.INVISIBLE);
            tv_ntution.setVisibility(View.INVISIBLE);

            imageLumiereCardShow[7] = "-1";
            isIntutionFlip = true;
        }
        if (!selectedRandomIndexs.contains(8)) {
            iv_besoin.setVisibility(View.INVISIBLE);
            iv_besoin_trans.setVisibility(View.INVISIBLE);
            tv_b.setVisibility(View.INVISIBLE);
            tv_esoin.setVisibility(View.INVISIBLE);

            imageLumiereCardShow[8] = "-1";
            isBesoinFlip = true;
        }

        ll_month_sensage.setVisibility(View.GONE);
        UiHelper.TEXTSIZE(tv_lumiere_description, 60);
    }

    private void initLumiereIndexArray() {
        rowsIdentity = new RowsIdentity();

        if (gameType == PlayCardType.PROFILE) {
            selectedRandomIndexs = new ArrayList<Integer>(Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8));
            cardsLumiere = app.cardMng.getProfileCard(selectedRandomIndexs.size());
        } else {
            //index with or without Energie row
            ArrayList<ArrayList<Integer>> indexArr = new ArrayList<>();
            if (gameType == PlayCardType.LUMIERE_RANDOM || gameType == PlayCardType.LUMIERE_RANDOM_ALL) {
                indexArr.add(new ArrayList<Integer>(Arrays.asList(1, 2, 4, 5, 6)));
                indexArr.add(new ArrayList<Integer>(Arrays.asList(0, 1, 3, 4, 8)));
                indexArr.add(new ArrayList<Integer>(Arrays.asList(0, 4, 5, 7, 8)));
                indexArr.add(new ArrayList<Integer>(Arrays.asList(2, 3, 4, 6, 7)));
                indexArr.add(new ArrayList<Integer>(Arrays.asList(1, 3, 5, 6, 8)));
                indexArr.add(new ArrayList<Integer>(Arrays.asList(1, 2, 3, 7, 8)));
                indexArr.add(new ArrayList<Integer>(Arrays.asList(0, 2, 3, 5, 7)));
                indexArr.add(new ArrayList<Integer>(Arrays.asList(0, 1, 5, 6, 7)));
                indexArr.add(new ArrayList<Integer>(Arrays.asList(0, 2, 4, 6, 8)));
                indexArr.add(new ArrayList<Integer>(Arrays.asList(1, 2, 3, 5, 7, 6)));
                indexArr.add(new ArrayList<Integer>(Arrays.asList(0, 1, 3, 5, 7, 8)));
                indexArr.add(new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4, 6, 8)));
                indexArr.add(new ArrayList<Integer>(Arrays.asList(0, 1, 4, 5, 6, 8)));
                indexArr.add(new ArrayList<Integer>(Arrays.asList(0, 2, 3, 4, 7, 8)));
                indexArr.add(new ArrayList<Integer>(Arrays.asList(0, 2, 4, 5, 6, 7)));
            }
            //index with Energie row only
            else if (gameType == PlayCardType.LUMIERE_TODAY) {
                indexArr.add(new ArrayList<Integer>(Arrays.asList(1, 2, 4, 5, 6)));
                indexArr.add(new ArrayList<Integer>(Arrays.asList(0, 1, 3, 4, 8)));
                indexArr.add(new ArrayList<Integer>(Arrays.asList(0, 4, 5, 7, 8)));
                indexArr.add(new ArrayList<Integer>(Arrays.asList(2, 3, 4, 6, 7)));
                indexArr.add(new ArrayList<Integer>(Arrays.asList(0, 2, 4, 6, 8)));
                indexArr.add(new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4, 6, 8)));
                indexArr.add(new ArrayList<Integer>(Arrays.asList(0, 1, 4, 5, 6, 8)));
                indexArr.add(new ArrayList<Integer>(Arrays.asList(0, 2, 3, 4, 7, 8)));
                indexArr.add(new ArrayList<Integer>(Arrays.asList(0, 2, 4, 5, 6, 7)));
            }

            totalLumiereIndexs = indexArr;
            Random r = new Random();
            int index = r.nextInt(totalLumiereIndexs.size() - 1);
            if (gameType == PlayCardType.LUMIERE_TODAY) {
                long timeToday = getTodayTimeMili();
                long timeCached = prefs.getLong(KEY_CURRENT_DAY_LUMIERE_TODAY, -1);
                if (timeToday == timeCached) {
                    int indexCached = prefs.getInt(KEY_LUMIERE_TODAY_INDEX, -1);
                    if (indexCached == -1) {
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putInt(KEY_LUMIERE_TODAY_INDEX, index);
                        //reset lại cached index + defi row + defi card
                        editor.remove(KEY_GET_PH_INDEXS);
                        editor.remove(KEY_GET_ALL_ROWS_DEFI);
                        editor.remove(KEY_GET_ROW_DEFI_INDEXS);
                        editor.remove(KEY_GET_CARD_DEFI_INDEXS);
                        editor.apply();
                    } else {
                        index = indexCached;
                    }
                } else {
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putInt(KEY_LUMIERE_TODAY_INDEX, index);
                    //reset lại cached index + defi row + defi card
                    editor.remove(KEY_GET_PH_INDEXS);
                    editor.remove(KEY_GET_ALL_ROWS_DEFI);
                    editor.remove(KEY_GET_ROW_DEFI_INDEXS);
                    editor.remove(KEY_GET_CARD_DEFI_INDEXS);
                    editor.apply();
                }
            } else if (gameType == PlayCardType.LUMIERE_RANDOM) {
                if (app.cardMng.isResetRandomIndex) {
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putInt(KEY_LUMIERE_RANDOM_INDEX, index);
                    //reset lại cached index + defi row + defi card
                    editor.remove(KEY_GET_PH_INDEXS);
                    editor.remove(KEY_GET_ALL_ROWS_DEFI);
                    editor.remove(KEY_GET_ROW_DEFI_INDEXS);
                    editor.remove(KEY_GET_CARD_DEFI_INDEXS);
                    editor.apply();
                } else {
                    int indexCached = prefs.getInt(KEY_LUMIERE_RANDOM_INDEX, -1);
                    if (indexCached == -1) {
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putInt(KEY_LUMIERE_RANDOM_INDEX, index);
                        //reset lại cached index + defi row + defi card
                        editor.remove(KEY_GET_PH_INDEXS);
                        editor.remove(KEY_GET_ALL_ROWS_DEFI);
                        editor.remove(KEY_GET_ROW_DEFI_INDEXS);
                        editor.remove(KEY_GET_CARD_DEFI_INDEXS);
                        editor.apply();
                    } else {
                        index = indexCached;
                    }
                }
            }
            selectedRandomIndexs = totalLumiereIndexs.get(index);
            cardsLumiere = app.cardMng.getLumiereCard(selectedRandomIndexs.size(), gameType);
        }
        showLumiereCards();
        if (gameType == PlayCardType.PROFILE) {
            calculateProfilePoint();
        } else {
            calculateLumierePoint();
        }
    }

    private long getTodayTimeMili() {
        Calendar todayCalendar = Calendar.getInstance(TimeZone.getDefault());
        todayCalendar.setTime(new Date());
        int y = todayCalendar.get(Calendar.YEAR);
        int m = todayCalendar.get(Calendar.MONTH);
        int d = todayCalendar.get(Calendar.DAY_OF_MONTH);
        todayCalendar = Calendar.getInstance(TimeZone.getTimeZone("UTC+0"));
        todayCalendar.set(Calendar.YEAR, y);
        todayCalendar.set(Calendar.MONTH, m);
        todayCalendar.set(Calendar.DAY_OF_MONTH, d);
        todayCalendar.set(Calendar.HOUR_OF_DAY, 0);
        todayCalendar.set(Calendar.MINUTE, 0);
        todayCalendar.set(Calendar.SECOND, 0);
        todayCalendar.set(Calendar.MILLISECOND, 0);
        long s = todayCalendar.getTimeInMillis();
        return s;
    }

    private void setLumiereCardsByIndex(int index, byte cardId, boolean isReverse) {
        switch (index) {
            case 0:
                harmonie = cardId;
                isReverseHarmonie = isReverse;
                break;
            case 1:
                aventure = cardId;
                isReverseAventure = isReverse;
                break;
            case 2:
                fruit = cardId;
                isReverseFruit = isReverse;
                break;
            case 3:
                complicate = cardId;
                isReverseComplicate = isReverse;
                break;
            case 4:
                energy = cardId;
                isReverseEnergy = isReverse;
                break;
            case 5:
                guerre = cardId;
                isReverseGuerre = isReverse;
                break;
            case 6:
                desire = cardId;
                isReverseDesire = isReverse;
                break;
            case 7:
                intution = cardId;
                isReverseIntution = isReverse;
                break;
            case 8:
                besoin = cardId;
                isReverseBesoin = isReverse;
                break;
            default:
                break;
        }
    }

    private void setCached(ArrayList<Integer> list, String withKey) {
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < list.size(); i++) {
            if (i == list.size() - 1) {
                str.append(String.valueOf(list.get(i)));
            } else {
                str.append(String.valueOf(list.get(i)) + ",");
            }
        }
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(withKey, str.toString());
        editor.apply();
    }

    private ArrayList<Integer> getCached(String withKey) {
        String savedStr = prefs.getString(withKey, "");
        ArrayList<Integer> savedList = new ArrayList<>();
        if (savedStr == "") {
            return savedList;
        } else {
            String[] separated = savedStr.split(",");
            for (int i = 0; i <= separated.length-1; i++) {
                savedList.add(Integer.parseInt(separated[i]));
            }
            return savedList;
        }
    }

    private void calculateProfilePoint() {
        double totalChoc = 0;
        double totalCharm = 0;
        double totalYin = 0;
        double totalYang = 0;

        ArrayList<String> finalStr = new ArrayList<>();
        ArrayList<LumiereDescription> descStr = new ArrayList<>();
        for (int i = 0; i < selectedRandomIndexs.size(); i++) {
            for (int j = 0; j < rowsIdentity.list.size(); j++) {
                if (rowsIdentity.list.get(j).id == selectedRandomIndexs.get(i)) {
                    //thêm chữ cái đầu của row + cardid
                    //ô fruit có card 21 = F21, ô energie có card 14 + subcard 22 = E14(22)
                    String eachStr = "";
                    String checkMinus = "";

                    RowIdentity currentRow = rowsIdentity.list.get(j);
                    Card currentCard = cardsLumiere.get(i);

                    /*
                    Check suffix của lá bài
                    Nếu mà lá cái bóng đã được rút ở subId trước đó mà vẫn rút lại phải lá cái bóng ở mainId thì sẽ thêm ˚ vào sau lá mainId
                     */
                    String suffix = "";
                    for (int strIndex = 0; strIndex < finalStr.size(); strIndex++) {
                        /*
                        Cắt chuỗi nếu mà trước đó có subId, ví dụ B-21(15) => lấy số 15
                         */
                        if (finalStr.get(strIndex).contains("(")) {
                            String[] separated = finalStr.get(strIndex).split("\\(");
                            if (separated.length >= 1) {
                                if (separated[1].substring(0, separated[1].length() - 1).equals(String.valueOf(currentCard.number))) {//Case số bình thường
                                    suffix = "˚";
                                } else if (separated[1].substring(0, separated[1].length() - 1).equals("M") && currentCard.number == 0) {//Case matka
                                    suffix = "˚";
                                }
                            }
                        }
                    }

                    if (currentCard.isLumiereReverse) {
                        eachStr = currentRow.name.substring(0, 1).toUpperCase() + "-" + currentCard.number;
                        checkMinus = "minus_";
                    } else {
                        String cardDesc = currentCard.number == 0 ? "M" : String.valueOf(currentCard.number);
                        eachStr = currentRow.name.substring(0, 1).toUpperCase() + cardDesc;
                    }

                    //nếu là đảo ngược thì thêm cái bóng khiến lá bài đảo ngược vào phía sau
                    if (!String.valueOf(currentCard.subId).equals("") && currentCard.isLumiereReverse) {
                        eachStr += currentCard.subId == 0 ? "(M)" : String.format("(%s)", String.valueOf(currentCard.subId));
                    }

                    eachStr += suffix;
                    finalStr.add(eachStr);
                    Log.i("Row description:", eachStr);

                    //tính điểm choc/charme
                    if (currentCard.type == Card.CHOC) {
                        totalChoc += currentRow.score;
                    } else if (currentCard.type == Card.CHARME) {
                        totalCharm += currentRow.score;
                    } else if (currentCard.type == Card.MATKA) {
                    } else if (currentCard.type == Card.CHOCCHARME) {
                        totalChoc += currentRow.score;
                        totalCharm += currentRow.score;
                    }

                    //tỉnh điểm yin/yang
                    if (!currentCard.isReverseYinYang) {
                        if (currentCard.number % 2 == 0) {
                            totalYin += currentRow.score;
                        } else {
                            totalYang += currentRow.score;
                        }
                    } else {
                        if (currentCard.number % 2 == 0) {
                            totalYang += currentRow.score;
                        } else {
                            totalYin += currentRow.score;
                        }
                    }

                    //get text lumiere
                    String textLink = "lumiere";
                    String lang = getCurrentLanguage().getLanguage();
                    if (lang == "vi") {
                        textLink += "_vi";
                    }

                    Random r = new Random();
                    int random = -1;
                    random = r.nextInt(2) + 1;

                    String defiStr = "";
                    String[] rowStr = Utils.GetLines(this.getAssets(),
                            textLink + "/ph" + random + "/" + currentRow.name.substring(0, 1).toUpperCase() + ".txt");
                    ;
                    String[] cardStr = null;
                    if (random == 1) {
                        cardStr = Utils.GetLines(this.getAssets(),
                                "facebook" + (lang == "vi" ? "_vi" : "")
                                        + "/flamma/" + checkMinus + currentCard.number);
                    } else {
                        cardStr = Utils.GetLines(this.getAssets(),
                                textLink + "/flamma/" + checkMinus + currentCard.number + ".txt");
                    }

                    if (rowStr != null && rowStr.length > 0) {
                        int randomIndex = -1;
                        randomIndex = random == 1 ? r.nextInt(3) : r.nextInt(5);
                        defiStr = rowStr[randomIndex] + " ";
                    }

                    if (cardStr != null && cardStr.length > 0) {
                        int randomIndex = -1;
                        randomIndex = r.nextInt(3);

                        String str = cardStr[randomIndex];
                        if (str.contains("!")) {
                            str = str.replace(" !", ".");
                        } else {
                            str += ".";
                        }
                        defiStr += str + "\n";
                    }

                    setLumiereCardsByIndex(selectedRandomIndexs.get(i), currentCard.number, currentCard.isLumiereReverse);
                    descStr.add(new LumiereDescription(random, defiStr));
                    break;
                }
            }
        }
        if (totalChoc == 0) {
            totalChoc = 1;
        }
        if (totalCharm == 0) {
            totalCharm = 1;
        }
        if (totalYin == 0) {
            totalYin = 1;
        }
        if (totalYang == 0) {
            totalYang = 1;
        }

        int charmePercent = (int) (totalCharm / (totalChoc + totalCharm) * 100);
        int chocPercent = 100 - charmePercent;
        int yinPercent = (int) (totalYin / (totalYin + totalYang) * 100);
        int yangPercent = 100 - yinPercent;

        Collections.shuffle(finalStr);

        StringBuilder cardFinalStr = new StringBuilder();
        for (int i = 0; i < finalStr.size(); i++) {
            cardFinalStr.append(finalStr.get(i) + ". ");
        }

        String charmeChocStr = "\nCharme " + charmePercent + "%, Choc " + chocPercent + "%\n";
        String yinYangStr = "Yin " + yinPercent + "%, Yang " + yangPercent + "%\n\n";
        Collections.sort(descStr, new Comparator<LumiereDescription>() {
            @Override
            public int compare(LumiereDescription l1, LumiereDescription l2) {
                return l1.compareTo(l2);
            }
        });

        StringBuilder lumiereFinalStr = new StringBuilder();
        for (int i = 0; i < descStr.size(); i++) {
            lumiereFinalStr.append(descStr.get(i).desc);
        }
        String lumiereDesc = cardFinalStr.toString() + charmeChocStr + yinYangStr + lumiereFinalStr.toString();
        tv_lumiere_description.setText(lumiereDesc);
        strDescriptionContent4Share = lumiereFinalStr.toString() + "\n" + cardFinalStr.toString() + charmeChocStr + yinYangStr;
    }

    private void calculateLumierePoint() {
        //nếu là lá bài today thì lá ở ô Energie sẽ là lá hôm nay
        //nếu là today thì sẽ lật ô energie còn random 1 lá thì sẽ lật lá bài đầu tiên
        if (gameType == PlayCardType.LUMIERE_TODAY) {
            energieIndex = selectedRandomIndexs.indexOf(4);//lấy index của energie để swap element in array
            Collections.swap(cardsLumiere, 0, energieIndex);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    showCardAtIndex(4);
                }
            }, 500);
        } else if (gameType == PlayCardType.LUMIERE_RANDOM) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    showCardAtIndex(selectedRandomIndexs.get(0));
                }
            }, 500);
        }
        double totalChoc = 0;
        double totalCharm = 0;
        double totalYin = 0;
        double totalYang = 0;

        boolean isGetIndexsFromCached = false;
        ArrayList<Integer> cachedPathIndexs = new ArrayList<>();
        ArrayList<Integer> cachedRowDefiIndexs = new ArrayList<>();
        ArrayList<Integer> cachedCardDefiIndexs = new ArrayList<>();

        if (gameType != PlayCardType.PROFILE) {
            if (getCached(KEY_GET_PH_INDEXS).size() > 0) {
                isGetIndexsFromCached = true;
                cachedPathIndexs = getCached(KEY_GET_PH_INDEXS);
                cachedRowDefiIndexs = getCached(KEY_GET_ROW_DEFI_INDEXS);
                cachedCardDefiIndexs = getCached(KEY_GET_CARD_DEFI_INDEXS);
            }
        }

        ArrayList<String> finalStr = new ArrayList<>();
        ArrayList<LumiereDescription> descStr = new ArrayList<>();
        for (int i = 0; i < selectedRandomIndexs.size(); i++) {
            for (int j = 0; j < rowsIdentity.list.size(); j++) {
                if (rowsIdentity.list.get(j).id == selectedRandomIndexs.get(i)) {
                    //thêm chữ cái đầu của row + cardid
                    //ô fruit có card 21 = F21, ô energie có card 14 + subcard 22 = E14(22)
                    String eachStr = "";
                    String checkMinus = "";

                    RowIdentity currentRow = rowsIdentity.list.get(j);
                    Card currentCard = cardsLumiere.get(i);

                    //nếu cardid có trong dimensions thì thêm *
                    if (currentCard.isLumiereReverse) {
                        eachStr = currentRow.name.substring(0, 1).toUpperCase() + "-" + currentCard.number;
                        if (currentRow.dimensions.contains(-currentCard.number)) {
                            eachStr += "*";
                        }
                        checkMinus = "minus_";
                    } else {
                        String cardDesc = currentCard.number == 0 ? "M" : String.valueOf(currentCard.number);
                        eachStr = currentRow.name.substring(0, 1).toUpperCase() + cardDesc;
                        if (currentRow.dimensions.contains(currentCard.number)) {
                            eachStr += "*";
                        }
                    }

                    //nếu là đảo ngược thì thêm cái bóng khiến lá bài đảo ngược vào phía sau
                    if (!String.valueOf(currentCard.subId).equals("") && currentCard.isLumiereReverse) {
                        eachStr += currentCard.subId == 0 ? "(M)" : String.format("(%s)", String.valueOf(currentCard.subId));
                    }
                    finalStr.add(eachStr);

                    //tính điểm choc/charme
                    if (currentCard.type == Card.CHOC) {
                        if (eachStr.contains("*")) {
                            totalChoc += 1;
                        }
                        totalChoc += currentRow.score;
                    } else if (currentCard.type == Card.CHARME) {
                        if (eachStr.contains("*")) {
                            totalCharm += 1;
                        }
                        totalCharm += currentRow.score;
                    } else if (currentCard.type == Card.MATKA) {
                    } else if (currentCard.type == Card.CHOCCHARME) {
                        if (eachStr.contains("*")) {
                            totalChoc += 1;
                            totalCharm += 1;
                        }
                        totalChoc += currentRow.score;
                        totalCharm += currentRow.score;
                    }

                    //tỉnh điểm yin/yang
                    if (!currentCard.isReverseYinYang) {
                        if (currentCard.number % 2 == 0) {
                            if (eachStr.contains("*")) {
                                totalYin += 1;
                            }
                            totalYin += currentRow.score;
                        } else {
                            if (eachStr.contains("*")) {
                                totalYang += 1;
                            }
                            totalYang += currentRow.score;
                        }
                    } else {
                        if (currentCard.number % 2 == 0) {
                            if (eachStr.contains("*")) {
                                totalYang += 1;
                            }
                            totalYang += currentRow.score;
                        } else {
                            if (eachStr.contains("*")) {
                                totalYin += 1;
                            }
                            totalYin += currentRow.score;
                        }
                    }

                    //get text lumiere
                    String textLink = "lumiere";
                    String lang = getCurrentLanguage().getLanguage();
                    if (lang == "vi") {
                        textLink += "_vi";
                    }

                    Random r = new Random();
                    int random = -1;
                    if (isGetIndexsFromCached) {
                        random = cachedPathIndexs.get(i);
                    } else {
                        random = r.nextInt(2) + 1;
                        cachedPathIndexs.add(random);
                    }

                    String defiStr = "";
                    String[] rowStr = Utils.GetLines(this.getAssets(),
                            textLink + "/ph" + random + "/" + currentRow.name.substring(0, 1).toUpperCase() + ".txt");
                    ;
                    String[] cardStr = null;
                    if (random == 1) {
                        cardStr = Utils.GetLines(this.getAssets(),
                                "facebook" + (lang == "vi" ? "_vi" : "")
                                        + "/flamma/" + checkMinus + currentCard.number);
                    } else {
                        cardStr = Utils.GetLines(this.getAssets(),
                                textLink + "/flamma/" + checkMinus + currentCard.number + ".txt");
                    }

                    if (rowStr != null && rowStr.length > 0) {
                        int randomIndex = -1;
                        if (isGetIndexsFromCached) {
                            randomIndex = cachedRowDefiIndexs.get(i);
                        } else {
                            randomIndex = random == 1 ? r.nextInt(3) : r.nextInt(5);
                            cachedRowDefiIndexs.add(randomIndex);
                        }
                        defiStr = rowStr[randomIndex] + " ";
                    }

                    if (cardStr != null && cardStr.length > 0) {
                        int randomIndex = -1;
                        if (isGetIndexsFromCached) {
                            randomIndex = cachedCardDefiIndexs.get(i);
                        } else {
                            randomIndex = r.nextInt(3);
                            cachedCardDefiIndexs.add(randomIndex);
                        }

                        String str = cardStr[randomIndex];
                        if (str.contains("!")) {
                            str = str.replace(" !", ".");
                        } else {
                            str += ".";
                        }
                        defiStr += str + "\n";
                    }

                    setLumiereCardsByIndex(selectedRandomIndexs.get(i), currentCard.number, currentCard.isLumiereReverse);
                    descStr.add(new LumiereDescription(random, defiStr));
                    break;
                }
            }
        }
        if (totalChoc == 0) {
            totalChoc = 1;
        }
        if (totalCharm == 0) {
            totalCharm = 1;
        }
        if (totalYin == 0) {
            totalYin = 1;
        }
        if (totalYang == 0) {
            totalYang = 1;
        }

        int charmePercent = (int) (totalCharm / (totalChoc + totalCharm) * 100);
        int chocPercent = 100 - charmePercent;
        int yinPercent = (int) (totalYin / (totalYin + totalYang) * 100);
        int yangPercent = 100 - yinPercent;

        if (gameType == PlayCardType.LUMIERE_RANDOM_ALL || gameType == PlayCardType.PROFILE) {
            //trộn tất cả mô tả lá bài
            Collections.shuffle(finalStr);
        } else {
            //cached lại mô tả cho lumiere để lần sau không bốc random nữa
            setCached(cachedPathIndexs, KEY_GET_PH_INDEXS);
            setCached(cachedRowDefiIndexs, KEY_GET_ROW_DEFI_INDEXS);
            setCached(cachedCardDefiIndexs, KEY_GET_CARD_DEFI_INDEXS);

            //nếu là today thì lá today sẽ được xếp lên đầu, còn lại sẽ trộn
            //nếu là random 1 lá thì lá index = 0 sẽ sếp desc lên đầu, còn lại sẽ trộn
            String todayStr = finalStr.get(energieIndex);
            finalStr.remove(energieIndex);
            Collections.shuffle(finalStr);
            finalStr.add(0, todayStr);
        }

        StringBuilder cardFinalStr = new StringBuilder();
        if (gameType == PlayCardType.LUMIERE_RANDOM_ALL || gameType == PlayCardType.PROFILE) {
            for (int i = 0; i < finalStr.size(); i++) {
                cardFinalStr.append(finalStr.get(i) + ". ");
            }
        } else {
            String cachedFinalStr = prefs.getString(KEY_GET_ALL_ROWS_DEFI, "");
            if (!cachedFinalStr.isEmpty()) {
                cardFinalStr.append(cachedFinalStr);
            } else {
                for (int i = 0; i < finalStr.size(); i++) {
                    cardFinalStr.append(finalStr.get(i) + ". ");
                }
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString(KEY_GET_ALL_ROWS_DEFI, cardFinalStr.toString());
                editor.apply();
            }
        }

        String charmeChocStr = "\nCharme " + charmePercent + "%, Choc " + chocPercent + "%\n";
        String yinYangStr = "Yin " + yinPercent + "%, Yang " + yangPercent + "%\n\n";
        Collections.sort(descStr, new Comparator<LumiereDescription>() {
            @Override
            public int compare(LumiereDescription l1, LumiereDescription l2) {
                return l1.compareTo(l2);
            }
        });

        StringBuilder lumiereFinalStr = new StringBuilder();
        for (int i = 0; i < descStr.size(); i++) {
            lumiereFinalStr.append(descStr.get(i).desc);
        }
        String lumiereDesc = cardFinalStr.toString() + charmeChocStr + yinYangStr + lumiereFinalStr.toString();
        tv_lumiere_description.setText(lumiereDesc);
        strDescriptionContent4Share = lumiereFinalStr.toString() + "\n" + cardFinalStr.toString() + charmeChocStr + yinYangStr;
    }

    private void showCardAtIndex(int index) {
        switch (index) {
            case 0:
                onClick(iv_harmonie_trans);
                break;
            case 1:
                onClick(iv_aventure_trans);
                break;
            case 2:
                onClick(iv_fruit_trans);
                break;
            case 3:
                onClick(iv_complicate_trans);
                break;
            case 4:
                onClick(iv_energy_trans);
                break;
            case 5:
                onClick(iv_guerre_trans);
                break;
            case 6:
                onClick(iv_desire_trans);
                break;
            case 7:
                onClick(iv_intution_trans);
                break;
            case 8:
                onClick(iv_besoin_trans);
                break;
        }
    }
    //SonLA_Thêm logic cho lumiere mode - E

    private Card getEnergyCard() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getDefault());

        Card[] monthCard;
        int dayOfWeek = calendar.get(Calendar.DAY_OF_MONTH);
        if (dayOfWeek < 26) { // get last 30 days from day 26 of last month
//            calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - 1);
            // do nothing

        } else { // get next 30 days from day 26 of this month
            calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) + 1);
        }

        int totalDayInMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        monthCard = new Card[totalDayInMonth];
        calendar.set(Calendar.DAY_OF_MONTH, 1); // startDay

        for (int i = 0; i < totalDayInMonth; i++) {
            calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR));
            calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH));
            monthCard[i] = getMonthCard(calendar.getTime());
            Log.e("MonthCardActivity", "=== monthCard[i].number === " + i + " = " + monthCard[i].number);
            // end of line, increase to next day
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        }

        List<Byte> list = new ArrayList<Byte>();
        for (int i = 0; i < monthCard.length; i++) {
            list.add(monthCard[i].number);
        }

        // count FIRST most card in month
//        Map.Entry<Byte, Integer> maxEntry = null;
        Map<Byte, Integer> unsortMap = new HashMap<Byte, Integer>();
        for (Byte temp : list) {
            Integer count = unsortMap.get(temp);
            unsortMap.put(temp, (count == null) ? 1 : count + 1);
        }

        Map<Byte, Integer> sortedMap = sortByComparator(unsortMap, false);

        List<Byte> listAllCardInMonth = new ArrayList<>();
        for (Map.Entry<Byte, Integer> key : sortedMap.entrySet()) {
            listAllCardInMonth.add(key.getKey());
        }
        Card energyCard = null;
        if (listAllCardInMonth.size() > 0) {
            energyCard = app.cardMng.getCardByNumber(listAllCardInMonth.get(0));
        }
        if (listAllCardInMonth.size() > 9) {
            for (int i = 1; i < 9; i++) {
                listEightRemainCard.add(listAllCardInMonth.get(i));
            }
        } else {
            for (int i = 1; i < listAllCardInMonth.size(); i++) {
                listEightRemainCard.add(listAllCardInMonth.get(i));
            }
        }

        if (listEightRemainCard.size() < 8) {
            int lackItem = 8 - listEightRemainCard.size();
            int nextRandomNumber;
            Random rd = new Random();
            boolean isDuplicate = false;
            for (int i = 0; i < lackItem; i++) {
                isDuplicate = false;
                while (!isDuplicate) {
                    nextRandomNumber = rd.nextInt(50) + 1;
                    if (!listEightRemainCard.contains((byte) nextRandomNumber)) {
                        listEightRemainCard.add((byte) nextRandomNumber);
                        isDuplicate = true;
                    }
                }
            }
        }

        return energyCard;
    }

    // Get each card in current month
    private Card getMonthCard(Date currentDate) {
        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        calendar.setTime(currentDate);
        int y = calendar.get(Calendar.YEAR);
        int m = calendar.get(Calendar.MONTH);
        int d = calendar.get(Calendar.DAY_OF_MONTH);
        calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC+0"));
        calendar.set(Calendar.YEAR, y);
        calendar.set(Calendar.MONTH, m);
        calendar.set(Calendar.DAY_OF_MONTH, d);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        long s = calendar.getTimeInMillis() + app.user.user_id * 1000;
        Log.e("MonthCardActivity", "=== getMonthCard() === calendar.getTime() " + calendar.getTime());
        Log.e("MonthCardActivity", "=== getMonthCard() === long s " + s);
        MyRandomSeed.seed(s);
        int id = MyRandomSeed.num(1, 50);
        Log.e("MonthCardActivity", "=== getMonthCard() === int id " + id);
        return app.cardMng.getCard(id);
    }

    private static Map<Byte, Integer> sortByComparator(Map<Byte, Integer> unsortMap, final boolean order) {
        List<Map.Entry<Byte, Integer>> list = new LinkedList<Map.Entry<Byte, Integer>>(unsortMap.entrySet());
        // Sorting the list based on values
        Collections.sort(list, new Comparator<Map.Entry<Byte, Integer>>() {
            public int compare(Map.Entry<Byte, Integer> o1,
                               Map.Entry<Byte, Integer> o2) {
                if (order) {
                    return o1.getValue().compareTo(o2.getValue());
                } else {
                    return o2.getValue().compareTo(o1.getValue());
                }
            }
        });
        // Maintaining insertion order with the help of LinkedList
        Map<Byte, Integer> sortedMap = new LinkedHashMap<Byte, Integer>();
        for (Map.Entry<Byte, Integer> entry : list) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }
        return sortedMap;
    }

    private void checkEnableSensageBtn() {
        if (gameType == PlayCardType.MONTH) {
            if (isAventureFlip && isBesoinFlip && isComplicateFlip && isDesireFlip && isEnergyFlip
                    && isFruitFlip && isGuerreFlip && isHarmonieFlip && isIntutionFlip) {
                btn_month_card_sensage.setClickable(true);
                btn_month_card_sensage.setAlpha(1.0f);
                btn_month_card_sensage.setEnabled(true);
            }
        } else {
            if (isAventureFlip && isBesoinFlip && isComplicateFlip && isDesireFlip && isEnergyFlip
                    && isFruitFlip && isGuerreFlip && isHarmonieFlip && isIntutionFlip) {
                tv_lumiere_description.setAlpha(1.0f);
                sv_container.post(new Runnable() {
                    @Override
                    public void run() {
                        sv_container.fullScroll(ScrollView.FOCUS_DOWN);
                    }
                });
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_harmonie_trans:
                iv_harmonie_trans.setVisibility(View.INVISIBLE);

                bm = app.cardMng.getCardByNumber(harmonie).getBitmap();
                bm = Bitmap.createScaledBitmap(bm, width, height, true);
                iv_harmonie.animate().rotationY(360).setDuration(400).start();
                iv_harmonie.setImageBitmap(bm);
                if (isReverseHarmonie) {
                    iv_harmonie.setScaleX(-1);
                    iv_harmonie.setScaleY(-1);
                }
                isHarmonieFlip = true;
                checkEnableSensageBtn();
                break;
            case R.id.iv_aventure_trans:
                iv_aventure_trans.setVisibility(View.INVISIBLE);

                bm = app.cardMng.getCardByNumber(aventure).getBitmap();
                bm = Bitmap.createScaledBitmap(bm, width, height, true);
                iv_aventure.animate().rotationY(360).setDuration(400).start();
                iv_aventure.setImageBitmap(bm);
                if (isReverseAventure) {
                    iv_aventure.setScaleX(-1);
                    iv_aventure.setScaleY(-1);
                }
                isAventureFlip = true;
                checkEnableSensageBtn();
                break;
            case R.id.iv_fruit_trans:
                iv_fruit_trans.setVisibility(View.INVISIBLE);

                bm = app.cardMng.getCardByNumber(fruit).getBitmap();
                bm = Bitmap.createScaledBitmap(bm, width, height, true);
                iv_fruit.animate().rotationY(360).setDuration(400).start();
                iv_fruit.setImageBitmap(bm);
                if (isReverseFruit) {
                    iv_fruit.setScaleX(-1);
                    iv_fruit.setScaleY(-1);
                }
                isFruitFlip = true;
                checkEnableSensageBtn();
                break;
            case R.id.iv_complicate_trans:
                iv_complicate_trans.setVisibility(View.INVISIBLE);

                bm = app.cardMng.getCardByNumber(complicate).getBitmap();
                bm = Bitmap.createScaledBitmap(bm, width, height, true);
                iv_complicate.animate().rotationY(360).setDuration(400).start();
                iv_complicate.setImageBitmap(bm);
                if (isReverseComplicate) {
                    iv_complicate.setScaleX(-1);
                    iv_complicate.setScaleY(-1);
                }
                isComplicateFlip = true;
                checkEnableSensageBtn();
                break;
            case R.id.iv_energy_trans:
                iv_energy_trans.setVisibility(View.INVISIBLE);

                bm = app.cardMng.getCardByNumber(energy).getBitmap();
                bm = Bitmap.createScaledBitmap(bm, width, height, true);
                iv_energy.animate().rotationY(360).setDuration(400).start();
                iv_energy.setImageBitmap(bm);
                if (isReverseEnergy) {
                    iv_energy.setScaleX(-1);
                    iv_energy.setScaleY(-1);
                }
                isEnergyFlip = true;
                checkEnableSensageBtn();
                break;
            case R.id.iv_guerre_trans:
                iv_guerre_trans.setVisibility(View.INVISIBLE);

                bm = app.cardMng.getCardByNumber(guerre).getBitmap();
                bm = Bitmap.createScaledBitmap(bm, width, height, true);
                iv_guerre.animate().rotationY(360).setDuration(400).start();
                iv_guerre.setImageBitmap(bm);
                if (isReverseGuerre) {
                    iv_guerre.setScaleX(-1);
                    iv_guerre.setScaleY(-1);
                }
                isGuerreFlip = true;
                checkEnableSensageBtn();
                break;
            case R.id.iv_desire_trans:
                iv_desire_trans.setVisibility(View.INVISIBLE);

                bm = app.cardMng.getCardByNumber(desire).getBitmap();
                bm = Bitmap.createScaledBitmap(bm, width, height, true);
                iv_desire.animate().rotationY(360).setDuration(400).start();
                iv_desire.setImageBitmap(bm);
                if (isReverseDesire) {
                    iv_desire.setScaleX(-1);
                    iv_desire.setScaleY(-1);
                }
                isDesireFlip = true;
                checkEnableSensageBtn();
                break;
            case R.id.iv_intution_trans:
                iv_intution_trans.setVisibility(View.INVISIBLE);

                bm = app.cardMng.getCardByNumber(intution).getBitmap();
                bm = Bitmap.createScaledBitmap(bm, width, height, true);
                iv_intution.animate().rotationY(360).setDuration(400).start();
                iv_intution.setImageBitmap(bm);
                if (isReverseIntution) {
                    iv_intution.setScaleX(-1);
                    iv_intution.setScaleY(-1);
                }
                isIntutionFlip = true;
                checkEnableSensageBtn();
                break;
            case R.id.iv_besoin_trans:
                iv_besoin_trans.setVisibility(View.INVISIBLE);

                bm = app.cardMng.getCardByNumber(besoin).getBitmap();
                bm = Bitmap.createScaledBitmap(bm, width, height, true);
                iv_besoin.animate().rotationY(360).setDuration(400).start();
                iv_besoin.setImageBitmap(bm);
                if (isReverseBesoin) {
                    iv_besoin.setScaleX(-1);
                    iv_besoin.setScaleY(-1);
                }
                isBesoinFlip = true;
                checkEnableSensageBtn();
                break;
            case R.id.btn_month_card_sensage:
                if (isNetworkStatusAvialable()) {
                    if (app.user.isLogin()) {
                        boolean isPaid = prefs.getBoolean(IS_PAID_MONTH, false);
                        if (isPaid) {
                            go(MonthCardResultActivity.class);
                        } else {
                            if (app.user.coin >= 50) {
                                app.user.submitCoin(-50, new HttpManager.OnResponse() {
                                    @Override
                                    public void onResponse(JSONObject jso) {
                                        app.user.updateCoinCount(jso);
                                        SharedPreferences.Editor editor = prefs.edit();
                                        editor.putBoolean(IS_PAID_MONTH, true);
                                        editor.apply();
                                        go(MonthCardResultActivity.class);
                                    }
                                });
                            } else {
                                go(MyCashActivity.class);
                            }
                        }

                    } else {
                        app.dlgLoginChoose = new LoginChooseDialog(this,this);
                        app.dlgLoginChoose.show();
                    }
                } else {
                    alert(getString(R.string.alert), getString(R.string.alert_offline), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                }

                // remember to clear result;
                resultTeamRevolte = 0;
                resultTeamZen = 0;
                resultTeamDestresse = 0;
                resultTeamCreation = 0;
                break;
            case R.id.iv_music:
                if (!loadMusicStatus()) {
                    saveMusicStatus(true);
                    iv_music.setAlpha(0.3f);
                    utils.stopMusic();
//                    utils.muteMusic();
                } else {
                    saveMusicStatus(false);
                    iv_music.setAlpha(1.0f);
                    if ((currentMonth % 2) == 0) { // tháng lẻ
                        utils.playMusic(R.raw.card_phuong_dong_dao);
                    } else { // tháng chẵn
                        utils.playMusic(R.raw.card_phuong_tay_dao);
                    }
//                    utils.unmuteMusic();
                }
                break;
            case R.id.iv_harmonie:
                showFullImage(harmonie, isReverseHarmonie);
                break;
            case R.id.iv_aventure:
                showFullImage(aventure, isReverseAventure);
                break;
            case R.id.iv_fruit:
                showFullImage(fruit, isReverseFruit);
                break;
            case R.id.iv_complicate:
                showFullImage(complicate, isReverseComplicate);
                break;
            case R.id.iv_energy:
                showFullImage(energy, isReverseEnergy);
                break;
            case R.id.iv_guerre:
                showFullImage(guerre, isReverseGuerre);
                break;
            case R.id.iv_desire:
                showFullImage(desire, isReverseDesire);
                break;
            case R.id.iv_intution:
                showFullImage(intution, isReverseIntution);
                break;
            case R.id.iv_besoin:
                showFullImage(besoin, isReverseBesoin);
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        boolean isPaid = prefs.getBoolean(IS_PAID_MONTH, false);
        if (isPaid) {
            ll_cash_sensage.setVisibility(View.INVISIBLE);
        } else {
            ll_cash_sensage.setVisibility(View.VISIBLE);
        }
    }

    private void showFullImage(Byte cardNum, boolean isRotate) {
        final Dialog d = new Dialog(this, R.style.Theme_Dialog);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = d.getWindow();
        window.setFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND, WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        window.setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(this, R.color.black_trans_50)));
        d.setContentView(R.layout.layout_full_image);
        d.setCancelable(true);
        d.setCanceledOnTouchOutside(true);

        TouchImageView iv_full_screen = (TouchImageView) d.findViewById(R.id.iv_full_screen);
        bm = app.cardMng.getCardByNumber(cardNum).getBitmap();
//                bm = Bitmap.createScaledBitmap(bm, width, height, true);
        iv_full_screen.setImageBitmap(bm);
        if (isRotate) {
            iv_full_screen.setScaleX(-1);
            iv_full_screen.setScaleY(-1);
        }
        ImageView iv_close_ic = (ImageView) d.findViewById(R.id.iv_close_ic);
        iv_close_ic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                d.dismiss();
            }
        });
        UiHelper.MARGIN(iv_close_ic, 0, 50, 50, 0);
        UiHelper.RESIZE(iv_close_ic, 100, 100);

        d.show();
    }

    private String getTitleResult() {
        // calculate Point for Team Title
        // H, D, F, B = 2 điểm
        calculatePoint(harmonie, isReverseHarmonie, 2);
        calculatePoint(desire, isReverseDesire, 2);
        calculatePoint(fruit, isReverseFruit, 2);
        calculatePoint(besoin, isReverseBesoin, 2);
        // A, C, G, I = 3 điểm
        calculatePoint(aventure, isReverseAventure, 3);
        calculatePoint(complicate, isReverseComplicate, 3);
        calculatePoint(guerre, isReverseGuerre, 3);
        calculatePoint(intution, isReverseIntution, 3);
        // E = 4 điểm.
        calculatePoint(energy, isReverseEnergy, 4);
        Log.e("Temp", "CREATION: " + resultTeamCreation);
        Log.e("Temp", "ZEN: " + resultTeamZen);
        Log.e("Temp", "DESTRESSE: " + resultTeamDestresse);
        Log.e("Temp", "REVOLTE: " + resultTeamRevolte);

        List<Integer> arrTeamResult = Arrays.asList(resultTeamCreation, resultTeamDestresse,
                resultTeamZen, resultTeamRevolte);

        int highestResult = Collections.max(arrTeamResult);
        String[] arrTitle;
        SharedPreferences.Editor editor = prefs.edit();
        if (highestResult == resultTeamCreation) {
            switch (getCurrentLanguage().getLanguage()) {
                case "vi":
                    arrTitle = Utils.GetLines(this.getAssets(), "month_group_word_vi/team_creation");
                    break;
                case "en":
                    arrTitle = Utils.GetLines(this.getAssets(), "month_group_word/team_creation");
                    break;
                case "ru":
                    arrTitle = Utils.GetLines(this.getAssets(), "month_group_word_ru/team_creation");
                    break;
                default:
                    arrTitle = Utils.GetLines(this.getAssets(), "month_group_word/team_creation");
                    break;
            }
            editor.putString(TITLE_TEAM, CREATION);
            editor.apply();
        } else if (highestResult == resultTeamDestresse) {
            switch (getCurrentLanguage().getLanguage()) {
                case "vi":
                    arrTitle = Utils.GetLines(this.getAssets(), "month_group_word_vi/team_destresse");
                    break;
                case "en":
                    arrTitle = Utils.GetLines(this.getAssets(), "month_group_word/team_destresse");
                    break;
                case "ru":
                    arrTitle = Utils.GetLines(this.getAssets(), "month_group_word_ru/team_destresse");
                    break;
                default:
                    arrTitle = Utils.GetLines(this.getAssets(), "month_group_word/team_destresse");
                    break;
            }
            editor.putString(TITLE_TEAM, SOUCI);
            editor.apply();
        } else if (highestResult == resultTeamRevolte) {
            switch (getCurrentLanguage().getLanguage()) {
                case "vi":
                    arrTitle = Utils.GetLines(this.getAssets(), "month_group_word_vi/team_revolte");
                    break;
                case "en":
                    arrTitle = Utils.GetLines(this.getAssets(), "month_group_word/team_revolte");
                    break;
                case "ru":
                    arrTitle = Utils.GetLines(this.getAssets(), "month_group_word_ru/team_revolte");
                    break;
                default:
                    arrTitle = Utils.GetLines(this.getAssets(), "month_group_word/team_revolte");
                    break;
            }
            editor.putString(TITLE_TEAM, REVOLTE);
            editor.apply();
        } else if (highestResult == resultTeamZen) {
            switch (getCurrentLanguage().getLanguage()) {
                case "vi":
                    arrTitle = Utils.GetLines(this.getAssets(), "month_group_word_vi/team_zen");
                    break;
                case "en":
                    arrTitle = Utils.GetLines(this.getAssets(), "month_group_word/team_zen");
                    break;
                case "ru":
                    arrTitle = Utils.GetLines(this.getAssets(), "month_group_word_ru/team_zen");
                    break;
                default:
                    arrTitle = Utils.GetLines(this.getAssets(), "month_group_word/team_zen");
                    break;
            }
            editor.putString(TITLE_TEAM, ZEN);
            editor.apply();
        } else {
            arrTitle = Utils.GetLines(this.getAssets(), "month_group_word/team_error");
            editor.putString(TITLE_TEAM, "Error!");
            editor.apply();
        }

        String finalTitle = arrTitle[rd.nextInt(arrTitle.length)];
        Log.e("Temp", "Title " + finalTitle);
        return finalTitle;
    }

    private String getFullSentences() {
        String wordHarmonie;
        String wordAventure;
        String wordFruit;
        String wordComplicate;
        String wordEnergy;
        String wordGuerre;
        String wordDesire;
        String wordIntution;
        String wordBesoin;
        // get word for Harmonie
        if ((harmonie % 2) == 0) { // Chẵn = "Vous représentez " + Các từ được lựa chọn nằm trong nhóm 1 + " aux yeux des autres."
            String condition2_1 = getResources().getString(R.string.h_even_1);
            String condition2_2 = getResources().getString(R.string.h_even_2);
            String[] arrGroup;
            if (isReverseHarmonie) {
                switch (getCurrentLanguage().getLanguage()) {
                    case "vi":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_vi/group_1/minus_" + harmonie);
                        break;
                    case "en":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_1/minus_" + harmonie);
                        break;
                    case "ru":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_ru/group_1/minus_" + harmonie);
                        break;
                    default:
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_1/minus_" + harmonie);
                        break;
                }
            } else {
                switch (getCurrentLanguage().getLanguage()) {
                    case "vi":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_vi/group_1/" + harmonie);
                        break;
                    case "en":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_1/" + harmonie);
                        break;
                    case "ru":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_ru/group_1/" + harmonie);
                        break;
                    default:
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_1/" + harmonie);
                        break;
                }
            }
            String condition3 = arrGroup[rd.nextInt(arrGroup.length)].trim();
            String condition4 = null;
            switch (getCurrentLanguage().getLanguage()) {
                case "vi":
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word_vi/" + harmonie);
                    break;
                case "en":
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word/" + harmonie);
                    break;
                case "ru":
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word_ru/" + harmonie);
                    break;
                default:
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word/" + harmonie);
                    break;
            }
            wordHarmonie = condition2_1 + " " + condition3 + " " + condition2_2 + " " + condition4;
        } else { // Lẻ = "Vous incarnez " + Các từ được lựa chọn nằm trong nhóm 1
            String condition2 = getResources().getString(R.string.h_odd);
            String[] arrGroup;
            if (isReverseHarmonie) {
                switch (getCurrentLanguage().getLanguage()) {
                    case "vi":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_vi/group_1/minus_" + harmonie);
                        break;
                    case "en":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_1/minus_" + harmonie);
                        break;
                    case "ru":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_ru/group_1/minus_" + harmonie);
                        break;
                    default:
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_1/minus_" + harmonie);
                        break;
                }
            } else {
                switch (getCurrentLanguage().getLanguage()) {
                    case "vi":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_vi/group_1/" + harmonie);
                        break;
                    case "en":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_1/" + harmonie);
                        break;
                    case "ru":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_ru/group_1/" + harmonie);
                        break;
                    default:
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_1/" + harmonie);
                        break;
                }
            }
            String condition3 = arrGroup[rd.nextInt(arrGroup.length)].trim();
            String condition4 = null;
            switch (getCurrentLanguage().getLanguage()) {
                case "vi":
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word_vi/" + harmonie);
                    break;
                case "en":
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word/" + harmonie);
                    break;
                case "ru":
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word_ru/" + harmonie);
                    break;
                default:
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word/" + harmonie);
                    break;
            }
            wordHarmonie = condition2 + " " + condition3 + "." + " " + condition4;
        }

        // get word for Aventure
        if ((aventure % 2) == 0) { // Chẵn = “Vous faites preuve ” + 1 từ được lựa chọn nằm trong nhóm 4
            String condition2 = getResources().getString(R.string.a_even);
            String[] arrGroup;
            if (isReverseAventure) {
                switch (getCurrentLanguage().getLanguage()) {
                    case "vi":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_vi/group_4/minus_" + aventure);
                        break;
                    case "en":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_4/minus_" + aventure);
                        break;
                    case "ru":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_ru/group_4/minus_" + aventure);
                        break;
                    default:
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_4/minus_" + aventure);
                        break;
                }
            } else {
                switch (getCurrentLanguage().getLanguage()) {
                    case "vi":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_vi/group_4/" + aventure);
                        break;
                    case "en":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_4/" + aventure);
                        break;
                    case "ru":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_ru/group_4/" + aventure);
                        break;
                    default:
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_4/" + aventure);
                        break;
                }
            }
            String condition3 = arrGroup[rd.nextInt(arrGroup.length)].trim();
            String condition4 = null;
            switch (getCurrentLanguage().getLanguage()) {
                case "vi":
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word_vi/" + aventure);
                    break;
                case "en":
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word/" + aventure);
                    break;
                case "ru":
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word_ru/" + aventure);
                    break;
                default:
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word/" + aventure);
                    break;
            }
            wordAventure = condition2 + " " + condition3 + "." + " " + condition4;
        } else { // Lẻ = “Vous pratiquez ” + 1 từ được lựa chọn nằm trong nhóm 1
            String condition2 = getResources().getString(R.string.a_odd);
            String[] arrGroup;
            if (isReverseAventure) {
                switch (getCurrentLanguage().getLanguage()) {
                    case "vi":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_vi/group_1/minus_" + aventure);
                        break;
                    case "en":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_1/minus_" + aventure);
                        break;
                    case "ru":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_ru/group_1/minus_" + aventure);
                        break;
                    default:
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_1/minus_" + aventure);
                        break;
                }
            } else {
                switch (getCurrentLanguage().getLanguage()) {
                    case "vi":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_vi/group_1/" + aventure);
                        break;
                    case "en":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_1/" + aventure);
                        break;
                    case "ru":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_ru/group_1/" + aventure);
                        break;
                    default:
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_1/" + aventure);
                        break;
                }
            }
            String condition3 = arrGroup[rd.nextInt(arrGroup.length)].trim();
            String condition4 = null;
            switch (getCurrentLanguage().getLanguage()) {
                case "vi":
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word_vi/" + aventure);
                    break;
                case "en":
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word/" + aventure);
                    break;
                case "ru":
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word_ru/" + aventure);
                    break;
                default:
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word/" + aventure);
                    break;
            }
            wordAventure = condition2 + " " + condition3 + "." + " " + condition4;
        }

        // get word for Fruit
        if ((fruit % 2) == 0) { // Chẵn = "Vous répandez " + 1 từ được lựa chọn nằm trong nhóm 1
            String condition2 = getResources().getString(R.string.f_even);
            String[] arrGroup;
            if (isReverseFruit) {
                switch (getCurrentLanguage().getLanguage()) {
                    case "vi":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_vi/group_1/minus_" + fruit);
                        break;
                    case "en":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_1/minus_" + fruit);
                        break;
                    case "ru":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_ru/group_1/minus_" + fruit);
                        break;
                    default:
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_1/minus_" + fruit);
                        break;
                }
            } else {
                switch (getCurrentLanguage().getLanguage()) {
                    case "vi":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_vi/group_1/" + fruit);
                        break;
                    case "en":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_1/" + fruit);
                        break;
                    case "ru":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_ru/group_1/" + fruit);
                        break;
                    default:
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_1/" + fruit);
                        break;
                }
            }
            String condition3 = arrGroup[rd.nextInt(arrGroup.length)].trim();
            String condition4 = null;
            switch (getCurrentLanguage().getLanguage()) {
                case "vi":
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word_vi/" + fruit);
                    break;
                case "en":
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word/" + fruit);
                    break;
                case "ru":
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word_ru/" + fruit);
                    break;
                default:
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word/" + fruit);
                    break;
            }
            wordFruit = condition2 + " " + condition3 + "." + " " + condition4;
        } else { // Lẻ = "Vous diffusez " + 1 từ được lựa chọn nằm trong nhóm 1
            String condition2 = getResources().getString(R.string.f_odd);
            String[] arrGroup;
            if (isReverseFruit) {
                switch (getCurrentLanguage().getLanguage()) {
                    case "vi":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_vi/group_1/minus_" + fruit);
                        break;
                    case "en":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_1/minus_" + fruit);
                        break;
                    case "ru":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_ru/group_1/minus_" + fruit);
                        break;
                    default:
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_1/minus_" + fruit);
                        break;
                }
            } else {
                switch (getCurrentLanguage().getLanguage()) {
                    case "vi":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_vi/group_1/" + fruit);
                        break;
                    case "en":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_1/" + fruit);
                        break;
                    case "ru":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_ru/group_1/" + fruit);
                        break;
                    default:
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_1/" + fruit);
                        break;
                }
            }
            String condition3 = arrGroup[rd.nextInt(arrGroup.length)].trim();
            String condition4 = null;
            switch (getCurrentLanguage().getLanguage()) {
                case "vi":
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word_vi/" + fruit);
                    break;
                case "en":
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word/" + fruit);
                    break;
                case "ru":
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word_ru/" + fruit);
                    break;
                default:
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word/" + fruit);
                    break;
            }
            wordFruit = condition2 + " " + condition3 + "." + " " + condition4;
        }

        // get word for Complicate
        if ((complicate % 2) == 0) { // Chẵn = “Vous partagez ” + 1 từ được lựa chọn nằm trong nhóm 1
            String condition2 = getResources().getString(R.string.c_even);
            String[] arrGroup;
            if (isReverseComplicate) {
                switch (getCurrentLanguage().getLanguage()) {
                    case "vi":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_vi/group_1/minus_" + complicate);
                        break;
                    case "en":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_1/minus_" + complicate);
                        break;
                    case "ru":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_ru/group_1/minus_" + complicate);
                        break;
                    default:
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_1/minus_" + complicate);
                        break;
                }
            } else {
                switch (getCurrentLanguage().getLanguage()) {
                    case "vi":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_vi/group_1/" + complicate);
                        break;
                    case "en":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_1/" + complicate);
                        break;
                    case "ru":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_ru/group_1/" + complicate);
                        break;
                    default:
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_1/" + complicate);
                        break;
                }
            }
            String condition3 = arrGroup[rd.nextInt(arrGroup.length)].trim();
            String condition4 = null;
            switch (getCurrentLanguage().getLanguage()) {
                case "vi":
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word_vi/" + complicate);
                    break;
                case "en":
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word/" + complicate);
                    break;
                case "ru":
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word_ru/" + complicate);
                    break;
                default:
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word/" + complicate);
                    break;
            }
            wordComplicate = condition2 + " " + condition3 + "." + " " + condition4;
        } else { // Lẻ = “Vous tissez les liens ” + 1 từ được lựa chọn nằm trong nhóm 4
            String condition2 = getResources().getString(R.string.c_odd);
            String[] arrGroup;
            if (isReverseComplicate) {
                switch (getCurrentLanguage().getLanguage()) {
                    case "vi":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_vi/group_4/minus_" + complicate);
                        break;
                    case "en":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_4/minus_" + complicate);
                        break;
                    case "ru":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_ru/group_4/minus_" + complicate);
                        break;
                    default:
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_4/minus_" + complicate);
                        break;
                }
            } else {
                switch (getCurrentLanguage().getLanguage()) {
                    case "vi":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_vi/group_4/" + complicate);
                        break;
                    case "en":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_4/" + complicate);
                        break;
                    case "ru":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_ru/group_4/" + complicate);
                        break;
                    default:
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_4/" + complicate);
                        break;
                }
            }
            String condition3 = arrGroup[rd.nextInt(arrGroup.length)].trim();
            String condition4 = null;
            switch (getCurrentLanguage().getLanguage()) {
                case "vi":
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word_vi/" + complicate);
                    break;
                case "en":
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word/" + complicate);
                    break;
                case "ru":
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word_ru/" + complicate);
                    break;
                default:
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word/" + complicate);
                    break;
            }
            wordComplicate = condition2 + " " + condition3 + "." + " " + condition4;
        }

        // get word for Energy
        if ((energy % 2) == 0) { // Chẵn = "Vous êtes dans la dynamique " + 1 từ được lựa chọn nằm trong nhóm 5
            String condition2 = getResources().getString(R.string.e_even);
            String[] arrGroup;
            if (isReverseEnergy) {
                switch (getCurrentLanguage().getLanguage()) {
                    case "vi":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_vi/group_5/minus_" + energy);
                        break;
                    case "en":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_5/minus_" + energy);
                        break;
                    case "ru":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_ru/group_5/minus_" + energy);
                        break;
                    default:
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_5/minus_" + energy);
                        break;
                }
            } else {
                switch (getCurrentLanguage().getLanguage()) {
                    case "vi":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_vi/group_5/" + energy);
                        break;
                    case "en":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_5/" + energy);
                        break;
                    case "ru":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_ru/group_5/" + energy);
                        break;
                    default:
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_5/" + energy);
                        break;
                }
            }
            String condition3 = arrGroup[rd.nextInt(arrGroup.length)].trim();
            String condition4 = null;
            switch (getCurrentLanguage().getLanguage()) {
                case "vi":
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word_vi/" + energy);
                    break;
                case "en":
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word/" + energy);
                    break;
                case "ru":
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word_ru/" + energy);
                    break;
                default:
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word/" + energy);
                    break;
            }
            wordEnergy = condition2 + " " + condition3 + "." + " " + condition4;
        } else { // Lẻ = "Vous êtes dans l'énergie " + 1 từ được lựa chọn nằm trong nhóm 5
            String condition2 = getResources().getString(R.string.e_odd);
            String[] arrGroup;
            if (isReverseEnergy) {
                switch (getCurrentLanguage().getLanguage()) {
                    case "vi":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_vi/group_5/minus_" + energy);
                        break;
                    case "en":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_5/minus_" + energy);
                        break;
                    case "ru":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_ru/group_5/minus_" + energy);
                        break;
                    default:
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_5/minus_" + energy);
                        break;
                }
            } else {
                switch (getCurrentLanguage().getLanguage()) {
                    case "vi":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_vi/group_5/" + energy);
                        break;
                    case "en":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_5/" + energy);
                        break;
                    case "ru":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_ru/group_5/" + energy);
                        break;
                    default:
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_5/" + energy);
                        break;
                }
            }
            String condition3 = arrGroup[rd.nextInt(arrGroup.length)].trim();
            String condition4 = null;
            switch (getCurrentLanguage().getLanguage()) {
                case "vi":
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word_vi/" + energy);
                    break;
                case "en":
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word/" + energy);
                    break;
                case "ru":
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word_ru/" + energy);
                    break;
                default:
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word/" + energy);
                    break;
            }
            wordEnergy = condition2 + " " + condition3 + "." + " " + condition4;
        }

        // get word for Guerre
        if ((guerre % 2) == 0) { // Chẵn  = Các từ được lựa chọn nằm trong nhóm 1 + " est votre but. "
            String condition2 = getResources().getString(R.string.g_even);
            String[] arrGroup;
            if (isReverseGuerre) {
                switch (getCurrentLanguage().getLanguage()) {
                    case "vi":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_vi/group_5/minus_" + guerre);
                        break;
                    case "en":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_5/minus_" + guerre);
                        break;
                    case "ru":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_ru/group_5/minus_" + guerre);
                        break;
                    default:
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_5/minus_" + guerre);
                        break;
                }
            } else {
                switch (getCurrentLanguage().getLanguage()) {
                    case "vi":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_vi/group_5/" + guerre);
                        break;
                    case "en":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_5/" + guerre);
                        break;
                    case "ru":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_ru/group_5/" + guerre);
                        break;
                    default:
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_5/" + guerre);
                        break;
                }
            }
            String condition3 = arrGroup[rd.nextInt(arrGroup.length)].trim();
            String condition4 = null;
            switch (getCurrentLanguage().getLanguage()) {
                case "vi":
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word_vi/" + guerre);
                    break;
                case "en":
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word/" + guerre);
                    break;
                case "ru":
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word_ru/" + guerre);
                    break;
                default:
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word/" + guerre);
                    break;
            }
            condition3 = condition3.substring(0, 1).toUpperCase() + condition3.substring(1);
            wordGuerre = condition3 + " " + condition2 + "." + " " + condition4;
        } else { // Lẻ = "Vous visez " + Các từ được lựa chọn nằm trong nhóm 1
            String condition2 = getResources().getString(R.string.g_odd);
            String[] arrGroup;
            if (isReverseGuerre) {
                switch (getCurrentLanguage().getLanguage()) {
                    case "vi":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_vi/group_1/minus_" + guerre);
                        break;
                    case "en":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_1/minus_" + guerre);
                        break;
                    case "ru":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_ru/group_1/minus_" + guerre);
                        break;
                    default:
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_1/minus_" + guerre);
                        break;
                }
            } else {
                switch (getCurrentLanguage().getLanguage()) {
                    case "vi":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_vi/group_1/" + guerre);
                        break;
                    case "en":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_1/" + guerre);
                        break;
                    case "ru":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_ru/group_1/" + guerre);
                        break;
                    default:
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_1/" + guerre);
                        break;
                }
            }
            String condition3 = arrGroup[rd.nextInt(arrGroup.length)].trim();
            String condition4 = null;
            switch (getCurrentLanguage().getLanguage()) {
                case "vi":
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word_vi/" + guerre);
                    break;
                case "en":
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word/" + guerre);
                    break;
                case "ru":
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word_ru/" + guerre);
                    break;
                default:
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word/" + guerre);
                    break;
            }
            wordGuerre = condition2 + " " + condition3 + "." + " " + condition4;
        }

        // get word for Besoin
        if ((besoin % 2) == 0) { // Chẵn = “Vous vous nourrisez ” + 1 từ được lựa chọn nằm trong nhóm 4
            String condition2 = getResources().getString(R.string.b_even);
            String[] arrGroup;
            if (isReverseBesoin) {
                switch (getCurrentLanguage().getLanguage()) {
                    case "vi":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_vi/group_4/minus_" + besoin);
                        break;
                    case "en":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_4/minus_" + besoin);
                        break;
                    case "ru":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_ru/group_4/minus_" + besoin);
                        break;
                    default:
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_4/minus_" + besoin);
                        break;
                }
            } else {
                switch (getCurrentLanguage().getLanguage()) {
                    case "vi":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_vi/group_4/" + besoin);
                        break;
                    case "en":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_4/" + besoin);
                        break;
                    case "ru":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_ru/group_4/" + besoin);
                        break;
                    default:
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_4/" + besoin);
                        break;
                }
            }
            String condition3 = arrGroup[rd.nextInt(arrGroup.length)].trim();
            String condition4 = null;
            switch (getCurrentLanguage().getLanguage()) {
                case "vi":
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word_vi/" + besoin);
                    break;
                case "en":
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word/" + besoin);
                    break;
                case "ru":
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word_ru/" + besoin);
                    break;
                default:
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word/" + besoin);
                    break;
            }
            wordBesoin = condition2 + " " + condition3 + "." + " " + condition4;
        } else { // Lẻ = “Vous réclamez ” + 1 từ được lựa chọn nằm trong nhóm 5 (nhóm 5 chưa có trên app)
            String condition2 = getResources().getString(R.string.b_odd);
            String[] arrGroup;
            if (isReverseBesoin) {
                switch (getCurrentLanguage().getLanguage()) {
                    case "vi":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_vi/group_5/minus_" + besoin);
                        break;
                    case "en":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_5/minus_" + besoin);
                        break;
                    case "ru":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_ru/group_5/minus_" + besoin);
                        break;
                    default:
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_5/minus_" + besoin);
                        break;
                }
            } else {
                switch (getCurrentLanguage().getLanguage()) {
                    case "vi":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_vi/group_5/" + besoin);
                        break;
                    case "en":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_5/" + besoin);
                        break;
                    case "ru":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_ru/group_5/" + besoin);
                        break;
                    default:
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_5/" + besoin);
                        break;
                }
            }
            String condition3 = arrGroup[rd.nextInt(arrGroup.length)].trim();
            String condition4 = null;
            switch (getCurrentLanguage().getLanguage()) {
                case "vi":
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word_vi/" + besoin);
                    break;
                case "en":
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word/" + besoin);
                    break;
                case "ru":
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word_ru/" + besoin);
                    break;
                default:
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word/" + besoin);
                    break;
            }
            wordBesoin = condition2 + " " + condition3 + "." + " " + condition4;
        }

        // get word for Desire - CHOC_CHARM
        Card desireCard = app.cardMng.getCardByNumber(desire);
        if (desireCard.type == Card.CHOC) { // Choc = “Vous désirez secrètement ” + 1 từ được lựa chọn nằm trong nhóm 1
            String condition2 = getResources().getString(R.string.d_choc);
            String[] arrGroup;
            if (isReverseDesire) {
                switch (getCurrentLanguage().getLanguage()) {
                    case "vi":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_vi/group_1/minus_" + desire);
                        break;
                    case "en":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_1/minus_" + desire);
                        break;
                    case "ru":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_ru/group_1/minus_" + desire);
                        break;
                    default:
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_1/minus_" + desire);
                        break;
                }
            } else {
                switch (getCurrentLanguage().getLanguage()) {
                    case "vi":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_vi/group_1/" + desire);
                        break;
                    case "en":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_1/" + desire);
                        break;
                    case "ru":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_ru/group_1/" + desire);
                        break;
                    default:
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_1/" + desire);
                        break;
                }
            }
            String condition3 = arrGroup[rd.nextInt(arrGroup.length)].trim();
            String condition4 = null;
            switch (getCurrentLanguage().getLanguage()) {
                case "vi":
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word_vi/" + desire);
                    break;
                case "en":
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word/" + desire);
                    break;
                case "ru":
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word_ru/" + desire);
                    break;
                default:
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word/" + desire);
                    break;
            }
            wordDesire = condition2 + " " + condition3 + "." + " " + condition4;
        } else { // Charm = “Vous recherchez ”+ 1 từ được lựa chọn nằm trong nhóm 1
            String condition2 = getResources().getString(R.string.d_charm);
            String[] arrGroup;
            if (isReverseDesire) {
                switch (getCurrentLanguage().getLanguage()) {
                    case "vi":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_vi/group_1/minus_" + desire);
                        break;
                    case "en":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_1/minus_" + desire);
                        break;
                    case "ru":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_ru/group_1/minus_" + desire);
                        break;
                    default:
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_1/minus_" + desire);
                        break;
                }
            } else {
                switch (getCurrentLanguage().getLanguage()) {
                    case "vi":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_vi/group_1/" + desire);
                        break;
                    case "en":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_1/" + desire);
                        break;
                    case "ru":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_ru/group_1/" + desire);
                        break;
                    default:
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_1/" + desire);
                        break;
                }
            }
            String condition3 = arrGroup[rd.nextInt(arrGroup.length)].trim();
            String condition4 = null;
            switch (getCurrentLanguage().getLanguage()) {
                case "vi":
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word_vi/" + desire);
                    break;
                case "en":
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word/" + desire);
                    break;
                case "ru":
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word_ru/" + desire);
                    break;
                default:
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word/" + desire);
                    break;
            }
            wordDesire = condition2 + " " + condition3 + "." + " " + condition4;
        }

        // get word for Intution - CHOC_CHARM
        Card intutionCard = app.cardMng.getCardByNumber(intution);
        if (intutionCard.type == Card.CHOC) { // CHOC = Các từ được lựa chọn nằm trong nhóm 1 nhưng chữ đầu viết hoa " hante votre esprit. "
            String condition2 = getResources().getString(R.string.i_choc);
            String[] arrGroup;
            if (isReverseIntution) {
                switch (getCurrentLanguage().getLanguage()) {
                    case "vi":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_vi/group_1/minus_" + intution);
                        break;
                    case "en":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_1/minus_" + intution);
                        break;
                    case "ru":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_ru/group_1/minus_" + intution);
                        break;
                    default:
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_1/minus_" + intution);
                        break;
                }
            } else {
                switch (getCurrentLanguage().getLanguage()) {
                    case "vi":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_vi/group_1/" + intution);
                        break;
                    case "en":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_1/" + intution);
                        break;
                    case "ru":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_ru/group_1/" + intution);
                        break;
                    default:
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_1/" + intution);
                        break;
                }
            }
            String condition3 = arrGroup[rd.nextInt(arrGroup.length)].trim();
            String condition3UpperCase = condition3.substring(0, 1).toUpperCase() + condition3.substring(1);
            String condition4 = null;
            switch (getCurrentLanguage().getLanguage()) {
                case "vi":
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word_vi/" + intution);
                    break;
                case "en":
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word/" + intution);
                    break;
                case "ru":
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word_ru/" + intution);
                    break;
                default:
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word/" + intution);
                    break;
            }
            wordIntution = condition3UpperCase + " " + condition2 + " " + condition4;
        } else { // CHARME = "Vous pensez souvent " + các từ được lựa chọn nằm trong nhóm 6
            String condition2 = getResources().getString(R.string.i_charm);
            String[] arrGroup;
            if (isReverseIntution) {
                switch (getCurrentLanguage().getLanguage()) {
                    case "vi":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_vi/group_6/minus_" + intution);
                        break;
                    case "en":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_6/minus_" + intution);
                        break;
                    case "ru":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_ru/group_6/minus_" + intution);
                        break;
                    default:
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_6/minus_" + intution);
                        break;
                }
            } else {
                switch (getCurrentLanguage().getLanguage()) {
                    case "vi":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_vi/group_6/" + intution);
                        break;
                    case "en":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_6/" + intution);
                        break;
                    case "ru":
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word_ru/group_6/" + intution);
                        break;
                    default:
                        arrGroup = Utils.GetLines(this.getAssets(), "month_group_word/group_6/" + intution);
                        break;
                }
            }
            String condition3 = arrGroup[rd.nextInt(arrGroup.length)].trim();
            String condition4 = null;
            switch (getCurrentLanguage().getLanguage()) {
                case "vi":
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word_vi/" + intution);
                    break;
                case "en":
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word/" + intution);
                    break;
                case "ru":
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word_ru/" + intution);
                    break;
                default:
                    condition4 = Utils.GetText(this.getAssets(), "month_group_word/" + intution);
                    break;
            }
            wordIntution = condition2 + " " + condition3 + "." + " " + condition4;
        }

//                Log.e("Temp", wordAventure +  " === " + wordBesoin +  " === " + wordComplicate +  " === " + wordDesire +  " === " + wordEnergy +  " === " + wordFruit +  " === " + wordGuerre +  " === " + wordHarmonie +  " === " + wordIntution);

//        String[] arrWords = {wordAventure, wordBesoin, wordComplicate, wordDesire, wordEnergy,
//                wordFruit, wordGuerre, wordHarmonie, wordIntution};
//        List<String> arrSentences = new ArrayList<String>(Arrays.asList(arrWords));
//        Collections.shuffle(arrSentences);

//        List<String> arrTeamSentences = new ArrayList<String>();
//        List<String> arrRemainSentences = new ArrayList<String>();

        List<String> arrFinalSentences = new ArrayList<String>();

        if (prefs.getString(TITLE_TEAM, null) == CREATION) {
            verifyTeamCreation(aventure, wordAventure);
            verifyTeamCreation(besoin, wordBesoin);
            verifyTeamCreation(complicate, wordComplicate);
            verifyTeamCreation(desire, wordDesire);
            verifyTeamCreation(energy, wordEnergy);
            verifyTeamCreation(fruit, wordFruit);
            verifyTeamCreation(guerre, wordGuerre);
            verifyTeamCreation(harmonie, wordHarmonie);
            verifyTeamCreation(intution, wordIntution);
        } else if (prefs.getString(TITLE_TEAM, null) == REVOLTE) {
            verifyTeamRevolte(aventure, wordAventure);
            verifyTeamRevolte(besoin, wordBesoin);
            verifyTeamRevolte(complicate, wordComplicate);
            verifyTeamRevolte(desire, wordDesire);
            verifyTeamRevolte(energy, wordEnergy);
            verifyTeamRevolte(fruit, wordFruit);
            verifyTeamRevolte(guerre, wordGuerre);
            verifyTeamRevolte(harmonie, wordHarmonie);
            verifyTeamRevolte(intution, wordIntution);
        } else if (prefs.getString(TITLE_TEAM, null) == SOUCI) {
            verifyTeamDestresse(aventure, wordAventure);
            verifyTeamDestresse(besoin, wordBesoin);
            verifyTeamDestresse(complicate, wordComplicate);
            verifyTeamDestresse(desire, wordDesire);
            verifyTeamDestresse(energy, wordEnergy);
            verifyTeamDestresse(fruit, wordFruit);
            verifyTeamDestresse(guerre, wordGuerre);
            verifyTeamDestresse(harmonie, wordHarmonie);
            verifyTeamDestresse(intution, wordIntution);
        } else if (prefs.getString(TITLE_TEAM, null) == ZEN) {
            verifyTeamZen(aventure, wordAventure);
            verifyTeamZen(besoin, wordBesoin);
            verifyTeamZen(complicate, wordComplicate);
            verifyTeamZen(desire, wordDesire);
            verifyTeamZen(energy, wordEnergy);
            verifyTeamZen(fruit, wordFruit);
            verifyTeamZen(guerre, wordGuerre);
            verifyTeamZen(harmonie, wordHarmonie);
            verifyTeamZen(intution, wordIntution);
        } else {
            String[] arrWords = {wordAventure, wordBesoin, wordComplicate, wordDesire, wordEnergy,
                    wordFruit, wordGuerre, wordHarmonie, wordIntution};
            arrFinalSentences = new ArrayList<String>(Arrays.asList(arrWords));
            Collections.shuffle(arrFinalSentences);
        }

        Collections.shuffle(arrTeamSentences);
        Collections.shuffle(arrRemainSentences);
        if (arrFinalSentences.size() == 0) {
            arrFinalSentences.addAll(arrTeamSentences);
            arrFinalSentences.addAll(arrRemainSentences);
        }

        String sentencesAfter4LowerCase = arrFinalSentences.get(3).substring(0, 1).toLowerCase() + arrFinalSentences.get(3).substring(1);
        String sentencesAfter7LowerCase = arrFinalSentences.get(6).substring(0, 1).toLowerCase() + arrFinalSentences.get(6).substring(1);
        String sentencesAfter9LowerCase = arrFinalSentences.get(8).substring(0, 1).toLowerCase() + arrFinalSentences.get(8).substring(1);
        String finalSentences = arrFinalSentences.get(0) + " " + arrFinalSentences.get(1) + " " + arrFinalSentences.get(2) + " " // nhóm 1 ko có từ nối
                + "<br><br>" + getResources().getString(R.string.link_sentence_before_4) + " " // Nhóm 2 (3 ô tiếp theo) : ở câu thứ 4 để từ “Plus encore, ”
                + sentencesAfter4LowerCase + " " + arrFinalSentences.get(4) + " " + arrFinalSentences.get(5) + " "
                + "<br><br>" + getResources().getString(R.string.link_sentence_before_7) + " " // Nhóm 3 (3 ô tiếp theo) ở câu thứ 7 thêm từ “De façon encore plus intense
                + sentencesAfter7LowerCase + " " + arrFinalSentences.get(7) + " "
                + "<br><br>" + getResources().getString(R.string.link_sentence_before_9) + " " // Nhóm 4 ở câu thứ 9 là câu cuối cùng, thêm cụm từ “Et, plus que tout, ”
                + sentencesAfter9LowerCase;

        Log.e("Temp", "== " + finalSentences);
        return finalSentences;
    }

    private void calculatePoint(int cardCell, boolean isReverse, int bonusPoint) {
        if (isReverse) {
            cardCell = cardCell * -1;
        }

        if (Arrays.asList(TEAM_CREATION).contains(cardCell)) {
            resultTeamCreation = resultTeamCreation + bonusPoint;
        } else if (Arrays.asList(TEAM_DETRESSE).contains(cardCell)) {
            resultTeamDestresse = resultTeamDestresse + bonusPoint;
        } else if (Arrays.asList(TEAM_REVOLTE).contains(cardCell)) {
            resultTeamRevolte = resultTeamRevolte + bonusPoint;
        } else if (Arrays.asList(TEAM_ZEN).contains(cardCell)) {
            resultTeamZen = resultTeamZen + bonusPoint;
        }

    }

    private void verifyTeamCreation(int cardCell, String word) {
        if (Arrays.asList(TEAM_CREATION).contains(cardCell)) {
            arrTeamSentences.add(word);
        } else {
            arrRemainSentences.add(word);
        }
    }

    private void verifyTeamRevolte(int cardCell, String word) {
        if (Arrays.asList(TEAM_REVOLTE).contains(cardCell)) {
            arrTeamSentences.add(word);
        } else {
            arrRemainSentences.add(word);
        }
    }

    private void verifyTeamDestresse(int cardCell, String word) {
        if (Arrays.asList(TEAM_DETRESSE).contains(cardCell)) {
            arrTeamSentences.add(word);
        } else {
            arrRemainSentences.add(word);
        }
    }

    private void verifyTeamZen(int cardCell, String word) {
        if (Arrays.asList(TEAM_ZEN).contains(cardCell)) {
            arrTeamSentences.add(word);
        } else {
            arrRemainSentences.add(word);
        }
    }

    private Card getRandomCard(int number) {
        Card[] randomCard = app.cardMng.getMonthCardByRandom(number);
        Card currentRandomCard = app.cardMng.getResultCard();
        return currentRandomCard;
    }

    private void saveCurrentMonthAndCard() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getDefault());
        int dayOfWeek = calendar.get(Calendar.DAY_OF_MONTH);
        if (dayOfWeek < 26) { // get last 30 days from day 26 of last month
            // do nothing
//            calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - 1);
        } else { // get next 30 days from day 26 of this month
            calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) + 1);
        }

        Card harmonieCard = null;
        Card aventureCard = null;
        Card fruitCard = null;
        Card complicateCard = null;
        Card guerreCard = null;
        Card desireCard = null;
        Card intutionCard = null;
        Card besoinCard = null;
        Card energyCard = getEnergyCard();
        //////////////////////////////////////////////
        /////=======================//////////////////
        //////////////////////////////////////////////
//        if ( isExternalStorageWritable() ) {
//
//            File appDirectory = new File( Environment.getExternalStorageDirectory() + "/PhenixTempFolder" );
//            File logDirectory = new File( appDirectory + "/log" );
//            File logFile = new File( logDirectory, "logcat" + System.currentTimeMillis() + ".txt" );
//
//            // create app folder
//            if ( !appDirectory.exists() ) {
//                appDirectory.mkdir();
//            }
//
//            // create log folder
//            if ( !logDirectory.exists() ) {
//                logDirectory.mkdir();
//            }
//
//            // clear the previous logcat and then write the new one to the file
//            try {
//                Process process = Runtime.getRuntime().exec("logcat -c");
//                process = Runtime.getRuntime().exec("logcat -f " + logFile);
//            } catch ( IOException e ) {
//                e.printStackTrace();
//            }
//
//        } else if ( isExternalStorageReadable() ) {
//            // only readable
//        } else {
//            // not accessible
//        }
        //////////////////////////////////////////////
        /////=======================//////////////////
        //////////////////////////////////////////////

        if (listEightRemainCard.size() >= 8) {
            Collections.shuffle(listEightRemainCard);

            int hId = app.cardMng.getCardId(listEightRemainCard.get(0));
            harmonieCard = app.cardMng.getCard(hId);
            int aId = app.cardMng.getCardId(listEightRemainCard.get(1));
            aventureCard = app.cardMng.getCard(aId);
            int fId = app.cardMng.getCardId(listEightRemainCard.get(2));
            fruitCard = app.cardMng.getCard(fId);
            int cId = app.cardMng.getCardId(listEightRemainCard.get(3));
            complicateCard = app.cardMng.getCard(cId);
            int gId = app.cardMng.getCardId(listEightRemainCard.get(4));
            guerreCard = app.cardMng.getCard(gId);
            int dId = app.cardMng.getCardId(listEightRemainCard.get(5));
            desireCard = app.cardMng.getCard(dId);
            int iId = app.cardMng.getCardId(listEightRemainCard.get(6));
            intutionCard = app.cardMng.getCard(iId);
            int bId = app.cardMng.getCardId(listEightRemainCard.get(7));
            besoinCard = app.cardMng.getCard(bId);
        } else {
            return;
        }


        harmonie = harmonieCard.number;
        aventure = aventureCard.number;
        fruit = fruitCard.number;
        complicate = complicateCard.number;
        guerre = guerreCard.number;
        desire = desireCard.number;
        intution = intutionCard.number;
        besoin = besoinCard.number;
        energy = energyCard.number;

        isReverseHarmonie = harmonieCard.isReverse;
        isReverseAventure = aventureCard.isReverse;
        isReverseFruit = fruitCard.isReverse;
        isReverseComplicate = complicateCard.isReverse;
        isReverseEnergy = energyCard.isReverse;
        isReverseGuerre = guerreCard.isReverse;
        isReverseDesire = desireCard.isReverse;
        isReverseIntution = intutionCard.isReverse;
        isReverseBesoin = besoinCard.isReverse;

        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(CURRENT_MONTH, calendar.get(Calendar.MONTH));
        editor.putInt(VALUE_HARMONIE, harmonieCard.number);
        editor.putInt(VALUE_AVENTURE, aventureCard.number);
        editor.putInt(VALUE_FRUIT, fruitCard.number);
        editor.putInt(VALUE_COMPLICATE, complicateCard.number);
        editor.putInt(VALUE_GUERRE, guerreCard.number);
        editor.putInt(VALUE_DESIRE, desireCard.number);
        editor.putInt(VALUE_INTUTION, intutionCard.number);
        editor.putInt(VALUE_BESOIN, besoinCard.number);
        editor.putInt(VALUE_ENERGY, energyCard.number);
        editor.putBoolean(IS_REVERSE_HARMONIE, harmonieCard.isReverse);
        editor.putBoolean(IS_REVERSE_AVENTURE, aventureCard.isReverse);
        editor.putBoolean(IS_REVERSE_FRUIT, fruitCard.isReverse);
        editor.putBoolean(IS_REVERSE_COMPLICATE, complicateCard.isReverse);
        editor.putBoolean(IS_REVERSE_GUERRE, guerreCard.isReverse);
        editor.putBoolean(IS_REVERSE_DESIRE, desireCard.isReverse);
        editor.putBoolean(IS_REVERSE_INTUTION, intutionCard.isReverse);
        editor.putBoolean(IS_REVERSE_BESOIN, besoinCard.isReverse);
        editor.putBoolean(IS_REVERSE_ENERGY, energyCard.isReverse);

        editor.putBoolean(IS_PAID_MONTH, false);
        switch (getCurrentLanguage().getLanguage()) {
            case "vi":
                editor.putString(TITLE_VI, getTitleResult());
                editor.putString(SENTENCES_VI, getFullSentences());
                break;
            case "en":
                editor.putString(TITLE_EN, getTitleResult());
                editor.putString(SENTENCES_EN, getFullSentences());
                break;
            default:
                editor.putString(TITLE, getTitleResult());
                editor.putString(SENTENCES, getFullSentences());
                break;
        }

        editor.apply();
    }

    private void loadCurrentMonthAndCard() {
        harmonie = (byte) prefs.getInt(VALUE_HARMONIE, -1);
        aventure = (byte) prefs.getInt(VALUE_AVENTURE, -1);
        fruit = (byte) prefs.getInt(VALUE_FRUIT, -1);
        complicate = (byte) prefs.getInt(VALUE_COMPLICATE, -1);
        energy = (byte) prefs.getInt(VALUE_ENERGY, -1);
        guerre = (byte) prefs.getInt(VALUE_GUERRE, -1);
        desire = (byte) prefs.getInt(VALUE_DESIRE, -1);
        intution = (byte) prefs.getInt(VALUE_INTUTION, -1);
        besoin = (byte) prefs.getInt(VALUE_BESOIN, -1);
        isReverseHarmonie = prefs.getBoolean(IS_REVERSE_HARMONIE, false);
        isReverseAventure = prefs.getBoolean(IS_REVERSE_AVENTURE, false);
        isReverseFruit = prefs.getBoolean(IS_REVERSE_FRUIT, false);
        isReverseComplicate = prefs.getBoolean(IS_REVERSE_COMPLICATE, false);
        isReverseEnergy = prefs.getBoolean(IS_REVERSE_ENERGY, false);
        isReverseGuerre = prefs.getBoolean(IS_REVERSE_GUERRE, false);
        isReverseDesire = prefs.getBoolean(IS_REVERSE_DESIRE, false);
        isReverseIntution = prefs.getBoolean(IS_REVERSE_INTUTION, false);
        isReverseBesoin = prefs.getBoolean(IS_REVERSE_BESOIN, false);
    }

    private String getLumiereCardIdByPosition(int position) {
        StringBuilder result = new StringBuilder();
        switch (position) {
            case 0: {
                result.append(harmonie);
                break;
            }
            case 1: {
                result.append(aventure);
                break;
            }
            case 2: {
                result.append(fruit);
                break;
            }
            case 3: {
                result.append(complicate);
                break;
            }
            case 4: {
                result.append(energy);
                break;
            }
            case 5: {
                result.append(guerre);
                break;
            }
            case 6: {
                result.append(desire);
                break;
            }
            case 7: {
                result.append(intution);
                break;
            }
            case 8: {
                result.append(besoin);
                break;
            }
        }
        return result.toString();
    }

    public void doShareLumiere(View view) {
        for (int i = 0; i < imageLumiereCardShow.length; i++) {
            if (imageLumiereCardShow[i].equals("1")) {
                imageLumiereCardShow[i] = getLumiereCardIdByPosition(i);
            }
        }
        String lumiereQuestion = tv_lumiere_question.getText().toString().trim();
        Intent intent = new Intent(getApplicationContext(), ShareLumiereActivity.class);
        intent.putExtra("shareImages", imageLumiereCardShow);
        intent.putExtra("lumiereQuestion", lumiereQuestion);
        intent.putExtra("lumiereDescription", strDescriptionContent4Share);
        startActivityForResult(intent, REQUEST_CODE_SHARE);
    }

    public void shareLumiere(Uri uri) {
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
        shareIntent.setType("image/jpeg");
        startActivity(Intent.createChooser(shareIntent, getResources().getString(R.string.title_share)));
    }

    public void shareLumiere(Bitmap bm) {
        String bitmapPath = MediaStore.Images.Media.insertImage(getContentResolver(), bm, "palette", "share palette");
        Uri bitmapUri = Uri.parse(bitmapPath);
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("image/png");
        intent.putExtra(Intent.EXTRA_STREAM, bitmapUri);
        startActivity(Intent.createChooser(intent, "Share"));
    }

    public static int REQUEST_CODE_SHARE = 2705;
    public static String RESULT_IMAGE_SHARE = "RESULT_IMAGE_SHARE";

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_SHARE) {
            if (resultCode == RESULT_OK) {
                if (data.hasExtra(RESULT_IMAGE_SHARE)) {
                    Bitmap bmShare = data.getParcelableExtra(RESULT_IMAGE_SHARE);
                    shareLumiere(bmShare);
                }
            }
        }
    }
}
