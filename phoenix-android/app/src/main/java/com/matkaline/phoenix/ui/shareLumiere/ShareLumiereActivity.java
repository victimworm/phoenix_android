package com.matkaline.phoenix.ui.shareLumiere;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.matkaline.phoenix.MonthCardActivity;
import com.matkaline.phoenix.R;
import com.matkaline.phoenix.RootActivity;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

import ulitis.DateTimeHelper;
import ulitis.UiHelper;

public class ShareLumiereActivity extends RootActivity {

    private LinearLayout tvName1a, tvName1b, tvName1c, tvName2a, tvName2b, tvName2c, tvName3a, tvName3b, tvName3c;
    private TextView tv_h, tv_armonie, tv_a, tv_venture, tv_f, tv_ruit, tv_c, tv_omplicate, tv_e,
            tv_nergy, tv_g, tv_uerre, tv_d, tv_esir, tv_i, tv_ntution, tv_b, tv_esoin;
    private ImageView img1a, img1b, img1c, img2a, img2b, img2c, img3a, img3b, img3c;

    private TextView tvTitle, tvUserInfo, tvQuestionTitle, tvQuestionContent, tvDescription;
    private ImageView imgLogo, imgAppStore, imgGooglePlay;
    private TextView txtAppName;

    private ShareLumiereActivity currentActivity;
    private String[] lumiereImages = null;
    private String lumiereQuestion = null;
    private String lumiereDescription = null;

    private ConstraintLayout cstImageShare;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        if (null != intent) {
            lumiereImages = intent.getStringArrayExtra("shareImages");
            lumiereQuestion = intent.getStringExtra("lumiereQuestion");
            lumiereDescription = intent.getStringExtra("lumiereDescription");
        }

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        currentActivity = this;

        setContentView(R.layout.activity_share_lumiere);

        initViewComponents();
    }

    private View.OnClickListener doShowFullImage(final ImageView imgView) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bitmap bitmap = ((BitmapDrawable) imgView.getDrawable()).getBitmap();
                ShowFullCardDialog dialog = new ShowFullCardDialog(currentActivity);
                dialog.setBitmap(bitmap);
                dialog.show();
            }
        };
    }

    private void initViewComponents() {
        cstImageShare = findViewById(R.id.activity_share_lumiere_cst_root);

        tvName1a = findViewById(R.id.activity_share_lumiere_ll_card_1a_name);
        tvName1b = findViewById(R.id.activity_share_lumiere_ll_card_1b_name);
        tvName1c = findViewById(R.id.activity_share_lumiere_ll_card_1c_name);

        tvName2a = findViewById(R.id.activity_share_lumiere_ll_card_2a_name);
        tvName2b = findViewById(R.id.activity_share_lumiere_ll_card_2b_name);
        tvName2c = findViewById(R.id.activity_share_lumiere_ll_card_2c_name);

        tvName3a = findViewById(R.id.activity_share_lumiere_ll_card_3a_name);
        tvName3b = findViewById(R.id.activity_share_lumiere_ll_card_3b_name);
        tvName3c = findViewById(R.id.activity_share_lumiere_ll_card_3c_name);

        tv_h = findViewById(R.id.tv_h);
        tv_armonie = findViewById(R.id.tv_armonie);
        tv_a = findViewById(R.id.tv_a);
        tv_venture = findViewById(R.id.tv_venture);
        tv_f = findViewById(R.id.tv_f);
        tv_ruit = findViewById(R.id.tv_ruit);
        tv_c = findViewById(R.id.tv_c);
        tv_omplicate = findViewById(R.id.tv_omplicate);
        tv_e = findViewById(R.id.tv_e);
        tv_nergy = findViewById(R.id.tv_nergy);
        tv_g = findViewById(R.id.tv_g);
        tv_uerre = findViewById(R.id.tv_uerre);
        tv_d = findViewById(R.id.tv_d);
        tv_esir = findViewById(R.id.tv_esir);
        tv_i = findViewById(R.id.tv_i);
        tv_ntution = findViewById(R.id.tv_ntution);
        tv_b = findViewById(R.id.tv_b);
        tv_esoin = findViewById(R.id.tv_esoin);

        UiHelper.TEXTSIZE(tv_h, 42);
        UiHelper.TEXTSIZE(tv_a, 42);
        UiHelper.TEXTSIZE(tv_f, 42);
        UiHelper.TEXTSIZE(tv_c, 42);
        UiHelper.TEXTSIZE(tv_e, 42);
        UiHelper.TEXTSIZE(tv_g, 42);
        UiHelper.TEXTSIZE(tv_d, 42);
        UiHelper.TEXTSIZE(tv_i, 42);
        UiHelper.TEXTSIZE(tv_b, 42);
        UiHelper.TEXTSIZE(tv_armonie, 25);
        UiHelper.TEXTSIZE(tv_venture, 25);
        UiHelper.TEXTSIZE(tv_ruit, 25);
        UiHelper.TEXTSIZE(tv_omplicate, 25);
        UiHelper.TEXTSIZE(tv_nergy, 25);
        UiHelper.TEXTSIZE(tv_uerre, 25);
        UiHelper.TEXTSIZE(tv_esir, 25);
        UiHelper.TEXTSIZE(tv_ntution, 25);
        UiHelper.TEXTSIZE(tv_esoin, 25);

        img1a = findViewById(R.id.activity_share_lumiere_img_card_1a);
        img1a.setOnClickListener(doShowFullImage(img1a));
        img1b = findViewById(R.id.activity_share_lumiere_img_card_1b);
        img1b.setOnClickListener(doShowFullImage(img1b));
        img1c = findViewById(R.id.activity_share_lumiere_img_card_1c);
        img1c.setOnClickListener(doShowFullImage(img1c));

        img2a = findViewById(R.id.activity_share_lumiere_img_card_2a);
        img2a.setOnClickListener(doShowFullImage(img2a));
        img2b = findViewById(R.id.activity_share_lumiere_img_card_2b);
        img2b.setOnClickListener(doShowFullImage(img2b));
        img2c = findViewById(R.id.activity_share_lumiere_img_card_2c);
        img2c.setOnClickListener(doShowFullImage(img2c));

        img3a = findViewById(R.id.activity_share_lumiere_img_card_3a);
        img3a.setOnClickListener(doShowFullImage(img3a));
        img3b = findViewById(R.id.activity_share_lumiere_img_card_3b);
        img3b.setOnClickListener(doShowFullImage(img3b));
        img3c = findViewById(R.id.activity_share_lumiere_img_card_3c);
        img3c.setOnClickListener(doShowFullImage(img3c));

        tvTitle = findViewById(R.id.activity_share_lumiere_txt_title);
        UiHelper.TEXTSIZE(tvTitle, 65);
        UiHelper.PADDING(tvTitle, 60, 40, 30, 0);
        tvTitle.setText(R.string.title_result);

        tvUserInfo = findViewById(R.id.activity_share_lumiere_txt_user_info);
        UiHelper.TEXTSIZE(tvUserInfo, 55);
        UiHelper.PADDING(tvUserInfo, 60, 0, 30, 0);

        tvQuestionTitle = findViewById(R.id.activity_share_lumiere_txt_question_title);
        UiHelper.TEXTSIZE(tvQuestionTitle, 55);
        UiHelper.PADDING(tvQuestionTitle, 60, 30, 30, 0);
        tvQuestionTitle.setText(R.string.title_question_of);

        tvQuestionContent = findViewById(R.id.activity_share_lumiere_txt_question_content);
        UiHelper.TEXTSIZE(tvQuestionContent, 55);
        UiHelper.PADDING(tvQuestionContent, 60, 0, 30, 0);

        tvDescription = findViewById(R.id.activity_share_lumiere_txt_description);
        UiHelper.TEXTSIZE(tvDescription, 55);
        UiHelper.PADDING(tvDescription, 60, 25, 30, 0);

        imgLogo = findViewById(R.id.activity_share_lumiere_iv_icon_launcher);
        UiHelper.RESIZE(imgLogo, 110, 110);

        txtAppName = findViewById(R.id.activity_share_lumiere_tv_telecharge);
        UiHelper.TEXTSIZE(txtAppName, 45);
        UiHelper.PADDING(txtAppName, 20, 0, 0, 0);

        imgGooglePlay = findViewById(R.id.activity_share_lumiere_img_google_play_logo);
        UiHelper.RESIZE(imgGooglePlay, 342, 103);
        imgAppStore = findViewById(R.id.activity_share_lumiere_img_apple_store_logo);
        UiHelper.RESIZE(imgAppStore, 342, 103);

        initLumiereCard();
        initDescription();
    }

    private void initDescription() {
        String userName = app.user.user_name;
        Date timeGetCard = app.cardMng.datSelectCard;
        if (null == timeGetCard) {
            timeGetCard = DateTimeHelper.TODAY();
        }
        String date = DateTimeHelper.STRINGFROMDATEV2(timeGetCard, getCurrentLanguage());

        String userInfo = userName + ", " + date;
        tvUserInfo.setText(userInfo);

        if (lumiereQuestion != null || !lumiereQuestion.equals("")) {
            if (!lumiereQuestion.equals("")) {
                tvQuestionContent.setText(lumiereQuestion);
            } else {
                tvQuestionTitle.setVisibility(View.GONE);
                tvQuestionContent.setVisibility(View.GONE);
            }
        } else {
            tvQuestionTitle.setVisibility(View.GONE);
            tvQuestionContent.setVisibility(View.GONE);
        }

        if (lumiereDescription != null) {
            tvDescription.setText(lumiereDescription);
        }
    }

    private void initLumiereCard() {
        if (lumiereImages != null) {
            //harmonie
            if (lumiereImages[0].equals("-1")) {
                img1a.setVisibility(View.INVISIBLE);
                tvName1a.setVisibility(View.INVISIBLE);
            } else {
                img1a.setVisibility(View.VISIBLE);
                tvName1a.setVisibility(View.VISIBLE);

                Byte cardID = new Byte(lumiereImages[0]);
                Bitmap bm = app.cardMng.getCardByNumber(cardID).getBitmap();
                img1a.setImageBitmap(bm);
            }

            //aventure
            if (lumiereImages[1].equals("-1")) {
                img1b.setVisibility(View.INVISIBLE);
                tvName1b.setVisibility(View.INVISIBLE);
            } else {
                img1b.setVisibility(View.VISIBLE);
                tvName1b.setVisibility(View.VISIBLE);

                Byte cardID = new Byte(lumiereImages[1]);
                Bitmap bm = app.cardMng.getCardByNumber(cardID).getBitmap();
                img1b.setImageBitmap(bm);
            }

            //fruit
            if (lumiereImages[2].equals("-1")) {
                img1c.setVisibility(View.INVISIBLE);
                tvName1c.setVisibility(View.INVISIBLE);
            } else {
                img1c.setVisibility(View.VISIBLE);
                tvName1c.setVisibility(View.VISIBLE);

                Byte cardID = new Byte(lumiereImages[2]);
                Bitmap bm = app.cardMng.getCardByNumber(cardID).getBitmap();
                img1c.setImageBitmap(bm);
            }

            //complicate
            if (lumiereImages[3].equals("-1")) {
                img2a.setVisibility(View.INVISIBLE);
                tvName2a.setVisibility(View.INVISIBLE);
            } else {
                img2a.setVisibility(View.VISIBLE);
                tvName2a.setVisibility(View.VISIBLE);

                Byte cardID = new Byte(lumiereImages[3]);
                Bitmap bm = app.cardMng.getCardByNumber(cardID).getBitmap();
                img2a.setImageBitmap(bm);
            }

            //energy
            if (lumiereImages[4].equals("-1")) {
                img2b.setVisibility(View.INVISIBLE);
                tvName2b.setVisibility(View.INVISIBLE);
            } else {
                img2b.setVisibility(View.VISIBLE);
                tvName2b.setVisibility(View.VISIBLE);

                Byte cardID = new Byte(lumiereImages[4]);
                Bitmap bm = app.cardMng.getCardByNumber(cardID).getBitmap();
                img2b.setImageBitmap(bm);
            }

            //guerre
            if (lumiereImages[5].equals("-1")) {
                img2c.setVisibility(View.INVISIBLE);
                tvName2c.setVisibility(View.INVISIBLE);
            } else {
                img2c.setVisibility(View.VISIBLE);
                tvName2c.setVisibility(View.VISIBLE);

                Byte cardID = new Byte(lumiereImages[5]);
                Bitmap bm = app.cardMng.getCardByNumber(cardID).getBitmap();
                img2c.setImageBitmap(bm);
            }

            //desire
            if (lumiereImages[6].equals("-1")) {
                img3a.setVisibility(View.INVISIBLE);
                tvName3a.setVisibility(View.INVISIBLE);
            } else {
                img3a.setVisibility(View.VISIBLE);
                tvName3a.setVisibility(View.VISIBLE);

                Byte cardID = new Byte(lumiereImages[6]);
                Bitmap bm = app.cardMng.getCardByNumber(cardID).getBitmap();
                img3a.setImageBitmap(bm);
            }

            //intution
            if (lumiereImages[7].equals("-1")) {
                img3b.setVisibility(View.INVISIBLE);
                tvName3b.setVisibility(View.INVISIBLE);
            } else {
                img3b.setVisibility(View.VISIBLE);
                tvName3b.setVisibility(View.VISIBLE);

                Byte cardID = new Byte(lumiereImages[7]);
                Bitmap bm = app.cardMng.getCardByNumber(cardID).getBitmap();
                img3b.setImageBitmap(bm);
            }

            //besoin
            if (lumiereImages[8].equals("-1")) {
                img3c.setVisibility(View.INVISIBLE);
                tvName3c.setVisibility(View.INVISIBLE);
            } else {
                img3c.setVisibility(View.VISIBLE);
                tvName3c.setVisibility(View.VISIBLE);

                Byte cardID = new Byte(lumiereImages[8]);
                Bitmap bm = app.cardMng.getCardByNumber(cardID).getBitmap();
                img3c.setImageBitmap(bm);
            }
        }
    }

    private Bitmap takeScreenshot() {
//        View rootView = getWindow().getDecorView().findViewById(android.R.id.content);
//        View screenView = rootView.getRootView();

        //Share mới
        cstImageShare.setDrawingCacheEnabled(true);
        cstImageShare.buildDrawingCache(true);
        Bitmap bitmap = Bitmap.createBitmap(cstImageShare.getDrawingCache());
        cstImageShare.setDrawingCacheEnabled(false);

        return bitmap;
    }

    private File imagePath;

/*    private void saveBitmap(Bitmap bitmap) {
        imagePath = new File(Environment.getExternalStorageDirectory() + "/screenshot.png");
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(imagePath);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
//            Log.e("MyDebug", e.getMessage(), e);
            System.out.println(e.getMessage());
        } catch (IOException e) {
//            Log.e("MyDebug", e.getMessage(), e);
            System.out.println(e.getMessage());
        }
    }*/

    private boolean isSharing = false;

    public void doShareLumiere(View view) {
        if (isSharing == false) {
            isSharing = true;
            Bitmap bitmap = takeScreenshot();
            Intent returnIntent = new Intent();
            returnIntent.putExtra(MonthCardActivity.RESULT_IMAGE_SHARE, bitmap);
            setResult(ShareLumiereActivity.RESULT_OK, returnIntent);
            finish();
        }
    }
}
