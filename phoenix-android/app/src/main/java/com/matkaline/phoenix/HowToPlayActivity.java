package com.matkaline.phoenix;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;

import ulitis.Constants;
import ulitis.UiHelper;
import ulitis.Utils;

public class HowToPlayActivity extends RootActivity implements MediaPlayer.OnCompletionListener, View.OnClickListener {
    private WebView webHowto;
    private ImageView image, iv_music;
    private TextView tv_my_cash;
    private MediaPlayer mediaPlayer = null;
    int[] tracks = new int[2];
    int currentTrack = 0;
    private Utils utils;
    private SharedPreferences prefs = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_how_to_play);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        image = (ImageView) findViewById(R.id.image_phoenix);
        utils = new Utils(this);
        init();

        tv_my_cash = (TextView) findViewById(R.id.tv_my_cash);
        if(app.user.coin > 0) {
            tv_my_cash.setText(String.valueOf(app.user.coin));
        } else {
            tv_my_cash.setText(String.valueOf(0));
        }
        switch (getCurrentLanguage().getLanguage()){
            case "vi":
                webViewSetDataLeftAlign(webHowto,loadData("howtoplay_vn"),UiHelper.GETSIZE(80));
                break;
            case "en":
                webViewSetDataLeftAlign(webHowto,loadData("howtoplay_en"),UiHelper.GETSIZE(80));
                break;
            case "ru":
                webViewSetDataLeftAlign(webHowto,loadData("howtoplay_ru"),UiHelper.GETSIZE(80));
                break;
            default:
                webViewSetDataLeftAlign(webHowto,loadData("howtoplay.txt"),UiHelper.GETSIZE(80));
                break;
        }
        iv_music.setOnClickListener(this);

        webHowto.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                if (url.contains("contact@matkaline.com")) {
                    Intent intent = new Intent(Intent.ACTION_SENDTO);
                    intent.setData(Uri.parse("mailto:" + "contact@matkaline.com"));
                    startActivity(intent);
                } else {
                    view.loadUrl(url);
                }

                return true;
            }
        });
//        webHowto.loadUrl(url);
    }
    @Override
    protected void onStop() {
        super.onStop();
        stopMusic();
    }

//    private boolean loadMusicStatus() {
//        return prefs.getBoolean(Constants.MUSIC_STATUS, false);
//    }

    public void stopMusic() {
        if(mediaPlayer!=null)
        {
            try {
                if(mediaPlayer.isPlaying()) {
                    mediaPlayer.stop();
                }
                mediaPlayer.reset();//It requires again setDataSource for player object.
                mediaPlayer.release();
            } catch (IllegalStateException e) {
//                Log.e("Error", e.getMessage());
            } catch (NullPointerException e) {

            }


        }
    }
    private String loadData(String inFile) {
        String tContents = "";
        InputStream stream;
        AssetManager assetManager = getAssets();
        //Log.v("MyDug", "Content" + tContents);
        try {
            stream = assetManager.open(inFile);
            int size = stream.available();
            byte[] buffer = new byte[size];
            stream.read(buffer);
            stream.close();
            tContents = new String(buffer, "UTF-8");
            //Log.v("MyDug", "Content = " + tContents);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return tContents;
    }
    @Override
    public void onResume() {
        super.onResume();
        if(!loadMusicStatus()) {
            iv_music.setAlpha(1.0f);
        } else {
            iv_music.setAlpha(0.3f);
        }
    }
    private void init() {
        prefs = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        setTitleScreen(getResources().getString(R.string.myHowtoplay_title));
        webHowto = (WebView) findViewById(R.id.webHowToPlay);
        webViewSetting(webHowto);
        iv_music = (ImageView) findViewById(R.id.iv_music);

        UiHelper.RESIZE(iv_music, 60, 60);
        UiHelper.RESIZE(image,551,411);
        UiHelper.MARGIN(image,0,75,0,144);

        webViewSetting(webHowto);

        View lyTitle = findViewById(R.id.inTitleBack);
        lyTitle.findViewById(R.id.inBackButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                left(v);
            }
        });
        if(!loadMusicStatus()) {
            tracks[0] = R.raw.card_phuong_dong_dao;
            tracks[1] = R.raw.card_phuong_tay_dao;
            mediaPlayer = MediaPlayer.create(getApplicationContext(), tracks[currentTrack]);
            mediaPlayer.setOnCompletionListener(this);
//        mediaPlayer.setLooping(true);
            mediaPlayer.start();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        mediaPlayer.release();
        if (currentTrack < tracks.length) {
            currentTrack++;
            if(currentTrack > 1) {
                currentTrack = 0;
            }
            mediaPlayer = MediaPlayer.create(getApplicationContext(), tracks[currentTrack]);
            mediaPlayer.setOnCompletionListener(this);
            mediaPlayer.start();
        }
    }

    private void webViewSetDataLeftAlign(WebView web,String myData,int padding) {
        String pish = "<html><head>" +
                "<meta name=\"viewport\" content=\"target-densitydpi=device-dpi, width=device-width, user-scalable=no\"/>" +
                "<style type=\"text/css\">body {text-align: left; color: #FFFFFF; padding-top:+"+(10*padding)+"px+;padding-left: "+ padding +"px;padding-right:"+padding+"px}</style></head><body>";
        String pas = "</body></html>";

        String myHtmlString = pish + myData + pas;
        web.loadDataWithBaseURL("file:///android_asset/", myHtmlString, "text/html", "UTF-8", null);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_music:
                if(!loadMusicStatus()) {
                    saveMusicStatus(true);
                    iv_music.setAlpha(0.3f);
                    utils.stopMusic();
//                    utils.muteMusic();
                } else {
                    saveMusicStatus(false);
                    iv_music.setAlpha(1.0f);
//                    utils.playMusic(R.raw.card_phoenix);
//                    utils.unmuteMusic();
                }
                break;

        }
    }
}
