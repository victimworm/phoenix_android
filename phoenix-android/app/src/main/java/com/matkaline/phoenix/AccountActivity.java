package com.matkaline.phoenix;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import apis.API;
import de.hdodenhof.circleimageview.CircleImageView;
import http.HttpManager;
import http.retrofit.RequestParams;
import ulitis.Constants;
import ulitis.GalleryManager;
import ulitis.ImageLoader;
import ulitis.UiHelper;

/**
 * Created by nmcuong on 9/6/2016.
 */
public class AccountActivity extends RootActivity {

    private int requestCode = -1;
    private CircleImageView imageUser;
    private ImageView iconCamera;
    private ImageView iconCalendar;
    private TextView txtUserName, txtCoin, edtAccountBy;
    private EditText edtBirthday, edtEmail, edtCity, edtCountry, edtPhone;
    private LinearLayout linearEdit;
    private LinearLayout btnCanlendar;

    /**
     * DatePickerDialog
     */
    private Calendar calendar;
    public int day;
    public int month;
    public int year;
    Bitmap bitmap;

    LinearLayout btSave;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_myprofile);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        initUI();
        init();
        initDate();
        app.user.insertAvatar(imageUser);
        edtPhone.setFilters(new InputFilter[]{filterPhoneNumber, new InputFilter.LengthFilter(11)});
        txtUserName.setText(app.user.user_name);
        txtCoin.setText(app.user.coin + " "+ getResources().getString(R.string.unit_coins));
        edtEmail.setText(app.user.email);
        edtBirthday.setText(app.user.birthday);
        edtCity.setText(app.user.city);
        edtCountry.setText(app.user.country);
        edtPhone.setText(app.user.phone);
        edtAccountBy.setText(app.user.type == 1 ? "Facebook" : "Google");
        //verifyStoragePermissions(this);
    }

    private void initUI() {
        imageUser = (CircleImageView) findViewById(R.id.imageUser);
        iconCamera = (ImageView) findViewById(R.id.camera);
        iconCalendar = (ImageView) findViewById(R.id.imgBtCalendar);
        btnCanlendar = (LinearLayout) findViewById(R.id.line_iconCalen);
        txtUserName = (TextView) findViewById(R.id.textViewName);
        txtCoin = (TextView) findViewById(R.id.textViewCoin);
        edtAccountBy = (TextView) findViewById(R.id.textedtAccountBy);

        linearEdit = (LinearLayout) findViewById(R.id.linear_edit);
//
        edtBirthday = (EditText) findViewById(R.id.editBirthday);
        edtEmail = (EditText) findViewById(R.id.editEmail);
        edtCity = (EditText) findViewById(R.id.editCity);
        edtCountry = (EditText) findViewById(R.id.editCounty);
        edtPhone = (EditText) findViewById(R.id.editPhone);
        imageUser.setEnabled(false);

        imageUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GalleryManager.openGallery(AccountActivity.this);
            }
        });
        setEnableEdit(false);
        LinearLayout lineAvatar = (LinearLayout) findViewById(R.id.line_avatar);
        UiHelper.RESIZE(lineAvatar,-1,700);

        LinearLayout lineCal = (LinearLayout) findViewById(R.id.line_iconCalen);
        UiHelper.RESIZE(lineCal,100,-1);

        RelativeLayout relayAvatar = (RelativeLayout) findViewById(R.id.relative_avatar);
        UiHelper.RESIZE(relayAvatar,329,329);
        UiHelper.MARGIN(relayAvatar,0,55,0,36);
        UiHelper.RESIZE(iconCamera,100,100);
        UiHelper.TEXTSIZE(txtUserName,75);
        UiHelper.TEXTSIZE(txtCoin,60);
        UiHelper.RESIZE(iconCalendar,74,74);

        UiHelper.RESIZE(linearEdit,-1,-1);
        UiHelper.PADDING(linearEdit,48,48,48,48);

        TextView etxtBir = (TextView) findViewById(R.id.etitBirthday);
        TextView etxtEml = (TextView) findViewById(R.id.etitEmail);
        TextView etxtCit = (TextView) findViewById(R.id.etitCity);
        TextView etxtCou = (TextView) findViewById(R.id.etitCountry);
        TextView etxtPho = (TextView) findViewById(R.id.etitPhone);
        TextView etxtAcc = (TextView) findViewById(R.id.etitAccount);
        UiHelper.TEXTSIZE(etxtBir,55);
        UiHelper.TEXTSIZE(etxtEml,55);
        UiHelper.TEXTSIZE(etxtCit,55);
        UiHelper.TEXTSIZE(etxtCou,55);
        UiHelper.TEXTSIZE(etxtPho,55);
        UiHelper.TEXTSIZE(etxtAcc,55);

        UiHelper.TEXTSIZE(edtBirthday,55);
        UiHelper.TEXTSIZE(edtEmail,55);
        UiHelper.TEXTSIZE(edtCity,55);
        UiHelper.TEXTSIZE(edtCountry,55);
        UiHelper.TEXTSIZE(edtPhone,55);
        UiHelper.TEXTSIZE(edtAccountBy,55);
        UiHelper.PADDING(edtBirthday,30,0,0,0);
        UiHelper.PADDING(edtEmail,30,0,0,0);
        UiHelper.PADDING(edtCity,30,0,0,0);
        UiHelper.PADDING(edtCountry,30,0,0,0);
        UiHelper.PADDING(edtPhone,30,0,0,0);
        UiHelper.PADDING(edtAccountBy,30,0,0,0);

    }

    /**
     * Loc thong tin edit
     */
    private void init() {
        setTitleScreen(getResources().getString(R.string.myProfile_title));
        View lyTitle = findViewById(R.id.inTitleBack);
        lyTitle.findViewById(R.id.inBackButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                initBack(v);
            }
        });

        lyTitle.findViewById(R.id.btn_Edit).setVisibility(View.VISIBLE);
        lyTitle.findViewById(R.id.btn_Edit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateEditUI(v);
            }
        });
        btSave = (LinearLayout) lyTitle.findViewById(R.id.btn_Save);
        btSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btSave.setEnabled(false);
                updateInforEdit(v);
            }
        });
    }
    private void initBack(final View v){
        if (findViewById(R.id.btn_Save).getVisibility() == View.VISIBLE && isNetworkStatusAvialable()) {
            dialogBuilder(getResources().getString(R.string.alert), getResources().getString(R.string.noti_saveInfo),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            setResult(Activity.RESULT_CANCELED);
                            updateInforEdit(v);
                            left(v);
                        }
                    }
                    , new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            left(v);
                        }
                    });
        } else if (findViewById(R.id.btn_Save).getVisibility() == View.VISIBLE && !isNetworkStatusAvialable()) {
            left(v);
        } else {
            left(v);
        }
    }
    private InputFilter filter = new InputFilter() {
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            for (int i = start; i < end; i++) {
                if (!Character.isLetterOrDigit(source.charAt(i))) {
                    return "";
                }
            }
            return null;
        }
    };


    private InputFilter filterEmail = new InputFilter() {
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            for (int i = start; i < end; i++) {
                if (Character.isLetterOrDigit(source.charAt(i)) || source.charAt(i) == '@' || source.charAt(i) == '.') {
                } else {
                    return "";
                }
            }
            return null;
        }
    };
    private InputFilter filterPhoneNumber = new InputFilter() {
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            for (int i = start; i < end; i++) {
                if (!Character.isDigit(source.charAt(i))) {
                    return "";
                }
            }
            return null;
        }
    };

    /**
     * check user login aready
     * and show User Infor on editable status
     */

    public void updateEditUI(View v) {
        if (!isNetworkStatusAvialable()) {
            alert(getString(R.string.alert),getString(R.string.alert_offline), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
        } else {
            findViewById(R.id.btn_Save).setVisibility(View.VISIBLE);
            findViewById(R.id.btn_Edit).setVisibility(View.GONE);
            iconCamera.setVisibility(View.VISIBLE);
            iconCalendar.setVisibility(View.VISIBLE);
            imageUser.setEnabled(true);
            btnCanlendar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    btnCanlendar.setEnabled(false);
                    DatePickerDialog date = new DatePickerDialog(AccountActivity.this, datePickerDialog, year, month, day);
                    date.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialogInterface) {
                            btnCanlendar.setEnabled(true);
                        }
                    });
                    date.getDatePicker().setMaxDate(System.currentTimeMillis());
                    date.show();
                }
            });
            setEnableEdit(true);
            edtBirthday.setText(app.user.birthday);
            edtEmail.setText(app.user.email);
            edtCity.setText(app.user.city);
            edtCountry.setText(app.user.country);
            edtPhone.setText(app.user.phone);
        }
    }

    /**
     * user click then will submit user infor to server
     * @param v
     */
    public void updateInforEdit(View v) {
        if (!isNetworkStatusAvialable()) {
            alert(getString(R.string.alert),getString(R.string.alert_offline), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            btSave.setEnabled(true);
        } else {
            findViewById(R.id.btn_Edit).setVisibility(View.VISIBLE);
            findViewById(R.id.btn_Save).setVisibility(View.GONE);
            iconCamera.setVisibility(View.GONE);
            iconCalendar.setVisibility(View.INVISIBLE);
            setEnableEdit(false);
            submit();
            imageUser.setEnabled(false);

        }
    }

    /**
     * Luu thong tin len Server
     */

    private void submit() {
        String url = API.URL_UPDATE + app.user.user_id + "/update";

        RequestParams params = new RequestParams();
        params.addParam("country",edtCountry.getText().toString());
        params.addParam("city",edtCity.getText().toString());
        params.addParam("phone",edtPhone.getText().toString());

        String strBirthday = edtBirthday.getText().toString();
        if (!strBirthday.equals("0000-00-00")){
            params.addParam("birthday",strBirthday);
        }

        if (bitmap!=null){
            params.addParam("avatar",app.user.getSmalFileAvatar(bitmap));
        }

        app.httpManager.doPost(url, params, new HttpManager.OnResponse() {
            @Override
            public void onResponse(JSONObject json) {
                app.user.updateEdit(json);
                if (bitmap!=null) {
                    app.user.saveCacheAvatar(bitmap);
                    bitmap = null;
                }
                btSave.setEnabled(true);
            }
        });
    }

    public boolean validate() {
        if (isEmpty(edtBirthday)) {
            alert("Thông báo", "Bạn chưa chọn ngày sinh.");
            return false;
        } else if (isEmpty(edtCity)) {
            alert("Thông báo", "Bạn chưa nhập thành phố bạn ở.");
            return false;
        }
        return true;
    }

    public boolean isEmpty(EditText str) {
        return str.getText().toString().trim().equals("");
    }

    public void initDate() {
        calendar = Calendar.getInstance();
        day = calendar.get(Calendar.DAY_OF_MONTH);
        month = calendar.get(Calendar.MONTH);
        year = calendar.get(Calendar.YEAR);
    }

    private DatePickerDialog.OnDateSetListener datePickerDialog = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
            edtBirthday.setText(selectedYear + "-" + (selectedMonth + 1) + "-" + selectedDay);
        }
    };


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            final Uri selectionImageUri = (data == null) ? GalleryManager.mCapturedImageURI : data.getData();
            if (selectionImageUri != null) {
                if (requestCode == Constants.ACTION_REQUEST_IMAGE) {
                    openFileFromGallery(selectionImageUri);
                }
            }
        }
    }

    

    /**
     *
     * @param uri
     */
    private void openFileFromGallery(Uri uri) {
//        Uri uri = data.getData();
        String path = GalleryManager.getPathFileFromUri(uri, this);
        bitmap = null;
        File f = null;
        if (path != null && (f = new File(path)).exists()) {
            bitmap = ImageLoader.DECODEFILE(f);
        } else {
            try {
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
            } catch (IOException e) {
                e.printStackTrace();
                alert(e.getMessage(),null);
            }
        }
        if (bitmap!=null) {
            float rw = 320f / bitmap.getWidth();
            float rh = 320f / bitmap.getHeight();
            if (rw < 1) {
                float rate = rw < rh ? rw : rh;
                int w = (int) (rate * bitmap.getWidth());
                int h = (int) (rate * bitmap.getHeight());
                bitmap = Bitmap.createScaledBitmap(bitmap, w, h, false);
            }
            imageUser.setImageBitmap(bitmap);
        }

    }

    private void setEnableEdit(boolean value){
        edtCity.setEnabled(value);
        edtCountry.setEnabled(value);
        edtPhone.setEnabled(value);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
    }

}
