package com.matkaline.phoenix;

import android.content.res.Resources;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.TypedValue;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import adapters.YourWeekAdapter;
import dbs.HistoryWeek;
import ulitis.Constants;
import ulitis.UiHelper;
import ulitis.Utils;

public class YourWeekActivity extends RootActivity {

    private TextView txtTitle;
    private Utils utils;
    private List<HistoryWeek> history;
    private YourWeekAdapter adapter;
    private ListView listview;
    private int columnWidth;
    private ImageView imageBack;
    private LinearLayout line1;
    private TextView tv_my_cash;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_your_week);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        utils = new Utils(this);
        resize();

        tv_my_cash = (TextView) findViewById(R.id.tv_my_cash);
        if(app.user.coin > 0) {
            tv_my_cash.setText(String.valueOf(app.user.coin));
        } else {
            tv_my_cash.setText(String.valueOf(0));
        }

        InitilizeGridLayout();
        history = HistoryWeek.GetCards();
        adapter = new YourWeekAdapter(YourWeekActivity.this,columnWidth,history);
        listview.setAdapter(adapter);
    }


    private void resize(){
        listview = (ListView) findViewById(R.id.list_view_yourweek);
        txtTitle = (TextView) findViewById(R.id.titYourWeek);
        line1 = (LinearLayout) findViewById(R.id.linear_week);
        imageBack = (ImageView) findViewById(R.id.icon_back);
        UiHelper.TEXTSIZE(txtTitle,60);
        UiHelper.MARGIN(txtTitle,0,40,0,120);
        UiHelper.RESIZE(line1,165,135);
        UiHelper.RESIZE(imageBack,43,73);
        UiHelper.MARGIN(imageBack,0,35,0,45);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
    }

    /**
     * initilization grid layout
     */
    private void InitilizeGridLayout() {
        Resources r = getResources();
        float padding = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, Constants.GRID_PADDING, r.getDisplayMetrics());
        columnWidth = (int) ((utils.getScreenWidth() - ((Constants.NUM_OF_COLUMNS + Constants.NUM_OF_COLUMNS) * padding) - padding) / Constants.NUM_OF_COLUMNS);
        listview.setDividerHeight((int)padding*2);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        adapter.destroy();
    }

}
