package com.matkaline.phoenix.utils.worker;

public interface WorkerRunnable {
    <T> T run();
}
