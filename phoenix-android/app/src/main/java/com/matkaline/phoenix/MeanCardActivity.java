package com.matkaline.phoenix;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import models.models.cards.CardItem;
import ulitis.UiHelper;

public class MeanCardActivity extends RootActivity {

//    private ImageView imageView;
    private ImageView imageBack;
    private TextView txtTitle;
    private LinearLayout line1;

    private CardItem item;
    private WebView webDesc;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.meancard_activity);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        resize();
        item = app.gameInforCardItem;
        try {
            String img = "<div style=\"text-align:center;\"><img src=\"" + item.image_id + ".jpg\" alt=\"\" width=\""+UiHelper.GETSIZE(689)+"\" height=\""+UiHelper.GETSIZE(1343)+"\"></div>"+
                    "<div style=\"height:"+UiHelper.GETSIZE(120)+"px\"></div>";
            switch (getCurrentLanguage().getLanguage()) {
                case "vi":
                    webViewSetData(webDesc, img, LoadData("content_card_vn" + app.cardID), 30, item.reverse);
                    break;
                case "en":
                    webViewSetData(webDesc, img, LoadData("content_card" + app.cardID), 30, item.reverse);
                    break;
                default:
                    webViewSetData(webDesc, img, LoadData("content_card" + app.cardID), 30, item.reverse);
                    break;
            }
        } catch (NullPointerException ex){
            ex.printStackTrace();
            finish();
            overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
        }

        webDesc.getSettings().setBuiltInZoomControls(true);
        webDesc.getSettings().setDisplayZoomControls(false);

        webDesc.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                String text = url.replace("file:///android_asset/","");
                int number = Integer.parseInt(text);
                int numberId = Math.abs(number);
                CardItem cardItem = new CardItem(numberId);
                if(number < 0) {
                    cardItem.reverse = true;
                } else {
                    cardItem.reverse = false;
                }
                app.cardID = app.cardMng.getCardId(cardItem);
                app.gameInforCardItem = cardItem;
//                Intent intent = new Intent(getApplicationContext(), MeanCardActivity.class);
//                startActivity(intent);
//                finish();
                Intent intent = getIntent();
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                finish();
                startActivity(intent);

                return super.shouldOverrideUrlLoading(view, url);
            }
        });
    }

    private void resize(){
        line1 = (LinearLayout) findViewById(R.id.linear_mean);
        txtTitle = (TextView) findViewById(R.id.titMeaningCard);
        imageBack = (ImageView) findViewById(R.id.icon_back);

        webDesc = (WebView) findViewById(R.id.webDesc);
        webViewSetting(webDesc);

        UiHelper.RESIZE(line1,165,135);
        UiHelper.RESIZE(imageBack,43,73);
        UiHelper.MARGIN(imageBack,0,35,0,45);
        UiHelper.TEXTSIZE(txtTitle,60);
        UiHelper.MARGIN(txtTitle,0,0,0,120);

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
    }

}
