package com.matkaline.phoenix.utils.worker;

import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class WorkerThread {

    public static synchronized void doOnWorkerThread(final WorkerRunnable runnable, final OnCompleted onQueryFinish) {
        Single.create(new SingleOnSubscribe<Object>() {
                    @Override
                    public void subscribe(SingleEmitter emitter) throws Exception {
                        Object object = runnable.run();
                        if (object == null) {
                            emitter.onError(new Throwable("null object error"));
                        } else {
                            emitter.onSuccess(object);
                        }
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSingleObserver<Object>() {

                    @Override
                    public void onSuccess(Object o) {
                        if (onQueryFinish == null) return;
                        onQueryFinish.onFinish(o);
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (onQueryFinish == null) return;
                        onQueryFinish.onError(e.toString());
                    }
                });
    }

}
