package com.matkaline.phoenix.data.local.prefs;

import android.content.Context;
import android.content.SharedPreferences;

public class AppPreferencesHelper implements PreferencesHelper {

    private static final String PREF_KEY_YOUR_QUESTION = "PREF_KEY_YOUR_QUESTION";
    private static final String PREF_KEY_CURRENT_TODAY = "current_today";
    private static final String PREF_KEY_QUESTION_NOTI = "PREF_KEY_QUESTION_NOTI";
    private static final String PREF_KEY_CARD_TYPE = "PREF_KEY_CARD_TYPE";

    private final SharedPreferences mPrefs;

    public AppPreferencesHelper(Context context, String prefFileName) {
        mPrefs = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE);
    }

    @Override
    public void setYourQuestion(String question) {
        mPrefs.edit().putString(PREF_KEY_YOUR_QUESTION, question).apply();
    }

    @Override
    public String getYourQuestion() {
        return mPrefs.getString(PREF_KEY_YOUR_QUESTION, null);
    }

    @Override
    public void removeYourQuestion() {
        mPrefs.edit().remove(PREF_KEY_YOUR_QUESTION).apply();
    }

    @Override
    public void removeTodayStored() {
        mPrefs.edit().remove(PREF_KEY_CURRENT_TODAY).apply();
    }

    @Override
    public void removeNotificationQuestionData() {
        mPrefs.edit().remove(PREF_KEY_QUESTION_NOTI).apply();
        mPrefs.edit().remove(PREF_KEY_CARD_TYPE).apply();
    }

    @Override
    public void saveTypeCardOnNotification(int type) {
        mPrefs.edit().putInt(PREF_KEY_CARD_TYPE, type).apply();
    }

    @Override
    public int getCardTypeOnNotification() {
        return mPrefs.getInt(PREF_KEY_CARD_TYPE, -1);
        /*
        int type = mPrefs.getInt(PREF_KEY_CARD_TYPE, 0);
        byte result = 0;
        switch (type) {
            case 0: {
                //CHOC = 0;//XUNG DOT
                result = 0;
                break;
            }
            case 1: {
                //CHARME = 1;//HAP DAN
                result = 1;
                break;
            }
            case 2: {
                //CHOCCHARME = 2;//
                result = 2;
                break;
            }
            case 3: {
                //MATKA = 3;//NO CHOC NO CHARME
                result = 3;
                break;
            }
        }
        return result;
        */
    }
}
