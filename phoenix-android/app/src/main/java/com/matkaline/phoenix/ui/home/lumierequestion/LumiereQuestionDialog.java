package com.matkaline.phoenix.ui.home.lumierequestion;

import android.app.Dialog;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.matkaline.phoenix.R;
import com.matkaline.phoenix.ui.home.questionchoose.ConfirmSkipQuestionDialog;
import com.matkaline.phoenix.ui.home.questionchoose.IFConfirmSkipQuestionFeedback;

import ulitis.UiHelper;

public class LumiereQuestionDialog {

    private IFLumiereQuestionListener mListener;

    public void setFeedbackDialogListener(IFLumiereQuestionListener listener) {
        this.mListener = listener;
    }

    private int deviceWith = 0;
    private Context context;
    private WindowManager manager;

    private LumiereQuestionDialog() {

    }

    public static LumiereQuestionDialog doCreate(Context context, WindowManager manager) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        manager.getDefaultDisplay().getMetrics(displayMetrics);

        LumiereQuestionDialog dialog = new LumiereQuestionDialog();
        dialog.deviceWith = displayMetrics.widthPixels;
        dialog.context = context;
        dialog.manager = manager;

        return dialog;
    }

    public void show() {
        if (this.context == null) {
            return;
        }

        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_lumiere_question);

        ImageButton btnClose = dialog.findViewById(R.id.dialog_lumiere_question_btn_close);
        final EditText edtQuestion = dialog.findViewById(R.id.dialog_lumiere_question_edt_question);
        ImageView imgBackCard = dialog.findViewById(R.id.dialog_lumiere_question_image_card);

        UiHelper.RESIZE(btnClose, 80, 80);
        UiHelper.MARGIN(btnClose, 0, 30, 30, 0);
        UiHelper.TEXTSIZE(edtQuestion, 46);
        UiHelper.MARGIN(edtQuestion, 50, 40, 50, 0);
        UiHelper.PADDING(edtQuestion, 30, 23, 0, 23);
        UiHelper.PADDING(imgBackCard, 10, 10, 10, 10);
        UiHelper.MARGIN(imgBackCard, 0, 40, 0, 0);

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        imgBackCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String question = edtQuestion.getText().toString().trim();
                if (question.equals("")) {
                    ConfirmSkipQuestionDialog confirmDialog = new ConfirmSkipQuestionDialog(context, manager);
                    confirmDialog.setFeedback(new IFConfirmSkipQuestionFeedback() {
                        @Override
                        public void goGetCardWithoutQuestion() {
                            if (mListener != null) {
                                mListener.doListen(question);
                            }
                            dialog.dismiss();
                        }
                    });
                    confirmDialog.show();
                } else {
                    if (mListener != null) {
                        mListener.doListen(question);
                    }
                    dialog.dismiss();
                }
            }
        });

        dialog.show();

        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(dialog.getWindow().getAttributes());
        int width = (int) (deviceWith * 0.93);
        layoutParams.width = width;
        dialog.getWindow().setAttributes(layoutParams);
    }
}
