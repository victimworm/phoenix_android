package com.matkaline.phoenix;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

import models.models.cards.Card;
import ulitis.DateTimeHelper;
import ulitis.UiHelper;

public class NextMonthCardResultActivity extends RootActivity {
    private WebView webDesc;
    private ImageView imageBack;
    private TextView txtTitle;
    private LinearLayout line1;
    private SharedPreferences prefs;
    private String sentences;
    private String title;
    private TextView tv_user_name;
    private TextView txtDate;
    private TextView tv_team_result_card;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_next_month_card_result);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        resize();
        prefs = app.getSharedPreferences(app.getPackageName(), app.MODE_PRIVATE);
        switch (getCurrentLanguage().getLanguage()) {
            case "vi":
                sentences = prefs.getString(NextMonthActivity.SENTENCES_VI, null);
                title = prefs.getString(NextMonthActivity.TITLE_VI, null);
                break;
            case "en":
                sentences = prefs.getString(NextMonthActivity.SENTENCES_EN, null);
                title = prefs.getString(NextMonthActivity.TITLE_EN, null);
                break;
            default:
                sentences = prefs.getString(NextMonthActivity.SENTENCES, null);
                title = prefs.getString(NextMonthActivity.TITLE, null);
                break;
        }

        // Set user name
        if (app.user.isLogin() && !TextUtils.isEmpty(app.user.user_name)) {
            tv_user_name.setText(app.user.user_name);
        }

        // Set date time
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getDefault());
        calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) + 1); // plus for next month
        int dayOfWeek = calendar.get(Calendar.DAY_OF_MONTH);
        if(dayOfWeek < 26) { // get last 30 days from day 26 of last month
//            calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - 1);
            // do nothing

        } else { // get next 30 days from day 26 of this month
            calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) + 1);
        }

        String date = null;
        switch (getCurrentLanguage().getLanguage()) {
            case "vi":
                date = DateTimeHelper.dateLocale(calendar.getTime(), Locale.getDefault());
                break;
            case "en":
                date = DateTimeHelper.dateLocale(calendar.getTime(), Locale.US);
                break;
            default:
                date = DateTimeHelper.dateLocale(calendar.getTime(), Locale.FRANCE);
                date = date.substring(0,1).toUpperCase() + date.substring(1);
                break;
        }

        txtDate.setText(date);

        // Set title
        if(!TextUtils.isEmpty(title)) {
            txtTitle.setText(title);
        }

        // Set title team & color
        String titleTeam = prefs.getString(NextMonthActivity.TITLE_TEAM, null);
        if (!TextUtils.isEmpty(titleTeam) && titleTeam.equals(MonthCardActivity.CREATION)) {
            titleTeam = getString(R.string.team_creation);
            tv_team_result_card.setText(titleTeam);
            tv_team_result_card.setTextColor(Color.GREEN);
        } else if (!TextUtils.isEmpty(titleTeam) && titleTeam.equals(MonthCardActivity.REVOLTE)) {
            titleTeam = getString(R.string.team_revolte);
            tv_team_result_card.setText(titleTeam);
            tv_team_result_card.setTextColor(Color.RED);
        } else if (!TextUtils.isEmpty(titleTeam) && titleTeam.equals(MonthCardActivity.SOUCI)) {
            titleTeam = getString(R.string.team_souci);
            tv_team_result_card.setText(titleTeam);
            tv_team_result_card.setTextColor(Color.BLUE);
        } else if (!TextUtils.isEmpty(titleTeam) && titleTeam.equals(MonthCardActivity.ZEN)) {
            titleTeam = getString(R.string.team_zen);
            tv_team_result_card.setText(titleTeam);
            tv_team_result_card.setTextColor(Color.YELLOW);
        }
        try {
            if(!TextUtils.isEmpty(sentences)) {
                webViewSetDataLeftAlign(webDesc, sentences, 30);
            }
        } catch (NullPointerException ex){
            ex.printStackTrace();
            finish();
            overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
        }

        webDesc.getSettings().setBuiltInZoomControls(true);
        webDesc.getSettings().setDisplayZoomControls(false);
    }

    private void resize(){
        line1 = (LinearLayout) findViewById(R.id.linear_month_result);
        txtTitle = (TextView) findViewById(R.id.tv_month_card_result_title);
        imageBack = (ImageView) findViewById(R.id.icon_back);

        tv_user_name = (TextView) findViewById(R.id.tv_user_name);
        txtDate = (TextView) findViewById(R.id.txtDate);
        tv_team_result_card = (TextView) findViewById(R.id.tv_team_result_card);

        webDesc = (WebView) findViewById(R.id.webResult);
        webViewSetting(webDesc);

        UiHelper.TEXTSIZE(tv_team_result_card, 60);
        UiHelper.MARGIN(tv_team_result_card, 0, 15, 0, 100);
        UiHelper.TEXTSIZE(txtDate, 45);
        UiHelper.TEXTSIZE(tv_user_name, 45);

        UiHelper.RESIZE(line1,165,135);
        UiHelper.RESIZE(imageBack,43,73);
        UiHelper.MARGIN(imageBack,0,35,0,45);
        UiHelper.TEXTSIZE(txtTitle,60);
        UiHelper.MARGIN(txtTitle,0,0,0,0);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
    }

    private void webViewSetDataLeftAlign(WebView web,String myData,int padding) {
        String pish = "<html><head>" +
                "<meta name=\"viewport\" content=\"target-densitydpi=device-dpi, width=device-width, user-scalable=no\"/>" +
                "<style type=\"text/css\">body {text-align: left; color: #FFFFFF; padding-top:+"+(10*padding)+"px+;padding-left: "+ padding +"px;padding-right:"+padding+"px}</style></head><body>";
        String pas = "</body></html>";

        String myHtmlString = pish + myData + pas;
        web.loadDataWithBaseURL("file:///android_asset/", myHtmlString, "text/html", "UTF-8", null);
    }

}
