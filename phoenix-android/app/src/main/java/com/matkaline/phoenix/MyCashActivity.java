package com.matkaline.phoenix;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.inappbilling.inappbilling.util.IabHelper;
import com.example.inappbilling.inappbilling.util.IabResult;
import com.example.inappbilling.inappbilling.util.Inventory;
import com.example.inappbilling.inappbilling.util.Purchase;

import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;
import http.HttpManager;
import ulitis.UiHelper;

public class MyCashActivity extends RootActivity {


    private CircleImageView imgAvatar;
    private TextView txtCoins;
    private TextView txtUserName;
    private LinearLayout[] lyBuy;
    private HttpManager.OnResponse onResponse = new HttpManager.OnResponse() {
        @Override
        public void onResponse(JSONObject jso) {
//            Log.v("MyDebug","JSON = " + jso);
            app.user.updateCoinCount(jso);
            enableBuyCoin(true);
            consumeItem();
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_cash);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        init();
        initUI();
        enableBuyCoin(true);
    }

    IabHelper mHelper;

    private void init() {
        showProgress();
        String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAgwOY+EytZQZ3/BKZ/kGx4RQ0vToHYbfeOH3uCoL/gl7VYQt9lGjP7aTwg57HYRRcme0UagGlSiEx7QE6jfC/xzKzWmwjJ7YO7xAI9oUyV+hWdxKrjCptVfLtsPmmFWxaCDUv5tTUxUWGB+AebdVZg9v8whbqiwQEzIIEz+TYDVz1lfyMPWVty7w/Cl/gvtmIcoXAIFwQjDRaXP+gxIw2r7GffRLlhlWl/GHlyh76ZRroMJqFI3gEbyrWBeDn4J1uxRKDQoTAnX1GEZVBd12X9vmbc9zeGYDmmrHvMXXpxCCRTcSKeOV4rdzoz4nnb219Ve6SefwRO3nEdaXXlxnpfwIDAQAB";
        mHelper = new IabHelper(this, base64EncodedPublicKey);
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    alert("LOGCATE", "In-app Billing setup failed: " + result.getMessage(), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            finish();
                        }
                    });
                } else {
                    hideProgress();
                }
            }
        });

    }

    private void initUI() {
        setTitleScreen(getResources().getString(R.string.myCashTitle));

        imgAvatar = (CircleImageView) findViewById(R.id.imgAvatar);
        txtUserName = (TextView) findViewById(R.id.txtUsername);
        txtCoins = (TextView) findViewById(R.id.txtCoins);
        app.user.insertAvatar(imgAvatar);
        txtUserName.setText(app.user.user_name);
        txtCoins.setText(app.user.coin + " "+getResources().getString(R.string.unit_coins));
        View lyTitle = findViewById(R.id.inTitleBack);
        lyTitle.findViewById(R.id.inBackButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                left(v);
            }
        });
        lyTitle.findViewById(R.id.btnBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                left(v);
            }
        });

        UiHelper.RESIZE(imgAvatar,329,329);
        UiHelper.MARGIN(imgAvatar,0,100,0,70);
        UiHelper.TEXTSIZE(txtUserName,75);
        UiHelper.MARGIN(txtUserName,0,0,0,40);
        UiHelper.TEXTSIZE(txtCoins,60);
        UiHelper.MARGIN(txtCoins,0,0,0,130);

        TextView title = (TextView) findViewById(R.id.titlePackage);
        UiHelper.TEXTSIZE(title,55);
        UiHelper.MARGIN(title,0,150,0,130);

        lyBuy = new LinearLayout[3];

        lyBuy[0] = (LinearLayout) findViewById(R.id.lyBuyCoin1);
        UiHelper.RESIZE(lyBuy[0],324,582);
        UiHelper.MARGIN(lyBuy[0],40,0,35,360);
        lyBuy[1] = (LinearLayout) findViewById(R.id.lyBuyCoin2);
        UiHelper.RESIZE(lyBuy[1],324,582);
        UiHelper.MARGIN(lyBuy[1],35,0,35,360);
        lyBuy[2] = (LinearLayout) findViewById(R.id.lyBuyCoin3);
        UiHelper.RESIZE(lyBuy[2],324,582);
        UiHelper.MARGIN(lyBuy[2],35,0,40,360);

        ImageView img1 = (ImageView) findViewById(R.id.icon_Coin1);
        UiHelper.RESIZE(img1,143,143);
        UiHelper.MARGIN(img1,0,120,0,40);
        TextView txtNum1 = (TextView) findViewById(R.id.txtNumber1);
        UiHelper.TEXTSIZE(txtNum1,48);
        UiHelper.MARGIN(txtNum1,0,0,0,50);
        TextView txtCoin1 = (TextView) findViewById(R.id.txtCoin1);
        UiHelper.TEXTSIZE(txtCoin1,100);
        txtCoin1.setText("80");

        ImageView img2 = (ImageView) findViewById(R.id.icon_Coin2);
        UiHelper.RESIZE(img2,143,143);
        UiHelper.MARGIN(img2,0,120,0,35);
        TextView txtNum2 = (TextView) findViewById(R.id.txtNumber2);
        UiHelper.TEXTSIZE(txtNum2,48);
        UiHelper.MARGIN(txtNum2,0,0,0,50);
        TextView txtCoin2 = (TextView) findViewById(R.id.txtCoin2);
        UiHelper.TEXTSIZE(txtCoin2,100);
        txtCoin2.setText("500");

        ImageView img3 = (ImageView) findViewById(R.id.icon_Coin3);
        UiHelper.RESIZE(img3,143,143);
        UiHelper.MARGIN(img3,0,120,0,35);
        TextView txtNum3 = (TextView) findViewById(R.id.txtNumber3);
        UiHelper.TEXTSIZE(txtNum3,48);
        UiHelper.MARGIN(txtNum3,0,0,0,50);
        TextView txtCoin3 = (TextView) findViewById(R.id.txtCoin3);
        UiHelper.TEXTSIZE(txtCoin3,100);
        txtCoin3.setText("1300");
    }

    static String ITEM_SKU = "";
    private byte purchasePakageType = -1;

    public void buyCoin(View v) {
        if (isNetworkStatusAvialable()) {
            enableBuyCoin(false);
            String purchaseToken = "";
            switch (v.getId()) {
                case R.id.lyBuyCoin1:
                    ITEM_SKU = "com.matkaline.phoenix.buy_1";
                    purchaseToken = "buy_coin_1";
                    purchasePakageType = 1;
                    break;
                case R.id.lyBuyCoin2:
                    ITEM_SKU = "com.matkaline.phoenix.buy_2";
                    purchaseToken = "buy_coin_2";
                    purchasePakageType = 2;
                    break;
                case R.id.lyBuyCoin3:
                    ITEM_SKU = "com.matkaline.phoenix.buy_3";
                    purchaseToken = "buy_coin_3";
                    purchasePakageType = 3;
//                go(MyCashActivityDemo.class);
                    break;

            }
//            Log.v("MyDebug","ITEM_SKU= "+ITEM_SKU);
            mHelper.launchPurchaseFlow(this, ITEM_SKU, 10000, new IabHelper.OnIabPurchaseFinishedListener() {
                @Override
                public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
                    if (result.isFailure()) {
//                        Log.v("MyDebug","result.getResponse():"+ result.getResponse());
                        switch (result.getResponse()) {
                            case 7:
                                consumeItem();
                                break;
                            case -1005:
                            default:
                                enableBuyCoin(true);
                                break;
                        }
                        enableBuyCoin(true);
                        return;
                    } else if (purchase.getSku().equals(ITEM_SKU)) {
//                        Log.v("MyDebug", "purchase.getSku().equals(ITEM_SKU)" + "\tpurchasePakageType:" + purchasePakageType + "\t(purchase.getSku():"+purchase.getSku() + "\tmDeveloperPayload:"+purchase.getDeveloperPayload());
                        showProgress();
                        switch (purchasePakageType) {
                            case 1:
                                app.user.submitCoin(+80, onResponse);
                                break;
                            case 2:
                                app.user.submitCoin(+500, onResponse);
                                break;
                            case 3:
                                app.user.submitCoin(+1300, onResponse);
                                break;
                        }
//                        consumeItem();
                        txtCoins.setText(app.user.coin + " " + getResources().getString(R.string.unit_coins));

                    }
                }
            }, purchaseToken);
        }else{
            alert(getString(R.string.alert), getString(R.string.alert_offline), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
        }
    }

    private void enableBuyCoin(boolean b){
        for(LinearLayout ly : lyBuy){
            ly.setEnabled(b);
        }
    }

    private void consumeItem() {
//        Log.v("MyDebug","consumeItem()");
        showProgress();
        enableBuyCoin(false);
        mHelper.queryInventoryAsync(new IabHelper.QueryInventoryFinishedListener() {
            @Override
            public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
                if (result.isFailure()) {
//                    alert("Thông báo","Fail onQueryInventoryFinished");
                } else {
                    mHelper.consumeAsync(inventory.getPurchase(ITEM_SKU), new IabHelper.OnConsumeFinishedListener() {
                        @Override
                        public void onConsumeFinished(Purchase purchase, IabResult result) {
                            if (result.isSuccess()) {
                                enableBuyCoin(true);
                                hideProgress();
                                finish();
                            } else {
//                                alert("Thông báo","Fail purchase");
                            }
                        }
                    });
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mHelper != null) mHelper.dispose();
        mHelper = null;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
    }
}
