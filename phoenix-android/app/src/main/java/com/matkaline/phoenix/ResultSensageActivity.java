package com.matkaline.phoenix;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import ulitis.UiHelper;

public class ResultSensageActivity extends RootActivity {
    private WebView webDesc;
    private ImageView imageBack;
    private TextView txtTitle;
    private LinearLayout line1;
    private SharedPreferences prefs;
    private String sentences;
    private String title;
    private TextView tv_user_name;
    private TextView txtDate;
    private TextView tv_team_result_card;
    private TextView tv_name, tv_team, tv_externa;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_sensage);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        resize();
        prefs = app.getSharedPreferences(app.getPackageName(), app.MODE_PRIVATE);
        sentences = prefs.getString(ResultActivity.SENTENCE_SENSAGE_DEFI, null);
        if(TextUtils.isEmpty(sentences)) {
            sentences = prefs.getString(ResultActivity.SENTENCE_SENSAGE, null);
        }

        try {
            if(!TextUtils.isEmpty(sentences)) {
                webViewSetDataLeftAlign(webDesc, sentences, 30);
            }
        } catch (NullPointerException ex){
            ex.printStackTrace();
            finish();
            overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
        }

        webDesc.getSettings().setBuiltInZoomControls(true);
        webDesc.getSettings().setDisplayZoomControls(false);
    }

    private void resize(){
        line1 = (LinearLayout) findViewById(R.id.linear_month_result);
        txtTitle = (TextView) findViewById(R.id.tv_month_card_result_title);
        imageBack = (ImageView) findViewById(R.id.icon_back);

        tv_user_name = (TextView) findViewById(R.id.tv_user_name);
        txtDate = (TextView) findViewById(R.id.txtDate);
        tv_team_result_card = (TextView) findViewById(R.id.tv_team_result_card);

        tv_name = (TextView) findViewById(R.id.tv_name);
        tv_team = (TextView) findViewById(R.id.tv_team);
        tv_externa = (TextView) findViewById(R.id.tv_externa);

        webDesc = (WebView) findViewById(R.id.webResult);
        webViewSetting(webDesc);

        UiHelper.TEXTSIZE(tv_team_result_card, 60);
        UiHelper.MARGIN(tv_team_result_card, 0, 15, 0, 100);
        UiHelper.TEXTSIZE(txtDate, 45);
        UiHelper.TEXTSIZE(tv_user_name, 45);

        UiHelper.RESIZE(line1,165,135);
        UiHelper.RESIZE(imageBack,43,73);
        UiHelper.MARGIN(imageBack,0,35,0,45);
        UiHelper.TEXTSIZE(txtTitle,60);
        UiHelper.MARGIN(txtTitle,0,0,0,0);

        UiHelper.TEXTSIZE(tv_name,45);
        UiHelper.MARGIN(tv_name,0,0,0,0);
        UiHelper.TEXTSIZE(tv_team,45);
        UiHelper.MARGIN(tv_team,10,0,0,0);
        UiHelper.TEXTSIZE(tv_externa,45);
        UiHelper.MARGIN(tv_externa,10,0,0,0);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
    }

    private void webViewSetDataLeftAlign(WebView web,String myData,int padding) {
        String pish = "<html><head>" +
                "<meta name=\"viewport\" content=\"target-densitydpi=device-dpi, width=device-width, user-scalable=no\"/>" +
                "<style type=\"text/css\">body {text-align: left; color: #FFFFFF; padding-top:+"+(10*padding)+"px+;padding-left: "+ padding +"px;padding-right:"+padding+"px}</style></head><body>";
        String pas = "</body></html>";

        String myHtmlString = pish + myData + pas;
        web.loadDataWithBaseURL("file:///android_asset/", myHtmlString, "text/html", "UTF-8", null);
    }
}
