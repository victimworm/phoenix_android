package com.matkaline.phoenix.utils;

public final class AppConstants {

    public final static String PREF_NAME = "phoenix_pref_file";

    private AppConstants() {

    }
}
