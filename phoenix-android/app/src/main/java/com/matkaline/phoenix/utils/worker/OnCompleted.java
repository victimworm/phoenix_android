package com.matkaline.phoenix.utils.worker;

public interface OnCompleted<T> {
    void onFinish(T object);
    void onError(String error);
}
