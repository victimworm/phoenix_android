package tests;

import android.os.AsyncTask;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class TestRequest {

    public void requestThread(){
       new AsyncTask() {
           @Override
           protected Object doInBackground(Object[] objects) {
               request();
               return null;
           }
       }.execute();
    }

	public void request(){
		
            URL url;
			try {
				url = new URL("http://phoenix.jit.vn/api/card/20?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjMsImlzcyI6Imh0dHA6XC9cL3Bob2VuaXguaml0LnZuXC9hcGlcL2F1dGhcL2xvZ2luIiwiaWF0IjoxNDc2NzY0Nzc0LCJleHAiOjE0NzY3NjgzNzQsIm5iZiI6MTQ3Njc2NDc3NCwianRpIjoiZjliNjNlZTgwMjA0MzNiZTgyM2Q4YTc3ZWNmM2U1YTQifQ.-XhqOLiYHY1nBWbgr-3BlwmSUKGLwgXzjNmf61vU7-E");
				HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
				int reqCode = urlConnection.getResponseCode();
				System.out.println("reqCode = "+reqCode);
				
				byte[] buff = convertInputStreamToByteArray(urlConnection.getInputStream());
	            String json = new String(buff, "UTF-8");
	            
	            System.out.println("json = "+json);
				
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            
	}
	
	public byte[] convertInputStreamToByteArray(InputStream inputStream) {
        byte[] bytes = null;
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            byte data[] = new byte[1024];
            int count;
            while ((count = inputStream.read(data)) != -1) {
                bos.write(data, 0, count);
            }
            bos.flush();
            bos.close();
            inputStream.close();
            bytes = bos.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bytes;
    }
	
}
