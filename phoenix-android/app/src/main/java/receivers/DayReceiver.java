package receivers;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import androidx.core.app.NotificationCompat;

import com.matkaline.phoenix.App;
import com.matkaline.phoenix.R;
import com.matkaline.phoenix.ui.splash.SplashActivity;

import java.util.Calendar;

import alarms.DayAlarmManager;
import models.models.cards.CardsManager;
import ulitis.Constants;


public class DayReceiver extends BroadcastReceiver {

    private NotificationManager mManager;
    private App app;

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction() != null && intent.getAction().equals(DayAlarmManager.ACTION_REFRESH_DAY)) {
            init(context);
        }
    }

    /**
     * sinh truong hop cho notification
     */
    private int genCaseNotifi() {
        int i = app.cardMng.getCaseNotif();
        switch (i) {
            case CardsManager.NOTIF_LIST:
                app.cardMng.getId();
                break;
            case CardsManager.NOTIF_DEFAULT:
                CardsManager.id = 168;
                break;
        }
        return i;
    }

    private void init(Context context) {
        app = (App) context.getApplicationContext();

        Calendar calendar = Calendar.getInstance();
        int currentTime = calendar.get(Calendar.HOUR_OF_DAY);

        String msg;
        String timeFrame;

        if (currentTime > 5 && currentTime <= 11) { // morning
            // random question with random title
            app.cardMng.genNotifyQuestion();
            msg = context.getString(R.string.title_notification) + " : " + app.cardMng.question;
            timeFrame = Constants.Notification.NOTIFY_MORNING;
        } else if (currentTime > 11 && currentTime <= 17) { // noon
            // today msg
            msg = context.getString(R.string.title_today);
            timeFrame = Constants.Notification.NOTIFY_NOON;
        } else { // evening & night
            // random question with fix title (dream question)
            msg = context.getString(R.string.title_dream);
            timeFrame = Constants.Notification.NOTIFY_NIGHT;
        }

        //Create Notification
        Intent notificationIntent = new Intent(context, SplashActivity.class);
        notificationIntent.putExtra(Constants.Notification.NOTIFY_24_HOURS, timeFrame);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        mManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String CHANNEL_ID = "Phoenix_Channel";
            NotificationChannel channel = mManager.getNotificationChannel(CHANNEL_ID);
            if (channel == null) {
                CharSequence name = "Phoenix Notification";
                int importance = NotificationManager.IMPORTANCE_HIGH;

                NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
                mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                mChannel.enableVibration(true);
                mChannel.enableLights(true);
                mManager.createNotificationChannel(mChannel);
            }

            //Create a notification and set the notification channel
            NotificationCompat.Builder phoenixNotification = new NotificationCompat.Builder(context)
                    .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_notification))
                    .setSmallIcon(R.drawable.ic_notification)
                    .setContentTitle(context.getString(R.string.app_name))
                    .setContentText(msg)
                    .setAutoCancel(true)
                    .setLights(Color.BLUE, 500, 500)
                    .setSound(alarmSound)
                    .setAutoCancel(true)
                    .setContentIntent(contentIntent)
                    .setChannelId(CHANNEL_ID);

            mManager.notify(app.atomicInteger.incrementAndGet(), phoenixNotification.build());
        } else {
            NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
            builder.setSmallIcon(R.drawable.ic_notification);
            builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_notification));
            builder.setContentTitle(context.getString(R.string.app_name));
            builder.setContentText(msg);
            builder.setAutoCancel(true);
            builder.setLights(Color.BLUE, 500, 500);
            long[] pattern = {500, 500, 500, 500, 500, 500, 500, 500, 500};
            builder.setVibrate(pattern);
            builder.setSound(alarmSound);
            builder.setContentIntent(contentIntent);

            mManager.notify(app.atomicInteger.incrementAndGet(), builder.build());
        }
    }

}
