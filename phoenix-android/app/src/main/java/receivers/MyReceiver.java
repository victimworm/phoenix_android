package receivers;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import androidx.core.app.NotificationCompat;

import com.matkaline.phoenix.App;
import com.matkaline.phoenix.R;
import com.matkaline.phoenix.ui.splash.SplashActivity;

import models.models.cards.CardsManager;

/**
 * Created by Nguyen on 26-May-17.
 */

public class MyReceiver extends BroadcastReceiver {

    private NotificationManager mManager;
    private App app;

    @Override
    public void onReceive(Context context, Intent intent) {
        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            init(context);
        } else {
            init(context);
        }
    }

    /**
     * sinh truong hop cho notification
     */
    private int genCaseNotifi() {
        int i = app.cardMng.getCaseNotif();
        switch (i) {
            case CardsManager.NOTIF_LIST:
                app.cardMng.getId();
                break;
            case CardsManager.NOTIF_DEFAULT:
                CardsManager.id = 168;
                break;
        }
        return i;
    }

    private void init(Context context) {
        app = (App) context.getApplicationContext();
        int icase = genCaseNotifi();
        String msg;
        if (icase != CardsManager.NOTIF_TODAY) {
            msg = context.getString(R.string.title_notification) + " : " + app.cardMng.getQuestionFromUs();
        } else {
            msg = context.getString(R.string.title_today);
        }

        Intent notificationIntent = new Intent(context, SplashActivity.class);
        notificationIntent.putExtra("icase", icase);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        mManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String CHANNEL_ID = "Phoenix_Channel";
            NotificationChannel notificationChannel = mManager.getNotificationChannel(CHANNEL_ID);
            if (notificationChannel == null) {
                CharSequence name = "Phoenix Notification";
                int importance = NotificationManager.IMPORTANCE_HIGH;

                NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
                mChannel.setVibrationPattern(new long[]{500, 500, 500, 500, 500, 500, 500, 500, 500});
                mChannel.enableVibration(true);
                mChannel.enableLights(true);
                mManager.createNotificationChannel(mChannel);
            }

            //Create notification with channel
            NotificationCompat.Builder notification = new NotificationCompat.Builder(context)
                    .setSmallIcon(R.drawable.ic_notification)
                    .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_notification))
                    .setContentTitle(context.getString(R.string.app_name))
                    .setContentText(msg)
                    .setAutoCancel(true)
                    .setLights(Color.BLUE, 500, 500)
                    .setSound(alarmSound)
                    .setChannelId(CHANNEL_ID)
                    .setContentIntent(contentIntent);

        } else {
            NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
            builder.setSmallIcon(R.drawable.ic_notification);
            builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_notification));
            builder.setContentTitle(context.getString(R.string.app_name));
            builder.setContentText(msg);
            builder.setAutoCancel(true);
            builder.setLights(Color.BLUE, 500, 500);
            long[] pattern = {500, 500, 500, 500, 500, 500, 500, 500, 500};
            builder.setVibrate(pattern);
            builder.setSound(alarmSound);
            builder.setContentIntent(contentIntent);
            mManager.notify(app.atomicInteger.incrementAndGet(), builder.build());
        }
    }

}
