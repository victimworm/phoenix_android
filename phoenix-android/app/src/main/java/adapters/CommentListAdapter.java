package adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.matkaline.phoenix.App;
import com.matkaline.phoenix.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import apis.API;
import http.HttpManager;
import http.retrofit.RequestParams;
import models.Comment;
import ulitis.DateTimeHelper;
import ulitis.ImageLoader;
import ulitis.UiHelper;
import ulitis.Utils;

/**
 * Created by nguyen hong phuc on 9/14/2016.
 */
public class CommentListAdapter extends ArrayAdapter<Comment> {

    private ImageLoader imgLoader = null;

    public CommentListAdapter(Context context, int resource, List<Comment> objects) {
        super(context, resource, objects);
        imgLoader = new ImageLoader(context);
    }

    @Override
    public View getView(int pos, View v, ViewGroup parent) {
        final Comment item = getItem(pos);
        LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (v == null) {
            v = inflater.inflate(R.layout.custom_comment_list, parent, false);
        }

        TextView txtUser = (TextView) v.findViewById(R.id.titleUser);
        txtUser.setText(item.user.user_name);

        TextView txtComment = (TextView) v.findViewById(R.id.contextComment);
        txtComment.setText(item.content);

        TextView txtTime = (TextView) v.findViewById(R.id.textTimeComment);
        txtTime.setText(DateTimeHelper.GetStringFromDate(item.created_at));

        TextView txtLikeCount =  (TextView) v.findViewById(R.id.textNumberLike);
        txtLikeCount.setText(String.valueOf(item.number_like));

        ImageView imgUser = ((ImageView) v.findViewById(R.id.imageUser));

        item.user.insertAvatar(imgUser);

        TextView txtLike = (TextView) v.findViewById(R.id.textLike);
        if (item.liked){
            txtLike.setText(getContext().getString(R.string.dislike));
        }else{
            txtLike.setText(getContext().getString(R.string.like));
        }
        txtLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    like(item);
                }
        });

        UiHelper.RESIZE(imgUser,210,210);
        UiHelper.MARGIN(imgUser,0,32,48,0);
//        UiHelper.MARGIN(v.findViewById(R.id.lyItem),0,0,0,74);
        UiHelper.PADDING(v.findViewById(R.id.lyItem),70,0,70,74);


        UiHelper.TEXTSIZE(txtUser,55);
        UiHelper.TEXTSIZE(txtComment,48);
        UiHelper.TEXTSIZE(txtTime,36);
        UiHelper.TEXTSIZE(txtLike,36);
        UiHelper.MARGIN(txtLike,29,0,0,0);
        LinearLayout lyLike = (LinearLayout) v.findViewById(R.id.lyLike);
        UiHelper.RESIZE(lyLike,340,49);
        UiHelper.TEXTSIZE(txtLikeCount,36);
        return v;
    }

    private void like(final Comment comment){
        if (Utils.isNetworkStatusAvialable(getContext())) {
            App app = (App) getContext().getApplicationContext();

            RequestParams params = new RequestParams();
            params.addParam("like", comment.liked ? 2 : 1);

            app.httpManager.doPost(API.URL_COMMENT + comment.id + "/like/" + app.user.user_id, params, new HttpManager.OnResponse() {
                @Override
                public void onResponse(JSONObject jso) {
                    if (comment.liked) {
                        comment.liked = false;
                        comment.number_like--;
                    } else {
                        comment.liked = true;
                        comment.number_like++;
                    }
                    notifyDataSetChanged();
                }
            });
        }else{
            Utils.AlertNetwork(getContext());
        }
    }

}
