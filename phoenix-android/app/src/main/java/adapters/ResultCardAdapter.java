package adapters;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Build;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.akexorcist.localizationactivity.core.LanguageSetting;
import com.matkaline.phoenix.R;

import java.util.Arrays;
import java.util.List;

import models.models.cards.Card;
import models.models.cards.CardsManager;
import ulitis.DateTimeHelper;
import ulitis.UiHelper;
import ulitis.Utils;
import views.TouchImageView;


public class ResultCardAdapter extends RecyclerView.Adapter<ResultCardAdapter.ViewHolder> {
    private Card cards;
    private RecycleCallback mRecycleCallback;
//    private ImageFetcher mImageFetcher;
    private Context mContext;
    private CardsManager cardsManager;
    private final Integer[] TEAM_CREATION = {-1, -3,-5,-7,-9,-11, 15, 17, 19, 21, 23, 25};
    private final Integer[] TEAM_ZEN = {-2, -4, -6, -8, -10, 14, 16, 18, 20, 22, 24};
    private final Integer[] TEAM_DETRESSE = {2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24};
    private final Integer[] TEAM_REVOLTE = {1, 3, 5, 7, 9, 11, -15, -17, -19, -21, -23, -25};
    private final String CREATION = "Team Création";
    private final String REVOLTE = "Team Révolte";
    private final String SOUCI = "Team Souci";
    private final String ZEN = "Team Zen";
    public String descCard = "";

    public void setRecycleCallback(RecycleCallback recycleCallback) {
        this.mRecycleCallback = recycleCallback;
    }

//    public void setCards(List<Card> settingsItems) {
//        if (settingsItems == null || settingsItems.size() == 0) {
//            return;
//        }
//        this.cards = new ArrayList<>();
//        this.cards.addAll(settingsItems);
//        notifyDataSetChanged();
//    }

    public ResultCardAdapter(Context context, Card providerInfo, CardsManager cardManager) {
        this.cards = providerInfo;
        this.mContext = context;
        this.cardsManager = cardManager;
//        addDefaultWebViews();
    }

//    public ResultCardAdapter(Context context) {
//        setHasStableIds(true);
//        this.cards = new ArrayList<>();
//        this.mRecycleCallback = mRecycleCallback;
//        this.mContext = context;
//    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_result_card_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;

        // create a new view
//        LayoutInflater inflater = LayoutInflater.from(
//                parent.getContext());
//        View v =
//                inflater.inflate(R.layout.layout_result_card_item, parent, false);
//        // set the view's size, margins, paddings and layout parameters
//        ViewHolder vh = new ViewHolder(v);
//        return vh;
    }

    private boolean isResult = false;

    public void result(View v) {
        isResult = true;
        Log.e("Temp", "button is clicked");
//        txtMsg.setVisibility(View.GONE);
//        linearButton.setVisibility(View.INVISIBLE);
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
//        setHasStableIds(false);
        final Card itemCard = getItemAt(position);
        holder.imgCard.setImageBitmap(itemCard.bm);
        if (itemCard.isReverse){
            holder.imgCard.setScaleX(-1);
            holder.imgCard.setScaleY(-1);
        }
        holder.webDes.setBackgroundColor(0x00000000);
        if (Build.VERSION.SDK_INT >= 11) holder.webDes.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
        holder.webDes.setInitialScale(100);
        holder.webDes.getSettings().setDefaultFontSize(UiHelper.GETSIZE(60));

        if (cardsManager.datSelectCard != null && !cardsManager.isButton) {
            holder.txtDate.setText(mContext.getString(R.string.title_future) +": " + DateTimeHelper.STRINGFROMDATE(cardsManager.datSelectCard, LanguageSetting.getLanguage(holder.itemView.getContext())));
        } else if(cardsManager.isButton){
            holder.txtDate.setText(DateTimeHelper.STRINGFROMDATE(cardsManager.datSelectCard, LanguageSetting.getLanguage(holder.itemView.getContext())));
        } else {
            if (cardsManager.question != null) {
                cardsManager.getQuestionFromUs();
                holder.txtDate.setText(cardsManager.question);
            } else {
                holder.txtDate.setText(mContext.getString(R.string.title_question_of));
            }
        }

        if (!cardsManager.reconcile) {//chua hoa giai : ghi nhan la dc quan tot hay xau
            cardsManager.isBad = itemCard.isBad();
            if (itemCard.isBad() && cardsManager.datSelectCard != null) {
                holder.txtMsg.setText(mContext.getString(R.string.message_text));
                holder.linearButton.setVisibility(View.VISIBLE);
            } else {
                holder.txtMsg.setVisibility(View.GONE);// GONE -> INVISIBLE
                holder.linearButton.setVisibility(View.GONE); // GONE -> INVISIBLE
            }
        } else {
            holder.txtMsg.setText(mContext.getString(R.string.after_merging));
            holder.linearButton.setVisibility(View.GONE); // GONE -> INVISIBLE
        }

        String  strDes = cardsManager.getExplain();
        descCard = Html.fromHtml(strDes).toString();
        webViewSetData(holder.webDes,strDes, 1);
        String titleTeam = addTeamOfCard(itemCard);
        if(!TextUtils.isEmpty(titleTeam) && titleTeam.equals(CREATION)) {
            holder.tv_team_result_card.setText(titleTeam);
            holder.tv_team_result_card.setTextColor(Color.GREEN);
        } else if(!TextUtils.isEmpty(titleTeam) && titleTeam.equals(REVOLTE)) {
            holder.tv_team_result_card.setText(titleTeam);
            holder.tv_team_result_card.setTextColor(Color.RED);
        } else if(!TextUtils.isEmpty(titleTeam) && titleTeam.equals(SOUCI)) {
            holder.tv_team_result_card.setText(titleTeam);
            holder.tv_team_result_card.setTextColor(Color.BLUE);
        } else if(!TextUtils.isEmpty(titleTeam) && titleTeam.equals(ZEN)) {
            holder.tv_team_result_card.setText(titleTeam);
            holder.tv_team_result_card.setTextColor(Color.YELLOW);
        }

        String[] arrGroup;
        if(itemCard.isReverse) {
            arrGroup = Utils.GetLines(mContext.getAssets(),"month_group_word/group_1/minus_" + itemCard.number);
            Log.e("Temp", "path: " + "month_group_word/group_1/minus_" + itemCard.number);
        } else {
            arrGroup = Utils.GetLines(mContext.getAssets(),"month_group_word/group_1/" + itemCard.number);
            Log.e("Temp", "path: " + "month_group_word/group_1/" + itemCard.number);
        }
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        Resources r = mContext.getResources();
        int px = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                10,
                r.getDisplayMetrics()
        );
        params.setMargins(0, px, 0, 0);
        if(arrGroup != null && arrGroup.length > 0) {
            for(int i = 0; i < arrGroup.length; i++) {
                TextView cardLine = new TextView(mContext);
                cardLine.setLayoutParams(params);
                cardLine.setTextColor(Color.WHITE);
                cardLine.setText(arrGroup[i]);
                holder.ll_six_line_msg.addView(cardLine);
            }
        }

//        holder.layoutCard.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
//            @Override
//            public boolean onPreDraw() {
//                double webViewHeightDouble = holder.layoutCard.getHeight()/2;
//                int webViewHeight = (int) webViewHeightDouble;
//                if( webViewHeight != 0 ){
//                    Toast.makeText(mContext, "height:"+webViewHeight,Toast.LENGTH_SHORT).show();
//                    UiHelper.RESIZE(holder.layoutCard,-1,webViewHeight);
//                    holder.layoutCard.getViewTreeObserver().removeOnPreDrawListener(this);
//                }
//                return false;
//            }
//        });
    }

    private String addTeamOfCard(Card currentCard) {
        if(Arrays.asList(TEAM_CREATION).contains((int)currentCard.number)) {
            return CREATION;
        } else if(Arrays.asList(TEAM_DETRESSE).contains((int)currentCard.number)) {
            return SOUCI;
        } else if(Arrays.asList(TEAM_REVOLTE).contains((int)currentCard.number)) {
            return REVOLTE;
        } else if(Arrays.asList(TEAM_ZEN).contains((int)currentCard.number)) {
            return ZEN;
        } else {
            return null;
        }
    }


    @Override
    public int getItemCount() {
        return 1;
    }

    private Card getItemAt(int pos) {
        if (pos >= 0 && pos < getItemCount()) {
            return cards;
        }
        return null;
    }


    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView txtMsg;
        private TextView txtDate;
        private WebView webDes;
        private TextView txtTitle;
        private TextView tv_team_result_card;
        private Button btResult, btMerge;
        private TouchImageView imgCard;
//    private ImageButton menu;
//    private View imageBG;
private LinearLayout ll_six_line_msg;
//        private Button btn_defi;
        private LinearLayout linearTitle, linearButton;
        private RelativeLayout layoutCard;

        public String descCard = "";





        public ViewHolder(View itemView) {
            super(itemView);
            txtTitle = (TextView) itemView.findViewById(R.id.txtTitleRst);
            txtMsg = (TextView) itemView.findViewById(R.id.txtMessage);
            txtDate = (TextView) itemView.findViewById(R.id.txtDate);
            webDes = (WebView) itemView.findViewById(R.id.webDesc);
            ll_six_line_msg = (LinearLayout) itemView.findViewById(R.id.ll_six_line_msg);
            tv_team_result_card = (TextView) itemView.findViewById(R.id.tv_team_result_card);
            btResult = (Button) itemView.findViewById(R.id.btResult);
            btMerge = (Button) itemView.findViewById(R.id.btMerge);
            imgCard = (TouchImageView) itemView.findViewById(R.id.image);
            linearTitle = (LinearLayout) itemView.findViewById(R.id.linear_title_result);
            linearButton = (LinearLayout) itemView.findViewById(R.id.linear_btn_conflict);
            layoutCard = (RelativeLayout) itemView.findViewById(R.id.layout_meanCard);
//            btn_defi = (Button) itemView.findViewById(R.id.btn_defi);
//            UiHelper.RESIZE(btn_defi,400,115);
//            UiHelper.TEXTSIZE(btn_defi,40);

            UiHelper.TEXTSIZE(txtTitle,60);
            UiHelper.MARGIN(txtTitle,0,50,0,0);
            UiHelper.TEXTSIZE(txtDate,55);
            UiHelper.TEXTSIZE(txtMsg,55);
            UiHelper.MARGIN(txtMsg,96,0,96,0);
            UiHelper.RESIZE(btResult,260,115);
            UiHelper.RESIZE(btMerge,400,115);
            UiHelper.TEXTSIZE(btResult,55);
            UiHelper.TEXTSIZE(btMerge,40);
            UiHelper.MARGIN(btResult,0,50,15,50);
            UiHelper.MARGIN(btMerge,15,50,0,50);
            UiHelper.RESIZE(imgCard,689,1343);
//            UiHelper.RESIZE(layoutCard,-1,2280);
//            UiHelper.PADDING(webDes,96,0,96,48);
//            itemView.setOnClickListener(this);
//            ripple_provider_info.setOnRippleCompleteListener(this);

        }

        @Override
        public void onClick(View v) {

        }

    }

    public interface RecycleCallback {
        void onClickCallback(List<Card> providerInfo,
                             int activationLogId);

        void onCancelServiceCallBack(int activationLogId);
    }

    private void contentCard() {


//        L.V("RESULT CARDID = ",cardsManager.getCardId(itemCard));
//        UiHelper.MARGIN(webDes,0,1400,0,0,true);
//        webDes.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent e) {
//                if  (e.getAction() == MotionEvent.ACTION_DOWN) {
//                    if (!isShowingDesc){
//                        webViewAnimation();
//                        return true;
//                    }else {
//                        exdown = e.getX();
//                        eydown = e.getY();
//                        t = System.currentTimeMillis();
//                    }
//                }else if (e.getAction() == MotionEvent.ACTION_UP) {
//                    if (e.getX() == exdown && e.getY() == eydown){
//                        if (System.currentTimeMillis() - t < 100) {
//                            webViewAnimation();
//                        }
//                    }
//                }
//                return false;
//            }
//        });
    }

    protected void webViewSetData(WebView web, String myData, int padding) {
        String pish = "<html><head>" +
                "<meta name=\"viewport\" content=\"target-densitydpi=device-dpi, width=device-width, user-scalable=no\"/>" +
                "<style type=\"text/css\">body {text-align: justify; color: #FFFFFF;padding-left: "+ padding +"px;padding-right:"+padding+"px}</style></head><body>";
        String pas = "</body></html>";

        String myHtmlString = pish + myData + pas;
        web.loadDataWithBaseURL("file:///android_asset/", myHtmlString, "text/html", "UTF-8", null);
    }

    private void setImage(Card card) {

    }
}
