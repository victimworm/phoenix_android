package adapters;

import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.akexorcist.localizationactivity.core.LanguageSetting;
import com.matkaline.phoenix.App;
import com.matkaline.phoenix.MeanCardActivity;
import com.matkaline.phoenix.R;
import com.matkaline.phoenix.RootActivity;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import dbs.HistoryWeek;
import models.models.cards.Card;
import models.models.cards.CardItem;
import ulitis.DateTimeHelper;

/**
 * Created by Nguyen on 25-May-17.
 */

public class YourWeekAdapter extends ArrayAdapter<HistoryWeek> implements View.OnClickListener {
    private RootActivity _activity;
    private int imageWidth;
    private List<HistoryWeek> historyWeeks;
    private Map<String,Bitmap> bms = new HashMap<String,Bitmap>();

    private ImageView image_left, image_right;
    private TextView date;

    public YourWeekAdapter(RootActivity _activity, int imageWidth,List<HistoryWeek> numberCard) {
        super(_activity, imageWidth, numberCard);
        this._activity = _activity;
        this.historyWeeks = numberCard;
        this.imageWidth = imageWidth;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        HistoryWeek hisw = getItem(position);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(imageWidth, (int) (imageWidth*228f/123),1);
        if (convertView == null){
            LayoutInflater inflater = LayoutInflater.from(_activity);
            convertView = inflater.inflate(R.layout.custom_yourweek, parent,false);
        }
        date = (TextView) convertView.findViewById(R.id.text_thu);
        date.setText(DateTimeHelper.FULLDATE(new Date(hisw.date_card), LanguageSetting.getLanguage(convertView.getContext())));
        image_left = (ImageView) convertView.findViewById(R.id.image_1);
        image_right = (ImageView) convertView.findViewById(R.id.image_2);
        showCard(image_left,layoutParams,hisw.card_1);
        showCard(image_right,layoutParams,hisw.card_2);
        return convertView;
    }

    /**
     * hien thi anh quan bai
     * chu y : dung chieu nguoc xuoi
     * va gan su kien click vao quan bai
     */
    private void showCard(ImageView image,LinearLayout.LayoutParams layoutParams,int cardId){
        image.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        image.setAdjustViewBounds(true);
        image.setLayoutParams(layoutParams);
        Card card =  _activity.app.cardMng.card(cardId);
        image.setImageBitmap(card.getBitmap());
        image.setScaleX(1);
        image.setScaleY(1);
        if (card.isReverse){
            image.setScaleX(-1);
            image.setScaleY(-1);
        }
        image.setTag(R.id.card_id,cardId);
        CardItem cIte = new CardItem(card.number);
        cIte.reverse = card.isReverse;
        image.setTag(R.id.card_item,cIte);
        image.setOnClickListener(this);
    }

    public void destroy() {
        Set<String> keys =  bms.keySet();
        for(String key : keys){
            bms.get(key).recycle();
        }
    }

    /**
     * event when click to card
     * @param v
     */
    @Override
    public void onClick(View v) {
        App app = (App) v.getContext().getApplicationContext();
        app.cardID = (int) v.getTag(R.id.card_id);
        app.gameInforCardItem = (CardItem) v.getTag(R.id.card_item);
        _activity.go(MeanCardActivity.class);
    }

    class OnClickCard implements View.OnClickListener{

        @Override
        public void onClick(View v) {

        }
    }

}
