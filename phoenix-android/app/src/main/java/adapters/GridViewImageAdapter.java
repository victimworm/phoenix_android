package adapters;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.matkaline.phoenix.MeanCardActivity;
import com.matkaline.phoenix.R;
import com.matkaline.phoenix.RootActivity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import models.models.cards.CardItem;

/**
 * Created by nvdai on 9/27/2016.
 */
public class GridViewImageAdapter extends BaseAdapter {

    private RootActivity _activity;
    private List<CardItem> _filePaths = new ArrayList<CardItem>();
    private int imageWidth;
    private Map<String,Bitmap> bms = new HashMap<String,Bitmap>();

    public static final int CENTER = 0;
    public static final int TWO_CENTER = 1;
    private ImageView image_left, image_right, image_center;

    public GridViewImageAdapter(Activity _activity, List<CardItem> numberCard, int imageWidth) {
        this._activity = (RootActivity) _activity;
        this._filePaths = numberCard;
        this.imageWidth = imageWidth;
    }

    @Override
    public int getCount() {
        return this._filePaths.size();
    }

    @Override
    public Object getItem(int position) {
        return this._filePaths.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        CardItem item = _filePaths.get(position);
        if (item.image_id == 0 || item.image_id == 13){
            return CENTER;
        } else{
            return TWO_CENTER;
        }
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Bitmap bitmap = null;
        CardItem item = _filePaths.get(position);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(imageWidth, (int) (imageWidth*228f/123),1);

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(_activity);
            if (getItemViewType(position) == CENTER){
                convertView = inflater.inflate(R.layout.custom_listview_center,parent,false);
            } else if(getItemViewType(position) == TWO_CENTER) {
                convertView = inflater.inflate(R.layout.custom_listview, parent, false);
            }
        }

        try {
            bitmap = BitmapFactory.decodeStream(_activity.getAssets().open("image/"+item.image_id+".jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (getItemViewType(position) == TWO_CENTER){
            assert convertView != null;
            image_left = (ImageView) convertView.findViewById(R.id.image_left);
            image_right = (ImageView) convertView.findViewById(R.id.image_right);
            image_left.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            image_left.setAdjustViewBounds(true);
            image_left.setLayoutParams(layoutParams);
            image_right.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            image_right.setAdjustViewBounds(true);
            image_right.setLayoutParams(layoutParams);

            if (position <= 12) {
                image_left.setImageBitmap(_activity.rotated(180, bitmap));
                image_right.setImageBitmap(bitmap);
            } else if(position >= 14){
                image_right.setImageBitmap(_activity.rotated(180, bitmap));
                image_left.setImageBitmap(bitmap);
            }
            image_left.setOnClickListener(new OnImageClickListener_Left(position));
            image_right.setOnClickListener(new OnImageClickListener_Right(position));

        } else if(getItemViewType(position) == CENTER){
            assert convertView != null;
            image_center = (ImageView) convertView.findViewById(R.id.image_center);
            image_center.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            image_center.setAdjustViewBounds(true);
            layoutParams.gravity= Gravity.CENTER;
            image_center.setLayoutParams(layoutParams);
            image_center.setImageBitmap(bitmap);
            image_center.setOnClickListener(new OnImageClickListener_Center(position));
        }

        return convertView;
    }

    public void destroy() {
        Set<String> keys =  bms.keySet();
        for(String key : keys){
            bms.get(key).recycle();
        }
    }

    class OnImageClickListener_Left implements View.OnClickListener {

        int _position;
        public OnImageClickListener_Left(int position) {
            this._position = position;
        }
        @Override
        public void onClick(View v) {
            if(_position <= 12){
                _filePaths.get(_position).reverse = true;
            } else {
                _filePaths.get(_position).reverse = false;
            }
            _activity.app.cardID = _activity.app.cardMng.getCardId(_filePaths.get(_position));
            _activity.app.gameInforCardItem = _filePaths.get(_position);
            _activity.go(MeanCardActivity.class);
        }
    }
    class OnImageClickListener_Right implements View.OnClickListener {

        int _position;
        public OnImageClickListener_Right(int position) {
            this._position = position;
        }
        @Override
        public void onClick(View v) {
            if(_position >= 14){
                _filePaths.get(_position).reverse = true;
            } else {
                _filePaths.get(_position).reverse = false;
            }
            _activity.app.cardID = _activity.app.cardMng.getCardId(_filePaths.get(_position));
            _activity.app.gameInforCardItem = _filePaths.get(_position);
            _activity.go(MeanCardActivity.class);
        }
    }
    class OnImageClickListener_Center implements View.OnClickListener {
        int _position;
        public OnImageClickListener_Center(int position) {
            this._position = position;
        }
        @Override
        public void onClick(View v) {
            _activity.app.cardID = _activity.app.cardMng.getCardId(_filePaths.get(_position));
            _activity.app.gameInforCardItem = _filePaths.get(_position);
            _activity.go(MeanCardActivity.class);
        }
    }
    /**
     * Resizing image size
     */
    public static Bitmap decodeFile(String filePath, int WIDTH, int HIGHT) {
        try {
            File f = new File(filePath);

            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);
            final int REQUIRED_WIDTH = WIDTH;
            final int REQUIRED_HIGHT = HIGHT;
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_WIDTH
                    && o.outHeight / scale / 2 >= REQUIRED_HIGHT)
                scale *= 2;

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Bitmap getBitmap(String path) {
        Bitmap thumBitmap = null;

        try {
            final int THUMBNAIL_SIZE = 300;

            FileInputStream fis = new FileInputStream(path);
            thumBitmap = BitmapFactory.decodeStream(fis);

            Float width = new Float(thumBitmap.getWidth());
            Float height = new Float(thumBitmap.getHeight());
            Float ratio = width / height;
            thumBitmap = Bitmap.createScaledBitmap(thumBitmap, (int) (THUMBNAIL_SIZE * ratio), THUMBNAIL_SIZE, false);

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            thumBitmap.compress(Bitmap.CompressFormat.JPEG, 90, outputStream);

        } catch (Exception ex) {

        }
        return thumBitmap;
    }


}
