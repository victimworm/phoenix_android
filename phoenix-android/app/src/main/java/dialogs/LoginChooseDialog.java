package dialogs;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.matkaline.phoenix.App;
import com.matkaline.phoenix.R;
import com.matkaline.phoenix.RootActivity;
import com.matkaline.phoenix.SignInActivity;

import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.Collection;

import ulitis.UiHelper;
import ulitis.Utils;

public class LoginChooseDialog extends RootDialog implements View.OnClickListener {

    private RelativeLayout btLoginFb;
    private RelativeLayout btLoginGg;
    private LoginButton loginFBButton;
    private LinearLayout lyTitle;
    private ImageView btClose;
    private LinearLayout close;
    public TextView txtTitle;

    public LoginChooseDialog(Context context, Activity activity) {
        super(context);
        noTitle();
        fullScreen();
        setContentView(R.layout.dialog_login);
        btLoginFb = (RelativeLayout) findViewById(R.id.btn_LoginByFacebook);
        btLoginGg = (RelativeLayout) findViewById(R.id.btn_LoginByGmail);
        loginFBButton = findViewById(R.id.login_fb_button);
        btClose = (ImageView) findViewById(R.id.buttonClose);
        txtTitle = (TextView) findViewById(R.id.textQuestionLogin);
        lyTitle = (LinearLayout) findViewById(R.id.line_titleQuestion);
        close = (LinearLayout) findViewById(R.id.line_Close);

        resize();

        btLoginGg.setOnClickListener(this);
        loginFBButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Collection<String> permissions = Arrays.asList("email", "public_profile", "user_friends");
                LoginManager.getInstance().logInWithReadPermissions(activity, permissions);
            }
        });

        close.setOnClickListener(this);

    }

    private CallbackManager fbLoginCallbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LoginManager.getInstance().registerCallback(App.getInstance().user.callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                System.out.println("Login Successs: "+loginResult.toString());
                Toast.makeText(getContext(), "Login success", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onCancel() {
                System.out.println("Login Cancel");
            }

            @Override
            public void onError(@NotNull FacebookException e) {
                System.out.println("Login Error");
            }
        });
    }

    @Override
    public void onClick(View view) {
        btLoginGg.setEnabled(false);
        switch (view.getId()) {
            case R.id.btn_LoginByFacebook:
                loginFb();
                break;
            case R.id.btn_LoginByGmail:
                loginGg();
                break;
            case R.id.line_Close:
                dismiss();
                break;
        }
        dismiss();
    }

//    private void loginFb() {
//        if(((RootActivity)ctx).isNetworkStatusAvialable()) {
////        app.user.loginByFacebook((Activity) ctx);
//            ((RootActivity) ctx).go(facebooks.HelloFacebookSampleActivity.class);
//        } else {
//            ((RootActivity)ctx).alert(ctx.getString(R.string.alert), ctx.getString(R.string.alert_offline), new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    dialog.dismiss();
//                }
//            });
//        }
//    }
    private void loginFb() {
        if(Utils.isNetworkStatusAvialable(ctx)) {
//        app.user.loginByFacebook((Activity) ctx);
            ((RootActivity) ctx).go(facebooks.HelloFacebookSampleActivity.class);
        } else {
            Utils.AlertNetwork(ctx);
        }
    }

    private void loginGg() {
        if(Utils.isNetworkStatusAvialable(ctx)) {
//         app.user.ggHelper.signIn((Activity) ctx);
            ((RootActivity)ctx).go(SignInActivity.class);
        } else {
            Utils.AlertNetwork(ctx);
        }
    }

    private void resize(){
        RelativeLayout lyBorder = (RelativeLayout) findViewById(R.id.box_dialog_login);
//        TextView txtFace = (TextView) findViewById(R.id.txtLoginFace);
        TextView txtGoogle = (TextView) findViewById(R.id.txtLoginGoogle);
        ImageView icGoogle = (ImageView) findViewById(R.id.icon_google);
//        ImageView icFace = (ImageView) findViewById(R.id.icon_face);
        UiHelper.MARGIN(lyTitle,60,0,60,0);
        UiHelper.RESIZE(lyBorder,1138,919);
        UiHelper.RESIZE(btLoginFb,856,107);
        UiHelper.RESIZE(btLoginGg,856,107);
        UiHelper.MARGIN(btLoginGg,141,150,141,0);
        UiHelper.MARGIN(btLoginFb,141,60,141,220);
        UiHelper.TEXTSIZE(txtTitle,48);
//        UiHelper.TEXTSIZE(txtFace,48);
        UiHelper.TEXTSIZE(txtGoogle,48);
        UiHelper.RESIZE(btClose,51,51);
        UiHelper.RESIZE(close,100,100);
//        UiHelper.MARGINLEFT(icFace,96);
        UiHelper.MARGINLEFT(icGoogle,96);
        UiHelper.RESIZE(icGoogle,51,41);
//        UiHelper.RESIZE(icFace,33,61);
    }
}
