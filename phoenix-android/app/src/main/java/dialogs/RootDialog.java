package dialogs;

import android.app.Dialog;
import android.content.Context;
import android.view.Window;
import android.view.WindowManager;

import com.matkaline.phoenix.App;


public class RootDialog extends Dialog {

    protected Context ctx;
    protected App app;

    public RootDialog(Context ctx) {
        super(ctx);
        this.ctx = ctx;
        app = App.getInstance();
    }

    protected void noTitle() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
    }

    protected void fullScreen() {
        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
    }

}
