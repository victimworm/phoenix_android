package dialogs;

import android.content.Context;
import android.view.View;

import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.matkaline.phoenix.CardsActivity;
import com.matkaline.phoenix.R;
import com.matkaline.phoenix.RootActivity;
import com.matkaline.phoenix.ui.home.IFQuestionFromUsNavigator;

import ulitis.UiHelper;

public class QuestionContentDialog extends RootDialog implements View.OnClickListener {

    public TextView txtQues;
    public ImageView imageQuestion;
    private ImageView btClose;
    private LinearLayout close;
    private IFQuestionFromUsNavigator mNavigator;

    public void setNavigator(IFQuestionFromUsNavigator navigator) {
        this.mNavigator = navigator;
    }

    public QuestionContentDialog(Context context) {
        super(context);
        noTitle();
        fullScreen();
        setContentView(R.layout.dialog_question_content);
        txtQues = (TextView) findViewById(R.id.textQuestion);
        btClose = (ImageView) findViewById(R.id.buttonClose);
        close = (LinearLayout) findViewById(R.id.line_Close);
        imageQuestion = (ImageView) findViewById(R.id.imageQuestion);

        RelativeLayout lybor = (RelativeLayout) findViewById(R.id.box_dialog_content);
        UiHelper.RESIZE(lybor,1138,919);
        UiHelper.RESIZE(imageQuestion,184,354);
        UiHelper.TEXTSIZE(txtQues,48);
        UiHelper.MARGIN(txtQues,60,0,60,90);
        UiHelper.RESIZE(btClose,51,51);
        UiHelper.RESIZE(close,100,100);

        imageQuestion.setOnClickListener(this);
        close.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageQuestion:
                //SonLA_ show question for lumiere - S
//                ((RootActivity) ctx).go(CardsActivity.class);
                if (mNavigator != null) {
                    mNavigator.showResult();
                }
                //SonLA_ show question for lumiere - E
                dismiss();
                break;
            case R.id.line_Close:
                dismiss();
                break;
        }
    }
}
