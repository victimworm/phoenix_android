package dialogs;

import android.app.Dialog;
import android.content.Context;
import android.widget.ProgressBar;

/**
 * Created by nguyen hong phuc on 10/14/2016.
 */
public class ProgressDialog extends RootDialog {

    private ProgressBar progressBar;

    public ProgressDialog(Context ctx) {
        super(ctx);
        noTitle();
        progressBar = new ProgressBar(ctx);
        setContentView(progressBar);
    }
}
