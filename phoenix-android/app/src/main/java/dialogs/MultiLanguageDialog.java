package dialogs;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.matkaline.phoenix.R;
import com.matkaline.phoenix.RootActivity;
import com.matkaline.phoenix.data.local.prefs.AppPreferencesHelper;
import com.matkaline.phoenix.utils.AppConstants;

import ulitis.LanguageHepler;

/**
 * Created by nvdai on 10/28/2016.
 */
public class MultiLanguageDialog extends RootDialog {

    private ListView listView;
    private String name[] = {ctx.getResources().getString(R.string.vietnam)
            , ctx.getResources().getString(R.string.english)
            , ctx.getResources().getString(R.string.french)
            , ctx.getResources().getString(R.string.russian)};

    public MultiLanguageDialog(final Context ctx) {
        super(ctx);
        fullScreen();
        setTitle(ctx.getResources().getString(R.string.title_language));
        setContentView(R.layout.dialog_multi_laguage);
        listView = (ListView) findViewById(R.id.list_language);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(ctx, android.R.layout.simple_list_item_1, name);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0: {
                        ((RootActivity) ctx).setLanguage("vi");
                        //LanguageHepler.changeLocale(ctx.getResources(),"vi");
                        break;
                    }
                    case 1: {
                        ((RootActivity) ctx).setLanguage("en");
                        //LanguageHepler.changeLocale(ctx.getResources(),"en");
                        break;
                    }
                    case 2: {
                        ((RootActivity) ctx).setLanguage("fr");
//                        LanguageHepler.changeLocale(ctx.getResources(),"fr");
                        break;
                    }
                    case 3: {
                        ((RootActivity) ctx).setLanguage("ru");
                        break;
                    }
                    default: {
                        break;
                    }
                }
                AppPreferencesHelper mpreferen = new AppPreferencesHelper(ctx, ctx.getPackageName());
                mpreferen.removeTodayStored();
                dismiss();
            }
        });

    }


}
