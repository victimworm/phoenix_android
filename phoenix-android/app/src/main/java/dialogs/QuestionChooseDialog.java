package dialogs;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.matkaline.phoenix.R;
import com.matkaline.phoenix.ui.home.IFQuestionChooseNavigator;

import ulitis.UiHelper;

public class QuestionChooseDialog extends RootDialog {

    Button btnYourQuestion;
    Button btnFromUS;
    private ImageView ibtClose;
    private TextView txtQuestion;
    private LinearLayout close;
    private IFQuestionChooseNavigator mNavigator;

    public void setNavigator(IFQuestionChooseNavigator navigator) {
        this.mNavigator = navigator;
    }

    public QuestionChooseDialog(Context context) {
        super(context);
        noTitle();
        fullScreen();
        setContentView(R.layout.dialog_question_choose);
        btnYourQuestion = findViewById(R.id.buttonYour);
        btnFromUS = findViewById(R.id.buttonFromUS);
        ibtClose = findViewById(R.id.buttonClose);
        close = findViewById(R.id.line_Close);
        txtQuestion = findViewById(R.id.textQuestionLogin);

        resize();

        btnFromUS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mNavigator != null) {
                    mNavigator.getQuestionFromUs();
                }
                dismiss();
            }
        });

        btnYourQuestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mNavigator != null) {
                    mNavigator.showQuestionPopup();
                }
                dismiss();
            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }

    private void resize() {
        RelativeLayout lyBorderDia = (RelativeLayout) findViewById(R.id.box_dialog_random);
        LinearLayout lyButton = (LinearLayout) findViewById(R.id.line_titleQuestion);
        UiHelper.RESIZE(lyBorderDia, 1138, 919);
        UiHelper.MARGIN(lyButton, 0, 30, 0, 140);
        UiHelper.RESIZE(btnYourQuestion, 410, 145);
        UiHelper.TEXTSIZE(btnYourQuestion, 38);
        UiHelper.RESIZE(btnFromUS, 410, 145);
        UiHelper.TEXTSIZE(btnFromUS, 38);
        UiHelper.RESIZE(ibtClose, 51, 51);
        UiHelper.RESIZE(close, 100, 100);
        UiHelper.TEXTSIZE(txtQuestion, 55);
    }
}
