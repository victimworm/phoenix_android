package apis;

/**
 * Created by nmcuong on 9/8/2016.
 */
public class API {
    public static final String URL = "https://phoenixfr.xyz";

    public static final String SIGNUP = URL + "/signup/";

    public static final String URL_UPDATE = URL + "/api/user/";

    public static final String LOGIN_SSN = URL + "/api/auth/login";

    public static final String API_CARD = URL + "/api/card";

    public static final String LOGOUT = URL + "/api/auth/logout";

    public static final String ANDROID_VERSION = URL + "/api/checkversion/1";

    public static final String URL_COMMENT = URL + "/api/comment/";
}
