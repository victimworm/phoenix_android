package http.retrofit;

import android.content.SharedPreferences;

import androidx.annotation.NonNull;

import com.matkaline.phoenix.App;
import com.matkaline.phoenix.BuildConfig;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import apis.API;
import okhttp3.Headers;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

public class RetrofitSetup {

    private final static short TIME_OUT = 120;

    public RetrofitSetup() {

    }

    private final Map<String, String> getHeaders() {
        Map<String, String> header = new HashMap<>();

        SharedPreferences sharedPreferences = App.getInstance()
                .getSharedPreferences(
                        App.getInstance().getPackageName(),
                        App.getInstance().MODE_PRIVATE
                );

        String token = sharedPreferences.getString("token", "");
        header.put("Authorization", "Bearer "+token);
        return header;
    }

    private RetrofitServiceApi retrofitServiceApi;

    public RetrofitServiceApi getRetrofitServiceApi() {
        if (retrofitServiceApi == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(API.URL)
                    .client(getOkHttpClient())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();
            retrofitServiceApi = retrofit.create(RetrofitServiceApi.class);
        }
        return retrofitServiceApi;
    }

    @NonNull
    private OkHttpClient getOkHttpClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .readTimeout(TIME_OUT, TimeUnit.SECONDS)
                .connectTimeout(TIME_OUT, TimeUnit.SECONDS)
                .writeTimeout(TIME_OUT, TimeUnit.SECONDS)
                .addInterceptor(chain -> {
                    Request original = chain.request();
                    Request request = original.newBuilder()
                            .headers(Headers.of(getHeaders()))
                            .method(original.method(), original.body())
                            .build();
                    final Response response = chain.proceed(request);
                    return response;
                });

        if (BuildConfig.DEBUG) {
            final HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(interceptor);
        }

        return builder.build();
    }

}
