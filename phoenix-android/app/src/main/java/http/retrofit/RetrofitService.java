package http.retrofit;

import com.matkaline.phoenix.BuildConfig;

import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;

import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

public abstract class RetrofitService {

    private final RetrofitSetup retrofit;

    public RetrofitService() {
        retrofit = new RetrofitSetup();
    }

    protected enum TypeRequest {POST, GET}

    protected void doRequest(
            String endpoint,
            TypeRequest typeRequest,
            RequestParams requestParams,
            ApiResponseCallBack<String> callback
    ) {

        if (requestParams == null) {
            requestParams = new RequestParams();
        }

        final Single<Response<ResponseBody>> observable;

        switch (typeRequest) {
            case POST: {
                observable = retrofit.getRetrofitServiceApi().doPost(endpoint, requestParams.getBody());
                break;
            }

            case GET: {
                observable = retrofit.getRetrofitServiceApi().doGet(endpoint, requestParams.getBody());
                break;
            }

            default:
                throw new IllegalStateException("Unexpected value: " + typeRequest);

        }

        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        new DisposableSingleObserver<Response<ResponseBody>>() {
                            @Override
                            public void onSuccess(Response<ResponseBody> responseBodyResponse) {
                                try {
                                    callback.onSuccess(responseBodyResponse.body().string());
                                } catch (Exception e) {
                                    if (BuildConfig.DEBUG) {
                                        callback.Failure(e.getMessage());
                                    }
                                }
                            }

                            @Override
                            public void onError(Throwable e) {
                                if (e instanceof SocketTimeoutException) {
                                    callback.Failure("No connection");
                                } else {
                                    callback.Failure(e.getMessage());
                                }
                            }
                        }
                );
    }
}
