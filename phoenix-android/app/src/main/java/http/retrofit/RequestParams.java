package http.retrofit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public class RequestParams {
    private LinkedHashMap<String, Object> param;

    public RequestParams() {
        this.param = new LinkedHashMap<>();
    }

    public void addParam(String key, Object value) {
        if (value != null) {
            this.param.put(key, value);
        }
    }


    public void addParam(String key, HashMap<String, String> value) {
        if (value != null) {
            for (Map.Entry<String, String> entry : value.entrySet()) {
                String keyItem = entry.getKey();
                String valueItem = entry.getValue();
                this.param.put("[" + key + "]" + "[" + keyItem + "]", valueItem);
            }
        }
    }

    public void addParam(String key, List<String> value) {
        if (value == null) return;
        if (value != null) {
            int i = 0;
            for (String valueItem : value) {
                this.param.put(key + "[" + i++ + "]", valueItem);
            }
        }
    }
    /*
    ...
     */
    public void addParam(String key, String[][] value) {
        if (value == null) return;
        if (value != null) {
            // int i = 0;
            for (int i=0;i<value.length;i++){
                for (int j=0;j<value[i].length;j++ ){
                    this.param.put(key + "[" + i + "]"+ "[" + j + "]", value[i][j]);
                }
            }
        }
    }

    public void removeParam(String key, boolean isArray) {
        if (key != null) {
            if (isArray) {
                ArrayList<String> keys = new ArrayList<>(this.param.keySet());
                for (String index : keys) {
                    if (index.startsWith(key + "[")) {
                        this.param.remove(index);
                    }
                }
            } else {
                this.param.remove(key);
            }
        }
    }

    public Map<String, Object> getBody() {
        return this.param;
    }
    public Map<String, RequestBody> getMultipartBody() {
        final Map<String, RequestBody> requestParams = new HashMap<>();
        for (Map.Entry<String, Object> item : param.entrySet()) {
            if(item.getValue() instanceof RequestBody) {
                requestParams.put(item.getKey(), (RequestBody) item.getValue());
            }else{
                RequestBody body = RequestBody.create(MediaType.parse("multipart/form-data"),item.getValue()+"");
                requestParams.put(item.getKey(), body);

            }
        }
        return requestParams;
    }

    public List<String> getArrayValueFromKey(String key) {
        List<String> values = new ArrayList<>();
        if (key != null) {
            ArrayList<String> keys = new ArrayList<>(this.param.keySet());
            for (String index : keys) {
                if (index.startsWith(key + "[")) {
                    values.add(this.param.get(index).toString());
                }
            }
        }
        return values;
    }

    public void addParam(String key, Map<Integer, List<String>> tags) {
        if (tags != null) {
            for (Integer keyMap: tags.keySet()) {
                for (int i=0;i<tags.get(keyMap).size();i++){
                    this.param.put(key + "[" + keyMap + "]" + "[" + i + "]", tags.get(keyMap).get(i));
                }

            }
        }
    }

    public void addParam(String key, ArrayList<HashMap<String,String>> tags) {
        if (tags != null) {
            int k = 0;
            for (HashMap<String,String> stringStringHashMap :tags){
                for (Map.Entry<String, String> entry : stringStringHashMap.entrySet()) {
                    String keyItem = entry.getKey();
                    String valueItem = entry.getValue();
                    this.param.put(key  + "[" + k + "]"+ "[" + keyItem + "]", valueItem);
                }
                k++;
            }

        }
    }

    private final String JSON_RAW_DATA = "JsOn_RaW_aAtA_kEy";

    public void putJSONRaw(String json) {
        this.param.put(JSON_RAW_DATA, json);
    }

    public String getJSONRaw() {
        if (this.param.containsKey(JSON_RAW_DATA)) {
            return (String) this.param.get(JSON_RAW_DATA);
        } else {
            return "";
        }
    }
}
