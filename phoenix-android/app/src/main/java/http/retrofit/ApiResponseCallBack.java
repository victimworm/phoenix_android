package http.retrofit;

public interface ApiResponseCallBack<T> {
    void onSuccess(T data);

    void Failure(String message);
}