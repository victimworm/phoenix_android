package http.retrofit;

import java.util.Map;

import io.reactivex.Single;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

public interface RetrofitServiceApi {

    @POST
    @FormUrlEncoded
    Single<Response<ResponseBody>> doPost(
            @Url String url,
            @FieldMap Map<String, Object> params
    );

    @GET
    Single<Response<ResponseBody>> doGet(
            @Url String url,
            @QueryMap Map<String, Object> params
    );

}
