package http;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

import com.matkaline.phoenix.App;
import com.matkaline.phoenix.R;
import com.matkaline.phoenix.RootActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;

import http.retrofit.ApiResponseCallBack;
import http.retrofit.RequestParams;
import http.retrofit.RetrofitService;
import models.User;
import ulitis.L;
import ulitis.Utils;

public class HttpManager extends RetrofitService {

    private String strUrl;
    private String json;
    private OnResponse onResponse;

    public byte[] convertInputStreamToByteArray(InputStream inputStream) {
        byte[] bytes = null;
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            byte data[] = new byte[1024];
            int count;
            while ((count = inputStream.read(data)) != -1) {
                bos.write(data, 0, count);
            }
            bos.flush();
            bos.close();
            inputStream.close();
            bytes = bos.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bytes;
    }


    private Context ctx;

    public HttpManager(Context ctx) {
        this.ctx = ctx;

    }

    private boolean isNetworkStatusAvialable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            NetworkInfo netInfos = connectivityManager.getActiveNetworkInfo();
            if (netInfos != null)
                if (netInfos.isConnected())
                    return true;
        }
        return false;
    }

    public void doGet(String endpoint, RequestParams params, OnResponse onResponse) {
        if (!Utils.isNetworkStatusAvialable(ctx)) {
            Utils.AlertNetwork(ctx);
            return;
        }
        doRequest(endpoint, TypeRequest.GET, params, new ApiResponseCallBack<String>() {
            @Override
            public void onSuccess(String data) {
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(data);
                } catch (Exception e) {

                }
                if (onResponse != null) {
                    onResponse.onResponse(jsonObject);
                }
            }

            @Override
            public void Failure(String message) {
                Utils.Alert(ctx, ctx.getString(R.string.alert), message);
            }
        });
    }

    public void doPost(String endpoint, RequestParams params, OnResponse onResponse) {
        if (!Utils.isNetworkStatusAvialable(ctx)) {
            Utils.AlertNetwork(ctx);
            return;
        }
        doRequest(endpoint, TypeRequest.POST, params, new ApiResponseCallBack<String>() {
            @Override
            public void onSuccess(String data) {
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(data);
                } catch (Exception e) {

                }
                onResponse.onResponse(jsonObject);
            }

            @Override
            public void Failure(String message) {
                Utils.Alert(ctx, ctx.getString(R.string.alert), message);
            }
        });
    }

    public void get(String url, Object[] params, OnResponse onResponse) {
        if (Utils.isNetworkStatusAvialable(ctx)) {
            json = null;
            for (int i = 0; i < params.length; i += 2) {
                url += (i == 0 ? "?" : "&") + params[i] + "=" + params[i + 1];
            }
//            Log.v("MyDebug0", "get:" + url);
            this.strUrl = url;
            this.onResponse = onResponse;
            RequestTask req = new RequestTask(RequestTask.GET);
            req.execute(params);
        } else {
            Utils.AlertNetwork(ctx);
        }
    }

    String crlf = "\r\n";
    String twoHyphens = "--";
    String boundary = "*****";

    private void postRequest(Object... objs) {
        HttpURLConnection httpUrlConnection = null;
        URL url = null;
        try {
            url = new URL(this.strUrl);
            httpUrlConnection = (HttpURLConnection) url.openConnection();
            httpUrlConnection.setUseCaches(false);
            httpUrlConnection.setDoOutput(true);
            httpUrlConnection.setRequestMethod("POST");
            httpUrlConnection.setRequestProperty("Connection", "Keep-Alive");
            httpUrlConnection.setRequestProperty("Cache-Control", "no-cache");
            httpUrlConnection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + this.boundary);
            DataOutputStream request = new DataOutputStream(httpUrlConnection.getOutputStream());
            if (objs != null) {
                for (int i = 0; i < objs.length; i += 2) {
                    if (objs[i + 1] instanceof File) {
                        File file = (File) objs[i + 1];
                        request.writeBytes(this.twoHyphens + this.boundary + this.crlf);
                        request.writeBytes("Content-Disposition: form-data; name=\"" + objs[i] + "\";filename=\"" + file.getName() + "\"" + this.crlf);
                        request.writeBytes(this.crlf);
                        int size = (int) file.length();
                        byte[] bytes = new byte[size];
                        try {
                            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
                            buf.read(bytes, 0, bytes.length);
                            buf.close();
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        request.write(bytes);
                        request.writeBytes(this.crlf);
                        request.writeBytes(this.twoHyphens + this.boundary + this.twoHyphens + this.crlf);
                    } else {
                        // Upload POST Data
                        request.writeBytes(twoHyphens + boundary + this.crlf);
                        request.writeBytes("Content-Disposition: form-data; name=\"" + objs[i] + "\"" + this.crlf);
                        request.writeBytes("Content-Type: text/plain" + this.crlf);
                        request.writeBytes(this.crlf);
                        String str = String.valueOf(objs[i + 1]);
                        byte[] buff = str.getBytes("UTF-8");
                        request.write(buff);
                        request.writeBytes(this.crlf);

                    }
                }
            }
            request.writeBytes(twoHyphens + boundary + twoHyphens + this.crlf);
            request.flush();
            request.close();
            L.V(httpUrlConnection.getHeaderFields());
            ////Log.v("MyDebug0", "POST: response code : " + httpUrlConnection.getResponseCode());

//			if (200 != httpUrlConnection.getResponseCode()) {
//				throw new CustomException("Failed to upload code:" + httpUrlConnection.getResponseCode() + " " + httpUrlConnection.getResponseMessage());
//			}

            InputStream responseStream = new BufferedInputStream(httpUrlConnection.getInputStream());
            BufferedReader responseStreamReader = new BufferedReader(new InputStreamReader(responseStream));
            String line = "";
            StringBuilder stringBuilder = new StringBuilder();
            while ((line = responseStreamReader.readLine()) != null) {
                stringBuilder.append(line).append("\n");
            }
            responseStreamReader.close();
            json = stringBuilder.toString();

            responseStream.close();
            httpUrlConnection.disconnect();
        } catch (MalformedURLException e) {
//            Utils.ALERT(ctx,e.getMessage());
//            Utils.Alert(ctx,ctx.getString(R.string.alert),e.getMessage());
            e.printStackTrace();
        } catch (ProtocolException e) {
//            Utils.ALERT(ctx,e.getMessage());
//            Utils.Alert(ctx,ctx.getString(R.string.alert),e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
//            Utils.ALERT(ctx,e.getMessage());
//            Utils.Alert(ctx,ctx.getString(R.string.alert),e.getMessage());
            e.printStackTrace();
        } finally {
            if (httpUrlConnection != null) {
                httpUrlConnection.disconnect();
            }
        }
    }

    private void getRequest(Object... objs) {
        URL url;
        HttpURLConnection urlConnection = null;
        try {
            url = new URL(this.strUrl);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            L.V(urlConnection.getHeaderFields());
            L.V("response-code : ", urlConnection.getResponseCode());
            byte[] buff = convertInputStreamToByteArray(urlConnection.getInputStream());
            json = new String(buff, "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
    }

    public void setContext(RootActivity ctx) {
        this.ctx = ctx;
    }

    private class RequestTask extends AsyncTask<Object, Void, Void> {

        public static final byte GET = 0;
        public static final byte POST = 1;

        private byte m;


        public RequestTask(byte m) {
            this.m = m;
        }

        @Override
        protected void onPreExecute() {//begin progress
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {//end progress
            super.onPostExecute(aVoid);
            if (onResponse != null) {
                JSONObject jso = null;
                if (json != null) {
                    L.V("json:" + json);
                    try {
                        jso = new JSONObject(json);
                    } catch (JSONException e) {
//                        Utils.ALERT(ctx,e.getMessage());
//                        Utils.Alert(ctx,ctx.getString(R.string.alert),e.getMessage());
                        e.printStackTrace();
                    }
                } else {
//                    Utils.ALERT(ctx,json);
//                    Utils.Alert(ctx,ctx.getString(R.string.alert),json);
                }
                if (jso == null)
                    jso = new JSONObject();
                onResponse.onResponse(jso);
            }
        }

        @Override
        protected Void doInBackground(Object... params) {
            switch (m) {
                case GET:
                    getRequest(params);
                    break;
                case POST:
                    postRequest(params);
                    break;
            }
            return null;
        }
    }

    public interface OnResponse {
        void onResponse(JSONObject jso);
    }

}
