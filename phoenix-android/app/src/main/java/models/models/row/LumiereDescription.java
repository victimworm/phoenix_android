package models.models.row;

import androidx.annotation.NonNull;

public class LumiereDescription implements Comparable<LumiereDescription> {
    public int type;
    public String desc;

    @Override
    public int compareTo(@NonNull LumiereDescription lumiereDesc) {
        return type - lumiereDesc.type;
    }

    public LumiereDescription(int cardType, String cardDesc) {
        type = cardType;
        desc = cardDesc;
    }
}
