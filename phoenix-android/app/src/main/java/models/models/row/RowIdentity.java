package models.models.row;

import java.util.ArrayList;

public class RowIdentity {
    public int id;
    public String name;
    public int score;
    public ArrayList<Integer> dimensions;

    public RowIdentity(int rowId, String rowName, int rowScore, ArrayList<Integer> rowDimens) {
        id = rowId;
        name = rowName;
        score = rowScore;
        dimensions = rowDimens;
    }
}