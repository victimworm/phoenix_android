package models.models.row;

import java.util.ArrayList;
import java.util.Arrays;

public class RowsIdentity {
    public ArrayList<RowIdentity> list;

    public RowsIdentity() {
        list = new ArrayList<>();
        list.add(new RowIdentity(0,"harmonie",2, new ArrayList<Integer>(Arrays.asList(-20,-11,-2,8,17))));
        list.add(new RowIdentity(1,"aventure",3, new ArrayList<Integer>(Arrays.asList(-18,-9,1,10,19))));
        list.add(new RowIdentity(2,"fruit",2, new ArrayList<Integer>(Arrays.asList(-22,-4,6,15,24))));
        list.add(new RowIdentity(3,"complicate",3, new ArrayList<Integer>(Arrays.asList(-25,-16,-7,3,12,21))));
        list.add(new RowIdentity(4,"energy",4, new ArrayList<Integer>(Arrays.asList(-23,-14,-5,5,14,23))));
        list.add(new RowIdentity(5,"guerre",2, new ArrayList<Integer>(Arrays.asList(-21,-12,3,7,16,25))));
        list.add(new RowIdentity(6,"desire",3, new ArrayList<Integer>(Arrays.asList(-24,-15,-6,4,13,22))));
        list.add(new RowIdentity(7,"intution",2, new ArrayList<Integer>(Arrays.asList(-19,-10,-1,9,18))));
        list.add(new RowIdentity(8,"besoin",3, new ArrayList<Integer>(Arrays.asList(-17,-8,2,11,20))));
    }
}
