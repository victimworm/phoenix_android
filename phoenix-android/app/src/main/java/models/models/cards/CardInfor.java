package models.models.cards;

/**
 * Created by nguyen hong phuc on 9/19/2016.
 */
public class CardInfor {

    public int number;
    public boolean reverse;

    public CardInfor(int number, boolean reverse) {
        this.number = number;
        this.reverse = reverse;
    }

}
