package models.models.cards;

import android.graphics.Bitmap;

/**
 * Created by nvdai on 9/29/2016.
 */
public class CardItem {
    public int image_id;//la so tren quan bai = 0 ~ 25
    public boolean reverse;

    public CardItem(int numberCard) {
        this.image_id = numberCard;
    }

    public int getImage_id() {
        return image_id;
    }

    public void setImage_id(int image_id) {
        this.image_id = image_id;
    }
}
