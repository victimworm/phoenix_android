package models.models.cards;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;

import com.matkaline.phoenix.App;
import com.matkaline.phoenix.R;

import java.io.IOException;

import http.HttpManager;

public class Card {

    public static final byte CHOC = 0;//XUNG DOT
    public static final byte CHARME = 1;//HAP DAN
    public static final byte CHOCCHARME = 2;//
    public static final byte MATKA = 3;//NO CHOC NO CHARME

    public int id;
    public byte type;
    public byte number;
    public boolean isReverse;
    public Bitmap bm;
    private Bitmap bmLock;
    public float x, y;
    public float w = 185, h = 353;
    public float a;
    public float b;
    public float a3d;
    private Rect src, dst;
    public boolean selected;
    public int color;

    //SonLA_Add lumiere - S
    public boolean isLumiereReverse = false;
    public boolean isReverseYinYang = false;
    public byte typeTemp;
    public byte subId;
    //SonLA_Add lumiere - E

    public static final byte RED = 0;
    public static final byte BLUE = 1;
    public static final byte ORGIN = 2;
    public static final byte YELLOW = 3;
    public static final byte GREEN = 4;
    public static final byte WHITE = 5;
    public boolean done = false;
    private App app;
    private Context ctx;


    private HttpManager httpManager;

    private Paint paWhite = new Paint();{
        paWhite.setStyle(Paint.Style.STROKE);
        paWhite.setStrokeWidth(2);
        paWhite.setColor(0XFFFFFFFF);
    }

    public Card(Context ctx, byte number) {
        this.number = number;
        this.ctx = ctx;
        app = App.getInstance();
        bmLock = BitmapFactory.decodeResource(ctx.getResources(), R.drawable.card_back);
//        try {
//            bm = BitmapFactory.decodeStream(ctx.getAssets().open("" + number + ".jpg"));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        src = new Rect(0, 0, bm.getWidth(), bm.getHeight());
        dst = new Rect();

        httpManager = new HttpManager(ctx);

    }

    public void draw(Canvas c) {
        src = new Rect(0, 0, bmLock.getWidth(), bmLock.getHeight());
        if (!selected) {
            dst.left = (int) (x - w / 2);
            dst.right = (int) (dst.left + w);
            dst.top = (int) (y - h / 2);
            dst.bottom = (int) (dst.top + h);
            c.drawBitmap(bmLock, src, dst, null);
        }
    }

    public void drawSelected(Canvas c){
        if (selected) {
            int d = 50;
            dst.left = (int) (x - w / 2 - d / 2);
            dst.right = (int) (dst.left + w + d);
            dst.top = (int) (y - h / 2 - d / 2);
            dst.bottom = (int) (dst.top + h + d);
            c.drawBitmap(bmLock, src, dst, null);
        }
    }

    /**
     * return bitmap of card
     * @return
     */
    public Bitmap getBitmap(){
        if (bm == null || bm.isRecycled()) {
            try {
                bm = BitmapFactory.decodeStream(ctx.getAssets().open(number + ".jpg"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return bm;
    }

    public void drawBig(Canvas c, boolean isTop) {
        bm = getBitmap();
        double cosa =  Math.cos(Math.toRadians(a3d));
        float dw = (float) (Math.abs(w*4*cosa));
        dst.left = (int) (x - dw / 2);
        dst.right = (int) (dst.left + dw);
        dst.top = (int) (y - h * 4 / 2);
        dst.bottom = (int) (dst.top + h * 4);



        done = a3d <= 10;
        if(done && isTop) { // incase the done value change true fast, need doneFlip value to stop it
            if (app.cardMng.cardsResult[1].number == 0){
                rotate(c);
            } else if (app.cardMng.cardsResult[1].number == 13){
                if (app.cardMng.cardsResult[0].isBad()){
                    rotate(c);
                }
            }else if (app.cardMng.cardsResult[0].color == app.cardMng.cardsResult[1].color){
                if ((app.cardMng.cardsResult[0].number + app.cardMng.cardsResult[1].number) % 2 == 0) {//cung chan || cung le
                    rotate(c);
                }
            }
        } else {
            a = 0;
            if (isReverse){
                a = 180; // hanhld check if rootcause is here for first reverse after withdraw card
            }
            c.save();
            c.rotate(a, x, y);
            if (a3d<90) {
                src = new Rect(0, 0, bm.getWidth(), bm.getHeight());
                c.drawBitmap(bm, src, dst, null);
            }else{
                src = new Rect(0, 0, bmLock.getWidth(), bmLock.getHeight());
                c.drawBitmap(bmLock, src, dst, null);
            }
            a3d+=(0-a3d)/10;
            c.restore();
        }
    }

    private void rotate(Canvas c) {
        if(b<180) {
            b = b+3;
        }

        if (isReverse){
            b = 180; // hanhld check if rootcause is here for first reverse after withdraw card
        }
        c.save();
        c.rotate(b, x, y);
        if (a3d<90) {
            src = new Rect(0, 0, bm.getWidth(), bm.getHeight());
            c.drawBitmap(bm, src, dst, null);
        }else{
            src = new Rect(0, 0, bmLock.getWidth(), bmLock.getHeight());
            c.drawBitmap(bmLock, src, dst, null);
        }
        a3d+=(0-a3d)/10;
        c.restore();
    }
    public void drawBigRotate(Canvas c) {
        bm = getBitmap();
        double cosa =  Math.cos(Math.toRadians(a3d));
        float dw = (float) (Math.abs(w*4*cosa));
        dst.left = (int) (x - dw / 2);
        dst.right = (int) (dst.left + dw);
        dst.top = (int) (y - h * 4 / 2);
        dst.bottom = (int) (dst.top + h * 4);
        Log.e("CANVAS", "== " + b);
        if(b<180) {
            b = b+3;
        }

        if (isReverse){
            b = 180; // hanhld check if rootcause is here for first reverse after withdraw card
        }
        c.save();
        c.rotate(b, x, y);
        if (a3d<90) {
            src = new Rect(0, 0, bm.getWidth(), bm.getHeight());
            c.drawBitmap(bm, src, dst, null);
        }else{
            src = new Rect(0, 0, bmLock.getWidth(), bmLock.getHeight());
            c.drawBitmap(bmLock, src, dst, null);
        }
        a3d+=(0-a3d)/10;
        c.restore();
    }

    public void recycle(){
        if (bm!=null && !bm.isRecycled())
            bm.recycle();
    }

    public void drawRect(Canvas c){
        dst.left = (int) (x - w / 2);
        dst.right = (int) (dst.left + w);
        dst.top = (int) (y - h / 2);
        dst.bottom = (int) (dst.top + h);
        c.drawRect(dst,paWhite);
    }

    public boolean onTouchEvent(float ex, float ey) {
        return !(ex < x - w / 2 || ex > x + w / 2 || ey < y - h / 2 || ey > y + h / 2);
    }

    /**
     * retur true is Conflict - return true : Xung Dot
     *
     * @return
     */
    public boolean isBad() {
        if (number == 0) return false;//MATKA is BAD and GOOD
        if (number == 13) return false;//PHOENIX not BAD not GOOD
        if (!isReverse) {
            return number <= 12 && number >= 1;
        } else {
            return number >= 14 && number <= 25;
        }
    }

    @Override
    public String toString() {
         return "{"+number+"}";
    }

    public void showBack() {
        b = 0;
        a3d = 180;
    }

    public void loadBigBitmap() {
        if (bm == null || bm.isRecycled()) {
            try {
                bm = BitmapFactory.decodeStream(ctx.getAssets().open(number + ".jpg"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}