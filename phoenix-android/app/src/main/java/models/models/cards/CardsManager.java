package models.models.cards;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.util.Log;
import android.view.MotionEvent;

import com.matkaline.phoenix.App;
import com.matkaline.phoenix.RootActivity;
import com.matkaline.phoenix.data.local.prefs.AppPreferencesHelper;
import com.matkaline.phoenix.utils.AppConstants;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.TimeZone;

import models.models.randoms.MyRandomSeed;
import ulitis.DateTimeHelper;
import ulitis.Utils;
import views.CardsView;

public class CardsManager {
    private static final String TAG = "CARD_MNG_NUM";
    private static final String TAG_IDS = "CARD_MNG_IDS";
    public static final int NOTIF_LIST = 1;
    public static final int NOTIF_DEFAULT = 2;
    public static final int NOTIF_TODAY = 3;
    private List<CardInfor> lsCard = new ArrayList<CardInfor>();
    public boolean isBad;
    public static int id;
    private RootActivity rootActivity;
    private SharedPreferences prefs;

    public void setRootActivity(RootActivity activity) {
        this.rootActivity = activity;
    }

    {
        lsCard.add(null);
        for (int id = 1; id <= 12; id++) {
            lsCard.add(new CardInfor(id, true));
        }
        for (int id = 13; id <= 24; id++) {
            lsCard.add(new CardInfor(id + 1, false));
        }
        lsCard.add(new CardInfor(0, false));//id = 25
        lsCard.add(new CardInfor(13, false));//id = 26
        for (int id = 27; id <= 38; id++) {
            lsCard.add(new CardInfor(id - 26, false));
        }
        for (int id = 39; id <= 50; id++) {
            lsCard.add(new CardInfor(id - 26 + 1, true));
        }
    }

    public Card[] cards = new Card[26];
    private float w, h;


    public Date datSelectCard = null;

    private App app;
    private Context ctx;
    public Card[] cardsResult = new Card[3];
    public boolean reconcile;// = true , thi la co hoa giai
    public String question;
    private List<String> questions;

    public boolean isButton = false;

    //SonLA_paid lumiere random - S
    public int deltaNumber = 0;//trọng số khi lumiere random
    public boolean isResetRandomIndex;
    //SonLA_paid lumiere random - E

    public CardsManager(Context ctx) {
        this.ctx = ctx;
        app = App.getInstance();
        for (byte i = 0; i < cards.length; i++) {
            cards[i] = new Card(ctx, i);
        }
        cards[0].type = Card.MATKA;
        for (int i = 1; i <= 12; i++) {
            cards[i].type = Card.CHOC;
        }
        cards[13].type = Card.CHOCCHARME;
        for (int i = 14; i <= 25; i++) {
            cards[i].type = Card.CHARME;
        }

        cards[0].color = Card.WHITE;
        cards[1].color = Card.RED;
        cards[2].color = Card.BLUE;
        cards[3].color = Card.RED;
        cards[4].color = Card.BLUE;
        cards[5].color = Card.RED;
        cards[6].color = Card.BLUE;
        cards[7].color = Card.RED;
        cards[8].color = Card.BLUE;
        cards[9].color = Card.RED;
        cards[10].color = Card.BLUE;
        cards[11].color = Card.RED;
        cards[12].color = Card.BLUE;
        cards[13].color = Card.ORGIN;
        cards[14].color = Card.YELLOW;
        cards[15].color = Card.GREEN;
        cards[16].color = Card.YELLOW;
        cards[17].color = Card.GREEN;
        cards[18].color = Card.YELLOW;
        cards[19].color = Card.GREEN;
        cards[20].color = Card.YELLOW;
        cards[21].color = Card.GREEN;
        cards[22].color = Card.YELLOW;
        cards[23].color = Card.GREEN;
        cards[24].color = Card.YELLOW;
        cards[25].color = Card.GREEN;

        arrGood = Utils.GetLines(ctx.getAssets(), "mean_card/random_good.txt");
        arrBad = Utils.GetLines(ctx.getAssets(), "mean_card/random_bad.txt");
        arrSegment1 = Utils.GetLines(ctx.getAssets(), "mean_card/random_segment1.txt");
        arrSegment3 = Utils.GetLines(ctx.getAssets(), "mean_card/random_segment3.txt");

        load();
    }

    private String[] arrGood;
    private String[] arrBad;

    private String[] arrSegment1;
    private String[] arrSegment3;


    private String getRandom(String[] arr) {
        return arr[rd.nextInt(arr.length)];
    }

    /**
     * return random 1 String from 6 String Good
     *
     * @return
     */
    public String getRandomCharm() {
        switch (rootActivity.getCurrentLanguage().getLanguage()) {
            case "vi":
                arrBad = Utils.GetLines(ctx.getAssets(), "mean_card/random_bad.txt");
                break;
            case "en":
                arrBad = Utils.GetLines(ctx.getAssets(), "mean_card/random_bad.txt");
                break;
            case "ru":
                arrBad = Utils.GetLines(ctx.getAssets(), "mean_card/random_bad_ru.txt");
                break;
            default:
                arrBad = Utils.GetLines(ctx.getAssets(), "mean_card/random_bad.txt");
                break;
        }
        return arrGood[rd.nextInt(arrGood.length)];
    }

    /**
     * return random 1 String from 6 String Bad
     *
     * @return
     */
    public String getRandomChoc() {
        switch (rootActivity.getCurrentLanguage().getLanguage()) {
            case "vi":
                arrGood = Utils.GetLines(ctx.getAssets(), "mean_card/random_good.txt");
                break;
            case "en":
                arrGood = Utils.GetLines(ctx.getAssets(), "mean_card/random_good.txt");
                break;
            case "ru":
                arrGood = Utils.GetLines(ctx.getAssets(), "mean_card/random_good_ru.txt");
                break;
            default:
                arrGood = Utils.GetLines(ctx.getAssets(), "mean_card/random_good.txt");
                break;
        }
        return arrBad[rd.nextInt(arrBad.length)];
    }

    public String getRandomSegment1() {
        switch (rootActivity.getCurrentLanguage().getLanguage()) {
            case "vi":
                arrSegment1 = Utils.GetLines(ctx.getAssets(), "mean_card/random_segment1.txt");
                break;
            case "en":
                arrSegment1 = Utils.GetLines(ctx.getAssets(), "mean_card/random_segment1.txt");
                break;
            case "ru":
                arrSegment1 = Utils.GetLines(ctx.getAssets(), "mean_card/random_segment1_ru.txt");
                break;
            default:
                arrSegment1 = Utils.GetLines(ctx.getAssets(), "mean_card/random_segment1.txt");
                break;
        }
        return arrSegment1[rd.nextInt(arrSegment1.length)];
    }

    public String getRandomSegment3() {
        switch (rootActivity.getCurrentLanguage().getLanguage()) {
            case "vi":
                arrSegment3 = Utils.GetLines(ctx.getAssets(), "mean_card/random_segment3.txt");
                break;
            case "en":
                arrSegment3 = Utils.GetLines(ctx.getAssets(), "mean_card/random_segment3.txt");
                break;
            case "ru":
                arrSegment3 = Utils.GetLines(ctx.getAssets(), "mean_card/random_segment3_ru.txt");
                break;
            default:
                arrSegment3 = Utils.GetLines(ctx.getAssets(), "mean_card/random_segment3.txt");
                break;
        }
        return arrSegment3[rd.nextInt(arrSegment3.length)];
    }

    public void soft(float w, float h) {
        this.w = w;
        this.h = h;
        soft();
    }

    public void soft() {
        for (int i = 0; i < cards.length; i++) {
            cards[i].w = 185 * CardsView.RATE;
            cards[i].h = 353 * CardsView.RATE;
            cards[i].selected = false;
        }
        float xfirst = (w - 1000 * CardsView.RATE) / 2 + cards[0].w / 2;
        float yfirst = 150 * CardsView.RATE + cards[0].h / 2;
        float dx = (w - xfirst * 2) / 4;
        float dy = 300 * CardsView.RATE;
        int col = 0;
        int row = 0;
        for (int i = 0; i < cards.length; i++) {
            cards[i].x = xfirst + col * dx;
            cards[i].y = yfirst + row * dy;
            col++;
            if (col >= 5) {
                col = 0;
                row++;
            }
        }
    }

    public void draw(Canvas c) {
        for (int i = 0; i < cards.length; i++) {
            cards[i].draw(c);
        }
    }

    public void drawOnHome(Canvas c) {
        for (int i = 0; i < cards.length; i++) {
            cards[i].draw(c);
        }
    }

    public void drawSelected(Canvas c) {
        if (cardSelected != null)
            cardSelected.drawSelected(c);
    }

    private Card cardSelected;

    public Card onTouchEvent(MotionEvent e) {
        float xe = e.getX();
        float ye = e.getY();
        reset();
        for (int i = 0; i < cards.length; i++) {
            if (cards[i].onTouchEvent(xe, ye)) {
                cards[i].selected = true;
                return cardSelected = cards[i];
            }
        }
        return null;
    }

    public void reset() {
        for (int i = 0; i < cards.length; i++) {
            cards[i].selected = false;
        }
        cardSelected = null;
    }


    public Card getLastCard() {
        Card card = cards[cards.length - 1];
        if (card.selected) {
            return cards[cards.length - 2];
        } else {
            return card;
        }
    }


    /**
     * @return
     */
    public Card[] getCardByDay() {
        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        calendar.setTime(datSelectCard);
        int y = calendar.get(Calendar.YEAR);
        int m = calendar.get(Calendar.MONTH);
        int d = calendar.get(Calendar.DAY_OF_MONTH);
        calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC+0"));
        calendar.set(Calendar.YEAR, y);
        calendar.set(Calendar.MONTH, m);
        calendar.set(Calendar.DAY_OF_MONTH, d);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        long s = calendar.getTimeInMillis() + app.user.user_id * 1000;
        Log.v("DATE_TEST", datSelectCard + "longtime: " + s);
        MyRandomSeed.seed(s);
        int id = MyRandomSeed.num(1, 50);
        cardsResult[0] = getCard(id);
        id = MyRandomSeed.num(1, 50);
        cardsResult[1] = getCard(id);
        checkCardDuplicate(1, new int[]{0});

        //can luon luon la hap dan
        id = MyRandomSeed.num(1, 50);
        cardsResult[2] = getCard(id);
        checkCardDuplicate(2, new int[]{0, 1});

        if (cardsResult[2].isBad()) {
            cardsResult[2].isReverse = !cardsResult[2].isReverse;
        }
        for (Card card : cardsResult) {
            {
                card.showBack();
            }
        }
        return cardsResult;
    }

    /**
     * check boc trung quan bai
     *
     * @param number
     * @param numbers
     */
    private void checkCardDuplicate(int number, int... numbers) {
        while (isDup(number, numbers)) {
            cardsResult[number] = cards[(cardsResult[number].number + 1) % 26];
        }
    }

    /**
     * return true if found item in array
     *
     * @return
     */
    private boolean isDup(int i, int... arr) {
        for (int j : arr) {
            if (cardsResult[i].number == cardsResult[j].number) {
                return true;
            }
        }
        return false;
    }

    public Card[] getCardByRandom() {
        Random rd = new Random();
        cardsResult[0] = getCard(rd.nextInt(50) + 1);
        cardsResult[1] = getCard(rd.nextInt(50) + 1);
        if (cardsResult[1].number == cardsResult[0].number) {
            cardsResult[1] = cards[(cardsResult[0].number + 1) % 26];
        }
        cardsResult[2] = getCard(rd.nextInt(50) + 1);//can luon luon la hap dan
        if (cardsResult[2].isBad()) {
            cardsResult[2].isReverse = !cardsResult[2].isReverse;
        }
        for (Card card : cardsResult) {
            {
                card.showBack();
            }
        }
        return cardsResult;
    }

    /**
     * game theo chế độ profile mode, trả lại 9 lá bài như rút tháng bình thường
     * nhưng sẽ có thêm mô tả + share như lumiere
     */
    public ArrayList<Card> getProfileCard(int count) {
        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        calendar.setTime(DateTimeHelper.TODAY());
        int y = calendar.get(Calendar.YEAR);
        int m = calendar.get(Calendar.MONTH);
        int d = calendar.get(Calendar.DAY_OF_MONTH);
        calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC+0"));
        calendar.set(Calendar.YEAR, y);
        calendar.set(Calendar.MONTH, m);
        calendar.set(Calendar.DAY_OF_MONTH, d);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        ArrayList<Card> cardArray = new ArrayList<Card>();
        Random r = new Random();
        long alphaNumber = app.user.user_id + r.nextInt(99999 - 10000) + 10000;
        long s = calendar.getTimeInMillis() + alphaNumber * 1000;
        MyRandomSeed.seed(s);

        ArrayList<Number> totalMainId = new ArrayList<Number>();
        ArrayList<Number> totalSubId = new ArrayList<Number>();

        for (int i = 0; i < count; i++) {
            Card mainCard = new Card(this.ctx, (byte) 0);
            Card subCard = new Card(this.ctx, (byte) 0);

            //get main card
            int id = MyRandomSeed.num(1, 50);
            mainCard = getCard(id);
            do {
                mainCard = cards[(mainCard.number + 1) % 26];
            } while (totalMainId.contains(mainCard.number));
            totalMainId.add(mainCard.number);

            //get sub card
            id = MyRandomSeed.num(1, 50);
            subCard = getCard(id);
            do {
                subCard = cards[(subCard.number + 1) % 26];
            } while (totalMainId.contains(subCard.number));
            mainCard.subId = subCard.number;

            mainCard = checkLumiereInfo(mainCard, subCard);
            if (mainCard.isLumiereReverse) {
                totalSubId.add(subCard.number);
            }

            cardArray.add(mainCard);
        }
        return cardArray;
    }

    /**
     * game theo chế độ lumiere mode, trả lại 5 hoặc 6 lá bài
     * nếu la lumiere today thì luôn luôn sẽ gồm ô bài Energie ở giữa
     */
    public ArrayList<Card> getLumiereCard(int count, PlayCardType type) {
        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        calendar.setTime(DateTimeHelper.TODAY());
        int y = calendar.get(Calendar.YEAR);
        int m = calendar.get(Calendar.MONTH);
        int d = calendar.get(Calendar.DAY_OF_MONTH);
        calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC+0"));
        calendar.set(Calendar.YEAR, y);
        calendar.set(Calendar.MONTH, m);
        calendar.set(Calendar.DAY_OF_MONTH, d);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        ArrayList<Card> cardArray = new ArrayList<Card>();
        long alphaNumber = 0;
        if (type == PlayCardType.LUMIERE_TODAY) {
            alphaNumber = app.user.user_id;
        } else {
            if (type == PlayCardType.LUMIERE_RANDOM) {
                alphaNumber = app.user.user_id + app.cardMng.deltaNumber;
            } else {
                Random r = new Random();
                alphaNumber = app.user.user_id + r.nextInt(99999 - 10000) + 10000;
            }
        }
        long s = calendar.getTimeInMillis() + alphaNumber * 1000;
        MyRandomSeed.seed(s);

        ArrayList<Number> totalMainId = new ArrayList<Number>();
        ArrayList<Number> totalSubId = new ArrayList<Number>();

        //rút trước lá bài today để lấy cardsResult
        if (type == PlayCardType.LUMIERE_TODAY) {
            app.cardMng.datSelectCard = DateTimeHelper.TODAY();
            app.cardMng.isButton = true;
            app.cardMng.genCards();
        }

        for (int i = 0; i < count; i++) {
            Card mainCard = new Card(this.ctx, (byte) 0);
            Card subCard = new Card(this.ctx, (byte) 0);

            //nếu là today thì index = 0 sẽ là lá today, sau đó sẽ swap với lá ở ô E
            if (type == PlayCardType.LUMIERE_TODAY || type == PlayCardType.LUMIERE_RANDOM) {
                if (i == 0) {
                    mainCard = cardsResult[0];
                    mainCard.subId = cardsResult[1].number;
                    totalMainId.add(mainCard.number);
                    mainCard = checkLumiereInfo(mainCard, cardsResult[1]);
                    if (mainCard.isLumiereReverse) {
                        totalSubId.add(cardsResult[1].number);
                    }
                    cardArray.add(mainCard);
                    continue;
                }
            }

            //get main card
            int id = MyRandomSeed.num(1, 50);
            mainCard = getCard(id);
            do {
//                mainCard = getCard((mainCard.number+1) % 26);
                mainCard = cards[(mainCard.number + 1) % 26];
            } while (totalMainId.contains(mainCard.number));
            totalMainId.add(mainCard.number);

            //get sub card
            id = MyRandomSeed.num(1, 50);
            subCard = getCard(id);
            do {
//                subCard = getCard((subCard.number+1) % 26);
                subCard = cards[(subCard.number + 1) % 26];
            } while (totalMainId.contains(subCard.number));
            mainCard.subId = subCard.number;

            //nếu sub card là cái bóng ACTIVE (khiến lá main lật ngược)
            //thì đảo ngược yin yang của lá đã rút
            if (totalSubId.contains(subCard.number)) {
                mainCard.isReverseYinYang = true;
            }

            mainCard = checkLumiereInfo(mainCard, subCard);
            if (mainCard.isLumiereReverse) {
                totalSubId.add(subCard.number);
            }

            cardArray.add(mainCard);
        }
        return cardArray;
    }

    /**
     * get kiểu lá bài và check lật ngược hay không trong lumiere mode
     * return lại lá bài với thông tin updated
     */
    private Card checkLumiereInfo(Card cardMain, Card cardSub) {
        Card newMainCard = cardMain;
        if (isReverse(newMainCard, cardSub)) {
            newMainCard.isLumiereReverse = true;
            for (int i = 1; i <= 12; i++) {
                if (i == newMainCard.number) {
                    newMainCard.typeTemp = Card.CHARME;
                }
            }

            for (int i = 14; i <= 25; i++) {
                if (i == newMainCard.number) {
                    newMainCard.typeTemp = Card.CHOC;
                }
            }
        } else {
            newMainCard.isLumiereReverse = false;
            for (int i = 1; i <= 12; i++) {
                if (i == newMainCard.number) {
                    newMainCard.typeTemp = Card.CHOC;
                }
            }

            for (int i = 14; i <= 25; i++) {
                if (i == newMainCard.number) {
                    newMainCard.typeTemp = Card.CHARME;
                }
            }
        }

        if (newMainCard.number == 0) {
            newMainCard.typeTemp = Card.MATKA;
        } else if (newMainCard.number == 13) {
            newMainCard.typeTemp = Card.CHOCCHARME;
        }
        return newMainCard;
    }

    /**
     * check lá bài có phải lật ngược hay không
     */
    private boolean isReverse(Card cardMain, Card cardSub) {
        //matka || phoenix sẽ không bao giờ bị lật
        if (cardMain.number == 0 || cardMain.number == 13) {
            return false;
        }

        //nếu cái bóng là matka thì sẽ lật ngược
        if (cardSub.number == 0) {
            return true;
        }

        //nếu cái bóng là phoenix và main là 1...12 thì sẽ lật ngược
        if (cardSub.number == 13 && cardMain.type == Card.CHOC) {
            return true;
        }

        //nếu cùng sóng và cùng chẵn/lẻ thì sẽ lật ngược
        if (cardMain.color == cardSub.color && ((cardMain.number + cardSub.number) % 2 == 0)) {
            return true;
        }
        return false;
    }

    public Card[] getMonthCardByRandom(int number) {
        Random rd = new Random();
        cardsResult[0] = getCard(number);
        cardsResult[1] = getCard(rd.nextInt(50) + 1);
        if (cardsResult[1].number == cardsResult[0].number) {
            cardsResult[1] = cards[(cardsResult[0].number + 1) % 26];
        }
        cardsResult[2] = getCard(rd.nextInt(50) + 1);//can luon luon la hap dan
        if (cardsResult[2].isBad()) {
            cardsResult[2].isReverse = !cardsResult[2].isReverse;
        }
        for (Card card : cardsResult) {
            {
                card.showBack();
            }
        }
        return cardsResult;
    }

    /**
     * tra ve quan bai tu quan boc va quan bong
     *
     * @return
     */
    public Card getResultCard() {
        if (!reconcile) {
            if (cardsResult[1].number == 0) {
                cardsResult[0].isReverse = true;
                return cardsResult[0];
            } else if (cardsResult[1].number == 13) {
                if (cardsResult[0].isBad()) {
                    cardsResult[0].isReverse = true;
                    return cardsResult[0];
                } else {
                    return cardsResult[0];
                }
            } else if (cardsResult[0].color == cardsResult[1].color) {
                if ((cardsResult[0].number + cardsResult[1].number) % 2 == 0) {//cung chan || cung le
                    cardsResult[0].isReverse = true;
                    return cardsResult[0];
                }
            }
            return cardsResult[0];
        } else {
            return cardsResult[2];
        }
    }

    /**
     * tra ve quan bai tu id cua server
     * xet truong hop quan bai nguoc thi van lay chieu la xuoi
     *
     * @param id
     * @return
     */
    public Card getCard(int id) {
        if (id < 0) return null;
        Card card = null;
        CardInfor cardInfor = lsCard.get(id);
        card = cards[cardInfor.number];
        card.isReverse = false;
//        card.isReverse = cardInfor.reverse;
//        if (cardInfor.reverse) {
//            card.isReverse = false;
//            int cardId = getCardId(card);
//            card = getCard(cardId);
//        }
        return card;
    }

    public Card getCardByNumber(Byte number) {
        Card card = null;
        card = cards[number];
        card.isReverse = false;
        return card;
    }

    /**
     * tra ve quan bai : tuong ung voi id (id lay theo server)
     * ko tuy tien thay doi chieu quan bai trong func
     *
     * @param id
     * @return
     */
    public Card card(int id) {
        if (id < 0) return null;
        Card card = null;
        CardInfor cardInfor = lsCard.get(id);
        card = cards[cardInfor.number];
        card.isReverse = cardInfor.reverse;
        return card;
    }

    private Random rd = new Random();

    /**
     * get id cau hoi ngau nhien
     *
     * @return
     */
    public int getId() {
        if (app.cardMng.cardsResult[0].type == Card.CHARME) {
            return id = rd.nextInt(181 - 1) + 1;
        } else if (app.cardMng.cardsResult[0].type == Card.CHOC) {
            return id = rd.nextInt(197 - 1) + 1;
        } else {
            return id = rd.nextInt(168 - 1) + 1;
        }

    }

    /**
     * get case for notification
     *
     * @return
     */
    public int getCaseNotif() {
        int i = rd.nextInt(2);
        if (i == 0) {
            i = rd.nextInt(2);
            if (i == 0) {
                return NOTIF_LIST;
            } else {
                return NOTIF_DEFAULT;
            }
        } else {
            return NOTIF_TODAY;
        }
    }

    public void getQuestionFromNotify(int type, String quest) {
        /*
        String prefixLanguage = "";
        String currentLanguage = rootActivity.getLanguage();
        if (currentLanguage.equals("vi")) {
            prefixLanguage = "_vn";
        } else if (currentLanguage.equals("en")) {
            prefixLanguage = "_en";
        }
        questions = new ArrayList<String>();
        BufferedReader reader = null;
        try {
            InputStream file = null;
            if (type == 1) {
                file = ctx.getAssets().open("question" + prefixLanguage + "_charm.txt");
            } else {
                file = ctx.getAssets().open("question" + prefixLanguage + "_choc.txt");
            }
            reader = new BufferedReader(new InputStreamReader(file, "UTF-8"));
            String line = null;
            while ((line = reader.readLine()) != null) {
                questions.add(line);
            }
            question =  questions.get(index);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        */
        question = quest;
        if (type == 1) {//Charme
            //nếu câu hỏi là charme, random card từ 14-25
            int indexMain = rd.nextInt(26 - 14) + 14;
            cardsResult[0] = cards[indexMain];

            int indexSub = rd.nextInt(26);
            while (isNeedReRoll(indexMain, indexSub)) {
                indexSub = rd.nextInt(26);
            }
            cardsResult[1] = cards[indexSub];

            int indexRecon = rd.nextInt(26 - 14) + 14;//reconcile always is charme
            while (indexRecon == indexMain || indexRecon == indexSub) {
                indexRecon = rd.nextInt(26 - 14) + 14;
            }
            cardsResult[2] = cards[indexRecon];
        } else {//Choc
            //nếu câu hỏi là choc, random card từ 1-12
            int indexMain = rd.nextInt(13 - 1) + 1;
            cardsResult[0] = cards[indexMain];

            int indexSub = rd.nextInt(26);
            while (isNeedReRoll(indexMain, indexSub)) {
                indexSub = rd.nextInt(26);
            }
            cardsResult[1] = cards[indexSub];

            int indexRecon = rd.nextInt(26 - 14) + 14;//reconcile always is charme
            while (indexRecon == indexMain || indexRecon == indexSub) {
                indexRecon = rd.nextInt(26 - 14) + 14;
            }
            cardsResult[2] = cards[indexRecon];
        }
    }

    private boolean isNeedReRoll(int numb1, int numb2) {
        if (numb2 == 0 || numb2 == 13) {//phoenix or matka always reroll - need confirm
            return true;
        }

        //cùng chẵn cùng lẻ
        if ((numb1 % 2 == 0) && (numb2 % 2 == 0)) {
            return true;
        }
        if ((numb1 % 2 != 0) && (numb2 % 2 != 0)) {
            return true;
        }

        //cùng sóng
        if ((numb1 > 0 && numb1 < 13) && (numb2 > 0 && numb2 < 13)) {
            return true;
        }
        if ((numb1 > 13 && numb1 < 26) && (numb2 > 13 && numb2 < 26)) {
            return true;
        }
        return false;
    }

    public void genNotifyQuestion() {
        String prefixLanguage = "";
        String currentLanguage = rootActivity.getCurrentLanguage().getLanguage();
        if (currentLanguage.equals("vi")) {
            prefixLanguage = "_vn";
        } else if (currentLanguage.equals("en")) {
            prefixLanguage = "_en";
        }

        int randomNumb = rd.nextInt(2);//random để lấy bừa 1 câu hỏi trong kho choc charm
        questions = new ArrayList<String>();
        BufferedReader reader = null;
        try {
            InputStream file = null;
            if (randomNumb == 1) {
                file = ctx.getAssets().open("questions" + prefixLanguage + "_charm.txt");
            } else {
                file = ctx.getAssets().open("questions" + prefixLanguage + "_choc.txt");
            }
            reader = new BufferedReader(new InputStreamReader(file, "UTF-8"));
            String line = null;
            while ((line = reader.readLine()) != null) {
                questions.add(line);
            }

            int index;
            if (randomNumb == 1) {
                index = rd.nextInt(181 - 1) + 1;
            } else {
                index = rd.nextInt(197 - 1) + 1;
            }
            question = questions.get(index);
            AppPreferencesHelper prefHelper = new AppPreferencesHelper(ctx, AppConstants.PREF_NAME);
            prefHelper.setYourQuestion(question);
            prefHelper.saveTypeCardOnNotification(randomNumb);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public String getQuestionFromUs() {
        questions = new ArrayList<String>();
        BufferedReader reader = null;
        switch (rootActivity.getCurrentLanguage().getLanguage()) {
            case "vi":
                try {
                    InputStream file = null;
                    if (app.cardMng.cardsResult[0].type == Card.CHARME) {
                        file = ctx.getAssets().open("questions_vn_charm.txt");
                    } else if (app.cardMng.cardsResult[0].type == Card.CHOC) {
                        file = ctx.getAssets().open("questions_vn_choc.txt");
                    } else {
                        file = ctx.getAssets().open("questions_vn.txt");
                    }
                    reader = new BufferedReader(new InputStreamReader(file, "UTF-8"));
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        questions.add(line);
                    }
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                break;
            case "en":
                try {
                    InputStream file = null;
                    if (app.cardMng.cardsResult[0].type == Card.CHARME) {
                        file = ctx.getAssets().open("questions_en_charm.txt");
                    } else if (app.cardMng.cardsResult[0].type == Card.CHOC) {
                        file = ctx.getAssets().open("questions_en_choc.txt");
                    } else {
                        file = ctx.getAssets().open("questions_en.txt");
                    }
                    reader = new BufferedReader(new InputStreamReader(file, "UTF-8"));
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        questions.add(line);
                    }
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                break;
            case "ru":
                try {
                    InputStream file = null;
//                    if(app.cardMng.cardsResult[0].type == Card.CHARME) {
//                        file = ctx.getAssets().open("questions_ru_charm.txt");
//                    } else if(app.cardMng.cardsResult[0].type == Card.CHOC) {
//                        file = ctx.getAssets().open("questions_ru_choc.txt");
//                    } else {
                    file = ctx.getAssets().open("questions_ru.txt");
//                    }
                    reader = new BufferedReader(new InputStreamReader(file, "UTF-8"));
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        questions.add(line);
                    }
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                break;
            default:
                try {
                    InputStream file = null;
                    if (app.cardMng.cardsResult[0].type == Card.CHARME) {
                        file = ctx.getAssets().open("questions_charm.txt");
                    } else if (app.cardMng.cardsResult[0].type == Card.CHOC) {
                        file = ctx.getAssets().open("questions_choc.txt");
                    } else {
                        file = ctx.getAssets().open("questions.txt");
                    }
                    reader = new BufferedReader(new InputStreamReader(file, "UTF-8"));
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        questions.add(line);
                    }
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                break;
        }
        question = questions.get(id);
        return question;
    }

    public void recycle() {
        for (Card card : cards) {
            card.recycle();
        }
    }

    public int getCardId(Card card) {
        for (int i = 1; i < lsCard.size(); i++) {
            CardInfor cardInfor = lsCard.get(i);
            switch (card.number) {
                case 0:
                case 13:
                    if (cardInfor.number == card.number) {
                        return i;
                    }
                    break;
                default:
                    if (card.number == cardInfor.number && card.isReverse == cardInfor.reverse) {
                        return i;
                    }
                    break;
            }

        }
        return -1;
    }

    public int getCardId(CardItem card) {
        for (int i = 1; i < lsCard.size(); i++) {
            CardInfor cardInfor = lsCard.get(i);
            switch (card.image_id) {
                case 0:
                case 13:
                    if (cardInfor.number == card.image_id) {
                        return i;
                    }
                    break;
                default:
                    if (card.image_id == cardInfor.number && card.reverse == cardInfor.reverse) {
                        return i;
                    }
                    break;
            }


        }
        return -1;
    }

    public int getCardId(Byte cardNumber) {
        for (int i = 1; i < lsCard.size(); i++) {
            CardInfor cardInfor = lsCard.get(i);
            switch (cardNumber) {
                case 0:
                case 13:
                    if (cardInfor.number == cardNumber) {
                        return i;
                    }
                    break;
                default:
                    if (cardNumber == cardInfor.number) {
                        return i;
                    }
                    break;
            }


        }
        return -1;
    }

    public void genCards() {
        recycle();
        isBad = false;
        reconcile = false;
        if (datSelectCard != null) {
            getCardByDay();
        } else {
            getCardByRandom();
        }
        save();
    }

    /**
     * tra ve phan giai thich quan bai
     *
     * @return
     */
    public String getExplain() { //QUAN TRONG KHONG XOA PHAN COMMENT NAY
        String strExplain = null;
        /*if (app.cardMng.datSelectCard!=null) {//chon TODAY | FUTURE
            if (!reconcile) {//chua hoac ko hoa giai
                if (cardsResult[0].isReverse) {//quan bai nguoc
                    Explain explain = Explain.get(-cardsResult[0].number);
                    strExplain = explain.expn1;
                    String strRandom = null;
                    if (cardsResult[0].isBad()) {
                        strRandom = getRandomBad();
                    } else {
                        strRandom = getRandomGood();
                    }
                    String strOMBRE = explain.OMBRE;
                    strRandom = strRandom.replace("[OMBRE]", strOMBRE);
                    strExplain = strExplain.replace("[SENSAGE OMBRE]", strRandom);
                } else {//quan bai
                    Explain explain = Explain.get(cardsResult[0].number);
                    strExplain = explain.expn1;
                }
            } else {
                Explain explain = null;
                if (cardsResult[2].number <=12 && cardsResult[2].number>=1){
                    explain = Explain.get(-cardsResult[2].number);
                }else{
                    explain = Explain.get(cardsResult[2].number);
                }

                Explain explain1 = Explain.get(cardsResult[0].number);

                String strSegment1 = getRandom(arrSegment1);
                strSegment1 = strSegment1.replace("[DÉFI]", explain.name);
                strSegment1 = strSegment1.replace("[ADJECTIF]", explain1.ADJECTIF);
                strSegment1 = strSegment1.replace("[NOM]", explain.NOM);
                String strSegment2 = explain.expn2;
                String strSegment3 = getRandom(arrSegment3);
                strExplain = strSegment1 + "<br>" + strSegment2 + "<br>" + strSegment3;
            }
        }else{//chon RANDOM|*/
        switch (rootActivity.getCurrentLanguage().getLanguage()) {
            case "vi":
                strExplain = Utils.GetText(ctx.getAssets(), "mean_card_vn/content_card_vn" + app.cardMng.getCardId(app.currentCard));
                break;
            case "en":
                strExplain = Utils.GetText(ctx.getAssets(), "mean_card_en/content_card" + app.cardMng.getCardId(app.currentCard));
                break;
            case "ru":
                strExplain = Utils.GetText(ctx.getAssets(), "mean_card_ru/content_card" + app.cardMng.getCardId(app.currentCard));
                break;
            default:
                strExplain = Utils.GetText(ctx.getAssets(), "mean_card/content_card" + app.cardMng.getCardId(app.currentCard));
                break;
        }


//        strExplain = Utils.GetText(ctx.getAssets(),"mean_card/content_card" + app.cardMng.getCardId(app.currentCard));
        /*}*/
        return strExplain;
    }

    private void load() {
        prefs = app.getSharedPreferences(app.getPackageName(), app.MODE_PRIVATE);
        int card_result_0 = prefs.getInt("card_result_0", -1);
        int card_result_1 = prefs.getInt("card_result_1", -1);
        int card_result_2 = prefs.getInt("card_result_2", -1);
        cardsResult[0] = getCard(card_result_0);
        cardsResult[1] = getCard(card_result_1);
        cardsResult[2] = getCard(card_result_2);
        for (int i = 0; i < cardsResult.length; i++) {
            if (cardsResult[i] != null)
                cardsResult[i].loadBigBitmap();
        }
        isBad = prefs.getBoolean("isBad", false);
        reconcile = prefs.getBoolean("reconcile", false);
        long t = prefs.getLong("date_selected_card", 0);
        if (t > 0) {
            datSelectCard = new Date(t);
        }
    }

    private void save() {
        SharedPreferences.Editor editor = prefs.edit();
        if (cardsResult[0] != null)
            editor.putInt("card_result_0", getCardId(cardsResult[0]));
        if (cardsResult[1] != null)
            editor.putInt("card_result_1", getCardId(cardsResult[1]));
        if (cardsResult[2] != null)
            editor.putInt("card_result_2", getCardId(cardsResult[2]));
        editor.putBoolean("isBad", isBad);
        editor.putBoolean("reconcile", reconcile);
        if (datSelectCard != null)
            editor.putLong("date_selected_card", datSelectCard.getTime());
        editor.commit();
    }


//    /**
//     * gen 60 ngay lien tuc cho user_id = 29
//     */
//    public void test(){
//        int user_id = 21;
//        int day_len = 200;
//        long time = System.currentTimeMillis();
//        for(int i=0;i<day_len;i++){
//            Date date = new Date(time);
//            Log.v("DATE_TEST","Date: "+ date);
//            Card[] cards = test(user_id,date);
//            Log.v(TAG,i+"\t"+date+"\t"+cards[0].number+"\t"+cards[1].number+"\t"+cards[2].number);
//            time += 86400000;
//        }
//    }
//
//    /**
//     * test gen quan bay
//     * cho user id cu the
//     * va date cu the
//     */
//    private Card[] test(int user_id,Date date){
//        int[] ids = new int[3];
//        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
//        calendar.setTime(date);
//        int y = calendar.get(Calendar.YEAR);
//        int m = calendar.get(Calendar.MONTH);
//        int d = calendar.get(Calendar.DAY_OF_MONTH);
//        calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC+0"));
//        calendar.set(Calendar.YEAR, y);
//        calendar.set(Calendar.MONTH, m);
//        calendar.set(Calendar.DAY_OF_MONTH, d);
//        calendar.set(Calendar.HOUR_OF_DAY, 0);
//        calendar.set(Calendar.MINUTE, 0);
//        calendar.set(Calendar.SECOND, 0);
//        calendar.set(Calendar.MILLISECOND, 0);
//        long s = calendar.getTimeInMillis() + user_id * 1000;
//        MyRandomSeed.seed(s);
//        int id = MyRandomSeed.num(1, 50);
//        cardsResult[0] = getCard(id);
//        id = MyRandomSeed.num(1, 50);
//        cardsResult[1] = getCard(id);
//        checkCardDuplicate(1,new int[]{0});
//
//        //can luon luon la hap dan
//        id = MyRandomSeed.num(1, 50);
//        cardsResult[2] = getCard(id);
//        checkCardDuplicate(2,new int[]{0,1});
//
//        if (cardsResult[2].isBad()) {
//            cardsResult[2].isReverse = !cardsResult[2].isReverse;
//        }
//        for(Card card : cardsResult){{
//            card.showBack();
//        }}
//        Log.v(TAG_IDS,"\t"+s+"\t"+date+"\t"+ids[0]+"\t"+ids[1]+"\t"+ids[2]);
//        return cardsResult;
//    }


}
