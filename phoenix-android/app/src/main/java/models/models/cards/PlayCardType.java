package models.models.cards;

public enum PlayCardType {
    MONTH,
    LUMIERE_RANDOM_ALL,
    LUMIERE_RANDOM,
    LUMIERE_TODAY,
    PROFILE
}
