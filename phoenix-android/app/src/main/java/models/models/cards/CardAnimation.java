package models.models.cards;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.util.Log;

import com.matkaline.phoenix.R;

/**
 * Created by nguyen hong phuc on 9/26/2016.
 */
public class CardAnimation {

    public float xTarget, yTarget;
    public float x, y;
    private Bitmap bm;

    public CardAnimation(Context ctx, float x, float y) {
        this.bm = BitmapFactory.decodeResource(ctx.getResources(), R.drawable.card_back);
        this.x = this.xTarget = x;
        this.y = this.yTarget = y;
    }

    public void draw(Canvas c) {
        float left = x - bm.getWidth() / 2;
        float top = y - bm.getHeight() / 1.1f;
        c.drawBitmap(bm, left, top, null);
    }

    public void update() {
        x += (xTarget - x) / 10;
        y += (yTarget - y) / 10;
    }

}
