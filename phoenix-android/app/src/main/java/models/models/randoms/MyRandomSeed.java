package models.models.randoms;

public class MyRandomSeed {

    private static long RSeed;

    public static void seed(long s) {
        RSeed = Abs(s) % 9999999 + 1;
        num(0, 9999999);
    }

    public static int num(int min, int max) {
        RSeed = (RSeed * 125) % 2796203;
        return (int) (RSeed % (max - min + 1) + min);
    }

    public static long Abs(long i){
        return i<0?-i:i;
    }

}