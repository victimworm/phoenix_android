package models;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.matkaline.phoenix.App;
import com.matkaline.phoenix.HomeActivity;
import com.matkaline.phoenix.R;
import com.matkaline.phoenix.RootActivity;
import com.matkaline.phoenix.SignInActivity;
import com.matkaline.phoenix.utils.DeviceUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Arrays;

import apis.API;
import facebooks.HelloFacebookSampleActivity;
import http.HttpManager;
import http.retrofit.RequestParams;
import ssn.GoogleHelper;
import ulitis.FileCache;
import ulitis.ImageLoader;
import ulitis.Utils;

public class User {

    private boolean isLogin = false;
    public String user_name;
    public String birthday;
    public String email;
    public String city;
    public String country;
    public String phone;
    public int type;
    public String avatar;
    public String avatar_replace = "";
    public Bitmap bmAvatar;
    public int coin = -1;
    public String token;

    public int user_id = -1;

    public Context ctx;
    public CallbackManager callbackManager;
    private Fblogin fblogin;

    private SharedPreferences prefs = null;
    public GoogleHelper ggHelper;
    private RootActivity rootActivity;

    public void loginByGg(String token) {
        //app.dlgLoginChoose.dismiss();
        RequestParams params = new RequestParams();
        params.addParam("access_token", token);
        params.addParam("type", "2");
        app.httpManager.doPost(API.LOGIN_SSN, params, new HttpManager.OnResponse() {
            @Override
            public void onResponse(JSONObject jso) {
                updateUserInfor(jso, (byte) 2);
            }
        });
    }

    public void avatarDownLoading(ImageView avatarUser) {
        FileCache fileCache = new FileCache(ctx);
        File f = fileCache.getFile(avatar);
//        L.V("file = "+f.exists());
        if (f.exists()) {
            bmAvatar = ImageLoader.DECODEFILE(f);
//            L.V("found avatar of "+user_name+" id="+user_id+ "  on cache ");
            if (bmAvatar != null) {
                avatarUser.setImageBitmap(bmAvatar);
            } else {
//                L.V("but is null "+f.getAbsolutePath());
            }
        } else {
//            L.V("not found avatar of "+user_name+" id="+user_id);
            String imageProfileUrl = avatar;
            if (DeviceUtils.isValidUrl(imageProfileUrl) == false) {
                if (avatar_replace.equals("") == false) {
                    imageProfileUrl = avatar_replace;
                }
            }
            app.imageLoader.DisplayImage(imageProfileUrl, avatarUser);
        }
    }

    /**
     * insert avatar cho image view
     *
     * @param avatarUser
     */
    public void insertAvatar(ImageView avatarUser) {
        if (bmAvatar != null) {
            avatarUser.setImageBitmap(bmAvatar);
        } else {
            avatarUser.setImageResource(R.drawable.icon_user);
            avatarDownLoading(avatarUser);
        }
    }

    private class Fblogin extends AsyncTask<AccessToken, String, String> {

//        ProgressDialog pDialog;
//        private Activity act;

        public Fblogin() {
//            this.act = act;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            pDialog = new ProgressDialog(act);
//            pDialog.setMessage("Loading Data..");
//            pDialog.setIndeterminate(false);
//            pDialog.setCancelable(false);
//            pDialog.show();
        }

        protected String doInBackground(AccessToken... params) {
            GraphRequest request = GraphRequest.newMeRequest(params[0],
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(JSONObject json, GraphResponse response) {
                            //Log.v("MyDebug0", json.toString());
                            updateUserInforFB(json);
                        }
                    });
            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,name,email,gender,birthday,picture");
            request.setParameters(parameters);
            request.executeAndWait();
            return null;
        }

        protected void onPostExecute(String file_url) {
//            pDialog.dismiss();
        }
    }

    public User(App app, int user_id, String user_name, String avatar) {
        this.app = app;
        this.ctx = app;
        this.user_id = user_id;
        this.user_name = user_name;
        this.avatar = avatar;
    }

    private App app;

    public User(App app) {
        this.app = app;
        LoginManager.getInstance().registerCallback(callbackManager = CallbackManager.Factory.create(),
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        System.out.println("Login");
                    }

                    @Override
                    public void onCancel() {
                        Log.v("MyDebug0", "LoginManager.onCancel");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        //Log.v("MyDebug0", "LoginManager.onError");
                        exception.printStackTrace();
                    }
                });
        init();
        ggHelper = new GoogleHelper();
    }

    public void loginByFbToPhoenixServer(LoginResult loginResult, final Activity act) {
        app.dlgLoginChoose.dismiss();
        type = 1;
        setIsLogin(true);
        String fbtoken = loginResult.getAccessToken().getToken();

        RequestParams params = new RequestParams();
        params.addParam("access_token", fbtoken);
        params.addParam("type", "1");

        User.this.app.httpManager.doPost(API.LOGIN_SSN, params, new HttpManager.OnResponse() {
            @Override
            public void onResponse(JSONObject jso) {
                updateUserInfor(jso, (byte) 1);
                act.finish();
            }
        });
    }

    public void setContext(Context ctx) {
        this.ctx = ctx;
    }

    /**
     * update infor from server
     */
//    public void submitCoin(final int ncoin) {
//        //Log.v("MyDebug0", "submit coin");
//        httpManager.get(API.URL_UPDATE + user_id + "/info", null, new HttpManager.OnResponse() {
//            @Override
//            public void onResponse(String jso) {
//                updateCoinCount(jso);//receiving coin number from server success
//                coin += ncoin;
//                save();
//                submitCoin();
//            }
//        });
//    }
    public void updateCoinCount(JSONObject jsoAll) {
        try {
            JSONObject jso = jsoAll.getJSONObject("data");
            coin = jso.getInt("coin");
            save();
        } catch (JSONException e) {
//            Utils.Alert(ctx,ctx.getString(R.string.alert),"Error");
//            Utils.ALERT(ctx,e.getMessage());
            e.printStackTrace();
        }
    }

    public void submitCoin(int ncoin, HttpManager.OnResponse onResponse) {
        if (Utils.isNetworkStatusAvialable(ctx)) {
            String url = API.URL_UPDATE + user_id + "/coin";

            RequestParams params = new RequestParams();
            params.addParam("coin_old", coin);
            params.addParam("change_coin", ncoin);
//            params.addParam("token", token);

            app.httpManager.doPost(url, params, onResponse);
        } else {
            Utils.AlertNetwork(ctx);
        }

    }

    public void updateUserInfor(JSONObject jsoAll, byte type) {
        try {
            token = jsoAll.getString("token");
            JSONObject jso = jsoAll.getJSONObject("user");
            user_id = jso.getInt("id");
            avatar = jso.getString("avatar");
            user_name = jso.getString("user_name");
            coin = jso.getInt("coin");
            birthday = jso.getString("birthday");
            email = jso.getString("email");
            city = jso.getString("city");
            country = jso.getString("country");
            phone = jso.getString("phone");
            this.type = type;
            setIsLogin(true);
        } catch (JSONException e) {
            Utils.Alert(ctx, ctx.getString(R.string.alert), "Login unsuccessful\n Your account is missing an email address.");
            e.printStackTrace();
            setIsLogin(false);
        }
    }

    public void updateEdit(JSONObject jsoAll) {
        try {
            JSONObject jso = jsoAll.getJSONObject("data");
            user_id = jso.getInt("id");
            avatar = jso.getString("avatar");
            user_name = jso.getString("user_name");
            coin = jso.getInt("coin");
            birthday = jso.getString("birthday");
            city = jso.getString("city");
            country = jso.getString("country");
            phone = jso.getString("phone");
//            L.V("user.updateEdit avatar : "+avatar);
            save();
            //((RootActivity)ctx).updateUISlidingMenu();
        } catch (JSONException e) {
//            Utils.Alert(ctx,ctx.getString(R.string.alert),"Error");
//            Utils.ALERT(ctx,e.getMessage());
            e.printStackTrace();
        }
    }

    private void updateUserInforFB(JSONObject json) {
        try {
            JSONObject jso = json.getJSONObject("picture");
            JSONObject jsData = jso.getJSONObject("data");
            avatar = jsData.getString("url");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void doLogOutGoogle(){
        clearData();
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(App.getInstance().getString(R.string.server_client_ID))
                .requestEmail()
                .build();

        GoogleSignIn.getClient(App.getInstance(), gso).signOut();
        ((RootActivity) ctx).go(SignInActivity.class);
    }

    public void logout() {
        if (Utils.isNetworkStatusAvialable(ctx)) {
            if (type == 1) {
                ((RootActivity) ctx).go(HelloFacebookSampleActivity.class);
            } else if (type == 2) {
                doLogOutGoogle();
            } else {
//            app.httpManager.get(API.LOGOUT, new Object[]{"token", token}, new HttpManager.OnResponse() {
//                @Override
//                public void onResponse(JSONObject json) {
//
//                }
//            });
                setIsLogin(false);
                Intent intent = new Intent(ctx, HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                ctx.startActivity(intent);
            }
        } else {
            Utils.Alert(ctx, ctx.getString(R.string.alert), ctx.getString(R.string.alert_offline));
        }

    }

    /**
     * login through facebook authentication
     */
    public void loginByFacebook(Activity act) {
//        if (isNetworkStatusAvialable()) {
//            fblogin = new Fblogin(act);
        LoginManager.getInstance().logInWithReadPermissions(act, Arrays.asList("email"));
//        }else{
//            alert("Thông báo","Đã mất kết nối internet");
//        }
//        //Log.v("MyDebug0","Login Fb");
    }

    public void setIsLogin(boolean b) {
        this.isLogin = b;
        if (!b) {
//            FileCache fileCache = new FileCache(ctx);
//            fileCache.clear();
//            MemoryCache memoryCache = new MemoryCache();
//            memoryCache.clear();
            if (app.imageLoader == null) {
                app.imageLoader = new ImageLoader(app);
            }
            app.imageLoader.clearCache();
            avatar = "";
            user_id = -1;
            user_name = "";
            birthday = "";
            email = "";
            city = "";
            country = "";
            phone = "";
            coin = 0;
            type = 0;
            token = null;
            if (bmAvatar != null)
                bmAvatar.recycle();
            bmAvatar = null;
        }
        save();
        ((RootActivity) ctx).updateUISlidingMenu();
    }

    public boolean isLogin() {
        return this.isLogin;
    }

    private void init() {
        prefs = app.getSharedPreferences(app.getPackageName(), app.MODE_PRIVATE);
        user_id = prefs.getInt("id", -1);
        isLogin = prefs.getBoolean("isLogin", false);
        user_name = prefs.getString("user_name", "");
        email = prefs.getString("email", "");
        birthday = prefs.getString("birthday", "");
        city = prefs.getString("city", "");
        country = prefs.getString("country", "");
        phone = prefs.getString("phone", "");
        avatar = prefs.getString("avatar", "");
        avatar_replace = prefs.getString("avatar_replace", "");
        coin = prefs.getInt("coin", 0);
        type = prefs.getInt("type", 0);
        token = prefs.getString("token", null);
    }

    public void save() {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("isLogin", isLogin);
        editor.putInt("id", user_id);
        editor.putString("user_name", user_name);
        editor.putString("avatar", avatar);
        editor.putString("avatar_replace", avatar_replace);
        editor.putString("email", email);
        editor.putString("birthday", birthday);
        editor.putString("city", city);
        editor.putString("country", country);
        editor.putString("phone", phone);
        editor.putInt("coin", coin);
        editor.putInt("type", type);
        editor.putString("token", token);
        editor.commit();
    }

    public void clearData() {
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove("isLogin");
        editor.remove("id");
        editor.remove("user_name");
        editor.remove("avatar");
        editor.remove("avatar_replace");
        editor.remove("email");
        editor.remove("birthday");
        editor.remove("city");
        editor.remove("country");
        editor.remove("phone");
        editor.remove("coin");
        editor.remove("type");
        editor.remove("token");
        editor.commit();
    }

    public void loginGgResult(int requestCode, int resultCode, Intent data) {
        GoogleSignInAccount act = ggHelper.result(requestCode, resultCode, data);
        if (act != null) {
            type = 2;
            setIsLogin(true);

            RequestParams params = new RequestParams();
            params.addParam("access_token", act.getIdToken());
            params.addParam("type", "2");

            app.httpManager.doPost(API.LOGIN_SSN, params, new HttpManager.OnResponse() {
                @Override
                public void onResponse(JSONObject jso) {
                    updateUserInfor(jso, (byte) 2);
                }
            });
        }
    }

    /**
     * set bitmap avatar, save cache avatar
     *
     * @param bmAvatar
     */
    public void saveCacheAvatar(Bitmap bmAvatar) {
        this.bmAvatar = bmAvatar;
        app.imageLoader.saveCache(bmAvatar, avatar);
    }

    /**
     * return avatar bitmap smaller
     *
     * @param bmAvatar
     * @return
     */
    public File getSmalFileAvatar(Bitmap bmAvatar) {
        String urlTmp = API.URL + "avatar/tmp.jpg";
        app.imageLoader.saveCache(bmAvatar, urlTmp);
        FileCache fileCache = new FileCache(ctx);
        return fileCache.getFile(urlTmp);
    }
}
