package models;

import java.util.Date;

/**
 * Created by nguyen hong phuc on 9/14/2016.
 */
public class Comment {

    public int id;
    public User user;
    public String content;
    public Date created_at;
    public int number_like;
    public boolean liked = false;

    public Comment(int id,User user, String content, int number_like, Date created_at,boolean liked) {
        this.id = id;
        this.user = user;
        this.content = content;
        this.number_like = number_like;
        this.created_at = created_at;
        this.liked = liked;
    }

}
