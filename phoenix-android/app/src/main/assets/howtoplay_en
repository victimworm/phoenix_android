<b>HOW TO PLAY</b><br>

<b>A PHILOSOPHICAL TAROT</b><br>
<p>The Phoenix Game is ​​the first philosophical game in the world, created by the French philosopher Vincent Cespedes.</p>
<p>With this unique game, you can question life while exploring its mysteries. The game allows you to examine events, personalities, situations, emotions, cultures, and relationships through a method that combines logic and chance and the creative impulse that lurks in your intuition.</p>
<p>The Phoenix Game also exists in a physical form. You can purchase a pack of 26 cards by writing to us at:  <a href="mailto:contact@matkaline.com" style="text-decoration: underline">contact@matkaline.com</a>.</p>

<b>A MAGICAL APPLICATION</b><br>
<p>Playful and profound, our application in several languages ​​allows us to find the right answers to our questions, to better understand the world around us and to think outside the box.</p>

<b>HOW IT WORKS</b><br>
<p>The Phoenix Game is simple to use, but its mechanism is very complex. The followers of the Game, called "Foyns", are passionate about the discovery of new information in and through the game.  For some, it is not a simple game but an initiatory and spiritual practice with its own language.  Nevertheless everyone is free to choose the degree of involvement that feels right for them.</p>

<p>Here are some features. They will increase in power and in number through future updates.</p>

<p>50 meanings or "Flames" make up this universal tarot.</p>
<p>Drawn at random, they answer the questions you have in mind as well as those that are suggested to you on request.</p>

<p>The 50 Flames are either in a state of shock (violent, exhausting, in control) or in a state of charm (joyful, energizing, letting go).</p>

<p>Even-numbered Flames are receptive, internal: the world comes to them.</p>
<p>Flames with an odd number are expansive, external: they rush towards the world.</p>

<p>26 Flames are upright and 24 Flames are in reverse (upside down). A Flame becomes reverse when its seal (the colored rune at the top right) is the same color as the seal of the Shadow, the last card in the deck.</p>
<p>Reverse Flames are three times more rare than upright Flames, and their state of charm or shock seems blurred.</p>

<p>With the Today function (available if you have logged in), a customized algorithm delivers you a personal Flame of the Day at midnight. If it is a shock flame, we offer a Challenge of the Day, to be done during your day.</p>
<p>The Sensage function gives you an intelligent interpretation of your Flames.</p>
<p>Your Flame of the Day determines your Team of the Day: Revolt (Red Seal), Marigold (Blue Seal), Zen (Yellow Seal) or Creation (Green Seal).</p>
<p>You can complete your Flame of the Day in Illumination: 5 or 6 Flames on the World.  (The game board consists of 9 Dimensions.)</p>

<p>The Fate feature reveals your Flame of the Day for a date in the Future: the day of your next birthday, an important night out, or your job interview ... Ideal for future projects!</p>

<p>The Month Profile feature gives you a view of your month in 9 Dimensions.</p>

<b>VINCENT CESPEDES</b>
<p>Recognized philosopher and composer, author of many books, Vincent Cespedes took 26 years to conceive his philosophical tarot.  In 1985, at age 12, he dreamed of inventing a universal language based on emotions.  He realized his dream at the launch of Phoenix Game in Paris in 2011.   Since then his creation has circumnavigated the world. It also comes in the form of a test for the revealing of human values: Deepro (link web <a href="http://deep-profiling.com">http://deep-profiling.com/</a>), also available as a mobile app.</p>
<img src="vincent.jpg" style="max-width: 100%; height: auto;"><br>

<p>"I created the Phoenix Game so that two strangers could connect by sharing information that is meaningful to them; their doubts, their torments, their passionate impulses and their victories against fate. Because life is too important for us to be content to live only one."</p>

<a href="http://vincentcespedes.net">Vincent Cespedes</a>
