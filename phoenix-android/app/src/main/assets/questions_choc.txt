﻿Comment progresser ?
Comment dépasser le score ?
Comment décoller ?
Comment commencer ?
Comment terminer ?
Comment faire preuve d'efficacité ?
Comment me faire comprendre ?
Comment convaincre ?
Comment voir les choses sous un autre angle ?
Comment garder la ligne ?
Comment suivre mon instinct ?
Comment survivre ?
Comment travailler ?
Comment rendre fous les autres ?
Comment parvenir à mes fins ?
Comment éviter la cata ?
Quel risque dois-je prendre ?
Quelle voie suivre ?
Où j'en suis avec l'amour ?
Où j'en suis avec mon travail ?
Où j'en suis avec ma santé ?
Où j'en suis avec l'argent ?
Où j'en suis avec ma famille ?
Où j'en suis avec mes amis ?
Où j'en suis avec mon passé ?
Où j'en suis avec mes projets ?
Où j'en suis avec mes problèmes ?
Où j'en suis avec mes désirs ?
Où j'en suis avec le temps ?
Où j'en suis avec le stress ?
Où j'en suis avec mon bonheur ?
Où j'en suis avec ma joie de vivre ?
Où j'en suis avec les autres ?
Pourquoi je me sens mal ?
Pourquoi je me sens bof ?
Pourquoi je me sens bizarre ?
Pourquoi je me sens timide ?
Pourquoi je me donne à fond ?
Pourquoi je me sens en-dehors du coup ?
Pourquoi je me sens à côté de la plaque ?
Pourquoi je me sens coupable ?
Pourquoi je me sens inutile ?
Pourquoi je me sens dynamique ?
Pourquoi je suis dans la lune ?
Qu'est-ce qui me rend bête ?
Qu'est-ce qui me rend stupide ?
Qu'est-ce je fuis ?
Qu'est-ce qui me rend pessimiste ?
Qu'est-ce qui me rend capable de tout ?
Qu'est-ce qui fait de moi la star ?
Qu'est-ce qui me fait exploser ?
Qu'est-ce qui me fait voyager ?
Qu'est-ce qui pourrait me faire du tort ?
Qu'est-ce qui me rend fébrile ?
Qu'est-ce qui me rend à cran ?
Qu'est-ce qui me rend accro ?
Qu'est-ce qui me lasse ?
Qu'est-ce qui me fait rire ?
Qu'est-ce qui me rend jusqu'au-boutiste ?
Qu'est-ce qui me rend débile ?
Qu'est-ce qui me fait délirer ?
Qu'est-ce qui me fait vriller ?
Qu'est-ce qui me propulse ?
Qu'est-ce qui me freine ?
Qu'est-ce qui m'énerve ?
Qu'est-ce qui me met en colère ?
Qu'est-ce qui me fait pleurer ?
Qu'est-ce qui m'excite ?
Qu'est-ce qui me trouble ?
Qu'est-ce qui m'intrigue ?
Qu'est-ce qui m'induit en erreur ?
Qu'est-ce qui me fait perdre ?
Qu'est-ce qui me fatigue ?
Qu'est-ce qui m'ennuie ?
Qu'est-ce qui me contrarie ?
Qu'est-ce qui me dégoûte ?
Qu'est-ce qui me révolte ?
Qu'est-ce qui m'indigne ?
Qu'est-ce qui me surprend ?
Qu'est-ce qui m'étonne ?
Qu'est-ce qui me rend fragile ?
Qu'est-ce qui me rend vulnérable ?
Ce pour quoi on me respecte ?
Ce pour quoi j'intimide les autres ?
Ce pour quoi on me veut ?
Ce pour quoi on m'envie ?
Ce pour quoi on m'évite ?
Qu'est-ce qui me fait douter ?
Qu'est-ce je dissimule ?
Qu'est-ce j'invente ?
Pourquoi je me prends la tête ?
Pourquoi j'hésite ?
Pourquoi je remets à demain ?
Qu'est-ce que je refuse de voir ?
Pourquoi je me plante ?
Pourquoi je traîne ?
Pourquoi ai-je la flemme ?
Pourquoi suis-je si sensible ?
Qu'est-ce que ça m'apporte de me forcer ?
Qu'est-ce que ça m'apporte de faire semblant ?
Qu'est-ce que ça m'apporte de tergiverser ?
Qu'est-ce que ça m'apporte de dire "non" ?
Qu'est-ce que ça m'apporte de me battre ?
Qu'est-ce que ça m'apporte d'être aimable ?
Qu'est-ce que ça m'apporte d'être sage ?
Qu'est-ce que ça m'apporte d'être serviable ?
Qu'est-ce que ça m'apporte de rester tranquille ?
Qu'est-ce que ça m'apporte d'être souple ?
Qu'est-ce que ça m'apporte d'être au top ?
Pourquoi je me ferme ?
Après quoi je cours ?
De quoi ai-je peur ?
De quoi suis-je capable ?
De quoi ai-je faim ?
De quoi ai-je envie ?
Quel est mon secret ?
Si j'ose, il se passe quoi ?
Si j'arrête, ça fait quoi ?
Si je continue, il se passe quoi ?
J'en suis où ?
Je veux quoi ?
Je me méfie de quoi ?
Je cache quoi ?
Quel schéma je répète ?
Quelle sorte d'artiste suis-je ?
Pourquoi se moque-t-on de moi ?
Je vais où ?
Pourquoi y a-t-il de la souffrance dans le monde ?
pourquoi dois-je réparer les problèmes des autres ?
Pourquoi tant d'émotions en moi ?
Pourquoi je m'empêtre dans mes sentiments ?
Pourquoi dois-je lutter pour joindre les deux bouts ?
Pourquoi ai-je du mal à exprimer mes sentiments ?
Pourquoi je ne peux pas dire "Je t'aime" ?
Pourquoi tant de haine dans le monde ?
Pourquoi he juge les autres ?
Pourquoi je me sens indigne ?
Pourquoi je ne me sens pas digne d'amour ?
Pourquoi je ne me sens pas à la hauteur ?
Pourquoi j'en veux aux autres pour mes malheurs ?
Pourquoi la vie est si difficile ?
Pourquoi ma santé vacille ?
Pourquoi je déteste mon corps ?
Pourquoi y a-t-il trop de violence dans le monde ?
Pourquoi ai-je du mal à m'entendre avec ma famile ?
Pourquoi suis-je si en colère ?
pourquoi les gens se rendent la vie si difficile ?
Pourquoi on ne s'entend pas bien ?
Pourquoi reste-t-on sourd aux avis divergents ?
Pourquoi est-ce si compliqué d'avoir une conversation intéressante ?
Pourquoi je me sens responsable de situations sur lesquelles je n'ai aucun contrôle ?
Pourquoi ai-je si peu de contrôle sur ma vie?
Pourquoi je me sens incapable ?
Pourquoi je laisse les autres prendre le pouvoir sur moi ?
Pourquoi les gens profitent de moi ?
Que dois-je apprendre de cette situation ?
Comment dois-je considérer mon dilemme actuel ?
Comment puis-je prendre les choses en main ?
Dans cette situation, ai-je d'autres options, que je ne vois pas ?
Est-ce le moment d'agir ou d'attendre ?
D'autres informations sur cette situations vont-elles venir ?
Qu'est-ce que je ne parviens pas à voir dans cette situation ?
Quelle leçon tirer de tout cela ?
Comment m'assurer que je perçois bien tout dans cette situation ?
Est-ce une situation passagère ?
Pourquoi ne vois-je pas comment naviguer ?
Pourquoi y a-t-il trop d'obstacles dans ma vie aujourd'hui ?
Pourquoi cela arrive-t-il systématiquement ?
Pourquoi suis-je dans cette situation ?
Quelle est ma prochaine étape ?
Pourquoi les gens construient-ils des murs ?
Pourquoi je construis des murs ?
Pourquoi je me cache ?
Pourquoi j'entretiens de vieux schémas de comportements ?
Pourquoi cela coince-t-il ?
Pourquoi dois-je avoir affaire à des gens stupides ?
Pourquoi les gens compliquent-ils ma vie ?
Pourquoi les gens sont-ils si ennuyeux ?
Pourquoi je me fais si mal comprendre ?
Pourquoi j'aime à chaque fois des personnes si peu fiables ?
Pourquoi tant de solitude ?
Pourquoi je ne me contente pas de ce que j'ai déjà ?
** NON ! On ne la met pas :)
Pourquoi est-il difficile pour moi de demander aux autres ce que je veux ?
Pourquoi ai-je peur de la confrontation ?
Pourquoi est-il difficile pour moi de faire des économies ?
Qui serai-je sans mon vécu personnel ?
Pourquoi je me sens hors de contrôle ?
Si je ne contrôle plus cette situation, ça va donner quoi ?
Que se passera-t-il si je pense d'abord à moi ?
Et si je me concentrais sur mes priorités aujourd'hui ?
Et si je suivais d'autres priorités aujourd'hui ?
Ai-je tout ce dont j'ai besoin ?
Suis-je en sécurité ?
Devrais-je me démener pour ça ?
Puis-je faire confiance à mon estomac ?
Dois-je plutôt suivre ma tête ou mon cœur ?
Vais-je gagner le pactole bientôt ?