﻿<b>L'Enfant Fou (-25) – ANAKON</b><br>
<i>L'immaturité capricieuse</i><br>
> Caprice. Susceptibilité. Maladresse. Enfantillage. Vulnérabilité. Impatience.<br>
<p>
<b>L’Enfant Fou</b> exténue son entourage par son agitation permanente, ses diableries et son égocentrisme instinctif. Il fait des misères aux plus faibles, considérant tout ce qu’il rencontre comme un jouet sur lequel il peut passer ses frustrations. Il se mêle de ce qui ne le regarde pas, et se rit de ce qui le regarde, en galopin de la pire espèce. Rien ne le raisonne. Les punitions aggravent même son indécence et ses polissonneries. Il a la liberté cruelle, la pensée balbutiante, l’émotivité puérile.<br>
Cette Flamme connote les défauts de ce qui n’a pas encore acquis sa pleine maturité. Elle marque l’énervement, l’agacement et l’infantilisme, mais aussi une vulnérabilité excessive et l’angoisse d’être abandonné par nos tuteurs, nos «&nbsp;parents&nbsp;», nos soutiens.<br>
La posture de <b>L’Enfant Fou</b> est celle de la naïveté et de l’ignorance, autant que du mécontentement continuel. Insatisfait, il se plaint de tout. Lunatique, il est la victime de ses humeurs contradictoires. Chichiteux, il veut tout et il ne veut rien. Turbulent et despotique, il use les nerfs pour commander. On ne peut que lui tenir tête, car il fait n’importe quoi. Pourtant, lui, qui contredit systématiquement pour tester ceux qui lui tiennent tête, ne supporte pas la contradiction&nbsp;! Il pique alors des crises mémorables, boude et use de mauvaise foi, tirant un malin plaisir de pousser le bouchon toujours un peu plus loin, pour voir jusqu’où l’on peut aller.<br>
</p><p>
<b>L’Enfant Fou</b> justifie la sèche fermeté de <b>1. L’Ancêtre</b>, car son absence de réflexion et de connaissance du monde l’expose à tous les dangers. Non structuré, se dandinant vers tout ce qui brille sans aucune retenue ni aucune prudence, il s’exaspère et nous exaspère, à l’opposé de <b>L’Enfant Franc</b> qui s’enchante et nous enchante. Le moindre effort le décourage. La perversité lui passe au-dessus de la tête&nbsp;: il manipule ses poupées, l’esprit toujours ailleurs, sans voir qu’il est lui-même une poupée que l’on manipule.<br>
</p><p>
<b>Team RÉVOLTE • choc externe<br>
Intensificatrices&nbsp;: <a href="1">1</a>., <a href="12">12</a>.<br>
Répondante&nbsp;: <a href="-14">-14</a>.<br>
Incompatible&nbsp;: <a href="-12">-12</a>.<br></b>
</p>