﻿<b>La Source (23) – PSAODI</b><br>
<i>La fécondité jaillissante</i><br>
> Optimisme. Jeunesse. Joie. Pétillance. Vivacité. Esprit d'initiative.<br>
<p>
Cette Flamme décrit l’émergence d’un foisonnement exubérant, l’éclosion printanière des circulations essentielles. À la fois mère nourricière à partir de laquelle la vie commence, et fontaine de Jouvence intarissable, régénérant les corps et transmettant aux âmes un gai bouillonnement d’images, de sensations et d’idées.<br>
Dans un pétillement aquatique, végétal et aérien, une femme noire, à six seins, se balance tête bêche, donnant vie à <b>La Source</b>, qui naît du prolongement de sa chevelure. Une scène rafraîchissante, tellement pleine de sens – comme les 26 cartes du Jeu – qu’il faudrait lui consacrer un gros traité pour en dégager toute la portée symbolique. Contentons-nous de souligner les traits principaux de <b>La Source</b>&nbsp;: profusion de vitalité et de créativité, origine d’une onde fertilisante, joie de vivre.<br>
</p><p>
Contrairement à <b>1. L’Ancêtre</b> qui surveille durement sa progéniture, sa lignée, <b>La Source</b> est à la fois l’origine et ce qui en découle, la production de vie et la vie qui afflue, le point de départ et le continuum. <br>
<b>La Source</b> signifie la naissance, le début d’une relation, l’audace d’entamer une action qui fera «&nbsp;boule de neige&nbsp;», mais aussi le surgissement de ce qui est enfoui. Ce dernier point apparaît clairement lorsqu’on considère son Sceau&nbsp;: une eau souterraine jaillit de la terre sous forme de palmier. Le palmier, attribut végétal du Phénix, figure également sur le dessin au niveau de la naissance de <b>La Source</b>&nbsp;: à la racine des cheveux de la femme qui la personnifie. Herbe géante, il évoque la sécurité alimentaire et l’origine de la civilisation, quand la chevelure symbolise l’indépendance et la force vitale.<br>
</p><p>
<b>Team CRÉATION • charme externe <br>
Intensificatrices&nbsp;: <a href="-3">-3</a>., <a href="-10">-10</a>.<br>
Répondante&nbsp;: <a href="16">16</a>.<br>
Incompatible&nbsp;: <a href="3">3</a>.<br></b>
</p>