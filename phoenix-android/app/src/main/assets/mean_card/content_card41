﻿<b>LICORNE FOLLE (-16) - KISON</b><br>
<i>L'obsession de la perfection</i><br>
> Perfectionnisme. Stress. Préoccupation. Exigence extrême. Scrupules.<br>
<p>
Vouloir que tout se passe bien, à tout prix, c’est concourir à ce que tout se passe mal. Tel est le drame récurrent de <b>La Licorne Folle</b>&nbsp;: faire de la perfection un défaut, tellement on y aspire&nbsp;; empêcher l’imprévisible de fleurir, en anticipant tout.<br>
À force de le préparer soigneusement et de le chérir, cette Flamme fait du bonheur une chose fragile comme un verre de cristal, et finit inévitablement par l’ébrécher<br>
Maniaque, sa peur de l’échec et de la mésentente la pousse à développer mille ruses et vérifications, qui s’avèrent finalement contreproductives. Elle exaspère souvent son entourage avec sa pureté virginale comme idée fixe. Elle fait face aux complications amoureuses par l’ascétisme et la chasteté, aux petites tracasseries du quotidien par un stress qui en fait de gros problèmes, aux écueils de toutes sortes par un optimisme délirant qui ne fait que les aggraver. Elle utilise son empathie à mauvais escient, devinant ce qu’on veut lui taire mais en l’exagérant, en le reliant de façon paranoïaque à d’autres faits, en se flagellant déjà comme si elle avait mérité quelques courroux. Elle renvoie à des situations où la perfection exigée rencontre un perfectionnisme délirant. Outre sa culpabilité de ne jamais être à la hauteur des missions qu’elle s’impose le plus souvent elle-même, <b>La Licorne Folle</b> hystérise tous les évènements qui réclament du sans-faute et du sang-froid. Elle est trop affairée à évaluer ses tares futures pour apprécier à sa juste valeur l’étendue des talents et des énergies dont elle dispose dès à présent.<br>
</p><p>
La «&nbsp;folie&nbsp;» de <b>La Licorne Folle</b> se confond avec le bonheurisme de consommation ambiant, cet affichage permanent d’un bonheur de façade qui, doublé d’une quête maladive du même bonheur, finit par vider le bonheur de sa puissance politique et naturellement subversive. Le bonheur est une puissance qui invente ses propres normes et sabote donc la «&nbsp;Normalité&nbsp;» majuscule. Mais <b>La Licorne Folle</b> en fait une méthode, une psychologie «&nbsp;positive&nbsp;», le résultat d’un coaching contraignant.<br>
</p><p>
<b>Team SOUCI • choc interne<br>
Intensificatrices&nbsp;: <a href="3">3</a>., <a href="10">10</a>.<br>
Répondante&nbsp;: <a href="-23">-23</a>.<br>
Incompatible&nbsp;: <a href="-3">-3</a>.<br></b>
</p>