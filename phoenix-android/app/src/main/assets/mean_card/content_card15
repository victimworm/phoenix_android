﻿<b>La Licorne (16) – KILI</b><br>
<i>Le sixième sens</i><br>
> Conciliation. Empathie. Diplomatie. Médiation. Disponibilité. Sincérité.<br>
<p>
Voici la grande conciliatrice de l’onde de charme&nbsp;: elle aime l’accord, harmonise les contraires, sublime la sensualité et détecte les poisons. <b>La Licorne</b> cherche à accorder autant que faire se peut sa volonté à celle des autres, mais sans y perdre son âme&nbsp;: au contraire, farouchement allergique à l’aliénation, elle ressent tout de suite lorsqu’on cherche à la manipuler et sa grande vélocité la rend imprenable – elle sème tous ses poursuivants. Flamme de la connaissance extra-sensorielle de la réalité, sorte d’appréhension extralucide qui nous donne directement accès aux vibrations du monde et aux causes finales.<br>
C’est la Flamme de la franchise absolue, tellement vraie qu’elle ne peut que mettre d’accord les gens de bonne foi. Médiatrice et messagère, elle célèbre la gentillesse et la volonté d’accord. Mais sa corne se transforme en arme dès que l’on songe à tirer profit de ses facultés.<br>
</p><p>
Dans le Jeu du Phénix, elle désigne tout ce qui relève de la spiritualisation de la matière. Avec elle, la douceur l’emporte sur la brutalité, la connaissance est à protéger, la pureté redevient admirable. Sa corne est une antenne qui capte l’énergie cosmique et les non-dits de l’entourage, non pas à des fins stratégiques, mais pour faciliter les échanges, désengorger les mésententes, accroître les chances d’amour.<br>
De toutes les Flammes, <b>La Licorne</b> est la plus consciente des flux arborescents de l’onde de charme et de l’onde de choc. Elle sait qu’un bienfait qu’elle dispense ici ou là lui reviendra par ailleurs. Aussi ne cherche-t-elle jamais à nuire et exploiter, mais accompagne, entretient, encourage, saisit les enjeux cachés, et, lorsqu’elle ne le sent pas, fausse compagnie en un éclair, quitte à pourfendre de sa clairvoyance ceux qui cherchent à la contenir, en leur disant leurs quatre vérités. <br>
</p><p>
<b>Team ZEN • charme interne<br>
Intensificatrices&nbsp;: <a href="-3">-3., <a href="-10">-10.<br>
Répondante&nbsp;: <a href="23">23.<br>
Incompatible&nbsp;: <a href="10">10</a>.<br></b>
</p>