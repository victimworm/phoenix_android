﻿<b>Le Tigre Fou (-5) – NAMORDIN</b><br>
<i>L’action suprême</i><br>
> Protection. Respect. Instinct. Agilité. Vigilance. Discrétion.<br>
<p>
Voici la Flamme de l'excellence dans l'action. D'autant plus excellente qu'elle fonctionne à l'instinct, sans calculer ses effets. Elle vise d'abord à protéger les enfants – les mères tigres élèvent seules leurs petits – jusqu'à être capable de commettre pire pour sauver l'avenir. Plus généralement, elle se bat pour des buts nobles.<br>
<b>Le Tigre Fou</b> désigne l'intelligence et la puissance instinctives mises au service de la défense de la beauté, de la vulnérabilité, d'un projet qui en vaut la peine ou d'un territoire menacé. Sa sensualité est trépidante et enveloppante, sans se départir d'une part de douces ténèbres. C'est la Flamme de l'onde de charme qui fait éclater toute sa puissance pour vaincre l'onde de choc, ou, pour prendre un verbe plus adéquat, pour la <i>neutraliser</i>. Son acuité est optimale, surtout lorsqu'il s'agit de percer la nuit, à travers l'opacité du décor et les voiles de l'inconscience.<br>
</p><p>
Protéger les enfants (tel le tigron qui se cache dans les taillis), cela signifie métaphoriquement se mettre du côté du plus faible, de l'opprimé, du défaillant, pour écarter tout ce qui le nuit et l'encourager à trouver progressivement son autonomie et son ampleur.<br>
<b>Le Tigre Fou</b> combat pour une juste cause – tellement juste, tellement digne, qu'elle l'autorise à passer à l'offensive sans la moindre hésitation. Mais, comme pour <b>Le Tigre</b> franc, on ne le voit pas venir. Discrétion, souplesse et animalité félines lui confèrent son aura et une majesté presque surnaturelle. <br>
</p><p>
<b>Team CRÉATION • charme externe <br>
Intensificatrices&nbsp;: <a href="18">18</a>., <a href="21">21</a>.<br>
Répondante&nbsp;: <a href="-8">-8</a>.<br>
Incompatible&nbsp;: <a href="-18">-18</a>.<br></b>
</p>